<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActionLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_logs', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('organization_id')->index()->nullable();
            $table->bigInteger('candidate_id')->index()->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->string('service_type')->nullable();
            $table->unsignedInteger('media_id')->nullable();
            $table->string('media_type')->nullable();
            $table->string('file_name', 150)->nullable();
            $table->json('location')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aws_logs');
    }
}
