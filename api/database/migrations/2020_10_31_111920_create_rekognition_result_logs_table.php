<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRekognitionResultLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rekognition_result_logs', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('organization_id')->nullable();
            $table->unsignedInteger('candidate_id')->nullable();
            $table->unsignedInteger('source_media_id')->nullable();
            $table->unsignedInteger('target_media_id')->nullable();
            $table->float('rekognition_result', 5,2)->nullable();
            $table->string('status', 100)->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rekognition_result_logs');
    }
}
