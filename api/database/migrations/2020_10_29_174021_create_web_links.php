<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebLinks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('web_links', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('organization_id');
            $table->unsignedBigInteger('candidate_id')->nullable();
            $table->unsignedBigInteger('applicant_id')->nullable();
            $table->uuid("link_token");
            $table->string("link_type");
            $table->string("to_phone_number")->nullable();
            $table->string("to_email")->nullable();
            $table->boolean("is_copy_link_generated")->default(false);
            $table->timestamp("used_at")->nullable();
            $table->timestamp("expire_at")->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('organization_id')->references('id')->on('organizations');
            $table->foreign('candidate_id')->references('id')->on('candidates');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('web_links');
    }
}
