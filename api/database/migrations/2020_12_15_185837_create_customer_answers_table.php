<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_answers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("organization_question_id");
            $table->text("answers");
            $table->timestamps();

            $table->foreign('organization_question_id')->references('id')->on('organization_questions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_answers');
    }
}
