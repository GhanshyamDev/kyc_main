<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->id();
            $table->uuid('candidate_xid')->unique()->index();
            $table->string('first_name', 50);
            $table->string('last_name', 50)->nullable();
            $table->string('email', 255);
            $table->string('phone_number', 10)->nullable();
            $table->string('cid', 25);
            $table->string('job_id', 100)->nullable();
            $table->unsignedInteger('organization_id')->index();
            $table->string('address', 255)->nullable();
            $table->string('designation', 100)->nullable();
            $table->text('comment')->nullable();
            $table->json('location')->nullable();
            $table->json('validation_image_location')->nullable();
            $table->string('current_status')->nullable()->default('New');
//            $table->dateTime('status_date')->nullble();
            $table->boolean('completed_status')->nullable();
            $table->boolean('master_image_changed')->default(false);
            $table->boolean('validation_image_changed')->default(false);
            $table->boolean('step_1_image_changed')->default(false);
            $table->boolean('step_2_image_changed')->default(false);
            $table->boolean('step_3_image_changed')->default(false);
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['organization_id', 'cid'], 'organization_candidate_unique');
            $table->unique(['organization_id','job_id','phone_number'], 'organization_job_phone_unique');
            $table->unique(['organization_id','job_id','email'], 'organization_job_email_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates');
    }
}
