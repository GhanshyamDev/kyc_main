<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->uuid('customer_xid')->unique()->index();
            $table->unsignedInteger('organization_id')->index();
            $table->string('first_name', 50);
            $table->string('cid');
            $table->string('last_name', 50)->nullable();
            $table->string('email', 255);
            $table->string('phone_number', 10)->nullable();
            $table->string('aadhar_number', 15)->nullable();
            $table->date('dob')->nullable();
            $table->string('address', 255)->nullable();
            $table->string('bank_name', 255)->nullable();
            $table->string('bank_ifsc_code', 255)->nullable();
            $table->string('bank_account_number', 255)->nullable();
            $table->boolean('is_aadhar_verified')->default(false);
            $table->boolean('passport_changed')->default(false);
            $table->boolean('pan_card_changed')->default(false);
            $table->boolean('identity_proof_changed')->default(false);
            //new/in_progress/pending_review/approve/reject/hold
            $table->string('current_status')->nullable()->default('new');
            $table->timestamp('current_status_changed_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            //pending/suspicious/verified
            $table->string('compare_status')->nullable()->default('pending');
            //pending/partial/completed
            $table->string('customer_form_fill_status')->nullable()->default('pending');
            $table->timestamp('customer_form_submitted_at')->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('organization_id')->references('id')->on('organizations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer');
    }
}
