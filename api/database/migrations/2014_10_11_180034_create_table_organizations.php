<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrganizations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('organization_xid')->index();
            $table->unsignedInteger('parent_organization_id')->nullable();
            $table->unsignedInteger('timezone_id')->nullable();
            $table->unsignedInteger('organization_type_id');
            $table->string('name')->nullable();
            $table->string('contact_person')->nullable();
            $table->string('email')->nullable();
            $table->string('phone_number')->nullable();
            $table->unsignedInteger('link_expire_hours')->default(24);
            $table->unsignedInteger('compare_score_threshold')->default(96);
            $table->boolean('is_active')->default(false);
            $table->boolean('allow_send_sms')->default(true);
            $table->boolean('allow_send_email')->default(true);
            $table->string('transaction_id')->nullable();
            $table->string('card_brand')->nullable();
            $table->string('card_last_four')->nullable();
            $table->dateTime('trial_ends_at')->nullable();
            $table->string('created_by')->nullable();// ->index()->nullable();
            $table->string('updated_by')->nullable(); //->index()->nullable();
            $table->string('deleted_by')->nullable(); //->index()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('timezone_id')->references('id')->on('timezones');
            $table->foreign('organization_type_id')->references('id')->on('organization_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
