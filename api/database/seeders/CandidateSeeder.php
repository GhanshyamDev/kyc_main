<?php

namespace Database\Seeders;

use App\Models\Candidate;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;

class CandidateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $now = Carbon::now()->toDateTimeString();

        $candidates = [];

        for($i = 1; $i <= 100; $i++) {
            $candidateNo = str_pad($i, 3, "0", STR_PAD_LEFT);
            $candidates[] = [
                'candidate_xid' => $faker->uuid,
                'cid' => 'cid_' . $candidateNo,
//                'job_id' => 'jid_' . $candidateNo,
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'email' => "candidate{$candidateNo}@test.test",
                'phone_number' => "78XXXXX{$candidateNo}",
                'organization_id' => $faker->numberBetween(1, 5),
                'address' => $faker->address,
                'designation' => $faker->randomElement(['Manager', 'Clerk', 'Professor', 'Teacher', 'Guard', 'CEO', 'CTO', 'Team Lead']),
                'comment' => $faker->text(45),
                'current_status' => $faker->randomElement(['New', 'Verified', 'In Progress', 'Suspicious']),
                'completed_status' => $faker->boolean,
                'created_at' => $now,
                'updated_at' => $now
            ];
        }

        Candidate::query()->insert($candidates);
    }
}
