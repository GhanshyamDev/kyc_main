<?php

namespace Database\Seeders;

use App\Models\Customer;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $now = Carbon::now()->toDateTimeString();

        $customers = [];
        for($i=1; $i<101; $i++) {
            $customerNo = str_pad($i, 3, "0", STR_PAD_LEFT);
            $customers[] = [
                'customer_xid' => $faker->uuid,
                'organization_id' => $faker->numberBetween(1, 5),
                'first_name' => $faker->firstName,
                'cid' => 'cid_' . $customerNo,
                'last_name' => $faker->lastName,
                'email' => "customer{$customerNo}@test.test",
                'phone_number' => '5825010' . $customerNo,
                'aadhar_number' => 'ABAB CDCD E' . $customerNo,
                'dob' => $faker->dateTimeBetween('-45 years', '-21 years'),
                'address' => $faker->address,
                'bank_name' => $faker->company,
                'bank_ifsc_code' => '',
                'bank_account_number' => $faker->bankAccountNumber,
                'is_aadhar_verified' => $faker->boolean,
                'passport_changed' => $faker->boolean,
                'pan_card_changed' => $faker->boolean,
                'identity_proof_changed' => $faker->boolean,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ];
        }

        Customer::query()->insert($customers);
    }
}
