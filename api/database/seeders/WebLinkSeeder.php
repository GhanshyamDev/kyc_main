<?php

namespace Database\Seeders;

use App\Models\Candidate;
use App\Models\Organization;
use App\Models\WebLink;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class WebLinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $candidates = Candidate::query()->take(10)->get();

        foreach ($candidates as $candidate) {
            $organization = Organization::query()->find($candidate->organization_id);

            WebLink::query()
                ->create([
                    'organization_id' => $candidate->organization_id,
                    'candidate_id' => $candidate->id,
                    'link_token' => Str::uuid(),
                    'link_type' => WebLink::LINK_TYPE_CANDIDATE_SELFIE_1,
                    'to_phone_number' => null,
                    'to_email' => $candidate->email,
                    'is_copy_link_generated' => false,
                    'expire_at' => now()->addHours($organization->link_expire_hours)
                ]);
        }
    }
}
