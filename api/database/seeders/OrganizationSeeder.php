<?php
namespace Database\Seeders;

use App\Models\Organization;
use App\Models\OrganizationType;
use App\Models\Role;
use App\Models\User;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class OrganizationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrganizationType::query()->create(["organization_type_name" => "SuperAdmin"]);
        OrganizationType::query()->create(["organization_type_name" => "Company"]);
        OrganizationType::query()->create(["organization_type_name" => "CompanyVendor"]);

        $roleSuper = Role::query()
            ->create([
                "role_xid" => "51561f27-6f87-4ace-bb46-0d104d465c1c",
                "name" => "admin",
                "organization_type" => "super",
            ]);
        $roleCompany = Role::query()
            ->create([
                "role_xid" => "685c29b5-acca-4ea1-9850-7231b2ac6d05",
                "name" => "admin",
                "organization_type" => "company",
            ]);
        $roleVendor = Role::query()
            ->create([
                "role_xid" => "6cf06d2a-0770-4269-a7ee-ff6f7a1cf0af",
                "name" => "admin",
                "organization_type" => "vendor",
            ]);

        //add super organization
        $organizationSuper = Organization::query()->create([
            "organization_xid" => Str::uuid(),
            "organization_type_id" => OrganizationType::SUPERADMIN,
            'name' => 'Super Organization',
            "is_active" => true,
            "timezone_id" => 248
        ]);
        $user = User::query()
            ->create([
                "user_xid" => Str::uuid(),
                "first_name" => "Admin",
                "last_name" => "Test",
                "phone_number" => '1234567890',
                "email" => "test@test.com",
                "password" => bcrypt("123456"),
                "is_active" => true,
                "is_superadmin" => true,
                "timezone_id" => 248
            ]);
        $user->organizations()->save($organizationSuper, ['role_id' => $roleSuper->id]);
        $this->seedUser($organizationSuper, $roleSuper);

        $organizationCompany[0] = Organization::query()->create([
            "organization_xid" => Str::uuid(),
            "parent_organization_id" => $organizationSuper->id,
            "organization_type_id" => OrganizationType::COMPANY,
            'name' => 'Company Organization 1',
            "is_active" => true,
            "timezone_id" => 248
        ]);
        $organizationCompany[1] = Organization::query()->create([
            "organization_xid" => Str::uuid(),
            "parent_organization_id" => $organizationSuper->id,
            "organization_type_id" => OrganizationType::COMPANY,
            'name' => 'Company Organization 2',
            "is_active" => true,
            "timezone_id" => 248
        ]);
        foreach ($organizationCompany as $key => $organization) {
            $this->seedUser($organization, $roleCompany);
        }

        $organizationVendor[0] = Organization::query()->create([
            "organization_xid" => Str::uuid(),
            "parent_organization_id" => $organizationCompany[0]->id,
            "organization_type_id" => OrganizationType::COMPANYVENDOR,
            'name' => 'Vendor Organization 1',
            "is_active" => true,
            "timezone_id" => 248
        ]);
        $organizationVendor[1] = Organization::query()->create([
            "organization_xid" => Str::uuid(),
            "parent_organization_id" => $organizationCompany[1]->id,
            "organization_type_id" => OrganizationType::COMPANYVENDOR,
            'name' => 'Vendor Organization 2',
            "is_active" => true,
            "timezone_id" => 248
        ]);
        foreach ($organizationVendor as $key => $organization) {
            $this->seedUser($organization, $roleVendor);
        }
    }

    private function seedUser($organization, $role) {
        $faker = Factory::create();
        $user = User::query()
            ->create([
                "user_xid" => Str::uuid(),
                "first_name" => $faker->firstName,
                "last_name" => $faker->lastName,
                "email" => $faker->email,
                "phone_number" => $faker->phoneNumber,
                "password" => bcrypt("123456"),
                "is_active" => true,
                "timezone_id" => 248
            ]);
        $user->organizations()->save($organization, ['role_id' => $role->id]);

        return $user;
    }
}
