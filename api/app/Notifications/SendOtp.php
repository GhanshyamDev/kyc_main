<?php

namespace App\Notifications;

use App\Services\Overrides\MailMessageCustom;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Config;

class SendOtp extends Notification
{
    use Queueable;
    private $candidate;
    private $otp;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($otp, $candidate)
    {
        //
        $this->candidate = $candidate;
        $this->otp = $otp;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        Config::set("app.name", $this->candidate->organization->name);
        $mailMessage = new MailMessageCustom();
        $mailMessage->extraData = ['organization' => $this->candidate->organization->name];
        return $mailMessage
            ->subject('Your New OTP.')
            ->greeting('Hi '. $this->candidate->first_name.",")
            ->line('Your OTP number is : '.$this->otp->otp_number)
            ->line('Thank you for using application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
