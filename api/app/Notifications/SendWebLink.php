<?php

namespace App\Notifications;

use App\Services\Overrides\MailMessageCustom;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Config;

class SendWebLink extends Notification  implements ShouldQueue
{
    use Queueable;
    public $webLink;
    private $candidate;

    public function __construct($webLink, $candidate)
    {
        $this->webLink = $webLink;
        $this->candidate = $candidate;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        Config::set("app.name", $this->candidate->organization->name);
        $mailMessage = new MailMessageCustom();
        $mailMessage->extraData = ['organization' => $this->candidate->organization->name];
        return $mailMessage
            ->subject('Fill the required information.')
            ->greeting('Hi '. $this->candidate->first_name.",")
            ->line('Organization '.$this->candidate->organization->name.' is requesting you to fill the required information')
            ->line('Click on below link to proceed.')
            ->action('Go to link', env('FRONT_APP_URL')."/ri/wl/c/".$this->webLink->link_token)
            ->line('Thank you for using application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
