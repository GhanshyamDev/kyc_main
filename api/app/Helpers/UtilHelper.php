<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Log;

class UtilHelper
{
    public static function correctImageOrientation($filename)
    {
        if (function_exists('exif_read_data')) {
            Log::info(exif_imagetype($filename));
            if (exif_imagetype($filename) !== IMAGETYPE_JPEG) {
                return;
            }
            $exif = exif_read_data($filename);
            Log::info('we', $exif);
            if ($exif && isset($exif['Orientation'])) {
                $orientation = $exif['Orientation'];
                if ($orientation != 1) {
                    $img = imagecreatefromjpeg($filename);
                    $deg = 0;
                    switch ($orientation) {
                        case 3:
                            $deg = 180;
                            break;
                        case 6:
                            $deg = 270;
                            break;
                        case 8:
                            $deg = 90;
                            break;
                    }
                    if ($deg) {
                        $img = imagerotate($img, $deg, 0);
                    }
                    // then rewrite the rotated image back to the disk as $filename
                    imagejpeg($img, $filename, 95);
                } // if there is some rotation necessary
            } // if have the exif orientation info
        } // if fu
    }

    public static function adjustPicOrientation($full_filename)
    {
        $exif = exif_read_data($full_filename);
        if ($exif && isset($exif['Orientation'])) {
            $orientation = $exif['Orientation'];
            if ($orientation != 1) {
                $img = imagecreatefromjpeg($full_filename);

                $mirror = false;
                $deg = 0;

                switch ($orientation) {
                    case 2:
                        $mirror = true;
                        break;
                    case 3:
                        $deg = 180;
                        break;
                    case 4:
                        $deg = 180;
                        $mirror = true;
                        break;
                    case 5:
                        $deg = 270;
                        $mirror = true;
                        break;
                    case 6:
                        $deg = 270;
                        break;
                    case 7:
                        $deg = 90;
                        $mirror = true;
                        break;
                    case 8:
                        $deg = 90;
                        break;
                }
                if ($deg) $img = imagerotate($img, $deg, 0);
                //if ($mirror) $img = _mirrorImage($img);
                $full_filename = str_replace('.jpg', "-O$orientation.jpg", $full_filename);
                imagejpeg($img, $full_filename, 95);
            }
        }
        return $full_filename;
    }
}
