<?php

namespace App\Helpers\Auditable;

/**
 * @property mixed deleter
 */
trait AuditableWithDeletesTrait
{
    use AuditableTrait;

    /**
     * Boot the audit trait for a model.
     *
     * @return void
     */
    public static function bootAuditableWithDeletesTrait()
    {
        static::observe(new AuditableWithDeletesTraitObserver);
    }

    /**
     * Get user model who deleted the record.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deleter()
    {
        return $this->belongsTo($this->getUserClass(), $this->getDeletedByColumn())->withDefault();
    }

    /**
     * Get column name for deleted by.
     *
     * @return string
     */
    public function getDeletedByColumn()
    {
        return 'deleted_by';
    }

    /**
     * Get deleted by user full name.
     *
     * @return string
     */
    public function getDeletedByNameAttribute()
    {
        return $this->deleter->first_name ?? '';
    }
}
