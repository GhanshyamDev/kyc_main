<?php

namespace App\Services;

use App\Services\Crud\ListQueryParams;
use App\Models\OrganizationUserXref;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class UserService extends BaseService
{
    public function getList(array $input = [])
    {
        $query = User::query();
        $listQueryParams = new ListQueryParams();
        $listQueryParams->searchColumns = ['email', 'first_name', 'last_name', 'phone_number'];
        $listQueryParams->filterColumns = ['email', 'is_active'];

        $query->whereHas('organizations');
        $query->with(['timezone']);
        if (Arr::get($input, 'organization_id')) {
            $query->whereHas('organizationUsers', function($query) use ($input) {
                $query->where('organization_id', $input['organization_id']);
            });

            $query = $query->with(['roles' => function($query) use ($input) {
                $query->wherePivot('organization_id', $input['organization_id']);
            }]);
        }
        $query = $this->listQuery($query, $input, $listQueryParams);

        return $this->resultQuery($query, $input);
    }

    public function getDetail($id, array $input = [])
    {
        $query = User::query();
        return $query->findOrFail($id);
    }

    public function create(array $input)
    {
        DB::beginTransaction();

        $user = User::query()->create([
            'user_xid' => $input['user_xid'],
            'first_name' => $input['first_name'],
            'last_name' => $input['last_name'],
            'email' => $input['email'],
            'phone_number' => $input['phone_number'],
            'timezone_id' => $input['timezone_id'],
            'password' => bcrypt($input['password']),
            'is_active' => true,
            //'is_superadmin' => false,
            'email_verified_at' => now(),
        ]);

        OrganizationUserXref::query()->create([
            'user_id' => $user->id,
            'organization_id' => $input['organization_id'],
            'role_id' => $input['role_id'],
        ]);

        $user->load(['roles' => function($query) use ($input) {
            $query->wherePivot('organization_id', $input['organization_id']);
        }]);

        DB::commit();

        return $user;
    }

    public function Update($id, array $input)
    {
        $userInput = Arr::except($input, ['organization_id', 'organization_xid', 'role_id']);

        DB::beginTransaction();
        $user = User::find($id);
        if (!empty($userInput['password'])) {
            $userInput['password'] = bcrypt($userInput['password']);
            foreach($user->tokens as $token) {
                $token->revoke();
            }
        } else {
            unset($userInput['password']);
        }

        User::query()
            ->where('id', $id)
            ->update($userInput);

        if(array_key_exists('role_id', $input)) {
            OrganizationUserXref::query()
                ->where('organization_id', $input['organization_id'])
                ->where('user_id', $user->id)
                ->update([
                    'role_id' => $input['role_id'],
                ]);

            $user->load(['roles' => function($query) use ($input) {
                $query->wherePivot('organization_id', $input['organization_id']);
            }]);
        }

        DB::commit();

        return $user;
    }

    public function delete($id, array $filter = [])
    {
        $user = User::find($id);

        DB::beginTransaction();
        OrganizationUserXref::query()
            ->where('organization_id', $filter['organization_id'])
            ->where('user_id', $user->id)
            ->delete();

        $user->delete();

        DB::commit();
    }

    public function changePassword($id, array $input)
    {
        $user = User::find($id);
        $user->password = bcrypt($input['password']);
        $user->first_login = false;
        $user->save();

        return $user;
    }
}
