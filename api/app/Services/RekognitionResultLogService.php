<?php


namespace App\Services;


use App\Models\RekognitionResultLog;

class RekognitionResultLogService extends BaseService
{
    public function getRekognitionResultByCandidateId($candidateId)
    {
        $resultLogs = RekognitionResultLog::query()->with(['sourceMedia', 'targetMedia'])
            ->where('candidate_id', $candidateId)->get();
        return $resultLogs;
    }
}
