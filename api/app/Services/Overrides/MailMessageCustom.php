<?php
namespace App\Services\Overrides;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Log;

class MailMessageCustom extends MailMessage
{
    public $extraData = [];

    public function data()
    {
        return array_merge($this->toArray(), $this->viewData, $this->extraData);
    }
}
