<?php


namespace App\Services;

use App\Services\Crud\ListQueryParams;
use App\Models\Candidate;
use App\Models\Organization;
use Illuminate\Support\Arr;

class ReportService extends BaseService
{
    /**
     * @var OrganizationService.
     */
    private $organizationService;

    public function __construct(OrganizationService $organizationService)
    {
        $this->organizationService = $organizationService;
    }

    public function getCandidateReport(array $input)
    {
        $query = Candidate::query()->with('organization');

        $listQueryParams = new ListQueryParams();
        $listQueryParams->searchColumns = ['email', 'first_name', 'last_name', 'phone_number', 'job_id', 'cid', 'address', 'comment', 'designation'];
        $listQueryParams->filterColumns = ['organization_id', 'current_status'];

        if(Arr::exists($input, 'filters') && Arr::exists($input['filters'], 'search')) {
            $input['search'] = $input['filters']['search'];
            $input['filters'] = Arr::except($input['filters'], 'search');
        }

        if(Arr::exists($input['filters'], 'organization') && !empty($input['filters']['organization'])) {
            $selectedOrganizationId = $this->organizationService->getIdByXid('Organizations', $input['filters']['organization']);
            $subOrganizations = $this->organizationService->getNestedListByOrganizationId($selectedOrganizationId);
            $query->whereIn('organization_id', $subOrganizations->pluck('id'));
        } else {
            $subOrganizations = $this->organizationService->getNestedListByOrganizationId($input['organization_id']);
            $query->whereIn('organization_id', $subOrganizations->pluck('id'));
        }

        $query = $this->listQuery($query, $input, $listQueryParams);

        /*if(Arr::exists($input, 'filters') && Arr::exists($input['filters'], 'fromDate')) {
            $query->where('status_date', '>=', $input['filters']['fromDate']);
        }

        if(Arr::exists($input, 'filters') && Arr::exists($input['filters'], 'toDate')) {
            $query->where('status_date', '<=', $input['filters']['toDate']);
        }*/

        return $this->resultQuery($query, $input);
    }

    public function getCandidateSummaryReport($input = [])
    {
//        $reportQuery = Organization::query()->with(['suspeciousCandidates', 'verifiedCandidates', 'candidates']);
        $query = Organization::query();
        $query->with([
            'timezone',
            'parentOrganization',
            'subOrganizations.timezone',
            'subOrganizations.parentOrganization',
            'subOrganizations.subOrganizations.timezone',
            'subOrganizations.subOrganizations.parentOrganization',
            'suspeciousCandidates', 'verifiedCandidates', 'candidates'
        ]);
        $query->where('id', $input["organization_id"]);
        $rows = $query->get();
        $resultRows = collect();
        foreach ($rows as $level1) {
            $org = clone $level1;
            $org->subOrganizations = null;
            $org->level = 1;
            $resultRows->add($org);
            if (isset($level1->subOrganizations)) {
                foreach ($level1->subOrganizations as $level2) {
                    $org = clone $level2;
                    $org->subOrganizations = null;
                    $org->level = 2;
                    $resultRows->add($org);
                    if (isset($level2->subOrganizations)) {
                        foreach ($level2->subOrganizations as $level3) {
                            $org = clone $level3;
                            $org->subOrganizations = null;
                            $org->level = 3;
                            $resultRows->add($org);
                        }
                    }
                }
            }
        }
        return $resultRows;

        /*if (Arr::exists($input, 'startDate')) {
            $reportQuery->where('status_date', '>=', $input['startDate']);
        }

        if (Arr::exists($input, 'endDate')) {
            $reportQuery->where('status_date', '<=', $input['endDate']);
        }

        return $reportQuery->get();*/
    }
}
