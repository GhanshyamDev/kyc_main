<?php

namespace App\Services;

use App\Services\Crud\ListQueryParams;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OrganizationService extends BaseService
{
    public function getList(int $organizationId, array $input = [])
    {
        $query = Organization::query();
        $query->with(['timezone', 'parentOrganization']);
        $listQueryParams = new ListQueryParams();
        $listQueryParams->searchColumns = ['name'];
        $query->where(function (Builder $query) use ($organizationId) {
            $query->where('parent_organization_id', $organizationId)
                ->orWhere('id', $organizationId);
        });
        $query = $this->listQuery($query, $input, $listQueryParams);
        return $this->resultQuery($query, $input);
    }

    public function getNestedListByOrganizationId(int $organizationId)
    {
        $query = Organization::query();
        $query->with([
            'timezone',
            'parentOrganization',
            'subOrganizations.timezone',
            'subOrganizations.parentOrganization',
            'subOrganizations.subOrganizations.timezone',
            'subOrganizations.subOrganizations.parentOrganization'
        ]);
        $query->where('id', $organizationId);
        $rows = $query->get();
        $resultRows = collect();
        foreach ($rows as $level1) {
            $org = clone $level1;
            $org->subOrganizations = null;
            $org->level = 1;
            $resultRows->add($org);
            if (isset($level1->subOrganizations)) {
                foreach ($level1->subOrganizations as $level2) {
                    $org = clone $level2;
                    $org->subOrganizations = null;
                    $org->level = 2;
                    $resultRows->add($org);
                    if (isset($level2->subOrganizations)) {
                        foreach ($level2->subOrganizations as $level3) {
                            $org = clone $level3;
                            $org->subOrganizations = null;
                            $org->level = 3;
                            $resultRows->add($org);
                        }
                    }
                }
            }
        }
        return $resultRows;
    }

    public function getNestedOrgCollection() {

    }

    public function create(array $input)
    {
        DB::beginTransaction();
        $organization = Organization::query()->create([
            'organization_xid' => $input['organization_xid'],
            'organization_type_id' => $input['organization_type_id'],
            'parent_organization_id' => $input['parent_organization_id'],
            'name' => $input['name'],
            'contact_person' => $input['contact_person'],
            'phone_number' => $input['phone_number'],
            'email' => $input['email'],
            'timezone_id' => $input['timezone_id'],
            'is_active' => $input['is_active'],
        ]);

        DB::commit();

        return $organization;
    }

    public function getDetail($id)
    {
        $query = Organization::query();
        return $query->findOrFail($id);
    }

    public function update($id, array $input)
    {
        $query = Organization::where('id', $id);
        $query->update($input);
        return $this->getDetail($id);
    }

    public function delete($id)
    {
        $query = Organization::query()->where('id', $id);
        $entity = $query->first();

        if ($entity)
            return $entity->delete();

        return false;
    }
}
