<?php

namespace App\Services;

use App\Models\Candidate;
use App\Models\Otp;
use App\Models\WebLink;
use App\Notifications\SendOtp;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class OtpService extends BaseService
{
    public function generate($input)
    {
        $webLink = WebLink::query()
            ->where("link_token", $input['link_token'])
            ->firstOrFail();

        $candidate = Candidate::query()->with(['organization'])->findOrFail($webLink->candidate_id);

        $digits = 4;
        $otp_number = rand(pow(10, $digits-1), pow(10, $digits)-1);

        $otp = Otp::query()->create([
            'candidate_id' => $webLink->candidate_id,
            'web_link_id' => $webLink->id,
            'otp_number' => $otp_number,
            'to_phone_number' => ($input['send_by']['send_sms']) ? $candidate->phone_number : null,
            'to_email' => ($input['send_by']['send_email']) ? $candidate->email : null,
            'expire_at' => now()->addMinutes(60)
        ]);

        //send, sms or email
        if ($input['send_by']['send_email']) {
            //send mail, sms by notifications
            Notification::route('mail', $candidate->email)
                ->notify(new SendOtp($otp, $candidate));
        }

        return $otp;
    }

    public function checkIsValidAccessToken($accessToken)
    {
        return Otp::query()
            ->where("access_token", $accessToken)
            ->whereNull("used_at")
            ->whereDate("expire_at", ">=", now())
            ->count() > 0;
    }

    public function getOtpDetailByAccessToken($accessToken)
    {
        return Otp::query()
                ->where("access_token", $accessToken)
                ->whereNull("used_at")
                ->whereDate("expire_at", ">=", now())
                ->firstOrFail();
    }

    public function checkIsValidOtpNumber($input = [])
    {
        $webLink = WebLink::query()
            ->where("link_token", $input['link_token'])
            ->firstOrFail();

        return Otp::query()
                ->where("web_link_id", $webLink->id)
                ->where("otp_number", $input['otp_number'])
                ->whereNull("used_at")
                ->whereDate("expire_at", ">=", now())
                ->count() > 0;
    }

    public function getAccessToken($input = [])
    {
        $webLink = WebLink::query()
            ->where("link_token", $input['link_token'])
            ->first();

        if (!$webLink) {
            throw ValidationException::withMessages([
                'message' => 'Otp not valid.'
            ]);
        }

        $otp = Otp::query()
                ->where("web_link_id", $webLink->id)
                ->where("otp_number", $input['otp_number'])
                ->whereNull("used_at")
                ->whereDate("expire_at", ">=", now())
                ->first();

        if (!$otp) {
            throw ValidationException::withMessages([
                'message' => 'Otp not valid.'
            ]);
        }

        $otp->access_token = Str::uuid();
        $otp->save();

        return $otp;
    }
}
