<?php


namespace App\Services\Crud;


class ListQueryParams {
    public static $perPage = 10;

    public $searchColumns = [];

    public $filterColumns = [];

    public $sortBy = 'created_at';

    public $sortOrder = 'desc';
}
