<?php

namespace App\Services\Crud;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

trait ListQuery
{
    protected function listQuery(Builder $query, array $input, ListQueryParams $listQueryParams)
    {
        $this->filterQuery($query, $input, $listQueryParams->filterColumns);
        $this->searchQuery($query, $input, $listQueryParams->searchColumns);
        $this->sortQuery($query, $input, $listQueryParams->sortBy, $listQueryParams->sortOrder);

        return $query;
    }

    protected function filterQuery(Builder $query, $input = [], $filterColumns = [])
    {
        foreach (Arr::get($input, 'filters', []) as $column => $value) {
            if ($value && in_array($column, $filterColumns)) {
                $query->where($column, $value);
            }
        }
    }

    protected function searchQuery(Builder $query, $input = [], $searchColumns = [])
    {
        if ($searchValue = Arr::get($input, 'search')) {
            $query->where(function (Builder $query) use ($searchValue, $searchColumns) {
                foreach ($searchColumns as $column) {
                    $query->orWhere($column, 'like', '%' . $searchValue . '%');
                }
            });
        }
    }

    protected function sortQuery(Builder $query, $input = [], $sortBy = 'created_at', $sortOrder = 'desc')
    {
        if ($sortByInput = Arr::get($input, 'sortBy')) {
            $query->orderBy($sortByInput, Arr::get($input, 'sortOrder', 'ASC'));
        } else {
            $query->orderBy($sortBy, $sortOrder);
        }
    }

    protected function resultQuery(Builder $query, $input = [], $perPage = 10)
    {
        if (Arr::has($input, 'page')) {
            $pageSize = (Arr::get($input, 'pageSize')) ? Arr::get($input, 'pageSize') : $perPage;
            return $query->paginate($pageSize);
        }

        return $query->get();
    }
}
