<?php

namespace App\Services;

use App\Services\Crud\ListQueryParams;
use App\Models\Organization;
use App\Models\OrganizationType;
use App\Models\Role;
use Illuminate\Support\Arr;

class RoleService extends BaseService
{
    public function getList(array $input = [])
    {
        $query = Role::query();

        $query = $this->listQuery($query, $input, new ListQueryParams());

        return $this->resultQuery($query, $input);
    }

    public function getRoleIdByOrganizationTypeId(int $organizationTypeId): int
    {
        if ($organizationTypeId === OrganizationType::SUPERADMIN) {
            return 1;
        }
        if ($organizationTypeId === OrganizationType::COMPANY) {
            return 2;
        }
        if ($organizationTypeId === OrganizationType::COMPANYVENDOR) {
            return 3;
        }
        return 0;
    }

    public function create(array $input)
    {
        $entity = Role::create($input);
        return $entity;
    }

    public function getDetail($id)
    {
        $query = Role::query();
        return $query->findOrFail($id);
    }

    public function update($id, array $input)
    {
        $query = Role::where('id', $id);
        $query->update($input);
        return $this->getDetail($id);
    }

    public function delete($id)
    {
        $query = Role::query()->where('id', $id);
        $entity = $query->first();

        if ($entity)
            return $entity->delete();

        return false;
    }
}
