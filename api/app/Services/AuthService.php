<?php

namespace App\Services;

use App\Models\PasswordReset;
use App\Models\VerifyUser;
use App\Models\OrganizationUserXref;
use App\Models\Timezone;
use App\Mail\VerifyMail;
use App\Models\User;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Str;

class AuthService
{
    public function signIn(array $inputs)
    {
        $user = User::query()
            ->where('email', $inputs['email'])
            ->first();

        if (!$user) {
            throw ValidationException::withMessages(['message' => 'Email or Password not correct.']);
        }

        if (!$user->is_active) {
            throw ValidationException::withMessages(['message' => 'User is inactive. Please contact Administrator.']);
        }

        if ($user && Hash::check($inputs['password'], $user->password)) {
            $tokenResult = $user->createToken('Personal Access Token');

            $token = $tokenResult->token;
            if (array_key_exists('remember_me', $inputs) && $inputs['remember_me']) {
                $token->expires_at = now()->addWeeks(1);
                $token->save();
            }

            $user->tokenResult = $tokenResult;
            $user->load('timezone');
            $userOrganizations = OrganizationUserXref::query()
                ->select([
                    "org.*",
                    "tz.id AS timezone_id",
                    "tz.gmt_offset", "tz.name as timezone_name", "r.name AS role_name", "r.role_xid"])
                ->join("organizations as org","org.id", '=', "organization_user_xrefs.organization_id")
                ->join("timezones as tz","tz.id", '=', "org.timezone_id")
                ->join("roles as r","r.id", '=', "organization_user_xrefs.role_id")
                ->where("user_id", $user->id)
                ->get()
                ->map(function($item) {
                    //$item->type = "role";
                    $item->timezone = new Timezone([
                        "id" => $item->timezone_id,
                        "gmt_offset" => $item->gmt_offset,
                        "name" => $item->timezone_name
                    ]);
                    //$item->name = $item->name.' ('.( Str::startsWith($item->role_name, 'admin_') ? "admin" : $item->role_name).')';
                    return $item;
                });

            $user->allOrganizations = $userOrganizations;

            return $user;
        }

        throw ValidationException::withMessages(['message' => 'Email or Password not correct.']);
    }

    /**
     * @param array $inputs
     *
     * @return User
     */
    public function signUp(array $inputs)
    {
        $user = new User();
        $user->name = $inputs['name'];
        $user->email = $inputs['email'];
        $user->password = bcrypt($inputs['password']);
        $user->save();

        $this->createOrSendVerification($user->email);

        return $user;
    }

    /**
     * @param array $inputs
     *
     * @return User
     */
    public function createResetPassword(array $inputs)
    {
        $user = User::where('email', $inputs['email'])->first();

        if (!$user)
            throw ValidationException::withMessages(['message' => 'Credentials does not match with our portal.']);

        $passwordReset = PasswordReset::query()->updateOrCreate([
            'email' => $user->email
        ]);

        $passwordReset->token = Str::random(60);
        $passwordReset->save();

        $user->notify(
            new PasswordResetRequest($passwordReset->token)
        );

        return $user;
    }

    /**
     * @param array $inputs
     *
     * @return mixed
     */
    public function resetPassword(array $inputs)
    {
        $passwordReset = PasswordReset::where([
            'token' => $inputs['token'],
            'email' => $inputs['email']
        ])->first();

        if (!$passwordReset)
            throw ValidationException::withMessages(['message' => 'Credentials does not match with our portal.']);

        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();
            throw ValidationException::withMessages(['message' => 'This password reset token is invalid.']);
        }

        $user = User::query()->where('email', $passwordReset->email)->first();

        if (!$user)
            throw ValidationException::withMessages(['message' => 'Credentials does not match with our portal.']);

        $user->is_active = 1;
        $user->password = Hash::make($inputs['password']);
        $user->save();
        $passwordReset->delete();

        $user->notify(new PasswordResetSuccess());

        return $user;
    }

    /**
     * @param $userEmail
     * @return mixed|string
     */
    public function createOrSendVerification($userEmail)
    {
        $verifyUser = VerifyUser::whereEmail($userEmail)->latest()->first();

        if ($verifyUser) {
            if(Carbon::parse($verifyUser->updated_at)->addMinutes(720)->isPast()) {
                $verifyUser->token = Str::random(70);
                $verifyUser->update();
            }
        } else {
            VerifyUser::whereEmail($userEmail)->delete();
            $verifyUser = new VerifyUser();
            $verifyUser->email = $userEmail;
            $verifyUser->token = Str::random(70);
            $verifyUser->save();
        }

        \Mail::to($userEmail)->send(new VerifyMail($verifyUser->token));
        return $verifyUser->token;
    }

    /**
     * @param $token
     * @return bool
     * @throws ValidationException
     */
    public function verification($token) {
        $verifyUser = VerifyUser::whereToken($token)->first();

        if(!$verifyUser || ($verifyUser && Carbon::parse($verifyUser->updated_at)->addMinutes(720)->isPast()))
            throw ValidationException::withMessages(['message' => 'Verification link has expired.']);

        $user = User::whereEmail($verifyUser->email)->first();
        $user->email_verified_at = now();
        $user->is_active = 1;
        $user->save();

        VerifyUser::whereToken($token)->delete();
        return true;
    }
}
