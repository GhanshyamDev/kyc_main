<?php


namespace App\Services;

use App\Models\ActionLog;
use App\Services\BaseService;
use Carbon\Carbon;
use http\Env;
use Illuminate\Support\Facades\Log;

class ActionService extends BaseService
{
    private $similarityThreshold = 95;

    public function uploadImage(array $input) {
        global $region, $awsBucket;

        $s3 = new Aws\S3\S3Client([
            'version' => 'latest',
            'scheme' =>'http',
            'region' => $region,
            'credentials' => [
                'key'    => env('AWS_ACCESS_KEY_ID'),
                'secret' => env('AWS_SECRET_ACCESS_KEY'),
            ]
        ]);

        $result = $s3->putObject([
            'Bucket' => env('AWS_BUCKET'),
            'Key'    => $input['fileName'],
            'SourceFile' => storage_path($input['fileName'])
        ])->toArray();

        $currentDateTime = Carbon::now()->toDateTimeString();
        ActionLog::query()->insert([
            'organization_id' => $input['organization_id'],
            'candidate_id' => $input['candidate_id'],
            'service_type' => 'Upload',
            'image_file' => $input['fileName'],
            'created_at' => $currentDateTime,
            'updated_at' => $currentDateTime
        ]);

        return $result['@metadata'];
    }

    public function rekognizeImage(array $input)
    {
        $rekoClient = new \Aws\Rekognition\RekognitionClient([
            //'profile' => 'default',
            'region'  => env('AWS_DEFAULT_REGION'),
            'version' => 'latest',
            'credentials' => [
                'key'    => env('AWS_ACCESS_KEY_ID'),
                'secret' => env('AWS_SECRET_ACCESS_KEY'),
            ]
        ]);

        try {
            $result = $rekoClient->compareFaces([
                'SimilarityThreshold' => $this->similarityThreshold,
                'SourceImage' => [
                    'S3Object' => [
                        'Bucket' => env('AWS_BUCKET'),
                        'Name' => env('AWS_FOLDER').$input['sourceImage'],
                    ],
                ],
                'TargetImage' => [
                    'S3Object' => [
                        'Bucket' => env('AWS_BUCKET'),
                        'Name' => env('AWS_FOLDER').$input['targetImage'],
                    ],
                ],
            ]);

            $result = $result->toArray();
        } catch(Exception $e) {
//            echo '<p class="mt-2">Can not compare images. Please check if all images contain a face.</p>';
//            die();
            $result = [];
        }

        $currentDateTime = Carbon::now()->toDateTimeString();

        ActionLog::query()->insert([
            'organization_id' => !empty($input['organization_id']) ? $input['organization_id'] : 3,
            'candidate_id' => !empty($input['candidate_id']) ? $input['candidate_id'] : 16,
            'service_type' => 'Compare',
            'media_id' => !empty($input['mediaId']) ? $input['mediaId'] : null,
            'media_type' => !empty($input['mediaType']) ? $input['mediaType'] : null,
            'file_name' => !empty($input['targetImage']) ? $input['targetImage'] : null,
            'created_at' => $currentDateTime,
            'updated_at' => $currentDateTime
        ]);

        return !empty($result['FaceMatches'][0]) ? round($result['FaceMatches'][0]['Similarity'],2) : 0;
    }
}
