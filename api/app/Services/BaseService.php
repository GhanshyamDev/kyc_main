<?php

namespace App\Services;

use App\Services\Crud\ListQuery;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class BaseService
{
    use ListQuery;

    public function getIdByXid(string $table, $xid)
    {
        $field = Str::singular($table);
        $row = DB::table($table)
            ->select(['id'])
            ->where($field."_xid", $xid)
            ->first();

        if ($row) {
            return $row->id;
        }

        return null;
    }

    public function getXidById(string $table, $id)
    {
        $field = Str::singular($table);
        $row = DB::table($table)
            ->select([$field."_xid"])
            ->where("id", $id)
            ->first();

        if ($row) {
            return $row->{$table."_xid"};
        }

        return null;
    }

    public function getIdsByXids(string $table, $xids = [])
    {
        if (!count($xids)){
            return [];
        }
        $field = Str::singular($table);
        $ids = DB::table($table)
            ->whereIn($field."_xid", $xids)
            ->pluck('id');

       return $ids;
    }

    public function getXidsByIds(string $table, $ids)
    {
        if (!count($ids)){
            return [];
        }

        $field = Str::singular($table);
        $ids = DB::table($table)
            ->whereIn("id", $ids)
            ->pluck($field."_xid");

        return $ids;
    }

    /*public function getList(array $input = [])
    {
        $query = $this->model::query();

        $query = $this->listQuery($query, $input);

        return $this->resultQuery($query, $input);
    }

    public function create(array $input)
    {
        if (!$this->belongsToOrganization) {
            unset($input['organization_id']);
        }

        $entity = $this->model::create($input);

        return $entity;
    }

    public function getDetail($id, array $input = [])
    {
        $query = $this->model::query();

        if ($this->belongsToOrganization) {
            $query->where('organization_id', $input['organization_id']);
        }

        return $query->findOrFail($id);
    }

    public function update($id, array $input)
    {
        $query = $this->model::where($this->primaryKey, $id);

        if ($this->belongsToOrganization) {
            $query->where('organization_id', $input['organization_id']);
        }

        $query->update($input);

        return $this->getDetailByGuid($id, $input);
    }

    public function delete($id, array $input = [])
    {
        $query = $this->model::where($this->primaryKey, $id);

        if ($this->belongsToOrganization) {
            $query->where('organization_id', $input['organization_id']);
        }

        $entity = $query->first();

        if ($entity)
            return $entity->delete();

        return false;
    }

    public function updateByGuid($id, array $input)
    {
        $query = $this->model::where('guid', $id);

        if ($this->belongsToOrganization) {
            $query->where('organization_id', $input['organization_id']);
        }

        $query->update($input);

        return $this->getDetailByGuid($id, $input);
    }

    public function deleteByGuid($id, array $input = [])
    {
        $query = $this->model::where('guid', $id);

        if ($this->belongsToOrganization) {
            $query->where('organization_id', $input['organization_id']);
        }

        $entity = $query->first();

        if ($entity)
            return $entity->delete();

        return false;
    }

    public function getDetailByGuid($id, array $input = [])
    {
        $query = $this->model::query();

        if ($this->belongsToOrganization) {
            $query->where('organization_id', $input['organization_id']);
        }

        foreach ($input as $column => $value) {
            $query->where($column, $value);
        }

        return $query->findOrFailByGuid($id);
    }*/
}
