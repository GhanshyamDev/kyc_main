<?php


namespace App\Services;

use App\Services\Crud\ListQueryParams;
use App\Models\Candidate;
use App\Models\RekognitionResultLog;
use App\Services\ActionService;
use Carbon\Carbon;
use Faker\Factory;
use http\Env\Response;
use Illuminate\Notifications\Action;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Psy\Util\Str;

class CandidateService extends BaseService
{
    public function getList(array $input)
    {
        $query = Candidate::query();

        $listQueryParams = new ListQueryParams();
        $listQueryParams->searchColumns = ['email', 'first_name', 'last_name', 'phone_number', 'job_id', 'cid', 'address', 'comment', 'designation'];
        $listQueryParams->filterColumns = ['current_status'];

        if(Arr::exists($input, 'filters') && Arr::exists($input['filters'], 'search')) {
            $input['search'] = $input['filters']['search'];
            $input['filters'] = Arr::except($input['filters'], 'search');
        }

        if (Arr::get($input, 'organization_id')) {
            $query->where('organization_id', $input['organization_id']);
        }

        $query = $this->listQuery($query, $input, $listQueryParams);
        return $this->resultQuery($query, $input);
    }

    public function create(array $input)
    {
        $candidate = Candidate::query()->create([
            'candidate_xid' => $input['candidate_xid'],
            'cid' => !empty($input['cid']) ? $input['cid'] : '',
            'first_name' => $input['first_name'],
            'last_name' => $input['last_name'],
            'email' => !empty($input['email']) ? $input['email'] : '',
            'phone_number' => !empty($input['phone_number']) ? $input['phone_number'] : '',
            'current_status' => 'New',
            'comment' => !empty($input['comment']) ? $input['comment'] : '',
            'organization_id' => $input['organization_id'],
            'address' => !empty($input['address']) ? $input['address'] : '',
            'designation' => !empty($input['designation']) ? $input['designation'] : '',
            'job_id' => !empty($input['job_id']) ? $input['job_id'] : ''
        ]);

        return $candidate;
    }

    public function update($id, array $input)
    {
        Candidate::query()->where('id', $id)
                    ->update($input);

        return $this->getDetail($id);
    }

    public function getDetail($id)
    {
        $candidate = Candidate::query()->with(['organization'])->findOrFail($id);
        $masterImage = $candidate->lastMedia('master_image');
        $finalValidationImage = $candidate->lastMedia('final_validation_image');
        $candidate->selfie_uploaded_on = optional($masterImage)->created_at;
        $candidate->final_selfie_uploaded_on = optional($finalValidationImage)->created_at;
        return $candidate;
    }

    public function delete($id)
    {
        $entity = Candidate::query()->find($id);

        if($entity)
            return $entity->delete();

        return false;
    }

    private function compareImageByTag($candidate, $imageTag, $compareScoreThreshold)
    {
        $masterImage = $candidate->lastMedia('master_image');
        $candidateImage = $candidate->lastMedia($imageTag);
        if($masterImage && $candidateImage) {
            $input['candidate_id'] = $candidate->id;
            $input['organization_id'] = $candidate->organization_id;
            $input['sourceImage'] = $masterImage->basename;
            $input['targetImage'] = $candidateImage->basename;
            $input['mediaId'] = $candidateImage->id;
            $input['mediaType'] = $imageTag;

            $actionService = new ActionService();
            $result = $actionService->rekognizeImage($input);

            $input['sourceId'] = $masterImage->id;
            $input['result'] = $result;
            $input['status'] = ($result >= $compareScoreThreshold) ? 'Verified' : 'Suspicious';
            $this->logRekognitionEntry($input);
        } else {
            $result = 0;
        }


        return $result;
    }

    public function markCompleted($id)
    {
        $candidate = Candidate::query()->find($id);
        if($candidate) {
            $candidate->completed_status = 1;
            $candidate->save();
            return true;
        }

        return false;
    }

    public function compareImages($id, $compareScoreThreshold)
    {
        $candidate = Candidate::query()->find($id);
        if($candidate) {
            $input['candidate_id'] = $id;

            if($candidate->master_image_changed || $candidate->step_1_image_changed) {
                $this->compareImageByTag($candidate, 'step_1_image', $compareScoreThreshold);
                $candidate->master_image_changed = 0;
                $candidate->step_1_image_changed = 0;
            }

            if($candidate->master_image_changed || $candidate->step_2_image_changed) {
                $this->compareImageByTag($candidate, 'step_2_image', $compareScoreThreshold);
                $candidate->master_image_changed = 0;
                $candidate->step_2_image_changed = 0;
            }

            if($candidate->master_image_changed || $candidate->step_3_image_changed) {
                $this->compareImageByTag($candidate, 'step_3_image', $compareScoreThreshold);
                $candidate->master_image_changed = 0;
                $candidate->step_3_image_changed = 0;
            }

            if($candidate->master_image_changed || $candidate->validation_image_changed) {
                $this->compareImageByTag($candidate, 'final_validation_image', $compareScoreThreshold);
                $candidate->master_image_changed = 0;
                $candidate->validation_image_changed = 0;
            }

            $candidate->current_status = ($this->updateCandidateStatus($candidate)) ? 'Verified' : 'Suspicious';
            $candidate->status_date = Carbon::now()->toDateTimeString();

            $candidate->save();

            return response()->json(['msg' => 'success']);
        }

        return response()->json(['msg' => 'failed']);
    }

    private function updateCandidateStatus(Candidate $candidate)
    {
        $candidateImagesStatuses = [];
        $candidateVerified = true;

        $candidateImagesStatuses['step1ImageStatus'] = $this->getCandidateImageStatusByTag($candidate, 'step_1_image');
        $candidateImagesStatuses['step2ImageStatus'] = $this->getCandidateImageStatusByTag($candidate, 'step_2_image');
        $candidateImagesStatuses['step3ImageStatus'] = $this->getCandidateImageStatusByTag($candidate, 'step_3_image');
        $candidateImagesStatuses['finalValidationImageStatus'] = $this->getCandidateImageStatusByTag($candidate, 'final_validation_image');

        foreach($candidateImagesStatuses as $key => $imageStatus) {
            $candidateVerified = $candidateVerified && $imageStatus === 'Verified';
        }

        return $candidateVerified;
    }

    private function getCandidateImageStatusByTag(Candidate $candidate, $imageTag)
    {
        $rekognitionLogStatus = '';
        $imageMedia = $candidate->lastMedia($imageTag);
        if($imageMedia) {
            $rekognitionResultLog = RekognitionResultLog::query()
                                    ->where('candidate_id', $candidate->id)
                                    ->where('target_media_id', $imageMedia->media_id)->get()->last();

            $rekognitionLogStatus = ($rekognitionResultLog) ? $rekognitionResultLog->status : '';
        }

        return $rekognitionLogStatus;
    }

    public function getCandidateImages($id)
    {
        $candidate = Candidate::query()->with(['media'])->find($id);

        $master_image = $candidate->lastMedia('master_image');
        $step_1_image = $candidate->lastMedia('step_1_image');
        $step_2_image = $candidate->lastMedia('step_2_image');
        $step_3_image = $candidate->lastMedia('step_3_image');
        $final_validation_image = $candidate->lastMedia('final_validation_image');

        $step_1_image_status = null;
        $step_2_image_status = null;
        $step_3_image_status = null;
        $final_validation_image_status = null;

        if($step_1_image) {
            $step_1_image_status = RekognitionResultLog::query()->where('target_media_id', $step_1_image->id)->get()->last();
        }

        if ($step_2_image) {
            $step_2_image_status = RekognitionResultLog::query()->where('target_media_id', $step_2_image->id)->get()->last();
        }
        if($step_3_image) {
            $step_3_image_status = RekognitionResultLog::query()->where('target_media_id', $step_3_image->id)->get()->last();
        }
        if($final_validation_image) {
            $final_validation_image_status = RekognitionResultLog::query()->where('target_media_id', $final_validation_image->id)->get()->last();
        }

        return [
            'master_image' => [
                'fileName' => ($master_image) ? $master_image->basename : '',
                'url' => ($master_image) ? $master_image->getUrl() : '',
                'id' => ($master_image) ? $master_image->id : null,
            ],
            'step_1_image' => [
                'fileName' => ($step_1_image) ? $step_1_image->basename : '',
                'url' => ($step_1_image) ? $step_1_image->getUrl() : '',
                'id' => ($step_1_image) ? $step_1_image->id : null,
                'imageStatus' => ($step_1_image_status) ? $step_1_image_status->status : '',
                'result' => ($step_1_image_status) ? $step_1_image_status->rekognition_result : 0
            ],
            'step_2_image' => [
                'fileName' => ($step_2_image) ? $step_2_image->basename : '',
                'url' => ($step_2_image) ? $step_2_image->getUrl() : '',
                'id' => ($step_2_image) ? $step_2_image->id : null,
                'imageStatus' => ($step_2_image_status) ? $step_2_image_status->status : '',
                'result' => ($step_2_image_status) ? $step_2_image_status->rekognition_result : 0
            ],
            'step_3_image' => [
                'fileName' => ($step_3_image) ? $step_3_image->basename : '',
                'url' => ($step_3_image) ? $step_3_image->getUrl() : '',
                'id' => ($step_3_image) ? $step_3_image->id : null,
                'imageStatus' => ($step_3_image_status) ? $step_3_image_status->status : '',
                'result' => ($step_3_image_status) ? $step_3_image_status->rekognition_result : 0
            ],
            'final_validation_image' => [
                'fileName' => ($final_validation_image) ? $final_validation_image->basename: '',
                'url' => ($final_validation_image) ? $final_validation_image->getUrl() : '',
                'id' => ($final_validation_image) ? $final_validation_image->id : null,
                'imageStatus' => ($final_validation_image_status) ? $final_validation_image_status->status : '',
                'result' => ($final_validation_image_status) ? $final_validation_image_status->rekognition_result : 0
            ],
            'master_image_changed' => $candidate->master_image_changed,
            'step_1_image_changed' => $candidate->step_1_image_changed,
            'step_2_image_changed' => $candidate->step_2_image_changed,
            'step_3_image_changed' => $candidate->step_3_image_changed,
            'final_validation_image_changed' => $candidate->validation_image_changed
        ];
    }

    public function logRekognitionEntry($input)
    {
        $currentDateTime = Carbon::now()->toDateTimeString();
        RekognitionResultLog::query()->insert([
            'organization_id' => !empty($input['organization_id']) ? $input['organization_id'] : null,
            'candidate_id' => !empty($input['candidate_id']) ? $input['candidate_id'] : null,
            'source_media_id' => $input['sourceId'],
            'target_media_id' => $input['mediaId'],
            'rekognition_result' => $input['result'],
            'status' => $input['status'],
            'user_id' => auth()->id(),
            'created_at' => $currentDateTime,
            'updated_at' => $currentDateTime
        ]);
    }
}
