<?php

namespace App\Services;

use App\Models\Customer;
use App\Services\Crud\ListQueryParams;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class CustomerService extends BaseService
{
    public function getList(array $input)
    {
        $query = Customer::query();

        $listQueryParams = new ListQueryParams();
        $listQueryParams->searchColumns = ['email', 'first_name', 'last_name', 'phone_number', 'cid', 'address', 'aadhar_number'];
        $listQueryParams->filterColumns = ['current_status'];

        if(Arr::exists($input, 'filters') && Arr::exists($input['filters'], 'search')) {
            $input['search'] = $input['filters']['search'];
            $input['filters'] = Arr::except($input['filters'], 'search');
        }

        if (Arr::get($input, 'organization_id')) {
            $query->where('organization_id', $input['organization_id']);
        }

        $query = $this->listQuery($query, $input, $listQueryParams);
        return $this->resultQuery($query, $input);
    }

    public function create(array $input)
    {
        $candidate = Customer::query()->create([
            'customer_xid' => $input['customer_xid'],
            'organization_id' => $input['organization_id'],
            'first_name' => $input['first_name'],
            'cid' => !empty($input['cid']) ? $input['cid'] : '',
            'last_name' => $input['last_name'],
            'email' => !empty($input['email']) ? $input['email'] : '',
            'phone_number' => !empty($input['phone_number']) ? $input['phone_number'] : '',
            'aadhar_number' => !empty($input['aadhar_number']) ? $input['aadhar_number'] : '',
            'dob' => !empty($input['dob']) ? $input['dob'] : '',
            'address' => !empty($input['address']) ? $input['address'] : '',
            'bank_name' => !empty($input['bank_name']) ? $input['bank_name'] : '',
            'bank_ifsc_code' => !empty($input['bank_ifsc_code']) ? $input['bank_ifsc_code'] : '',
            'bank_account_number' => !empty($input['bank_account_number']) ? $input['bank_account_number'] : '',
            'current_status' => 'New',
            'customer_form_submitted_at' => Carbon::now()->toDateTimeString()
        ]);

        return $candidate;
    }

    public function update($id, array $input)
    {
        Candidate::query()->where('id', $id)
            ->update($input);

        return $this->getDetail($id);
    }

    public function getDetail($id)
    {
        $customer = Customer::query()->with(['organization'])->findOrFail($id);
        /*$masterImage = $customer->lastMedia('master_image');
        $finalValidationImage = $customer->lastMedia('final_validation_image');*/
        /*$customer->selfie_uploaded_on = optional($masterImage)->created_at;
        $customer->final_selfie_uploaded_on = optional($finalValidationImage)->created_at;*/
        return $customer;
    }

    public function delete($id)
    {
        $entity = Customer::query()->find($id);

        if($entity)
            return $entity->delete();

        return false;
    }
}
