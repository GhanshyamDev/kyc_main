<?php

namespace App\Services;

use App\Services\Crud\ListQueryParams;
use App\Models\Candidate;
use App\Models\Organization;
use App\Models\OrganizationType;
use App\Models\Role;
use App\Models\WebLink;
use App\Notifications\SendWebLink;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class WebLinkService extends BaseService
{

    public function getDetailByLinkToken($linkToken)
    {
        $info = WebLink::query()
            ->with(['candidate', 'organization'])
            ->where("link_token", $linkToken)
            ->whereNull("used_at")
            ->whereDate("expire_at", ">=", now())
            ->firstOrFail();

        return $info;
    }

    public function send($input = [])
    {
        $organization = Organization::query()->findOrFail($input['organization_id']);
        $candidate = Candidate::query()->findOrFail($input['candidate_id']);
        $candidate->organization = $organization;

        $webLink = WebLink::query()->create([
            'organization_id' => $input['organization_id'],
            'candidate_id' => $input['candidate_id'],
            'link_token' => Str::uuid(),
            'link_type' => $input['link_type'],
            'to_phone_number' => ($input['send_by'] === WebLink::SEND_SMS) ? $candidate->phone_number : null,
            'to_email' => ($input['send_by'] === WebLink::SEND_EMAIL) ? $candidate->email : null,
            'is_copy_link_generated' => ($input['send_by'] === WebLink::GENERATE_LINK) ? true : false,
            'expire_at' => now()->addHours($organization->link_expire_hours)
        ]);
        //Log::debug($candidate);
        if ($input['send_by'] === WebLink::SEND_EMAIL) {
            //send mail, sms by notifications
            Notification::route('mail', $candidate->email)
                ->notify(new SendWebLink($webLink, $candidate));
        }

        return $webLink;
    }

    public function checkIsValidLinkToken($linkToken)
    {
        return WebLink::query()
            ->where("link_token", $linkToken)
            ->whereNull("used_at")
            ->whereDate("expire_at", ">=", now())
            ->count() > 0;
    }
}
