<?php

namespace App\Models;

//use App\Helpers\HasGuidTrait;
use Illuminate\Database\Eloquent\Model;
//use Spatie\MediaLibrary\HasMedia\HasMedia;
//use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

//class TempFile extends Model implements HasMedia
class TempFile extends Model
{
    //use HasGuidTrait, HasMediaTrait;
    public $timestamps = false;
    protected $guarded = [];
}
