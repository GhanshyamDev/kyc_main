<?php

namespace App\Models;

//use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Helpers\Auditable\AuditableWithDeletesTrait;
use App\Notifications\ResetPasswordLink;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, AuditableWithDeletesTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordLink($token));
    }

    public function verifyUser()
    {
        return $this->hasOne(VerifyUser::class);
    }

    public function organizations()
    {
        return $this->belongsToMany(Organization::class, OrganizationUserXref::class, 'user_id', 'organization_id')
            ->withPivot('role_id')
            ->withTimestamps();
    }

    public function organizationUsers()
    {
        return $this->hasMany(OrganizationUserXref::class);
    }

    public function organizationUser()
    {
        return $this->hasOne(OrganizationUserXref::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, "organization_user_xrefs", 'user_id', 'role_id')->withTimestamps();
    }

    public function timezone()
    {
        return $this->belongsTo(Timezone::class);
    }

    /**
     * Scope a query to only include Active users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function initials()
    {
        $words = explode(" ", $this->first_name.' '.$this->last_name);
        $initials = null;
        foreach ($words as $w) {
            $initials .= $w[0];
        }
        return strtoupper($initials);
    }

    public function fullName()
    {
        return $this->first_name.' '.$this->last_name;
    }
}
