<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Media;

class RekognitionResultLog extends Model
{
    use HasFactory;

    public function sourceMedia()
    {
        return $this->belongsTo(Media::class,  'source_media_id', 'id');
    }

    public function targetMedia()
    {
        return $this->belongsTo(Media::class, 'target_media_id', 'id');
    }
}
