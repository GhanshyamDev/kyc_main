<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrganizationType extends Model
{
    use HasFactory;

    protected $guarded = [];

    const SUPERADMIN = 1;
    const COMPANY = 2;
    const COMPANYVENDOR = 3;

    public function organizations()
    {
        return $this->hasMany(Organization::class);
    }
}
