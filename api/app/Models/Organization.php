<?php

namespace App\Models;

use App\Helpers\Auditable\AuditableWithDeletesTrait;
use http\Client\Curl\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Organization extends Model
{
    use AuditableWithDeletesTrait, SoftDeletes;

    protected $guarded = [];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function users()
    {
        return $this->belongsToMany(User::class, OrganizationUserXref::class, 'organization_id', 'user_id')
            ->withPivot('role_id')
            ->withTimestamps();
    }

    public function organizationType()
    {
        return $this->belongsTo(OrganizationType::class);
    }

    public function subOrganizations()
    {
        return $this->hasMany(Organization::class, "parent_organization_id");
    }

    public function parentOrganization()
    {
        return $this->belongsTo(Organization::class, "parent_organization_id");
    }

    public function organizationUsers()
    {
        return $this->hasMany(OrganizationUserXref::class);
    }

    public function organizationUser()
    {
        return $this->hasOne(OrganizationUserXref::class);
    }

    public function timezone()
    {
        return $this->belongsTo(Timezone::class);
    }

    public function suspeciousCandidates()
    {
        return $this->hasMany(Candidate::class, 'organization_id', 'id')->where('current_status', 'Suspicious');
    }

    public function verifiedCandidates()
    {
        return $this->hasMany(Candidate::class, 'organization_id', 'id')->where('current_status', 'Verified');
    }

    public function candidates()
    {
        return $this->hasMany(Candidate::class, 'organization_id', 'id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | FUNCTION
    |--------------------------------------------------------------------------
    */
}
