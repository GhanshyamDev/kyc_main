<?php

namespace App\Models;

use App\Helpers\Auditable\AuditableWithDeletesTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebLink extends Model
{
    use HasFactory, AuditableWithDeletesTrait, SoftDeletes;

    protected $guarded = [];

    const LINK_TYPE_CANDIDATE_SELFIE_1 = "candidate_selfie_1";
    const LINK_TYPE_CANDIDATE_SELFIE_2 = "candidate_selfie_2";

    const SEND_SMS = "send_sms";
    const SEND_EMAIL = "send_email";
    const GENERATE_LINK = "generate_link";

    public function candidate()
    {
        return $this->belongsTo(Candidate::class);
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }
}
