<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Http\Resources\RoleResource;
use App\Http\Resources\WebLinkInfoResource;
use App\Models\WebLink;
use App\Services\WebLinkService;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class WebLinkController extends ApiBaseController
{
    public $service;

    public $resource = WebLinkService::class;

    public function __construct(WebLinkService $webLinkService)
    {
        $this->service = $webLinkService;
    }

    public function getDetail($linkToken)
    {
        $detail = $this->service->getDetailByLinkToken($linkToken);

        return new WebLinkInfoResource($detail);
    }

    public function postSend(Request $request)
    {
        $rules = [
            "candidate_xid" => "required",
            "link_type" => ["required", Rule::in([
                WebLink::LINK_TYPE_CANDIDATE_SELFIE_1, WebLink::LINK_TYPE_CANDIDATE_SELFIE_2
            ])],
            "send_by" => ["required", Rule::in([
                WebLink::SEND_SMS, WebLink::SEND_EMAIL, WebLink::GENERATE_LINK,
            ])]
        ];

        $validatedData = $this->validate($request, $rules);

        $currentOrganization = $request->currentOrganization;
        $candidateId = $this->service->getIdByXid("candidates", $validatedData['candidate_xid']);

        $validatedData['organization_id'] = $currentOrganization->id;
        $validatedData['candidate_id'] = $candidateId;

        $entity = $this->service->send($validatedData);
        return response()->json([
            'data' => [
                'link_token' => $entity->link_token,
            ]
        ]);
    }

    public function postSendToCustomer(Request $request)
    {
        $rules = [
            "customer_xid" => "required",
            "link_type" => ["required", Rule::in([
                WebLink::LINK_TYPE_CANDIDATE_SELFIE_1, WebLink::LINK_TYPE_CANDIDATE_SELFIE_2
            ])],
            "send_by" => ["required", Rule::in([
                WebLink::SEND_SMS, WebLink::SEND_EMAIL, WebLink::GENERATE_LINK,
            ])]
        ];

        $validatedData = $this->validate($request, $rules);

        $currentOrganization = $request->currentOrganization;
        $candidateId = $this->service->getIdByXid("candidates", $validatedData['candidate_xid']);

        $validatedData['organization_id'] = $currentOrganization->id;
        $validatedData['candidate_id'] = $candidateId;

        $entity = $this->service->send($validatedData);
        return response()->json([
            'data' => [
                'link_token' => $entity->link_token,
            ]
        ]);
    }

    public function postVerify(Request $request)
    {
        $rules = [
            "link_token" => "required",
        ];

        $validatedData = $this->validate($request, $rules);

        if (!$this->service->checkIsValidLinkToken($validatedData["link_token"])) {
            throw ValidationException::withMessages(['message' => 'link not valid.']);
        }

        return response()->json(['data' => ['success' => true]]);
    }
}
