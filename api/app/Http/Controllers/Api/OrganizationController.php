<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Models\OrganizationType;
use App\Services\OrganizationService;
use App\Http\Resources\OrganizationResource;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class OrganizationController extends ApiBaseController
{
    /**
     * @var OrganizationService
     */
    public $service;

    public function __construct(OrganizationService $organizationService)
    {
        $this->service = $organizationService;
    }

    public function getList(Request $request)
    {
        $currentOrganization = $request->currentOrganization;

        if ($request->get('all')) {
            $user = auth()->user();
            $user->load(['organizationUser']);
            return OrganizationResource::collection($this->service->getNestedListByOrganizationId($user->organizationUser->organization_id));
        }

        if ($request->get('my')) {
            return OrganizationResource::collection($this->service->getNestedListByOrganizationId($currentOrganization->id));
        }

        $entities = $this->service->getList($currentOrganization->id, $request->all());
        return OrganizationResource::collection($entities);
    }

    public function postCreate(Request $request)
    {
        $currentOrganization = $request->currentOrganization;
        $validatedData = $this->validateInput($request);
        $validatedData["timezone_id"] = 248;
        $validatedData["parent_organization_id"] = $currentOrganization->id;
        $validatedData["organization_xid"] = Str::uuid();
        if ($currentOrganization->organization_type_id === OrganizationType::SUPERADMIN) {
            $validatedData["organization_type_id"] = OrganizationType::COMPANY;
        }
        if ($currentOrganization->organization_type_id === OrganizationType::COMPANY) {
            $validatedData["organization_type_id"] = OrganizationType::COMPANYVENDOR;
        }
        if ($currentOrganization->organization_type_id !== OrganizationType::SUPERADMIN &&
            $currentOrganization->organization_type_id !== OrganizationType::COMPANY) {
            throw ValidationException::withMessages([
                'message' => 'No Org. hierarchy under this Org. is supported in this version.'
            ]);
        }

        $entity = $this->service->create($validatedData);
        return new OrganizationResource($entity);
    }

    public function postUpdate(Request $request)
    {
        $xid = $request->input('organization_xid');
        $id = $this->service->getIdByXid("organizations", $xid);
        $validatedData = $this->validateInput($request, $id);
        $validatedData["timezone_id"] = 248;
        $entity = $this->service->update($id, $validatedData);
        return new OrganizationResource($entity);
    }

    public function getDetail(Request $request, $xid)
    {
        $id = $this->service->getIdByXid("organizations", $xid);
        $entity = $this->service->getDetail($id);
        return new OrganizationResource($entity);
    }

    public function postDelete(Request $request)
    {
        $xid = $request->input('organization_xid');
        $id = $this->service->getIdByXid("organizations", $xid);
        return response()->json(['success' => $this->service->delete($id)]);
    }

    /**
     * @param Request $request
     * @param null $id
     * @param boolean $id_required
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateInput(Request $request, $id = null) : array
    {
        $rules = [
            "name" => "required",
            "contact_person" => "required",
            "email" => "required",
            "phone_number" => "required",
            //"timezone_id" => "required",
            "is_active" => "required|boolean",
        ];

        /*if (!$id) {
            $addRules = [
                "first_name" => "required",
                "last_name" => "required",
                "phone_number" => "required",
                "email" => [
                    "required",
                    Rule::unique('users')->ignore($id, 'guid'),
                ],
                "password" => "required|confirmed",
            ];

            $rules = array_merge($rules, $addRules);
        }*/

        return $this->validate($request, $rules);
    }

    public function postProfile(Request $request)
    {
        $xid = $request->input('organization_xid');
        $id = $this->service->getIdByXid("organization", $xid);

        $this->validate($request, [
            "name" => "required",
            //"timezone_id" => "required",
        ]);

        $entity = $this->service->update($id, Arr::only($request->all(), ['timezone_id', 'name']));
        $entity->load('timezone');

        return new OrganizationResource($entity);
    }
}
