<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Models\Role;
use App\Services\RoleService;
use App\Services\UserService;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class UserController extends ApiBaseController
{
    /**
     * @var UserService
     */
    public $service;
    public $roleService;

    public function __construct(UserService $userService, RoleService $roleService)
    {
        $this->service = $userService;
        $this->roleService = $roleService;
    }

    /**
     * @param Request $request
     * @param null $id
     * @param boolean $id_required
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateInput(Request $request, $id = null): array
    {
        $rules = [
            "first_name" => "required",
            "last_name" => "required",
            "phone_number" => ["required", Rule::unique('users')->ignore($id, 'id')],
            "organization_xid" => "required",
            //"timezone_id" => "required",
            "email" => ["required", Rule::unique('users')->ignore($id, 'id')],
            "password" => "nullable|confirmed",
            "is_active" => "required|boolean",
            //"role_id" => "",
        ];

        if (!$id) {
            $rules['password'] = "required|confirmed";
        }

        $validated = $this->validate($request, $rules);
        //$validated = $this->validateroleid($validated);
        return $validated;
    }

    /*private function validateRoleId(array $validated)
    {
        if ($validated['is_active']) {
            $validated['role_id'] = ($validated['role_id']) ? Role::findOrFailByGuid($validated['role_id'])->id : null;
        } else {
            $validated['role_id'] = null;
        }
        return $validated;
    }*/

    public function getList(Request $request)
    {
        $input = $request->all();
        $currentOrganization = $request->currentOrganization;
        $input["organization_id"] = $currentOrganization->id;
        $entities = $this->service->getList($input);
        return UserResource::collection($entities);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function postCreate(Request $request)
    {
        $currentOrganization = $request->currentOrganization;
        $validatedData = $this->validateInput($request);
        $validatedData["timezone_id"] = 248;
        $validatedData["user_xid"] = Str::uuid();
        $validatedData["organization_id"] = $currentOrganization->id;
        $validatedData["role_id"] = $this->roleService->getRoleIdByOrganizationTypeId($currentOrganization->organization_type_id);
        $entity = $this->service->create($validatedData);
        return new UserResource($entity);
    }

    public function getDetail(Request $request, $xid)
    {
        $id = $this->service->getIdByXid("users", $xid);
        $entity = $this->service->getDetail($id);
        return new UserResource($entity);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function postUpdate(Request $request)
    {
        $xid = $request->input('user_xid');
        $id = $this->service->getIdByXid("users", $xid);
        $validatedData = $this->validateInput($request, $id);
        $validatedData["timezone_id"] = 248;
        $entity = $this->service->update($id, $validatedData);
        return new UserResource($entity);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function postDelete(Request $request)
    {
        $currentOrganization = $request->currentOrganization;
        $xid = $request->input('user_xid');
        $id = $this->service->getIdByXid("users", $xid);
        return response()->json([
            'success' => $this->service->delete($id, ["organization_id" => $currentOrganization->organizaion_id])
        ]);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function postUpdateProfile(Request $request)
    {
        $xid = $request->input('user_xid');
        $id = $this->service->getIdByXid("users", $xid);

        $this->validate($request, [
            "first_name" => "required",
            "last_name" => "required",
            "phone_number" => ["required", Rule::unique('users')->ignore($id, 'id')],
            //"organization_id" => "required",
            //"timezone_id" => "required",
            "email" => ["required", Rule::unique('users')->ignore($id, 'id')],
            "password" => "nullable|confirmed",
        ]);

        $entity = $this->service->update($id, Arr::only($request->all(), [
            //'timezone_id',
            'first_name',
            'last_name',
            'email',
            'phone_number',
            'password'
        ]));
        //$entity->load('timezone');
        return new UserResource($entity);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws ValidationException
     */
    public function postChangePassword(Request $request)
    {
        $xid = $request->input('user_xid');
        $id = $this->service->getIdByXid("users", $xid);

        $this->validate($request, [
            "password" => "required|confirmed|min:8",
        ]);

        $entity = $this->service->changePassword($id, $request->all());
        return new UserResource($entity);
    }
}
