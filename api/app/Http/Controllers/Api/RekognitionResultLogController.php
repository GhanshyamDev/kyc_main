<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\RekognitionResultLogResource;
use App\Services\RekognitionResultLogService;
use Illuminate\Http\Request;

class RekognitionResultLogController extends Controller
{
    /**
     * @var RekognitionResultLogService
     */
    private $rekognitionResultLogService;

    public function __construct(RekognitionResultLogService $rekognitionResultLogService)
    {
        $this->rekognitionResultLogService = $rekognitionResultLogService;
    }

    public function getRekognitionResultByCandidateId(Request $request, $candidate_xid)
    {
        $candidateId = $this->rekognitionResultLogService->getIdByXid("candidates", $candidate_xid);
        $resultLogs = $this->rekognitionResultLogService->getRekognitionResultByCandidateId($candidateId);
        return RekognitionResultLogResource::collection($resultLogs);
    }
}
