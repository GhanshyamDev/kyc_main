<?php

namespace App\Http\Controllers\Api;

use App\Helpers\UtilHelper;
use App\Http\Controllers\Controller;
use App\Models\ActionLog;
use App\Models\Candidate;
use App\Models\RekognitionResultLog;
use App\Models\TempFile;
use App\Models\WebLink;
use App\Services\CandidateService;
use App\Services\WebLinkService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Plank\Mediable\Exceptions\MediaUploadException;
use Plank\Mediable\Facades\MediaUploader;
use Plank\Mediable\HandlesMediaUploadExceptions;

class FileController extends Controller
{
    use HandlesMediaUploadExceptions;

    public $candidateService;
    public $webLinkService;

    public function __construct(CandidateService $candidateService, WebLinkService $webLinkService)
    {
        $this->candidateService = $candidateService;
        $this->webLinkService = $webLinkService;
    }

    public function postUploadCandidateImage(Request $request)
    {
        try {
            //Storage::disk('s3')->put('file.txt', 'Contents');
            $uploader = MediaUploader::fromSource($request->file('file'))
                ->useFilename(Str::uuid())
                ->setAllowedAggregateTypes(['image'])
                //->toDisk('s3')
                ->toDisk('public')
                ->toDirectory('canimg')
                ->upload();
        } catch (MediaUploadException $e) {
            throw $this->transformMediaUploadException($e);
        }

        //GUID to media package

        /** @var  Candidate */
        //$candidate = Candidate::find(2);
        //attach
        //$candidate->attachMedia($uploader, $request->input("field"));

        //Get
        //$media = $candidate->getMedia($request->input("field"));
        //$mediaAll = $candidate->getAllMediaByTag($request->input("field"));

        //detech
        //$candidate->detachMedia($request->input("field"));

        //$path = $file->store("canimg", "public");
        /*Log::info("test", $uploader->toArray());
        Log::info($uploader->getUrl());
        Log::info($media->toArray());
        Log::info($media->first()->getUrl());
        Log::info($mediaAll->toArray());*/

        return response()->json([
            'id' => $uploader->id,
            'fileName' => $uploader->filename,
            'url' => $uploader->getUrl()
        ]);
    }

    public function postUploadImage(Request $request, $xid)
    {
        $currentOrganization = $request->currentOrganization;
        $candidateId = $this->candidateService->getIdByXid("candidates", $xid);
        try {
            $uploader = MediaUploader::fromSource($request->file('file'))
                ->useFilename(Str::uuid())
                ->setAllowedAggregateTypes(['image'])
                ->toDisk('s3')
//                ->toDisk('public')
//                ->toDirectory('testciv')
                ->toDirectory('canimg')
                ->upload();
        } catch (MediaUploadException $e) {
            throw $this->transformMediaUploadException($e);
        }

        $mediaTag = $request->input("field");
        $candidate = Candidate::find($candidateId);
        //attach
        $candidate->attachMedia($uploader, $request->input("field"));

        // Update Candidate Current Status to in progress
        $candidate->current_status = 'In Progress';
        $candidate->status_date = Carbon::now()->toDateTimeString();

        switch ($mediaTag)
        {
            case "step_1_image":
                $candidate->step_1_image_changed = 1;
                break;
            case "step_2_image":
                $candidate->step_2_image_changed = 1;
                break;
            case "step_3_image":
                $candidate->step_3_image_changed = 1;
                break;
            default:
        }
        $candidate->save();

        $currentDateTime = Carbon::now()->toDateTimeString();
        ActionLog::query()->insert([
            'organization_id' => $currentOrganization->id,
            'candidate_id' => $candidateId,
            'service_type' => 'Upload',
            'media_id' => $uploader->id,
            'user_id' => auth()->user()->id,
            'media_type' => $request->input("field"),
            'created_at' => $currentDateTime,
            'updated_at' => $currentDateTime
        ]);

        return response()->json([
            'id' => $uploader->id,
            'fileName' => $uploader->filename,
            'url' => $uploader->getUrl()
        ]);
    }

    public function postUploadCandidateSelfieImage(Request $request, $linkToken)
    {
        $rules = [
            "file" => "required|mimes:jpeg,png",
        ];

        $this->validate($request, $rules);
        $webLink = $this->webLinkService->getDetailByLinkToken($linkToken);

        $path = $request->file('file')->store('public/tempimg');
        //UtilHelper::correctImageOrientation($filePath);
        $filePath = storage_path('app/'.$path);
        $image = Image::make($filePath);
        $image->orientate();
        $image->save();

        //Log::info($image->exif());
        //Log::info($path);
        //Log::info(Storage::disk('public')->has('tempimg/5hyY9wqYH99clGnEiocfTzVJDhJZdQdgBJTccsIA.jpeg'));
        //$importFromPath = MediaUploader::importPath('public', str_replace("public/", "", $path));

        try {
            //$uploader = MediaUploader::fromSource($request->file('file'))
            $uploader = MediaUploader::fromString($image->encode())
                ->useFilename(Str::uuid())
                ->setAllowedAggregateTypes(['image'])
                ->toDisk('s3')
                ->toDirectory('canimg')
                //->toDisk('public')
                ->upload();

            Storage::disk('public')->delete(str_replace("public/", "", $path));

        } catch (MediaUploadException $e) {
            throw $this->transformMediaUploadException($e);
        }
        //abort(400);
        return response()->json([
            'id' => $uploader->id,
            'fileName' => $uploader->filename,
            'url' => $uploader->getUrl()
        ]);
    }

    public function postRemoveCandidateImage(Request $request)
    {
        /** @var TempFile $tempFile */
        $tempFile = TempFile::query()
            ->where('guid', $request->input('uid'))
            ->firstOrFail();

        $tempFile->clearMediaCollection('temp_file');

        return response()->json([
            'message' => "Removed successfully.",
        ]);
    }
}
