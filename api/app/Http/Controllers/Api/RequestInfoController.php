<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Models\ActionLog;
use App\Models\Candidate;
use App\Models\Otp;
use App\Models\WebLink;
use App\Services\CandidateService;
use App\Services\OtpService;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Plank\Mediable\Media;

class RequestInfoController extends ApiBaseController
{

    public $candidateService;
    public $otpService;

    public function __construct(CandidateService $candidateService, OtpService $otpService)
    {
        $this->candidateService = $candidateService;
        $this->otpService = $otpService;
    }

    public function postCandidateSelfie1(Request $request)
    {
        $rules = [
            "vpto" => "required",
            "master_image" => "required",
            "first_name" => "required|max:30",
            "last_name" => "required|max:30",
            "address" => "required|max:255",
            "agree" => "required",
            "location" => "required"
        ];

        $validatedData = $this->validate($request, $rules);

        $otp = $this->otpService->getOtpDetailByAccessToken($validatedData["vpto"]);
        $otp->used_at = now();
        $otp->save();

        Otp::query()
            ->where("web_link_id", $otp->web_link_id)
            ->whereNull("used_at")
            ->delete();

        $webLink = WebLink::query()->findOrFail($otp->web_link_id);
        $webLink->used_at = now();
        $webLink->save();

        WebLink::query()
            ->where("candidate_id", $webLink->candidate_id)
            ->where("link_type", "candidate_selfie_1")
            ->whereNull("used_at")
            ->delete();

        $candidate = Candidate::query()->findOrFail($otp->candidate_id);
        $media = Media::query()->find($validatedData["master_image"]["id"]);
        $mediaTag = "master_image";
        $candidate->attachMedia($media, $mediaTag);
        // Update Candidate Current Status to in progress
        $candidate->current_status = 'In Progress';
        $candidate->status_date = \Carbon\Carbon::now()->toDateTimeString();
        $candidate->master_image_changed = 1;
        $candidate->first_name = $validatedData["first_name"];
        $candidate->last_name = $validatedData["last_name"];
        $candidate->address = $validatedData["address"];
        $candidate->location = json_encode($validatedData['location']);
        $candidate->save();

        $currentDateTime = now()->toDateTimeString();
        ActionLog::query()->insert([
            'organization_id' => $candidate->organization_id,
            'candidate_id' => $candidate->id,
            'service_type' => 'Upload',
            'media_id' => $media->id,
            'user_id' => null,
            'media_type' => $mediaTag,
            'location' => json_encode($validatedData['location']),
            'created_at' => $currentDateTime,
            'updated_at' => $currentDateTime
        ]);

        return response()->json(['data' => ['success' => true]]);
    }

    public function postCandidateSelfie2(Request $request)
    {
        $rules = [
            "vpto" => "required",
            "final_validation_image" => "required",
            "agree" => "required",
            "location" => "required"
        ];

        $validatedData = $this->validate($request, $rules);
        $otp = $this->otpService->getOtpDetailByAccessToken($validatedData["vpto"]);
        $otp->used_at = now();
        $otp->save();

        Otp::query()
            ->where("web_link_id", $otp->web_link_id)
            ->whereNull("used_at")
            ->delete();

        $webLink = WebLink::query()->findOrFail($otp->web_link_id);
        $webLink->used_at = now();
        $webLink->save();

        WebLink::query()
            ->where("candidate_id", $webLink->candidate_id)
            ->where("link_type", "candidate_selfie_2")
            ->whereNull("used_at")
            ->delete();

        $candidate = Candidate::query()->findOrFail($otp->candidate_id);
        $media = Media::query()->find($validatedData["final_validation_image"]["id"]);
        $mediaTag = "final_validation_image";
        $candidate->attachMedia($media, $mediaTag);
        // Update Candidate Current Status to in progress
        $candidate->current_status = 'In Progress';
        $candidate->status_date = \Carbon\Carbon::now()->toDateTimeString();
        $candidate->validation_image_changed = 1;
        $candidate->validation_image_location = json_encode($validatedData['location']);
        $candidate->save();

        $currentDateTime = now()->toDateTimeString();
        ActionLog::query()->insert([
            'organization_id' => $candidate->organization_id,
            'candidate_id' => $candidate->id,
            'service_type' => 'Upload',
            'media_id' => $media->id,
            'user_id' => null,
            'media_type' => $mediaTag,
            'location' => json_encode($validatedData['location']),
            'created_at' => $currentDateTime,
            'updated_at' => $currentDateTime
        ]);

        return response()->json(['data' => ['success' => true]]);
    }
}
