<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Models\Otp;
use App\Services\OtpService;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class OtpController extends ApiBaseController
{
    public $service;

    public $resource = OtpService::class;

    public function __construct(OtpService $otpService)
    {
        $this->service = $otpService;
    }

    public function postGenerate(Request $request)
    {
        $rules = [
            "link_token" => "required",
            "send_by" => ["required"],
            "send_by.send_sms" => "boolean",
            "send_by.send_email" => "boolean",
        ];

        $validatedData = $this->validate($request, $rules);
        $entity = $this->service->generate($validatedData);

        return response()->json([
            'success' => true,
        ]);
    }

    public function postValidate(Request $request)
    {
        $rules = [
            "otp_number" => "required",
            "link_token" => "required",
        ];

        $validatedData = $this->validate($request, $rules);

        $otp = $this->service->getAccessToken($validatedData);

        return response()->json([
            'access_token' => $otp->access_token
        ]);
    }

    public function postVerify(Request $request)
    {
        $rules = [
            "access_token" => "required",
        ];

        $validatedData = $this->validate($request, $rules);

        if (!$this->service->checkIsValidAccessToken($validatedData)) {
            throw ValidationException::withMessages([
                'message' => 'invalid request.'
            ]);
        }

        return response()->json(['success' => true]);
    }
}
