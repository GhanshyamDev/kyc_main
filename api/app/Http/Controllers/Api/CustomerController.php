<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CustomerResource;
use App\Services\CustomerService;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CustomerController extends Controller
{
    /**
     * CustomerController constructor.
     * @param CustomerService $customerService
     */
    public $service;

    public function __construct(CustomerService $customerService)
    {
        $this->service = $customerService;
    }

    public function getList(Request $request)
    {
        $currentOrganization = $request->currentOrganization;
        $input = $request->input();
        $input["organization_id"] = $currentOrganization->id;
        $entities = $this->service->getList($input);
        return CustomerResource::collection($entities);
    }

    public function postCreate(Request $request)
    {
        $currentOrganization = $request->currentOrganization;
        $validatedData = $this->validateInput($request);
        $validatedData["customer_xid"] = Str::uuid();
        $validatedData["organization_id"] = $currentOrganization->id;
        $entity = $this->service->create($validatedData);
        return new CustomerResource($entity);
    }

    public function postUpdate(Request $request)
    {
        $xid = $request->input('customer_xid');
        $id = $this->service->getIdByXid("customers", $xid);
        $validatedData = $this->validateInput($request, $id);
        $entity = $this->service->update($id, $validatedData);
        return new CustomerResource($entity);
    }

    public function getDetail(Request $request, $xid)
    {
        $id = $this->service->getIdByXid("customers", $xid);
        $entity = $this->service->getDetail($id);
        return new CustomerResource($entity);
    }

    /**
     * @param Request $request
     * @param null $id
     * @param boolean $id_required
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateInput(Request $request, $id = null) : array
    {
        $rules = [
            "first_name" => "required",
            "last_name" => "",
            "cid" => "required|unique:candidates,cid,${id},id,organization_id," . $request->currentOrganization->id,
            "phone_number" => [
                "required_without:email",
                "unique:candidates,phone_number,${id},id,organization_id," . $request->currentOrganization->id,
            ],
            "email" => [
                "required_without:phone_number",
                "unique:candidates,email,${id},id,organization_id," . $request->currentOrganization->id,
            ]
        ];

        if (!$id) {
            $addRules = [];

            $rules = array_merge($rules, $addRules);
        }

        return $this->validate($request, $rules);
    }
}
