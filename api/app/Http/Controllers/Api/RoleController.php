<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Http\Resources\RoleResource;
use App\Services\RoleService;
use Illuminate\Http\Request;

class RoleController extends ApiBaseController
{
    /**
     * @var RoleService
     */
    public $service;

    public $resource = RoleResource::class;

    public function __construct(RoleService $roleService)
    {
        $this->service = $roleService;
    }

    public function getList(Request $request)
    {
        $entities = $this->service->getList($request->all());
        return RoleResource::collection($entities);
    }

    public function postCreate(Request $request)
    {
        $validatedData = $this->validateInput($request);
        $entity = $this->service->create($validatedData);
        return new RoleResource($entity);
    }

    public function postUpdate(Request $request)
    {
        $xid = $request->input('role_xid');
        $id = $this->service->getIdByXid("roles", $xid);
        $validatedData = $this->validateInput($request, $id);
        $entity = $this->service->update($id, $validatedData);
        return new RoleResource($entity);
    }

    public function getDetail(Request $request, $xid)
    {
        $id = $this->service->getIdByXid("roles", $xid);
        $entity = $this->service->getDetail($id);
        return new RoleResource($entity);
    }

    public function postDelete(Request $request)
    {
        $xid = $request->input('role_xid');
        $id = $this->service->getIdByXid("roles", $xid);
        return response()->json(['success' => $this->service->delete($id)]);
    }

    /**
     * @param Request $request
     * @param null $id
     * @param boolean $id_required
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateInput(Request $request, $id = null): array
    {
        $rules = [
            "name" => "required|unique:roles,name,{$id}",
            "permissions" => "",
        ];

        return $this->validate($request, $rules);
    }
}
