<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CandidateResource;
use App\Services\ActionService;
use App\Services\CandidateService;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Plank\Mediable\Exceptions\MediaUploadException;
use Plank\Mediable\Facades\MediaUploader;

class CandidateController extends Controller
{
    /**
     * @var CandidateService
     */
    public $service;
    private $actionService;

    public function __construct(CandidateService $candidateService, ActionService $actionService)
    {
        $this->service = $candidateService;
        $this->actionService = $actionService;
    }

    public function getList(Request $request)
    {
        $currentOrganization = $request->currentOrganization;
        $input = $request->input();
        $input["organization_id"] = $currentOrganization->id;
        $entities = $this->service->getList($input);
        return CandidateResource::collection($entities);
    }

    public function postCreate(Request $request)
    {
        $currentOrganization = $request->currentOrganization;
        $validatedData = $this->validateInput($request);
        $validatedData["candidate_xid"] = Str::uuid();
        $validatedData["organization_id"] = $currentOrganization->id;
        $entity = $this->service->create($validatedData);
        return new CandidateResource($entity);
    }

    public function postUpdate(Request $request)
    {
        $xid = $request->input('candidate_xid');
        $id = $this->service->getIdByXid("candidates", $xid);
        $validatedData = $this->validateInput($request, $id);
        $entity = $this->service->update($id, $validatedData);
        return new CandidateResource($entity);
    }

    public function getDetail(Request $request, $xid)
    {
        $id = $this->service->getIdByXid("candidates", $xid);
        $entity = $this->service->getDetail($id);
        return new CandidateResource($entity);
    }

    public function getCandidateImages(Request $request, $xid)
    {
        $id = $this->service->getIdByXid("candidates", $xid);
        return $this->service->getCandidateImages($id);
    }

    public function postCompare(Request $request)
    {
        $this->validate($request, [
            'candidate_id' => 'required|exists:candidates,candidate_xid'
        ]);

        $xid = $request->input("candidate_id");
        $currentOrganization = $request->currentOrganization;
        $candidateId = $this->service->getIdByXid("candidates", $xid);
        return $this->service->compareImages($candidateId, $currentOrganization->compare_score_threshold);
    }

    public function postMarkCompleted(Request $request)
    {
        $this->validate($request, [
            'candidate_id' => 'required|exists:candidates,candidate_xid'
        ]);

        $xid = $request->input("candidate_id");
        $currentOrganization = $request->currentOrganization;
        $candidateId = $this->service->getIdByXid("candidates", $xid);
        if($this->service->markCompleted($candidateId)) {
            return response()->json(['msg' => 'success'], 200);
        } else {
            return response()->json(['msg' => 'Can\'t Mark as completed'], 422);
        }
    }

    public function postDelete(Request $request)
    {
        $this->validate($request, [
            'candidate_xid' => 'required|exists:candidates,candidate_xid'
        ], [
            'candidate_xid.required' => 'Candidate Id is required',
            'candidate_xid.exists' => 'Invalid Candidate Id'
            ]
        );

        $xid = $request->input('candidate_xid');
        $id = $this->service->getIdByXid("candidates", $xid);
        return response()->json(['success' => $this->service->delete($id)]);
    }

    public function postUploadCandidateImage(Request $request, $xid)
    {
        $id = $this->service->getIdByXid("candidates", $xid);
        try {
            $uploader = MediaUploader::fromSource($request->file('file'))
                ->useFilename(Str::uuid())
                ->setAllowedAggregateTypes(['image'])
                //->toDisk('s3')
                ->toDisk('public')
                ->toDirectory('canimg')
                ->upload();
        } catch (MediaUploadException $e) {
            throw $this->transformMediaUploadException($e);
        }

        $candidate = Candidate::find($id);
        //attach
        $candidate->attachMedia($uploader, $request->input("field"));

        return response()->json([
            'id' => $uploader->id,
            'fileName' => $uploader->filename,
            'url' => $uploader->getUrl()
        ]);
    }

    /**
     * @param Request $request
     * @param null $id
     * @param boolean $id_required
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateInput(Request $request, $id = null) : array
    {
        $rules = [
            "first_name" => "required",
            "last_name" => "",
            "cid" => "required|unique:candidates,cid,${id},id,organization_id," . $request->currentOrganization->id,
            "phone_number" => [
                "required_without:email",
                "unique:candidates,phone_number,${id},id,organization_id," . $request->currentOrganization->id . ",job_id," . $request->input('job_id'),
            ],
            "email" => [
                "required_without:phone_number",
                "unique:candidates,email,${id},id,organization_id," . $request->currentOrganization->id . ",job_id," . $request->input('job_id'),
            ],
            "address" => "",
            "designation" => "",
            "comment" => "",
            "job_id" => ""
        ];

        if (!$id) {
            $addRules = [];

            $rules = array_merge($rules, $addRules);
        }

        return $this->validate($request, $rules);
    }
}
