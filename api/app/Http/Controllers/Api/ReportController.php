<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ReportResource;
use App\Http\Resources\SummaryReportResource;
use App\Services\ReportService;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    /**
     * @var ReportService
     */
    private $service;

    public function __construct(ReportService $reportService)
    {
        $this->service = $reportService;
    }

    /**
     * @param Request $request
     */
    public function getCandidateReport(Request $request)
    {
        $currentOrganization = $request->currentOrganization;
        $input = $request->input();
        /*$subOrganizations = $currentOrganization->subOrganizations->pluck('id')->toArray();
        $input['organizations'] = array_merge([$currentOrganization->id], $subOrganizations);*/
        $input["organization_id"] = $currentOrganization->id;

        return ReportResource::collection($this->service->getCandidateReport($input));
    }

    public function getCandidateSummaryReport(Request $request)
    {
        $currentOrganization = $request->currentOrganization;
        $input = $request->input();
        $input["organization_id"] = $currentOrganization->id;
        return SummaryReportResource::collection($this->service->getCandidateSummaryReport($input));
    }
}
