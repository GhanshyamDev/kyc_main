<?php

namespace App\Http\Controllers\Api;

use App\Models\Timezone;
use App\Http\Controllers\Controller;
use App\Http\Resources\TimezoneResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GeneralController extends Controller
{

    public function __construct()
    {

    }

    public function getTimezones()
    {
        $timezones = Timezone::query()
            ->where('is_active', true)
            ->get();

        return TimezoneResource::collection($timezones);
    }

    public static function randomColorPart() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }
}
