<?php


namespace App\Http\Controllers;


use App\Services\OrganizationService;

class ApiBaseController extends Controller
{
    protected $userOrganizations;
    /**
     * @var OrganizationService
     */
    public $organizationService;

    public function __construct(OrganizationService $organizationService)
    {
        $this->organizationService = $organizationService;
    }

    public function currentUserOrganizations()
    {

    }
}
