<?php

namespace App\Http\Middleware;

use App\Models\Organization;
use Closure;
use Illuminate\Http\Request;

class OrganizationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($this->checkIfPathNotExcluded($request)) {
            return $next($request);
        }

        if ($request->header('oarsgf', false)) {
            $entity = Organization::query()->where('organization_xid', $request->header('oarsgf'))->first();

            //TODO: also check here weather user has the access of organization

            if ($entity) {
                $request = $request->merge(['current_organization_id' => $entity->id]);
                $request = $request->merge(['currentOrganization' => $entity]);
                return $next($request);
            }
        }

        return response()->json(['message' => 'oarsgf Missing.'], 401);
    }

    private function checkIfPathNotExcluded(Request $request)
    {
        $paths = [
            'api/file/upload-candidate-image',
            'api/file/remove-candidate-image',
            'api/request-info/*',
            'api/file/upload-candidate-selfie-image/*',
            'api/otp/*',
            'request-info/*',
            'api/web-link/verify',
            'api/web-link/detail/*',
            //'api/organization',
            'api/timezone',
            //'api/organization/*',
            'api/auth/*',
            'api/media/upload',
            'oauth/*',
            '/'
        ];

        foreach ($paths as $path) {
            if ($request->is($path)) {
                return true;
            }
        }

        return false;
    }
}
