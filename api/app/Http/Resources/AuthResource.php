<?php

namespace App\Http\Resources;

use Carbon\Carbon;

class AuthResource extends CrudResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $timezone = new TimezoneResource($this->whenLoaded('timezone'));
        return [
            'user' => [
                'user_xid' => $this->user_xid,
                //'is_organization_admin' => $this->is_organization_admin,
                'first_name' => $this->first_name,
                'last_name' => $this->last_name,
                'email' => $this->email,
                'phone_number' => $this->phone_number,
                'first_login' => $this->first_login,
                //'timezone_id' => $this->timezone_id,
                'timezone_name' => ($timezone) ? $timezone->name : null,
                'organizations' => OrganizationResource::collection((isset($this->allOrganizations) ? $this->allOrganizations : collect([]))),
                'roles' => RoleResource::collection($this->roles),
            ],
            $this->mergeWhen($this->tokenResult, function () {
                return [
                    'access_token' => $this->tokenResult->accessToken,
                    'token_type' => 'Bearer',
                    'expires_at' => Carbon::parse(
                        $this->tokenResult->token->expires_at
                    )->toDateTimeString(),
                ];
            })
        ];
    }
}
