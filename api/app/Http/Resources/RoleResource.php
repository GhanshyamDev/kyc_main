<?php

namespace App\Http\Resources;

class RoleResource extends CrudResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'role_xid' => $this->role_xid,
            'name' => $this->name,
            //'created_at' => $this->created_at,
            //'permissions' => PermissionResource::collection($this->permissions)
        ];
    }
}
