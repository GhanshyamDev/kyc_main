<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;
use Plank\Mediable\Media;
use Plank\Mediable\Mediable;

class RekognitionResultLogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $sourceFileName = ($this->sourceMedia) ? $this->sourceMedia->filename . "." . $this->sourceMedia->extension : '';
        $targetFileName = ($this->targetMedia) ? $this->targetMedia->filename . "." . $this->targetMedia->extension : '';
        $sourceImageUrl = ($this->sourceMedia) ? $this->sourceMedia->getUrl() : '';
        $targetImageUrl = ($this->targetMedia) ? $this->targetMedia->getUrl() : '';

        $mediaTag = DB::table('mediables')->where('media_id', $this->targetMedia->id)->first();


        return [
            'LogId' => $this->id,
            'sourceFileName' => $sourceFileName,
            'sourceFileUrl' => $sourceImageUrl,
            'sourceFileUri' => $this->base64_encode_image($sourceImageUrl, "jpg"),
            'targetFileName' => $targetFileName,
            'targetFileUrl' => $targetImageUrl,
            'targetFileUri' => $this->base64_encode_image($targetImageUrl, "jpg"),
            'imageType' => ($mediaTag) ? ucwords(str_replace("_", " ", $mediaTag->tag)) : '',
            'compareResult' => $this->rekognition_result,
            'compareDate' => $this->created_at,
            'status' => $this->status
        ];
    }

    function base64_encode_image($filename = '', $filetype = '')
    {
        $type = pathinfo($filename, PATHINFO_EXTENSION);
        $data = file_get_contents($filename);
        return 'data:image/' . $filetype . ';base64,' . base64_encode($data);
    }
}
