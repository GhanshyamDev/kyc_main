<?php

namespace App\Http\Resources;

class UserResource extends CrudResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'user_xid' => $this->user_xid,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'phone_number' => $this->phone_number,
            'timezone_id' => $this->timezone_id,
            'is_active' => $this->is_active,
            'first_login' => $this->first_login,
            'created_at' => $this->created_at,
            'name' => "{$this->first_name} {$this->last_name}",
            'timezone' => new TimezoneResource($this->whenLoaded('timezone')),
            'roles' => RoleResource::collection($this->whenLoaded('roles')),
            /*'role_id' => $this->whenLoaded('roles', function () {
                return ($this->roles->count() > 0) ? $this->roles->first()->guid : null;
            }),*/
        ];
    }
}
