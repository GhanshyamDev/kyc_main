<?php

namespace App\Http\Resources;

class TimezoneResource extends CrudResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'gmt_offset' => $this->gmt_offset,
        ];
    }
}
