<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'candidate_xid' => $this->candidate_xid,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'phone_number' => $this->phone_number,
            'cid' => $this->cid,
            'job_id' => $this->job_id,
            'address' => $this->address,
            'designation' => $this->designation,
            'comment' => $this->comment,
            'current_status' => $this->current_status,
            'status_date' => $this->status_date,
            'completed_status' => $this->completed_status,
            'organization_name' => ($this->organization) ? $this->organization->name : '',
            'organization_id' => $this->organization_id
        ];
    }
}
