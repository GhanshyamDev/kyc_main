<?php

namespace App\Http\Resources;


class OrganizationResource extends CrudResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'organization_xid' => $this->organization_xid,
            'organization_type' => $this->organization_type_id,
            //'parent_organization_xid' => $this->organization_xid,
            'name' => $this->name,
            'allow_send_sms' => $this->allow_send_sms,
            'allow_send_email' => $this->allow_send_email,
            'contact_person' => $this->contact_person,
            'phone_number' => $this->phone_number,
            'email' => $this->email,
            'level' => $this->level,
            'parent_organization_name' => optional($this->parentOrganization)->name,
            'is_active' => $this->is_active,
            //'is_admin' => $this->is_admin,
            'timezone_id' => $this->timezone_id,
            //'organization_type_id' => $this->organization_type_id,
            'timezone_name' => $this->timezone->name,
            //'timezone' => new TimezoneResource($this->timezone),
            /*'organization_user' => $this->whenPivotLoaded('organization_user_xrefs', function () {
                return [
                    "user_id" => $this->pivot->user_id,
                    "role_id" => $this->pivot->role_id,
                ];
            }),*/
            /*'organization_user' => $this->whenLoaded('organizationUser', function () {
                return [
                    "user_id" => $this->organizationUser->user_id,
                    "role_id" => $this->organizationUser->role_id,
                ];
            }),*/
        ];
    }
}
