<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SummaryReportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
//        return parent::toArray($request);
        return [
            'organizationName' => $this->name,
            'level' => $this->level,
            'suspeciousCount' => $this->suspeciousCandidates->count(),
            'verifiedCount' => $this->verifiedCandidates->count(),
            'totalCount' => $this->candidates->count()
        ];
    }
}
