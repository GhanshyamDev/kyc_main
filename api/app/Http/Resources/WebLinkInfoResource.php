<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WebLinkInfoResource extends CrudResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'first_name' => $this->candidate->first_name,
            'last_name' => $this->candidate->last_name,
            'designation' => $this->candidate->designation,
            'job_id' => $this->candidate->job_id,
            'email' => $this->hideEmail($this->candidate->email),
            'phone_number' => $this->hidePhoneNumber($this->candidate->phone_number),
            'address' => $this->candidate->address,
            'organization_name' => $this->organization->name,
            'allow_send_sms' => $this->organization->allow_send_sms,
            'allow_send_email' => $this->organization->allow_send_email,
            'link_type' => $this->link_type
        ];
    }

    function hidePhoneNumber($number)
    {
        return substr($number, 0, 2) . '******' . substr($number, -2);
    }

    function hideEmail($email)
    {
        //$split = explode("@", $email);
        return substr($email, 0, 3) . '******' . substr($email, -3);
    }


}
