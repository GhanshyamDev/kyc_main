<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group(['namespace' => 'App\Http\Controllers\Api'], function () {
    //Route::post('file/upload-candidate-image', 'FileController@postUploadCandidateImage');
    //Route::post('file/remove-candidate-image', 'FileController@postRemoveCandidateImage');
    Route::post('file/upload-candidate-selfie-image/{id}', 'FileController@postUploadCandidateSelfieImage');
    Route::post('web-link/verify', 'WebLinkController@postVerify');
    Route::get('web-link/detail/{token}', 'WebLinkController@getDetail');
    Route::post('otp/generate', 'OtpController@postGenerate');
    Route::post('otp/validate', 'OtpController@postValidate');
    Route::post('request-info/candidate-selfie1', 'RequestInfoController@postCandidateSelfie1');
    Route::post('request-info/candidate-selfie2', 'RequestInfoController@postCandidateSelfie2');

    Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
        Route::post('sign/in', 'AuthController@postSignIn');
        Route::post('sign/up', 'AuthController@postSignUp');
        Route::post('password/forgot', 'AuthController@postForgotPassword');
        Route::post('password/reset', 'AuthController@postResetPassword');
        Route::post('email/verify/{token}', 'AuthController@postVerification');
        Route::post('email/resend', 'AuthController@postResendVerification');

        Route::group(['middleware' => 'auth:api'], function () {
            Route::get('logout', 'AuthController@getLogout');
            Route::get('user', 'AuthController@getUser');
        });
    });

    Route::group(['middleware' => ['auth:api']], function () { //, 'organizationAdmin'
        Route::post('file/upload-image/{id}', 'FileController@postUploadImage');
        Route::get('timezones', 'GeneralController@getTimezones');
        Route::get('permissions', 'GeneralController@getPermissions');

        Route::group(['prefix' => 'user'], function () {
            Route::get('list', 'UserController@getList');
            Route::get('detail/{id}', 'UserController@getDetail');
            Route::post('create', 'UserController@postCreate');
            Route::post('update', 'UserController@postUpdate');
            Route::post('delete', 'UserController@postDelete');
            Route::post('updateProfile', 'UserController@postUpdateProfile');
            Route::post('change-password', 'UserController@postChangePassword');
        });

        Route::group(['prefix' => 'role'], function () {
            Route::get('list', 'RoleController@getList');
            /*Route::get('detail/{id}', 'RoleController@getDetailByGuid');
            Route::post('create', 'RoleController@postCreate');
            Route::post('update', 'RoleController@postUpdateByGuid');
            Route::post('delete', 'RoleController@postDeleteByGuid');*/
        });

        Route::group(['prefix' => 'organization'], function () {
            Route::get('list', 'OrganizationController@getList');
            Route::get('detail/{id}', 'OrganizationController@getDetail');
            Route::post('create', 'OrganizationController@postCreate');
            Route::post('update', 'OrganizationController@postUpdate');
            Route::post('delete', 'OrganizationController@postDelete');
            //Route::get('my-organizations', 'OrganizationController@getMyOrganizationsList');
            Route::post('profile', 'OrganizationController@postProfile');
        });

        Route::group(['prefix' => 'candidate'], function() {
            Route::get('list', 'CandidateController@getList');
            Route::get('detail/{id}', 'CandidateController@getDetail');
            Route::get('images/{id}', 'CandidateController@getCandidateImages');
            Route::post('create', 'CandidateController@postCreate');
            Route::post('update', 'CandidateController@postUpdate');
            Route::post('delete', 'CandidateController@postDelete');
            Route::post('compare', 'CandidateController@postCompare');
            Route::post('mark-completed', 'CandidateController@postMarkCompleted');
            Route::get('rekognition-result/{id}', 'RekognitionResultLogController@getRekognitionResultByCandidateId');
        });

        Route::group(['prefix' => 'customer'], function(){
            Route::get('list', 'CustomerController@getList');
            Route::get('detail/{id}', 'CustomerController@getDetail');
            Route::post('create', 'CustomerController@postCreate');
            Route::post('update', 'CustomerController@postUpdate');
            Route::post('delete', 'CustomerController@postDelete');
        });

        Route::group(['prefix' => 'report'], function() {
            Route::post('candidate-report', 'ReportController@getCandidateReport');
            Route::post('candidate-summary-report', 'ReportController@getCandidateSummaryReport');
        });

        Route::group(['prefix' => 'web-link'], function() {
            Route::post('send', 'WebLinkController@postSend');
        });

        Route::group(['prefix' => 'media/upload'], function(){
            Route::post('/', 'GeneralController@postUpload');
        });
    });
});
