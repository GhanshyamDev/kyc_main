import {LocaleObject} from "yup";

export const validation: LocaleObject = {
    string: {
        matches: 'Invalid input',
        email: 'Invalid email',
    },
    mixed: {
        required: 'This field is required',
    },
    date: {

    },
    number: {
        min: 'Value must minimum be ${min}',
    }
};
