export const message = {
    organizationChanged:'Organization changed successfully.',
    profileUpdated:'Info Updated successfully.',
    confirmDelete:'Do you want to delete?',
    recordAdded:'Record added successfully.',
    routeSaved:'Route saved successfully.',
    recordAddedUpdated:'Record added/updated successfully.',
    recordEdit:'Record edited successfully.',
    webLinkSent:'Web link has been sent.',
    webLinkEmailSent:'An email with web link has been sent to {email}',
    webLinkSMSSent:'An SMS with web link has been sent to {phone_number}',
    recordDeleted:'Record deleted successfully.',
    recordAddEditError:'Cannot add or update record try later.',
    recordDeleteError:'Cannot delete record try Later.',
    recordCopied:'Record copied successfully.',
    endGreaterThanStartDate:'End date should be greater than start date.',
    toDaysGreaterThanFromDays:'To days should be greater than start date.',
    acknowledgedSuccess:'Acknowledged successfully.',
    changeUserOrganization:'User organization changed successfully.',
    messageSent:'Message added successfully.',
    confirmAcknowledge: 'Do you want to Acknowledge?',
    devicePing: 'Record device ping successfully.',
    toGreaterThanFromDate:'This must be greater than From Date.',
    passwordChanged: 'Password has been changed successfully.'
};
export function getAddOrUpdateMessage(id:any){
    return (id)?message.recordEdit:message.recordAdded;
}
