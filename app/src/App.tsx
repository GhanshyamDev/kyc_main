import React from 'react';
import store from "./store/AppStore";
import {Provider} from "react-redux";
import AppContainer from "./components/layouts/AppContainer";

import './assets/scss/style.scss';

function App() {
    return (
        <Provider store={store}>
            <AppContainer />
        </Provider>
    );
}

export default App;
