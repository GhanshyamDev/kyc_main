import React, {Component} from 'react';
import {Link, RouteComponentProps, withRouter} from "react-router-dom";
import {Dispatch} from "redux";
import {IRootState} from "../../store/AppStore";
import {connect} from "react-redux";
import {CollapseNav} from "../shared/CollapseNav";
import {auth} from "../../util/Auth";
import {OrganizationType} from "../../util/Enums";

const mapStateToProps = ({ ui, page }: IRootState, ownProps: any) => {
    return { ui, page, ownProps };
};

const mapDispatcherToProps = (dispatch: Dispatch) => {
    return {
        dispatch,
    }
};

type ReduxType = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatcherToProps>;

interface IProps extends ReduxType {
    isShrinked?: boolean
}

type PropsType = RouteComponentProps & IProps;
const currentOrganization = auth.getCurrentOrganization();

class SideNav extends Component<PropsType, {}> {
    static defaultProps = {
        isShrinked: false
    };

    componentDidMount(): void {
        const body = document.querySelector('body');
        document.querySelectorAll('.sidebar .nav-item').forEach((el) => {

            el.addEventListener('mouseover', function() {
                if(body && body.classList.contains('sidebar-icon-only')) {
                    el.classList.add('hover-open');
                }
            });
            el.addEventListener('mouseout', function() {
                if(body && body.classList.contains('sidebar-icon-only')) {
                    el.classList.remove('hover-open');
                }
            });
        });
    }

    activeRoute(routeNames: Array<string>, activeClass: any = 'active'): string {
        let hasAnyRoute = false;

        routeNames.forEach((obj: any) => {
            if ([this.props.location.pathname].indexOf(obj) >= 0) {
                hasAnyRoute = true;
                return;
            }
        });

        return (hasAnyRoute) ? activeClass : '';
    }

    render() {
        const shrinked = (this.props.isShrinked) ? 'navbar-collapsed' : '';

        return (
            <>
                <nav className="sidebar sidebar-offcanvas" id="sidebar">
                    <ul className="nav">
                        <li className={`nav-item ${this.activeRoute(['/dashboard'])}`}>
                            <Link className={`nav-link ${this.activeRoute(['/dashboard'])}`} to="/dashboard">
                                <span className="menu-title">Dashboard</span>
                                <i className="mdi mdi-home menu-icon"/>
                            </Link>
                        </li>
                        {(currentOrganization?.organization_type === OrganizationType.SuperAdmin
                        || currentOrganization?.organization_type === OrganizationType.Company) &&
                        <li className={`nav-item ${this.activeRoute(['/organization/list'])}`}>
                            <Link className={`nav-link`} to="/organization/list">
                                <span className="menu-title">Organization</span>
                                <i className="mdi mdi-domain menu-icon"/>
                            </Link>
                        </li>
                        }

                        <li className={`nav-item ${this.activeRoute(['/user/list'])}`}>
                            <Link className={`nav-link ${this.activeRoute(['/user/list'])}`} to="/user/list">
                                <span className="menu-title">User</span>
                                <i className="mdi mdi-account menu-icon"/>
                            </Link>
                        </li>
                        <li className={`nav-item ${this.activeRoute(['/candidate/list'])}`}>
                            <Link className={`nav-link ${this.activeRoute(['/candidate/list'])}`} to="/candidate/list">
                                <span className="menu-title">Candidate</span>
                                <i className="mdi mdi-account menu-icon"/>
                            </Link>
                        </li>
                        <li className={`nav-item ${this.activeRoute(['/customer/list'])}`}>
                            <Link className={`nav-link ${this.activeRoute(['/customer/list'])}`} to="/customer/list">
                                <span className="menu-title">Customer</span>
                                <i className="mdi mdi-account menu-icon"/>
                            </Link>
                        </li>
                        <CollapseNav
                            title="Reports"
                            icon="mdi mdi-chart-bar"
                        >
                            <ul className="nav flex-column sub-menu">
                                <li className="nav-item"> <Link className={`nav-link ${this.activeRoute(['/report/candidate'])}`} to="/report/candidate">Candidate Report</Link></li>
                                <li className="nav-item"> <Link className={`nav-link ${this.activeRoute(['/report/candidate-summary-report'])}`} to="/report/candidate-summary-report">Summary Report</Link></li>
                            </ul>
                        </CollapseNav>
                    </ul>
                </nav>
            </>
        );
    }
}

export default withRouter(connect(mapStateToProps, mapDispatcherToProps)(SideNav));
