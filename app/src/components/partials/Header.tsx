import React, {useState} from 'react';
import {auth} from "../../util/Auth";
import {DropdownItem, DropdownMenu, DropdownToggle, NavItem, NavLink, UncontrolledDropdown} from "reactstrap";
import AppConstants from "../../util/AppConstants";
import {Link, useLocation, useHistory} from "react-router-dom";
import ModalWrp from "../shared/ModalWrp";
import {OrganizationChangeForm} from "../pageComponents/organization/OrganizationChangeForm";

interface IProps {

}

export default function Header(props: IProps) {
    const location = useLocation();
    const history = useHistory();
    const [showChangeOrganization, setShowChangeOrganization] = useState(false);
    const currentOrganization = auth.getCurrentOrganization();

    const toggleOffcanvas = () => {
        // @ts-ignore
        document.querySelector('.sidebar-offcanvas').classList.toggle('active');
    };

    const toggleRightSidebar = () => {
        // @ts-ignore
        document.querySelector('.right-sidebar').classList.toggle('open');
    };

    const activeRoute = (routeNames: Array<string>, activeClass: any = 'active') => {
        let hasAnyRoute = false;

        routeNames.forEach((obj: any) => {
            if ([location.pathname].indexOf(obj) >= 0) {
                hasAnyRoute = true;
                return;
            }
        });

        return (hasAnyRoute) ? activeClass : '';
    };

    const logout = () => {
        auth.logout();
        history.push({
            pathname: '/login',
        });
        window.location.reload();
    };

    return (
        <nav className="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
            <div className="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
                <Link className="navbar-brand brand-logo" to="/"><img src={AppConstants.IMAGES.LOGO} alt="logo" /></Link>
                <Link className="navbar-brand brand-logo-mini" to="/"><img src={AppConstants.IMAGES.LOGOMINI} alt="logo" /></Link>
            </div>
            <div className="navbar-menu-wrapper d-flex align-items-stretch right-header-menu">
                <button className="navbar-toggler navbar-toggler align-self-center" type="button" onClick={ () => document.body.classList.toggle('sidebar-icon-only') }>
                    <span className="mdi mdi-menu"/>
                </button>
                {/*<div className="search-field d-none d-md-block">
                    <form className="d-flex align-items-center h-100" action="#">
                        <div className="input-group">
                            <div className="input-group-prepend bg-transparent">
                                <i className="input-group-text border-0 mdi mdi-magnify"/>
                            </div>
                            <input type="text" className="form-control bg-transparent border-0" placeholder="Search projects"/>
                        </div>
                    </form>
                </div>*/}
                <ul className="navbar-nav navbar-nav-right">
                    <NavItem>
                        <NavLink href="#" onClick={() => setShowChangeOrganization(true)} className="nav-link">
                            {
                                (currentOrganization &&
                                    <span className='text-gray-600 d-inline-block header-states text-truncate'>{currentOrganization.name}</span>
                                )
                            }&emsp;
                            <i className="fas fa-cogs"/>
                        </NavLink>
                    </NavItem>
                    <UncontrolledDropdown nav inNavbar>
                        <div className="dropdown">
                            <DropdownToggle nav tag="button" caret>
                                <div className="nav-profile-text"><p className="mb-1 text-black">{auth.getFirstName()}</p></div>
                            </DropdownToggle>
                            <DropdownMenu right className="navbar-dropdown">
                                <DropdownItem tag="a" href="!#" className={activeRoute(['/profile'])} onClick={(evt) => {
                                    evt.preventDefault();history.push({pathname: '/profile'});
                                }}>
                                    {/*<Link className={`dropdown-item ${activeRoute(['/profile'])}`} to="/profile">*/}
                                    <i className="mdi mdi-account"/>{' '}
                                    <span>My Account</span>
                                    {/*</Link>*/}
                                </DropdownItem>
                                <DropdownItem tag="a" href="!#" onClick={evt => {
                                    evt.preventDefault(); logout();
                                }}>
                                    <i className="mdi mdi-logout mr-2"/> Signout
                                </DropdownItem>
                            </DropdownMenu>
                        </div>

                       {/* <Dropdown alignRight>
                            <Dropdown.Toggle className="nav-link">
                                <div className="nav-profile-img">
                                    <img src={require("../../assets/images/faces/face1.jpg")} alt="user"/>
                                    <span className="availability-status online"></span>
                                </div>
                                <div className="nav-profile-text">
                                    <p className="mb-1 text-black"><Trans>David Greymaax</Trans></p>
                                </div>
                            </Dropdown.Toggle>

                            <Dropdown.Menu className="navbar-dropdown">
                                <Dropdown.Item href="!#" onClick={evt =>evt.preventDefault()}>
                                    <i className="mdi mdi-cached mr-2 text-success"></i>
                                    <Trans>Activity Log</Trans>
                                </Dropdown.Item>
                                <Dropdown.Item href="!#" onClick={evt =>evt.preventDefault()}>
                                    <i className="mdi mdi-logout mr-2 text-primary"></i>
                                    <Trans>Signout</Trans>
                                </Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>*/}
                    </UncontrolledDropdown>
                    {/*<li className="nav-item">
                        <Dropdown alignRight>
                            <Dropdown.Toggle className="nav-link count-indicator">
                                <i className="mdi mdi-email-outline"></i>
                                <span className="count-symbol bg-warning"></span>
                            </Dropdown.Toggle>

                            <Dropdown.Menu className="preview-list navbar-dropdown">
                                <h6 className="p-3 mb-0"><Trans>Messages</Trans></h6>
                                <div className="dropdown-divider"></div>
                                <Dropdown.Item className="dropdown-item preview-item" onClick={evt =>evt.preventDefault()}>
                                    <div className="preview-thumbnail">
                                        <img src={require("../../assets/images/faces/face4.jpg")} alt="user" className="profile-pic"/>
                                    </div>
                                    <div className="preview-item-content d-flex align-items-start flex-column justify-content-center">
                                        <h6 className="preview-subject ellipsis mb-1 font-weight-normal"><Trans>Mark send you a message</Trans></h6>
                                        <p className="text-gray mb-0">
                                            1 <Trans>Minutes ago</Trans>
                                        </p>
                                    </div>
                                </Dropdown.Item>
                                <div className="dropdown-divider"></div>
                                <Dropdown.Item className="dropdown-item preview-item" onClick={evt =>evt.preventDefault()}>
                                    <div className="preview-thumbnail">
                                        <img src={require("../../assets/images/faces/face2.jpg")} alt="user" className="profile-pic"/>
                                    </div>
                                    <div className="preview-item-content d-flex align-items-start flex-column justify-content-center">
                                        <h6 className="preview-subject ellipsis mb-1 font-weight-normal"><Trans>Cregh send you a message</Trans></h6>
                                        <p className="text-gray mb-0">
                                            15 <Trans>Minutes ago</Trans>
                                        </p>
                                    </div>
                                </Dropdown.Item>
                                <div className="dropdown-divider"></div>
                                <Dropdown.Item className="dropdown-item preview-item" onClick={evt =>evt.preventDefault()}>
                                    <div className="preview-thumbnail">
                                        <img src={require("../../assets/images/faces/face3.jpg")} alt="user" className="profile-pic"/>
                                    </div>
                                    <div className="preview-item-content d-flex align-items-start flex-column justify-content-center">
                                        <h6 className="preview-subject ellipsis mb-1 font-weight-normal"><Trans>Profile picture updated</Trans></h6>
                                        <p className="text-gray mb-0">
                                            18 <Trans>Minutes ago</Trans>
                                        </p>
                                    </div>
                                </Dropdown.Item>
                                <div className="dropdown-divider"></div>
                                <h6 className="p-3 mb-0 text-center cursor-pointer">4 <Trans>new messages</Trans></h6>
                            </Dropdown.Menu>
                        </Dropdown>
                    </li>
                    <li className="nav-item">
                        <Dropdown alignRight>
                            <Dropdown.Toggle className="nav-link count-indicator">
                                <i className="mdi mdi-bell-outline"></i>
                                <span className="count-symbol bg-danger"></span>
                            </Dropdown.Toggle>
                            <Dropdown.Menu className="dropdown-menu navbar-dropdown preview-list">
                                <h6 className="p-3 mb-0"><Trans>Notifications</Trans></h6>
                                <div className="dropdown-divider"></div>
                                <Dropdown.Item className="dropdown-item preview-item" onClick={evt =>evt.preventDefault()}>
                                    <div className="preview-thumbnail">
                                        <div className="preview-icon bg-success">
                                            <i className="mdi mdi-calendar"></i>
                                        </div>
                                    </div>
                                    <div className="preview-item-content d-flex align-items-start flex-column justify-content-center">
                                        <h6 className="preview-subject font-weight-normal mb-1"><Trans>Event today</Trans></h6>
                                        <p className="text-gray ellipsis mb-0">
                                            <Trans>Just a reminder that you have an event today</Trans>
                                        </p>
                                    </div>
                                </Dropdown.Item>
                                <div className="dropdown-divider"></div>
                                <Dropdown.Item className="dropdown-item preview-item" onClick={evt =>evt.preventDefault()}>
                                    <div className="preview-thumbnail">
                                        <div className="preview-icon bg-warning">
                                            <i className="mdi mdi-settings"></i>
                                        </div>
                                    </div>
                                    <div className="preview-item-content d-flex align-items-start flex-column justify-content-center">
                                        <h6 className="preview-subject font-weight-normal mb-1"><Trans>Settings</Trans></h6>
                                        <p className="text-gray ellipsis mb-0">
                                            <Trans>Update dashboard</Trans>
                                        </p>
                                    </div>
                                </Dropdown.Item>
                                <div className="dropdown-divider"></div>
                                <Dropdown.Item className="dropdown-item preview-item" onClick={evt =>evt.preventDefault()}>
                                    <div className="preview-thumbnail">
                                        <div className="preview-icon bg-info">
                                            <i className="mdi mdi-link-variant"></i>
                                        </div>
                                    </div>
                                    <div className="preview-item-content d-flex align-items-start flex-column justify-content-center">
                                        <h6 className="preview-subject font-weight-normal mb-1"><Trans>Launch Admin</Trans></h6>
                                        <p className="text-gray ellipsis mb-0">
                                            <Trans>New admin wow</Trans>!
                                        </p>
                                    </div>
                                </Dropdown.Item>
                                <div className="dropdown-divider"></div>
                                <h6 className="p-3 mb-0 text-center cursor-pointer"><Trans>See all notifications</Trans></h6>
                            </Dropdown.Menu>
                        </Dropdown>
                    </li>*/}
                    {/*<li className="nav-item nav-logout d-none d-lg-block">
                        <a className="nav-link" href="!#" onClick={event => event.preventDefault()}>
                            <i className="mdi mdi-power"></i>
                        </a>
                    </li>*/}
                    {/*<li className="nav-item nav-settings d-none d-lg-block">
                        <button type="button" className="nav-link border-0" onClick={this.toggleRightSidebar} >
                            <i className="mdi mdi-format-line-spacing"></i>
                        </button>
                    </li>*/}
                </ul>
                <button className="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" onClick={toggleOffcanvas}>
                    <span className="mdi mdi-menu"/>
                </button>
                <ModalWrp title="Change Organization" showModal={showChangeOrganization} onCancel={() => setShowChangeOrganization(false)}>
                    <OrganizationChangeForm onCancel={() => setShowChangeOrganization(false)}/>
                </ModalWrp>
            </div>
            {/*<header className="navbar navbar-expand-lg  headerpos-fixed">
            <nav className="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
                <a className="navbar-brand mr-auto mr-lg-0" href="#"><img id="main-logo" src={AppConstants.IMAGES.LOGO} alt="" width="120" className="logo"/></a>
                <button className="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
                    <ul className="navbar-nav mr-auto">
                        <NavItem>
                            <Link className={`nav-link ${activeRoute(['/dashboard'])}`} to="/dashboard">
                                <i className="fas fa-tachometer-alt" />{' '}
                                <span>Dashboard</span>
                            </Link>
                        </NavItem>
                        <UncontrolledDropdown nav inNavbar>
                            <DropdownToggle nav caret>
                                Master
                            </DropdownToggle>
                            <DropdownMenu>
                                <DropdownItem tag="div">
                                    <Link className={`dropdown-item ${activeRoute(['/organization'])}`} to="/organization">
                                        <i className="fas fa-sitemap" />{' '}
                                        <span>Organization</span>
                                    </Link>
                                </DropdownItem>
                                <DropdownItem tag="div">
                                    <Link className={`dropdown-item ${activeRoute(['/user'])}`} to="/user">
                                        <i className="fas fa-user" />{' '}
                                        <span>Users</span>
                                    </Link>
                                </DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </ul>
                    <ul className="navbar-nav ml-auto">
                        <NavItem>
                            <NavLink href="#" onClick={() => setShowChangeOrganization(true)} className="nav-link">
                                {
                                    (currentOrganization &&
                                        <span className='text-gray-600 header-states'>{currentOrganization.organization_name}</span>
                                    )
                                }&emsp;
                                <i className="fas fa-cogs"/>
                            </NavLink>
                        </NavItem>
                        <UncontrolledDropdown nav>
                            <DropdownToggle nav>
                                <i className="fas fa-user"/>
                            </DropdownToggle>
                            <DropdownMenu>
                                <DropdownItem tag="div">
                                    <Link className={`dropdown-item ${activeRoute(['/profile'])}`} to="/profile">
                                        <i className="fas fa-user"/>{' '}
                                        <span>My Account</span>
                                    </Link>
                                </DropdownItem>
                                <button className="dropdown-item" onClick={() => logout()}><i className="fas fa-sign-out-alt"/> Logout</button>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </ul>
                </div>
            </nav>
            <ModalWrp title="Change Organization" showModal={showChangeOrganization} onCancel={() => setShowChangeOrganization(false)}>
                <OrganizationChangeForm onCancel={() => setShowChangeOrganization(false)}/>
            </ModalWrp>
        </header>*/}
        </nav>
    );
}
