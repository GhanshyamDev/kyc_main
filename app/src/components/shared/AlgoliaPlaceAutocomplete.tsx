import React from 'react';

/* eslint-disable global-require */
const Places = (typeof window !== 'undefined') && require('places.js');

interface IProps {
    onChange?: any,
    options: any,
    placeholder?: string
}

interface IState {
    isLoading: boolean,
    disabled: boolean
}

export class AlgoliaPlaceAutocomplete extends React.Component<IProps, IState> {
    autocomplete: any;
    autocompleteElem: any;
    constructor(props: any) {
        super(props);
        this.state = {
            isLoading: false,
            disabled: false
        };
    }

    componentDidMount() {
        if (!Places) return;

        this.autocomplete = Places({
            ...this.props.options,
            container: this.autocompleteElem,
        });

        if (this.props.onChange) {
            this.autocomplete.on("change", this.props.onChange);
        }
    }

    componentWillUnmount(): void {
        if (this.autocomplete) {
            this.autocomplete.removeAllListeners("onChange");
        }
    }

    render() {
        const {
            onChange,
            options,
            ...inputProps
        } = this.props;

        return (
            <div>
                <input
                    type="text"
                    aria-label={this.props.placeholder}
                    ref={(ref) => { this.autocompleteElem = ref; }}
                    {...inputProps}
                />
            </div>
        );
    }
}