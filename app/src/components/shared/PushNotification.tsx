import React, {Component} from 'react';

const PERMISSION_GRANTED = 'granted';
const PERMISSION_DENIED = 'denied';

const seqGen = () => {
    let i = 0;
    return () => {
        return i++;
    };
};
const seq = seqGen();

interface INotificationProps {
    ignore: boolean,
    disableActiveWindow: boolean,
    askAgain: boolean,
    notSupported: any,
    onPermissionGranted: any,
    onPermissionDenied: any,
    onShow: any,
    onClick: any,
    onClose: any,
    onError: any,
    timeout: number,
    title: string,
    options: any,
    swRegistration: any,
}

class PushNotification extends Component<INotificationProps, any> {
    static defaultProps = {
        ignore: false,
        disableActiveWindow: false,
        askAgain: false,
        timeout: 5000,
        options: {},
        swRegistration: null,
    };
    notifications: any = {};
    windowFocus: any  = true;
    onWindowFocus: any  = this._onWindowFocus.bind(this);
    onWindowBlur: any  = this._onWindowBlur.bind(this);

    constructor(props: any) {
        super(props);

        let supported = false;
        let granted = false;
        if (('Notification' in window) && Notification) {
            supported = true;
            if (Notification.permission === PERMISSION_GRANTED) {
                granted = true;
            }
        }

        this.state = {
            supported: supported,
            granted: granted
        };
    }

    _onWindowFocus(){
        this.windowFocus = true;
    }

    _onWindowBlur(){
        this.windowFocus = false;
    }

    _askPermission(){
        Notification.requestPermission((permission) => {
            let result = permission === PERMISSION_GRANTED;
            this.setState({
                granted: result
            }, () => {
                if (result) {
                    this.props.onPermissionGranted();
                } else {
                    this.props.onPermissionDenied();
                }
            });
        });
    }

    componentDidMount(){
        if (this.props.disableActiveWindow) {
            window.addEventListener('focus', this.onWindowFocus);
            window.addEventListener('blur', this.onWindowBlur);
        }

        if (!this.state.supported) {
            this.props.notSupported();
        } else if (this.state.granted) {
            this.props.onPermissionGranted();
        } else {
            if (Notification.permission === PERMISSION_DENIED){
                if (this.props.askAgain){
                    this._askPermission();
                } else {
                    this.props.onPermissionDenied();
                }
            } else {
                this._askPermission();
            }
        }
    }

    componentWillUnmount(){
        if (this.props.disableActiveWindow) {
            window.removeEventListener('focus', this.onWindowFocus);
            window.removeEventListener('blur', this.onWindowBlur);
        }
    }



    render() {
        let doNotShowOnActiveWindow = this.props.disableActiveWindow && this.windowFocus;
        if (!this.props.ignore && this.props.title && this.state.supported && this.state.granted && !doNotShowOnActiveWindow) {

            let opt = this.props.options;
            if (typeof opt.tag !== 'string') {
                opt.tag = 'web-notification-' + seq();
            }

            if (!this.notifications[opt.tag]) {
                if (this.props.swRegistration && this.props.swRegistration.showNotification) {
                    this.props.swRegistration.showNotification(this.props.title, opt);
                    this.notifications[opt.tag] = {};
                } else {
                    const n = new Notification(this.props.title, opt);
                    n.onshow = e => {
                        this.props.onShow(e, opt.tag);
                        setTimeout(() => {
                            this.close(n);
                        }, this.props.timeout);
                    };
                    n.onclick = e => { this.props.onClick(e, opt.tag); };
                    n.onclose = e => { this.props.onClose(e, opt.tag); };
                    n.onerror = e => { this.props.onError(e, opt.tag); };

                    this.notifications[opt.tag] = n;
                }
            }
        }

        // return null cause
        // Error: Invariant Violation: Notification.render(): A valid ReactComponent must be returned. You may have returned undefined, an array or some other invalid object.
        return (
            <input type='hidden' name='dummy-for-react-web-notification' style={{display: 'none'}} />
        );
    }

    close(n: any) {
        if (n && typeof n.close === 'function') {
            n.close();
        }
    }

    // for debug
    _getNotificationInstance(tag: any) {
        return this.notifications[tag];
    }
}

export default PushNotification;