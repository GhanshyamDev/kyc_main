import React, {Component, ReactNode} from 'react';
import {StoneDrawer} from "./StoneDrawer";

interface IProps {
    title: string,
    width: string,
    showDrawer: boolean
    children: ReactNode,
    onSubmit?: any,
    onClose?: any
}

class FormDrawerWrp extends Component<IProps> {

    onClose = () => {
        this.props.onClose();
    };

    render() {
        const {title, width, showDrawer} = this.props;
        return (
            <StoneDrawer
                title={title}
                width={width}
                onClose={this.onClose}
                showDrawer={showDrawer}
            >
                {this.props.children}
            </StoneDrawer>
        );
    }
}

export default FormDrawerWrp;