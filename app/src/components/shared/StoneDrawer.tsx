import React, {Component, ReactNode} from 'react';
import ReactDOM from 'react-dom';

interface IProps {
    title?: string,
    width: string,
    showDrawer?: boolean
    children: ReactNode,
    onSubmit?: any,
    onClose?: any
}

export class StoneDrawer extends Component<IProps> {
    el: any = document.createElement('div');

    componentDidMount() {
        if (this.props.showDrawer) {
            document.body.appendChild(this.el);
            this.el.style.display = 'contents';
            document.body.classList.add("overflow-hidden");
        }
    }

    componentWillUnmount() {
        if (this.props.showDrawer) {
            document.body.removeChild(this.el);
            document.body.classList.remove("overflow-hidden");
        }
    }

    getDrawerHtml() {
        const {showDrawer, width, title, children} = this.props;

        if (!showDrawer) {
            return <></>;
        }

        return (
            <>
                <div className="stone-drawer stone-drawer-right" style={{width: `${width}`}}>
                    <div className="stone-drawer-mask"/>
                    <div className="stone-drawer-content-wrapper">
                        <div className="stone-drawer-content">
                            {title &&
                            <div className="stone-drawer-header clearfix">
                                <div className="float-left">
                                    <h4 className="mb-0">{title}</h4>
                                </div>
                                <div className="float-right">
                                <span className="cursor-pointer" onClick={() => this.props.onClose()}><i
                                    className="fas fa-times"/></span>
                                </div>
                            </div>
                            }
                            <div className="stone-drawer-body">
                                {children}
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }

    render() {
        return ReactDOM.createPortal(
            this.getDrawerHtml(),
            this.el,
        );
    }
}