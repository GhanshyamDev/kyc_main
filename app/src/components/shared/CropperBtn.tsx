import React, {useState} from "react";
import {Button} from "reactstrap";
import ModalWrp from "./ModalWrp";
import {ReactCropperWrp} from "./ReactCropperWrp";

interface IProps{
    formTitle?:string,
    width:number,
    height:number,
    src:string,
    onCrop:any,
}
export function CropperBtn(props:IProps){
    const title = props.formTitle ? props.formTitle : `Crop (${props.width} x ${props.height})`;
    const [show,setShow] = useState(false);
    const handleOnCrop = (url:string) => {
        props.onCrop(url);
        setShow(false);
    };

    return(
        <div>
            <div><Button onClick={()=>{setShow(true)}}>Crop</Button></div>
            <ModalWrp title={title} showModal={show} size="lg" onCancel={()=>setShow(false)}>
                <ReactCropperWrp src={props.src} onCrop={handleOnCrop} width={props.width} height={props.height} />
            </ModalWrp>
        </div>
    )
}
CropperBtn.defaultProps = {
    width:100,
    height:100,
};
