import React, { useEffect, useRef, useState } from "react";
import AppConstants from "../../util/AppConstants";
import { Card, CardBody } from "reactstrap";
import "./../../assets/scss/datatable/custom.scss";
import ContentLoading from "./ContentLoading";

const $: any = require('jquery');
$.DataTable = require('datatables.net');
require('datatables.net-buttons');
require('datatables.net-buttons/js/buttons.flash.js');//# Flash file export
require('datatables.net-buttons/js/buttons.html5.js');//# HTML 5 file export
require('datatables.net-buttons/js/buttons.print.js');//# Print view button
require('datatables.net-buttons-dt');
require('../../util/datetime-moment');

$.DataTable.moment(AppConstants.DEFAULT_DATE_FORMAT);

export interface IDataTableWrapProps {
    title: string,
    fileName: string,
    columns: any[],
    isLoading?: boolean,
    onLoadData: (tableRef: any) => void,
    exportColumns: any,
    sortOrder: any
}

let dbTable: any = null;
export const DataTableWrap = (props: IDataTableWrapProps) => {
    const el = useRef(null);
    let $el: any = null;

    useEffect(() => {
        $el = $(el.current);
        dbTable = $el.DataTable(getDataTablesOptions());
        props.onLoadData(dbTable);
    }, []);

    const getDataTablesOptions = () => {
        return {
            data: [],
            columns: props.columns,
            paging: false,
            info: false,
            filter: false,
            order: props.sortOrder,
            dom: 'Bfrtip',
            buttons: [
                { extend: 'excelHtml5', title: props.title, filename: props.fileName,
                    exportOptions: { columns: props.exportColumns }
                },
                { extend: 'csvHtml5', title: props.title, filename: props.fileName,
                    exportOptions: { columns: props.exportColumns }
                },
                { extend: 'pdfHtml5', title: props.title, filename: props.fileName,
                    exportOptions: { columns: props.exportColumns }
                },
            ]
        }
    };
    return (
        <>
            {props.isLoading && (<ContentLoading show={props.isLoading} />)}
            <Card className={'br-g-300 shadow rounded-2 mt-3 mb-3'}>
                <div className="mt-2"><h5 className="ml-3">{props.title}</h5></div>
                <CardBody className="report-viewer">
                    <table className="display table table-responsive-md"
                        style={{ width: "100%" }} ref={el} />
                </CardBody>
            </Card>
        </>
    )
}