import React from 'react';
import {auth} from "../../util/Auth";

interface IProps {
    permission?: string,
    permissions?: string[],
    anyPermissions?: string[],
    children: any,
}

export class HashPermission extends React.Component<IProps, {}> {

    static defaultProps = {
        permission: '',
        permissions: '',
        children: '',
    };

    render() {
        if (this.props.permission && !auth.hasPermission(this.props.permission)) {
            return '';
        }
        if (this.props.permissions && !auth.hasPermissions(this.props.permissions)) {
            return '';
        }
        if (this.props.anyPermissions && !auth.hasAnyPermissions(this.props.anyPermissions)) {
            return '';
        }
        return (
            <>
                {this.props.children}
            </>
        )
    }
}