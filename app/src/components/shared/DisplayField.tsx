import React from "react";
import {startCase} from "../../util/Helpers";
import {Col} from "reactstrap";
import {ShowBoolean} from "./ShowBoolean";

interface IDisplayFieldProps {
    label?: string,
    name: string,
    data: any,
    render?: any,
    value?: any,
    md?:any,
}

class DisplayField extends React.Component<IDisplayFieldProps> {
    static defaultProps = {
        md:6
    };
    render() {
        if (this.props.render) {
            return this.props.render;
        }
        let {data, name} = this.props;
        let label = startCase((this.props.label) ? this.props.label : name);
        const value = (this.props.value) ? this.props.value : data[name];
        return (
            <Col md={this.props.md}>
                <div>
                    <span style={{color:'#bcc0c4'}}>{label}</span>
                </div>
                <div className="label-value
                ">
                    {(typeof value === "boolean")?<ShowBoolean value={value} labelOnTrue='Yes' labelOnFalse='No'/>:value}
                </div>
            </Col>
        )
    }
}

export default DisplayField;