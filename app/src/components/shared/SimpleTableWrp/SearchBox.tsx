import React, {useState} from "react";
import {Button, Input, InputGroup, InputGroupAddon} from "reactstrap";

export function SearchBox(props: { onSearch: any }) {
    const [value, setValue] = useState('');
    return (
        <InputGroup size="md">
            <Input type="text" placeholder="Search"
                   value={value}
                   onChange={(e: any) => setValue(e.target.value)}
                   onKeyPress={event => {
                       if (event.key === 'Enter') {
                           props.onSearch(value)
                       }
                   }}
                   className={'list-panel-search-input '}/>

            <InputGroupAddon addonType="append">
                <Button
                    onClick={() => props.onSearch(value)}
                    className="rounded-right"
                    color="primary"
                    size="sm">
                    <i className="fas fa-search"/>
                </Button>
            </InputGroupAddon>
        </InputGroup>
    )
}
