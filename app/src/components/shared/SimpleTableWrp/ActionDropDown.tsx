import React, {useState} from "react";
import {Dropdown, DropdownItem, DropdownMenu, DropdownToggle} from "reactstrap";

export function ActionDropDown(props: { show: boolean, handleOnAction: any }) {
    const [toggle, setToggle] = useState(false);
    if (!props.show) {
        return null
    }
    return (
        <Dropdown isOpen={toggle} toggle={() => setToggle(!toggle)} className="ml-1">
            <DropdownToggle className="px-2" color={'primary'}>
                <i className="fas fa-ellipsis-v"/>
            </DropdownToggle>
            <DropdownMenu right>
                <DropdownItem onClick={() => props.handleOnAction('export')} title="CSV">
                    Export CSV
                </DropdownItem>
            </DropdownMenu>
        </Dropdown>
    )
}

ActionDropDown.defaultProps = {
    show: false
};
