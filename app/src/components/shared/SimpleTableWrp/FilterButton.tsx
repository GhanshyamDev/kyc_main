import {Button} from "reactstrap";
import React from "react";

export function FilterButton(props: { show: boolean, onShowFilterPanel: any }) {
    if (!props.show) {
        return null
    }
    return (
        <Button onClick={() => props.onShowFilterPanel(true)} className='ml-1' color="primary"
                style={{padding: '.35rem'}}>
            <i className="fas fa-filter"/>
        </Button>
    )
}

FilterButton.defaultProps = {
    show: false
};
