import React from 'react';
import { Card, CardBody, Row, Col } from "reactstrap";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider from "react-bootstrap-table2-toolkit";
import { ActionDropDown } from "./ActionDropDown";
import { SearchBox } from "./SearchBox";
import { FilterButton } from "./FilterButton";
import { FilterDrawer } from "./FilterDrawer";

interface IPros {
    loading?: boolean,
    rowKey: string,
    data: any,
    title?: string,
    columns: any,
    showFilterButton: boolean,
    showSearch: boolean,
    showPagination: boolean,
    showFilterPanel?: boolean,
    onShowFilterPanel?: any,
    showExportButton: boolean,
    onRowStyleFormat?: (row: any, rowIndex: number) => {};

    onAction?: any,
    onChange?: any,
    onSearch?: any,

    filterForm?: any,
    filterTitle?: string,

    additionalFilter?: JSX.Element
}

export default function SimpleTableWrp(props: IPros) {
    const onSearch = (value: any, searchProps: any) => {
        if (props.onSearch) {
            props.onSearch(value);
            return
        }
        if (searchProps && searchProps.onSearch) {
            searchProps.onSearch(value);
        }
    };

    const onTableChange = (type: any, event: any) => {
        if (props.onChange) {
            props.onChange(event);
        }
    };

    return (
        <>

                <ToolkitProvider
                    keyField={props.rowKey}
                    data={props.data}
                    columns={props.columns}
                    search
                >{(toolkitProps: any) => (
                    <>
                        <Row>
                            <Col md={props.additionalFilter ? 3 : 6}>
                                <h4 className="float-lg-left float-md-left mt-1 no-margin-bottom">{props.title}</h4>
                            </Col>
                            <Col md={props.additionalFilter ? 9 : 6}>
                                {props.additionalFilter && (
                                    <div className="d-inline">
                                        {props.additionalFilter}
                                    </div>
                                )}
                                <div className="float-lg-right float-md-right form-inline mb-3">
                                    {props.showSearch &&
                                        <SearchBox onSearch={(value: string) => onSearch(value, toolkitProps.searchProps)} />
                                    }
                                    <FilterButton show={props.showFilterButton}
                                        onShowFilterPanel={props.onShowFilterPanel} />
                                    <ActionDropDown show={props.showExportButton} handleOnAction={props.onAction} />
                                </div>
                            </Col>
                        </Row>

                        <div className="dm-table margin-m-20px bg-g-100 mt-lg-0 mt-md-2 mt-2">
                            <BootstrapTable
                                {...toolkitProps.baseProps}
                                rowStyle={props.onRowStyleFormat}
                                bootstrap4
                                striped
                                bordered={false}
                                keyField={props.rowKey}
                                data={props.data}
                                columns={props.columns}
                                pagination={(props.showPagination) ? paginationFactory({}) : false}
                                onTableChange={onTableChange}
                                noDataIndication={NoDataIndication}
                            />
                        </div>
                    </>
                )}
                </ToolkitProvider>
            <FilterDrawer show={props.showFilterPanel} onShowFilterPanel={props.onShowFilterPanel} />
        </>
    );
}

SimpleTableWrp.defaultProps = {
    showExportButton: false,
    showFilterButton: false,
    rowKey: 'id',
    showSearch: false,
    showPagination: true,
    showFilterPanel: false,
    columns: {},
    data: [],
};

function NoDataIndication() {
    return (
        <div className="spinner">
            <div className="rect1" />
            <div className="rect2" />
            <div className="rect3" />
            <div className="rect4" />
            <div className="rect5" />
        </div>
    );
}

