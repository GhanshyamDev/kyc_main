import {StoneDrawer} from "../StoneDrawer";
import {Card, CardBody} from "reactstrap";
import React from "react";

interface IProps {
    show: boolean,
    title: string,
    onShowFilterPanel: any
    filterForm?: any,
}


export function FilterDrawer(props: IProps) {
    return (
        (props.show && <StoneDrawer
            title={props.title}
            width="400px"
            onClose={() => props.onShowFilterPanel(false)}
            showDrawer={props.show}
        >
            <Card>
                <CardBody>
                    {(props.filterForm) ? props.filterForm() : null}
                </CardBody>
            </Card>
        </StoneDrawer>) || null
    )
}

FilterDrawer.defaultProps = {
    show: false,
    title: "Filters"
};
