import React from 'react';
import {Badge} from "reactstrap";

interface IShowBooleanProps {
    value: boolean | number,
    labelOnTrue: string,
    labelOnFalse: string
}

export class ShowBoolean extends React.Component<IShowBooleanProps, {}> {
    static defaultProps = {
        value: false,
        labelOnTrue: 'Yes',
        labelOnFalse: 'No'
    };

    render() {
        const {value, labelOnTrue, labelOnFalse} = this.props;
        const bool = (value) ? 'info' : 'danger';
        return (
            <div>
                <Badge color={bool} className='badge'>
                    {(value) ? labelOnTrue : labelOnFalse}
                </Badge>
            </div>
        )
    }
}