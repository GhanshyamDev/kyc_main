import React, {Component, ReactNode} from 'react';
import {Modal, ModalHeader} from "reactstrap";


interface IProps {
    title: string,
    size?: string,
    showModal: boolean
    children: ReactNode,
    onSubmit?: any,
    onCancel?: any,
    className?: string,
    initialValues?: any
}

class ModalWrp extends Component<IProps, { visible: boolean }> {
    static defaultProps = {
        size: 'md'
    };

    constructor(props: any) {
        super(props);
        this.state = {
            visible: false
        };
    }

    onCancel = () => {
        this.props.onCancel();
    };

    toggle = () => {
        this.setState({
            visible: !this.state.visible
        });
    };

    render() {
        const {title, size, showModal} = this.props;
        return (
            <Modal isOpen={showModal} toggle={this.onCancel} size={size} className={this.props.className} contentClassName={'border-0 rounded-3'}>
                <ModalHeader className={'bg-g-200'} toggle={this.onCancel}>{title}</ModalHeader>
                {this.props.children}
            </Modal>
        );
    }
}

export default ModalWrp;
