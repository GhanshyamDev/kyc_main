import React, {Component} from 'react';
import {Spinner} from "reactstrap";

interface IContentLoadingProps {
    show: boolean
}

class ContentLoading extends Component <IContentLoadingProps, {}> {
    static defaultProps = {
        show: false
    };

    render() {
        if (!this.props.show) {
            return '';
        }

        return (
            <div className="content-loader">
                <div className="content-inner d-flex justify-content-center" style={{marginTop: "20px"}}>
                    <Spinner color="primary" />
                </div>
            </div>
        );
    }
}

export default ContentLoading;
