import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import React from "react";
import overlayFactory from "react-bootstrap-table2-overlay";


interface IBTableRemotePaginationProps {
    rowKey: string,
    data: any,
    columns: any,
    page: number,
    loading: boolean,
    totalSize: number,
    sizePerPage: number,
    onTableChange: any
}

function NoDataIndication() {
    return (
        <div className="spinner">
            <div className="rect1"/>
            <div className="rect2"/>
            <div className="rect3"/>
            <div className="rect4"/>
            <div className="rect5"/>
        </div>
    );
}

const overlay = overlayFactory(
    {
        spinner: true,
        styles: {
            overlay: (base: any) => {
                return ({
                    ...base,
                    background: 'rgba(91,150,255,0.15)',
                })
            }
        }
    }
);

export function BTableRemotePagination(props: IBTableRemotePaginationProps) {
    return (
        <div>
            <BootstrapTable
                bootstrap4
                remote
                striped
                bordered={false}
                loading={props.loading}
                keyField={props.rowKey}
                data={props.data}
                columns={props.columns}
                pagination={paginationFactory({
                    totalSize: props.totalSize,
                    page: props.page,
                    sizePerPage: props.sizePerPage
                })}
                onTableChange={props.onTableChange}
                noDataIndication={NoDataIndication}
                //overlay={overlay}
            />
        </div>
    );
}
