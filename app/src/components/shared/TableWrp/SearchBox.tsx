import React, {useState} from "react";
import {Button, Input, InputGroup, InputGroupAddon} from "reactstrap";

export function SearchBox(props: { search: string, onSearch: any }) {
    const [search, setSearch] = useState('');
    return (
        <>
            <InputGroup addonType="append" size="sm">
                <Input type="text" placeholder="Search"
                       value={search || ''}
                       onChange={(e: any) => setSearch(e.target.value)}
                       onKeyPress={event => {
                           if (event.key === 'Enter') {
                               props.onSearch(search)
                           }
                       }}
                       className={'list-panel-search-input '}/>
                <InputGroupAddon addonType="append">
                    <Button
                        onClick={() => props.onSearch(search)}
                        className="rounded-right"
                        color="primary"
                        size="sm"
                    >
                        <i className="fas fa-search"/>
                    </Button>
                </InputGroupAddon>
            </InputGroup>
        </>
    )
}
