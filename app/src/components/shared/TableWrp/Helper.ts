import {Badge} from "reactstrap";
import React from "react";

const pageSize = 10;

export interface IQueryParams {
    search: string | null,
    page: number,
    sortBy: any,
    pageSize: number,
    sortOrder: any,
    filters: any
}

export interface ITableData {
    list: any,
    total: number
}

export function setQueryParams(queryParams: any): IQueryParams {
    return {
        page: parseInt(queryParams.page) || 1,
        pageSize: parseInt(queryParams.pageSize) || pageSize,
        ...queryParams.search && { search: queryParams.search },
        ...queryParams.sortBy && { sortBy: queryParams.sortBy },
        ...queryParams.sortOrder && { sortOrder: queryParams.sortOrder },
        ...getFilters(queryParams.filters) && { filters: getFilters(queryParams.filters) },
    }
}

export function getQueryParams(queryParams: any = {}): IQueryParams {
    return {
        page: parseInt(queryParams.page) || 1,
        pageSize: parseInt(queryParams.pageSize) || pageSize,
        ...queryParams.search && { search: queryParams.search },
        ...queryParams.sortBy && { sortBy: queryParams.sortBy },
        ...queryParams.sortOrder && { sortOrder: queryParams.sortOrder },
        ...getFilters(queryParams.filters) && { filters: getFilters(queryParams.filters) },
    }
}

export function getFilters(filters: any) {
    if (!filters || !Object.keys(filters).length) {
        return null;
    }
    Object.keys(filters).forEach((field: string) => {
        if (filters[field] === undefined || filters[field] === null
            ||
            (typeof filters[field] !== "boolean" && filters[field].length === 0)) {
            delete filters[field];
        }
    });

    return filters;
}
