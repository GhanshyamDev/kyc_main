import React, {useState} from 'react';
import {Button, Card, CardBody} from "reactstrap";
import {StoneDrawer} from "../StoneDrawer";
import {useLocation} from "react-router-dom";
import {SearchBox} from "./SearchBox";
import {ActionDropDown} from "./ActionDropDown";
import {BTableRemotePagination} from "./BTableRemotePagination";
import {IQueryParams} from "./Helper";

const pageSize = 10;

export interface ITableData {
    list: any,
    total: number
}

interface IProps {
    loading?: boolean,
    rowKey: string,
    data: ITableData,
    title?: string,
    columns: any,
    queryParams: IQueryParams,
    showFilterButton?: boolean,
    showFilterPanel?: boolean,
    showInLineFilter?: boolean,
    onShowFilterPanel?: any,
    showActionDropdown?: boolean,
    showSearch?: boolean,
    onAction?: any,
    onChange?: any,
    filterForm?: any,
    filterTitle?: string,
    filters?: any
}

export function TableWrp(props: IProps) {
    const [search, setSearch] = useState('');
    //const location = useLocation();

    /*useEffect(() => {
        const urlQueryParams = qs.parse(location.search.substr(1));
        if (Object.keys(urlQueryParams).length > 0) {
            const filters = urlQueryParams.filters as any;
        }
    }, []);*/

    const triggerOnSearch = (search: string) => {
        setSearch(search);
        const queryParams = {
            ...props.queryParams,
            search: search,
            page: 1,
        };

        props.onChange(queryParams);
    };

    const handleOnAction = (action: string) => {
        props.onAction(action, props.queryParams);
    };

    const onTableChange = (type: any, event: any) => {
        const queryParams = {
            ...props.queryParams,
            page: event.page,
            pageSize: event.sizePerPage,
            sortBy: event.sortField || undefined,
            sortOrder: event.sortOrder || undefined,
        };

        props.onChange(queryParams);
    };

    return (
        <>
            <Card className="shadow-sm">
                <CardBody>
                    <h3 className="float-lg-left float-md-left mt-1 no-margin-bottom">{props.title}</h3>
                    <div className="float-lg-right float-md-right form-inline mb-3">
                        {props.showInLineFilter &&
                            <>
                                {(props.filterForm) ? props.filterForm() : null}
                            </>
                        }
                        {props.showSearch &&
                            <SearchBox search={search} onSearch={triggerOnSearch}/>
                        }
                        {props.showFilterButton &&
                            <Button onClick={() => props.onShowFilterPanel(true)} className='ml-1 mr-1' color="primary">
                                <i className="fas fa-filter"/>
                            </Button>
                        }
                        {props.showActionDropdown && <ActionDropDown handleOnAction={handleOnAction}/>}
                    </div>
                    <div className="dm-table margin-m-20px bg-g-100 mt-lg-5 mt-md-2 mt-2">
                        <BTableRemotePagination
                            page={props.queryParams.page}
                            sizePerPage={props.queryParams.pageSize}
                            rowKey={props.rowKey}
                            data={props.data.list}
                            columns={props.columns}
                            loading={(props.loading) ? props.loading : false}
                            totalSize={props.data.total}
                            onTableChange={onTableChange}
                        />
                    </div>
                </CardBody>
            </Card>
            {props.showFilterPanel &&
            <StoneDrawer
                title={props.filterTitle}
                width="400px"
                onClose={() => props.onShowFilterPanel(false)}
                showDrawer={props.showFilterPanel}
            >
                <Card>
                    <CardBody>
                        {(props.filterForm) ? props.filterForm() : null}
                    </CardBody>
                </Card>
            </StoneDrawer>
            }
        </>
    );
}

TableWrp.defaultProps = {
    rowKey: 'id',
    filterTitle: 'Filters',
    showFilterButton: false,
    showFilterPanel: false,
    showActionDropdown: false,
    showInLineFilter: false,
    showSearch: true,
    columns: {},
    queryParams: {
        search: null,
        page: 1,
        pageSize: pageSize,
        sortBy: null,
        sortOrder: null,
        filters: {},
    },
    data: {
        list: [],
        total: 0
    },
};



