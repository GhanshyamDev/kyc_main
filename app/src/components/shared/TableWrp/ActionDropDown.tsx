import React, {useState} from "react";
import {Dropdown, DropdownItem, DropdownMenu, DropdownToggle} from "reactstrap";

export function ActionDropDown(props: { handleOnAction: any }) {
    const [isOpen, setIsOpen] = useState(false);
    return (
        <Dropdown isOpen={isOpen} toggle={() => setIsOpen(!isOpen)}>
            <DropdownToggle className="px-2" color={'primary'} size="sm">
                <i className="fas fa-ellipsis-v"/>
            </DropdownToggle>
            <DropdownMenu right>
                <DropdownItem onClick={() => props.handleOnAction('export')} title="CSV">
                    Export CSV
                </DropdownItem>
            </DropdownMenu>
        </Dropdown>
    )
}
