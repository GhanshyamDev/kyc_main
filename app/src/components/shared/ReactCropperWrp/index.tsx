import React, {useRef, useState} from "react";
import Cropper from 'react-cropper';
import 'jquery/dist/jquery.min';
import '../../../../node_modules/cropperjs/dist/cropper.css';
import {Button, Col, ModalBody, ModalFooter, Row} from "reactstrap";
import {Controls} from "./Controls";

export interface Options {
    rotate: number,
    zoom: number,
    borderRadius: number,
    scaleX: number,
    scaleY: number,
}

const initialOptions: Options = {
    rotate: 0,
    zoom: 10,
    borderRadius: 0,
    scaleX: 1,
    scaleY: 1,
};

const $: any = require('jquery');

interface IProps {
    src: string,
    width: number,
    height: number,
    cropBoxResizable: boolean,
    onCrop: any,
}

export function ReactCropperWrp(props: IProps) {
    const cropperRef: any = useRef(null);
    const [options, setOptions] = useState<Options>(initialOptions);

    const onCrop = () => {
        props.onCrop(cropperRef?.current?.cropper.getCroppedCanvas({
            width: props.width,
            height: props.height,
        }).toDataURL());
    };

    const onChange = (items: Options) => {
        items = {...options,...items};
        cropperRef?.current?.cropper.rotateTo(items.rotate);
        cropperRef?.current?.cropper.zoomTo(items.zoom / 100);
        $('.cropper-view-box').css('border-radius', `${items.borderRadius}%`);
        cropperRef?.current?.cropper.scaleX(items.scaleX);
        cropperRef?.current?.cropper.scaleY(items.scaleY);
        setOptions({...options, ...items});
    };

    const onReset = () => {
        cropperRef?.current?.cropper.reset();
        setOptions(initialOptions);
    };

    return (
        <>
            <ModalBody>
                <Row>
                    <Col>
                        <Cropper
                            ref={cropperRef}
                            src={props.src}
                            style={{maxWidth: props.width, height: props.height}}
                            minCropBoxWidth={props.width}
                            minCropBoxHeight={props.height}
                            cropBoxResizable={props.cropBoxResizable}
                        />
                    </Col>
                    <Col>
                        <Controls
                            onChange={onChange}
                            options={options}
                        />
                    </Col>
                </Row>
            </ModalBody>
            <ModalFooter>
                <Button onClick={onCrop} color="primary">Crop</Button>
                <Button onClick={onReset} className="ml-3">Reset</Button>
            </ModalFooter>
        </>
    )
}

ReactCropperWrp.defaultProps = {
    width: 100,
    height: 100,
    cropBoxResizable: false
};
