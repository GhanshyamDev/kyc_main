import React from "react";
import {Button, ButtonGroup, Col, Input, Row} from "reactstrap";
import {Options} from "./index";

interface IProps {
    onChange: any,
    options:Options,
}

export function Controls(props: IProps) {
    const {options,onChange} = props;

    return (
        <>
        <Row>
            <Col>
                <label>Rotate : {options.rotate}</label>
                <Input
                    type="range"
                    min={0}
                    max={360}
                    value={options.rotate}
                    onChange={(e: any) => onChange({rotate: e.target.value})}
                />
            </Col>
        </Row>
        <Row>
            <Col>
                <label>Zoom : {options.zoom}</label>
                <Input
                    type="range"
                    min={1}
                    max={300}
                    value={options.zoom}
                    onChange={(e: any) => onChange({zoom: e.target.value})}
                />
            </Col>
        </Row>
        <Row>
            <Col>
                <div><label>Scale Horizontal</label></div>
                <ButtonGroup>
                    <Button outline color="primary" onClick={() => {
                        onChange({scaleX:options.scaleX == -1 ? options.scaleX + 2 : options.scaleX + 1});
                    }}>
                        <i className="fa fa-plus" aria-hidden="true"/>
                    </Button>
                    <Button outline color="primary" onClick={() => {
                        onChange({scaleX:options.scaleX == 1 ? options.scaleX - 2 : options.scaleX - 2});
                    }}>
                        <i className="fa fa-minus" aria-hidden="true"/>
                    </Button>
                </ButtonGroup>
            </Col>
            <Col>
                <div><label>Scale Vertical</label></div>
                <ButtonGroup>
                    <Button outline color="primary" onClick={() => {
                        onChange({scaleY:options.scaleY == -1 ? options.scaleY + 2 : options.scaleY + 1});
                    }}>
                        <i className="fa fa-plus" aria-hidden="true"/>
                    </Button>
                    <Button outline color="primary" onClick={() => {
                        onChange({scaleY:options.scaleY == 1 ? options.scaleY - 2 : options.scaleY - 2});
                    }}>
                        <i className="fa fa-minus" aria-hidden="true"/>
                    </Button>
                </ButtonGroup>
            </Col>
        </Row>
        {/* <Col md={6}>
                <label>Border Radius : {options.borderRadius}</label>
                <Input
                    type="range"
                    min={0}
                    max={50}
                    value={options.borderRadius}
                    onChange={(e: any) => onChange({borderRadius: e.target.value})}
                />
            </Col>*/}
        {/*<Col md={6}>
                <div><label>Move Image :</label></div>
                <ButtonGroup>
                    <Button outline color="primary" onClick={()=>{props.onMove(-10,0)}}>
                        <i className="fa fa-arrow-left" aria-hidden="true"/>
                    </Button>
                    <Button outline color="primary" onClick={()=>{props.onMove(10,0)}}>
                        <i className="fa fa-arrow-right" aria-hidden="true"/>
                    </Button>
                    <Button outline color="primary" onClick={()=>{props.onMove(0,-10)}}>
                        <i className="fa fa-arrow-up" aria-hidden="true"/>
                    </Button>
                    <Button outline color="primary" onClick={()=>{props.onMove(0,10)}}>
                        <i className="fa fa-arrow-down" aria-hidden="true"/>
                    </Button>
                </ButtonGroup>
            </Col>*/}
       </>
    )
}
