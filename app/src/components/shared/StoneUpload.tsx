import React, {useEffect, useRef, useState} from 'react';
import {Progress} from "reactstrap";
import {toast} from "react-toastify";
import axios from 'axios';
import {b64toBlob} from "../../util/Helpers";
import {auth} from "../../util/Auth";

interface IStoneUpload {
    field: string,
    fileTypes: string[], //'image/png', 'image/jpeg', 'image/gif'
    maxSelect: number,
    showPreview: boolean,
    previewHeight: string,
    cameraPictureWidth: string,
    previewUrl?: string|null,
    maxSize: number, //mb
    postUrl: string,
    onUploaded: any,
    authRequired: boolean,
    extraData: any,
    captureByCamera: boolean,
    isDisabled:boolean|false,
    candidateSelfie: boolean,
    placeholder: string,
    imageStatus: any,
}

//https://codepen.io/bhagwatchouhan/pen/jjLJoB
export function StoneUpload(props: IStoneUpload) {
    const [selectedFiles, setSelectedFiles] = useState<any>([]);
    const [loaded, setLoaded] = useState<number>(0);
    const [uploadedUrl, setUploadedUrl] = useState<string>("");
    const [captureBy, setCaptureBy] = useState<"upload"|"camera">("camera");
    const [cameraOn, setCameraOn] = useState<boolean>(false);
    const [cameraStream, setCameraStream] = useState<any>(null);

    const stream: any = useRef<HTMLVideoElement|null>(null);
    const capture: any = useRef<HTMLCanvasElement|null>(null);
    const snapshot: any = useRef<HTMLDivElement|null>(null);

    useEffect(() => {
        if (props.captureByCamera) {
            window.addEventListener("resize", winResize);
            winResize();
        }
    }, []);

    const winResize = () => {
        /*if (window.innerWidth <= 500) {
            stopStreaming();
            setCaptureBy("upload");
        } else {
            setCaptureBy("camera");
        }*/
    };

    const onChangeHandler = (event: any) => {
        const files = event.target.files;
        if(maxSelectFile(event) && checkMimeType(event) && checkFileSize(event)){
            // if return true allow to setState
            setUploadedUrl("");
            setSelectedFiles(files);
            setLoaded(0);
        }
    };

    const maxSelectFile = (event: any) => {
        let files = event.target.files;
        const maxSelect = (props.maxSelect) ? props.maxSelect : 1;
        if (files.length > maxSelect) {
            event.target.value = null;
            toast.warn(`Only ${maxSelect} images can be uploaded at a time`);
            return false;
        }
        return true;
    };

    const checkMimeType = (event: any) => {
        //getting file object
        const files = event.target.files;
        //define message container
        let err = [];
        // list allow mime type
        const types: any = props.fileTypes;
        // loop access array
        for(let x = 0; x < files.length; x++) {
            // compare file type find doesn't matach
            if (types.every((type: any) => files[x].type !== type)) {
                // create error message and assign to container
                //err[x] = files[x].type+' is not a supported format\n';
                err[x] = `File is not a supported format\n`;
            }
        }
        for(let z = 0; z<err.length; z++) {// if message not same old that mean has error
            // discard selected file
            toast.error(err[z]);
            event.target.value = null;
        }
        return true;
    };

    const checkFileSize = (event: any) => {
        const files = event.target.files;
        const maxSize = (props.maxSize) ? props.maxSize : 1;
        const size = maxSize * 1000000;
        let err = [];
        for(let x = 0; x<files.length; x++) {
            if (files[x].size > size) {
                err[x] = files[x].type+'is too large, please pick a smaller file\n';
            }
        }
        for(let z = 0; z<err.length; z++) {// if message not same old that mean has error
            // discard selected file
            toast.error(err[z]);
            event.target.value = null
        }
        return true;
    };

    const handleOnClickUpload = () => {
        const data = new FormData();
        for(let x = 0; x < selectedFiles.length; x++) {
            data.append("file", selectedFiles[x]);
        }
        data.append("field", props.field);
        uploadToServer(data);
    };

    const uploadToServer = (data: FormData) => {
        let headers: any = {
            'Content-Type': 'application/json'
        };

        if (props.authRequired) {
            console.log(props.authRequired);
            headers = {
                ...headers,
                Authorization: `Bearer ${auth.getToken()}`,
                oarsgf: auth.getCurrentOrganizationId()
            }
        }

        if (props.extraData) {
            data.append("extra_data", props.extraData);
        }

        axios.post(props.postUrl, data, {
            headers: headers,
            onUploadProgress: ProgressEvent => {
                setLoaded((ProgressEvent.loaded / ProgressEvent.total*100));
            },
        }).then(res => { // then print response status
            props.onUploaded(res.data);
            setUploadedUrl(res.data.url);
            setSelectedFiles([]);
            toast.success('upload success');

            if (captureBy === "camera") {
                stopStreaming();
            }
        }).catch(err => { // then print response status
            console.log(err);
            toast.error('upload fail');
        }).finally(() => setLoaded(0))
    };

    const getPreviewImage = () => {
        if (selectedFiles.length > 0) {
            return <img style={{maxHeight: props.previewHeight}} className="img-fluid" src={URL.createObjectURL(selectedFiles[0])} alt={selectedFiles[0].name} />;
        }
        if (uploadedUrl) {
            return <img style={{maxHeight: props.previewHeight}} className="img-fluid" src={uploadedUrl} alt="image" />;
        }
        if (props.previewUrl) {
            return <img style={{maxHeight: props.previewHeight}} className="img-fluid" src={props.previewUrl} alt="image" />;
        }
        return "";
    };

    const getPreviewImageCamera = () => {
        if (uploadedUrl) {
            return <img style={{maxHeight: props.previewHeight}} src={uploadedUrl} alt="image" />;
        }
        if (props.previewUrl) {
            return <img style={{maxHeight: props.previewHeight}} src={props.previewUrl} alt="image" />;
        }
        return "";
    };

    const hasImage = () => (selectedFiles.length > 0) || uploadedUrl || props.previewUrl || cameraOn;

    const startStreaming = () => {
        const mediaSupport = 'mediaDevices' in navigator;
        if(mediaSupport && null == cameraStream) {
            navigator.mediaDevices.getUserMedia({ video: true })
                .then((mediaStream) => {
                    setCameraStream(mediaStream);
                    stream.current.srcObject = mediaStream;
                    stream.current.play();
                    setCameraOn(true);
                })
                .catch( function( err ) {
                    console.log( "Unable to access camera: " + err);
                });
        } else {
            alert('Your browser does not support media devices.');
            return;
        }
    };

    const stopStreaming = () => {
        if(null != cameraStream) {
            const track = cameraStream.getTracks()[0];
            track.stop();
            stream.current.load();
            setCameraStream(null);
            //cameraStream = null;
            setCameraOn(false);
        }
    };

    const captureSnapshot = () => {
        if(null != cameraStream ) {
            const ctx = capture.current.getContext('2d');
            const img = new Image();
            ctx.drawImage(stream.current, 0, 0, capture.current.width, capture.current.height);
            img.src	= capture.current.toDataURL("image/jpeg");
            //img.width = 320;
            //snapshot.current.innerHTML = '';
            //snapshot.current.appendChild(img);

            //file upload
            const realData = img.src;
            const data = new FormData();
            data.append("file", b64toBlob(realData.split(",")[1], "image/jpeg"));
            data.append("field", props.field);
            uploadToServer(data);
        }
    };

    const previewPlaceHolder = () => {
        return <div className="d-flex justify-content-center align-items-center" style={{height: "100%"}}>
                <div className="h3 text-light-gray">{props.placeholder}</div>
              </div>
    };

    const captureByUpload = () => {
        const previewImage = getPreviewImage();
        const coloredBg = (previewImage) ? `` : 'bg-light';
        return (
            <>
                {props.showPreview &&
                <div className={`preview mb-2 w-100 p-0 text-center ${coloredBg}`} style={{height: props.previewHeight}}>
                    {previewImage
                        ? previewImage
                        : previewPlaceHolder()
                    }
                </div>
                }
                <div className="form-group mb-0 clearfix">
                    <label className="btn btn-primary btn-sm mb-0 float-left">
                        Choose File
                        <input type="file" disabled={props.isDisabled} className="d-none" onChange={onChangeHandler}/>
                    </label>
                    {selectedFiles.length > 0 &&
                    <button type="button"
                            disabled={loaded > 0}
                            className="btn btn-sm btn-success"
                            onClick={handleOnClickUpload}>
                        Upload
                    </button>
                    }
                    {(selectedFiles.length === 0 && hasImage()) &&
                        <div className="float-right">
                            {props.imageStatus}
                        </div>
                    }
                </div>
                {loaded > 0 &&
                    <div className="mt-2" style={{height: "16px"}}>
                        <Progress max="100" color="success" value={loaded}>{Math.round(loaded)}%</Progress>
                    </div>
                }
            </>
        )
    };

    const captureByCamera = () => {
        const snapClass = (cameraOn) ? "d-none" : "";
        const height = props.previewHeight.replace("px", "");
        return (
            <>
                <div className="video-capture-container text-center">
                    <video ref={stream} width={props.cameraPictureWidth} height={height}/>
                    <div ref={snapshot} style={{height: props.previewHeight}} className={`snap ${snapClass}`}>
                        {(cameraOn || hasImage())
                            ? getPreviewImageCamera()
                            : previewPlaceHolder()
                        }
                    </div>
                </div>
                <div>
                    {(!cameraOn)
                        ? <button type="button" className="btn btn-success btn-sm" onClick={startStreaming}><i className="mdi mdi-video"/></button>
                        : <button type="button" className="btn btn-danger btn-sm" onClick={stopStreaming}><i className="mdi mdi-video-off"/></button>
                    }
                    {cameraOn &&
                        <button type="button" className="btn btn-primary btn-sm" onClick={captureSnapshot}>Capture</button>
                    }
                </div>
                <canvas ref={capture} width={props.cameraPictureWidth} height={height} className="d-none"/>
                {loaded > 0 &&
                    <div className="mt-2" style={{height: "16px"}}>
                        <Progress max="100" color="success" value={loaded}>{Math.round(loaded)}%</Progress>
                    </div>
                }
            </>
        )
    };

    const handleOnCaptureChange = (e: any) => {
        const value = e.target.value;
        if (e.target.value === "upload") {
            stopStreaming();
        }
        setCaptureBy(value);
    };

    const selectCaptureBy = () => {
        return (
            <div className="mb-1">
                <div className="form-check-inline">
                    <label className="form-check-label">
                        <input type="radio" className="form-check-input" value="upload" checked={captureBy === "upload"} onChange={handleOnCaptureChange}/>Upload
                    </label>
                </div>
                <div className="form-check-inline">
                    <label className="form-check-label">
                        <input type="radio" className="form-check-input" value="camera" checked={captureBy === "camera"} onChange={handleOnCaptureChange}/>Camera
                    </label>
                </div>
            </div>
        )
    };

    const getFinalDisplay = () => {
        if (props.captureByCamera) {
            return (
                <>
                    {/*{selectCaptureBy()}*/}
                    {captureBy === "upload" ? captureByUpload() : ""}
                    {captureBy === "camera" ? captureByCamera() : ""}
                </>
            )
        } else {
            return (
                <>
                    {captureByUpload()}
                </>
            )
        }
    };
    //${hasImage() ? 'd-inline-block' : 'd-block'}
    return (
        <>
            <div className={`stone-file-upload-container`}>
                {getFinalDisplay()}
            </div>
        </>
    )
}
StoneUpload.defaultProps = {
    field: "file",
    authRequired: true,
    showPreview: false,
    extraData: null,
    fileTypes: [],
    maxSelect: 1,
    maxSize: 10,
    previewHeight: "160px",
    cameraPictureWidth: "200",
    captureByCamera: false,
    candidateSelfie: false,
    placeholder: "Upload Photo",
    imageStatus: ''
};