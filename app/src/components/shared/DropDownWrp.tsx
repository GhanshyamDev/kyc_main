import React from 'react';
import {Dropdown, DropdownMenu, DropdownToggle} from 'reactstrap';

interface ButtonDropDownWrpState {
    dropdownOpen:boolean
}
interface ButtonDropDownWrpProps {
    icon:any,
    className?:string,
    btnSize?:string,
    btnClass?:string
}

export class DropDownWrp extends React.Component<ButtonDropDownWrpProps, ButtonDropDownWrpState> {
    static defaultProps = {
        icon: <i className="fas fa-ellipsis-v text-primary"/>,
        btnSize:'sm',
        btnClass:'btn btn-primary btn-sm'
    };

    constructor(props: any) {
        super(props);
        this.state = {
            dropdownOpen:false
        };
    }

    toggle = () => {
        this.setState({
            ...this.state,
            dropdownOpen:!this.state.dropdownOpen
        })
    };

    render() {
        const {btnSize,btnClass,...props} = this.props;
        return (
            <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle} {...props}>
                <DropdownToggle
                    tag="span"
                    className="three-dot p-1 cursor-pointer"
                    data-toggle="dropdown"
                    aria-expanded={this.state.dropdownOpen}
                >
                    {this.props.icon}
                </DropdownToggle>
                <DropdownMenu className="list-dropdown" right>
                    {this.props.children}
                </DropdownMenu>
            </Dropdown>
        )
    }
}
