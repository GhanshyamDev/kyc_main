import React from "react";
import {startCase} from "../../util/Helpers";

interface ITableDisplayFieldProps {
    label?: string,
    name:string,
    data: any,
    render?: any,
    value?:any
}

class TableDisplayField extends React.Component<ITableDisplayFieldProps> {
    render() {
        if(this.props.render){
            return this.props.render;
        }
        let {data,name} = this.props;
        let label = startCase((this.props.label)?this.props.label:name);
        const value = (this.props.value)?this.props.value:data[name];
        return (
            <>
                <td className='title'>{label}</td>
                <td className='description'>{value}</td>
            </>
        )
    }
}
export default TableDisplayField;