import React from 'react';
import {Button as BootstrapButton, Spinner} from 'reactstrap';

interface IButtonProps {
    color?: string,
    size?: string,
    type?: any,
    disabled: boolean,
    isLoading: boolean,
    onClick?: any,

    spinnerColor?: string,
    spinnerType?: string,
    spinnerSize?: string
}

export class Button extends React.Component<IButtonProps, {}> {
    static defaultProps = {
        color: 'primary',
        size: 'small',
        type: 'button',
        disabled: false,
        isLoading: false,
        spinnerColor: 'light',
        spinnerType: 'border',
        spinnerSize: 'sm'
    };

    render() {
        const {...props} = this.props;
        const buttonProps = {
            color: this.props.color,
            size: this.props.size,
            type: this.props.type
        };
        const spinner = (props.isLoading) ?
            <Spinner size={props.spinnerSize} color={props.spinnerColor} type={props.spinnerType}/> : ' ';
        return (
            <BootstrapButton {...buttonProps} disabled={this.props.disabled} onClick={this.props.onClick}>
                {spinner}
                {' '}
                {props.children}
            </BootstrapButton>
        )
    }
}
