import withReactContent from "sweetalert2-react-content";
import Swal from "sweetalert2";

export interface IDeleteConfirmProps {
    title?: string,
    confirmText?: string,
    iconType?: "success" | "error" | "warning" | "info" | "question"
    showCancelButton?: boolean,
    confirmButtonText?: string,
    cancelButtonText?: string,
    onConfirm: () => void
    onCancel?: () => void
}

export const DeleteConfirm = (props: IDeleteConfirmProps) => {
    const MySwal: any = withReactContent(Swal);
    return MySwal.fire({
        title: props.title ?? 'Are you sure to delete?',
        //text: props.confirmText,
        icon: props.iconType ?? 'warning',
        showCancelButton: props.showCancelButton ?? true,
        confirmButtonText: props.showCancelButton ?? 'Yes',
        cancelButtonText: props.cancelButtonText ?? 'No'
    }).then((result: any) => {
        if (result.value) {
            props.onConfirm();
            // For more information about handling dismissals please visit
            // https://sweetalert2.github.io/#handling-dismissals
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            if (props.onCancel)
                props.onCancel();
        }
    })
};

export const selfieSubmitConfirm = (props: IDeleteConfirmProps) => {
    const MySwal: any = withReactContent(Swal);
    return MySwal.fire({
        title: 'Are you sure you want to submit?',
        //text: props.confirmText,
        //icon: props.iconType ?? 'warning',
        showCancelButton: props.showCancelButton ?? true,
        confirmButtonText: props.showCancelButton ?? 'Yes',
        cancelButtonText: props.cancelButtonText ?? 'No'
    }).then((result: any) => {
        if (result.value) {
            props.onConfirm();
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            if (props.onCancel)
                props.onCancel();
        }
    })
};