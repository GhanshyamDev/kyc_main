import React from "react";
import {Spinner} from "reactstrap";

export const loading = () =>
    <div>
        <div className="spinner-wrapper">
            <div className="donut"/>
        </div>
    </div>;