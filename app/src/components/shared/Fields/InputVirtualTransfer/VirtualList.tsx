import React, {useState} from "react";
import './index.scss';
import {Input} from "reactstrap";
import {AutoSizer, List} from 'react-virtualized';

interface IProps {
    data: any,
    labelField: any,
    valueField: any,
    checkedData: any,
    onChange: any,
    onAllToggle:any,
}

export function VirtualList(props: IProps) {
    const {data, labelField, checkedData, valueField, onChange} = props;
    const [search,setSearch] = useState('');

    const getFilteredData = (data:any,search:any) => {
        if(search === ""){
            return data;
        }
        return data.filter((item:any)=>{
            return item[labelField].match(new RegExp(search, "i"))
        })
    };

    const filterData = getFilteredData(data,search);

    const isAllChecked = ():boolean => {
        return data.length && data.filter((item:any)=>!checkedData.includes(item[valueField])).length === 0
    };

    const getAllCheckedData = () => {
        const dataValues = data.map((item:any)=>item[valueField]);
        return checkedData.filter((item:any)=>dataValues.includes(item))
    };

    const rowRenderer = ({key, index, style}:any) => {
        const item:any = filterData[index];
        const isChecked = checkedData.includes((item[valueField]));
        return (
            <div key={index} style={style}>
                <div className={`list-row cursor-pointer ${(isChecked && "checked") || ""}`} onClick={()=>onChange(item)}>
                    <input type="checkbox" className="cursor-pointer" onChange={()=>onChange(item)} checked={isChecked}/>
                    {item[labelField]}
                </div>
            </div>
        )
    };

    return (
        <div className="list">
            <div className="m-1 select-all-row">
                <input
                    type="checkbox"
                    className="align-self-center cursor-pointer"
                    checked={isAllChecked()}
                    onChange={(e:any)=>props.onAllToggle(!isAllChecked())}
                />
                <span className="ml-2">{`${getAllCheckedData().length} / ${data.length} ${(data.length > 1 && "Items") || "Item"}`}</span>
            </div>
            <div className="m-1">
                <Input placeholder="search" value={search} onChange={(e:any)=>setSearch(e.target.value)}/>
                {search !== "" && <i className="fa fa-times clear-icon cursor-pointer" onClick={()=>setSearch('')} />}
            </div>
            <div className="list-data">
                <AutoSizer>
                    {({height, width}) => (
                        <List
                            height={height}
                            rowCount={filterData.length}
                            rowHeight={40}
                            rowRenderer={rowRenderer}
                            width={width}
                        />
                    )}
                </AutoSizer>
            </div>
        </div>
    )
}

VirtualList.defaultProps = {
    labelField: "label",
    valueField: "value",
};
