import React, {useEffect, useState} from "react";
import {FieldLabel} from "../FieldLabel";
import {ErrorMessage, useField} from "formik";
import FormGroup from "reactstrap/lib/FormGroup";
import {Button, Col, Row} from "reactstrap";
import {VirtualList} from "./VirtualList";

interface IProps {
    label?: string,
    isRequired?: boolean,
    field: any,
    form: any,
    api?: any,
    data?: any,
    labelField: any,
    valueField: any,
}

export default function InputVirtualTransfer(props: IProps) {
    const [field,, helpers] = useField(props.field.name);
    const {label, isRequired, form, api, labelField, valueField} = props;
    const [data, setData] = useState([]);
    const [checkedData, setCheckedData] = useState<any>([]);

    useEffect(() => {
        if (api) {
            api().then((response: any) => {
                setData(response.data.data);
            });
        } else {
            setData(props.data)
        }
    }, []);

    const onChange = (row: any) => {
        let data = checkedData;
        const isChecked = data.includes(row[valueField]);
        if (isChecked) {
            data = data.filter((item: any) => item !== row[valueField]);
            setCheckedData(data);
        } else {
            setCheckedData([...checkedData, row[valueField]])
        }
    };

    const getLeftListData = () => {
        if (field.value && field.value.length) {
            return data.filter((item: any) => !field.value.includes(item[valueField]))
        }
        return data;
    };

    const getRightListData = () => {
        if (field.value && field.value.length) {
            return data.filter((item: any) => field.value.includes(item[valueField]))
        }
        return [];
    };

    const toRight = () => {
        const leftCheckedData = getLeftListData()
            .filter((item: any) => checkedData.includes(item[valueField]))
            .map((item: any) => item[valueField]);

        helpers.setValue( [...field.value, ...leftCheckedData]);
        setCheckedData([...checkedData.filter((item: any) => !leftCheckedData.includes(item))])
    };

    const toLeft = () => {
        const rightCheckedData = getRightListData()
            .filter((item: any) => checkedData.includes(item[valueField]))
            .map((item: any) => item[valueField]);

        helpers.setValue(field.value.filter((item: any) => !rightCheckedData.includes(item)));
        setCheckedData([...checkedData.filter((item: any) => !rightCheckedData.includes(item))])
    };

    const onAllToggle = (value: any, data: any) => {
        const dataValues = data.map((item: any) => item[valueField]);

        if (value) {
            setCheckedData([...new Set([...checkedData, ...dataValues])])
        } else {
            setCheckedData([...checkedData.filter((item: any) => !dataValues.includes(item))]);
        }
    };

    return (
        <FormGroup>
            <FieldLabel label={label} isRequired={isRequired}/>
            <Row>
                <Col className="p-2" sm={12} md={12} lg={4}>
                    <VirtualList
                        data={getLeftListData()}
                        checkedData={checkedData}
                        labelField={labelField}
                        valueField={valueField}
                        onChange={onChange}
                        onAllToggle={(value: any) => onAllToggle(value, getLeftListData())}
                    />
                </Col>

                <Col className="d-flex flex-column align-items-center justify-content-center" sm={12} md={12} lg={1}>
                    <Button outline className="m-1" onClick={toRight}>
                        <i className="fa fa-angle-right" aria-hidden="true"/>
                    </Button>
                    <Button outline className="m-1" onClick={toLeft}>
                        <i className="fa fa-angle-left" aria-hidden="true"/>
                    </Button>
                </Col>

                <Col className="p-2" sm={12} md={12} lg={4}>
                    <VirtualList
                        data={getRightListData()}
                        checkedData={checkedData}
                        labelField={labelField}
                        valueField={valueField}
                        onChange={onChange}
                        onAllToggle={(value: any) => onAllToggle(value, getRightListData())}
                    />
                </Col>
            </Row>
            <ErrorMessage name={field.name} component="div" className="input-error"/>
        </FormGroup>
    )
}
InputVirtualTransfer.defaultProps = {
    labelField: "label",
    valueField: "value",
};
