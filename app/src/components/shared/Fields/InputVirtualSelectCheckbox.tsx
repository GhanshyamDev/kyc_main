import React from 'react';
import Label from "reactstrap/lib/Label";
import FormGroup from "reactstrap/lib/FormGroup";
import Input from "reactstrap/lib/Input";
import {ErrorMessage} from "formik";
import {FieldLabel} from "./FieldLabel";
import {AutoSizer, CellMeasurer, CellMeasurerCache, List} from "react-virtualized";
import {InputGroup, InputGroupAddon} from "reactstrap";
import {NavLink} from "react-router-dom";


interface IInputCheckboxProps {
    label?: string,
    link?: string,
    field: any,
    form?: any,
    labelField?: any,
    valueField?: any,
    api?: any,
    options?: any,
    isRequired?: boolean,
    onLinkClick?: any,
    isDisabled?: boolean
}

interface IState {
    query: string,
    options: any,
    isLoading: boolean,
}

export class InputVirtualSelectCheckbox extends React.Component<IInputCheckboxProps, IState> {
    static defaultProps = {
        labelField: 'label',
        valueField: 'value',
        isRequired: false,
    };

    cache = new CellMeasurerCache({defaultHeight: 300, fixedWidth: true, minHeight: 300});
    mostRecentWidth = 0;
    resizeAllFlag = false;
    list: any;

    constructor(props: any) {
        super(props);
        this.state = {
            query: '',
            options: [],
            isLoading: false
        };
    }

    componentDidMount(): void {
        if (!this.props.api) {
            this.setState({options: this.props.options});
            return;
        }
        this.setState({isLoading: true});
        this.props.api().then((response: any) => {
            this.setState({
                isLoading: false,
                options: response.data.data
            });
        });
    }

    addOption(selectedItem: any) {
        const {form, field, valueField} = this.props;
        const name = field.name;
        const value = form.values[name];
        const selectedOptions = [...(value || []), selectedItem[valueField]];
        form.setFieldValue(name, selectedOptions);
    }

    deleteOption(selectedItem: any, isValue: boolean = false) {
        const {form, field, valueField} = this.props;
        const name = field.name;
        const filteredValue = form.values[name].filter((option: any) => {
            const val = isValue ? selectedItem : selectedItem[valueField];
            return option !== val
        });
        form.setFieldValue(name, filteredValue)
    }

    handleOnChange(selectedItem: any) {
        const {form, field} = this.props;
        const index = (form.values[field.name] || []).indexOf(selectedItem[this.props.valueField]);
        if (index === -1) {
            this.addOption(selectedItem)
        } else {
            this.deleteOption(selectedItem);
        }
    }

    handleOnSearch = (event: any) => {
        this.setState({query: event.target.value})
    };

    handleOnClearSearch = () => {
        this.setState({query: ''})
    };

    setListRef = (ref: any) => {
        this.list = ref;
    };

    clearAll = () => {
        const {form, field} = this.props;
        form.setFieldValue(field.name, [])
    };

    render() {
        const {
            field,
            form,
            labelField,
        } = this.props;

        const {options} = this.state;
        const selectedOptions = form.values[field.name] || [];
        const filteredOptions =
            (this.state.query
                && options.filter((item: any) =>
                    item[labelField].match(new RegExp(this.state.query, "i"))
                )
            ) || options
        ;

        return (
            <>
                <span className="select-checkbox-clear-all" onClick={this.clearAll}>Clear All</span>
                <div className='select-checkbox'>
                    <FieldLabel {...this.props}/>
                    <div style={{
                        border: 'solid 1px',
                        borderColor: "#dee2e6",
                        borderRadius: '4px',
                        borderBottom: "none",
                        borderBottomLeftRadius: '0',
                        borderBottomRightRadius: '0'
                    }}>
                        <InputGroup className='search-box'>
                            {/*<InputGroupAddon addonType='prepend' style={{overflowX:'scroll'}}>
                            {selectedOptions.map((value:any)=> {
                                    const label = options.find((item: any) => item[valueField] === value)[labelField];
                                    return(<Badge color="primary ml-1 mt-1" key={value}>
                                        {label}
                                        <i className='fas fa-times p-1'
                                           style={{cursor: 'pointer'}}
                                           onClick={() => this.deleteOption(value, true)}
                                        />
                                    </Badge>)
                                }
                            )}
                        </InputGroupAddon>*/}
                            <Input
                                name='search'
                                className='search-box-input border-0'
                                value={this.state.query}
                                placeholder='Search...'
                                autoComplete='off'
                                onChange={this.handleOnSearch}
                            />
                            {this.state.query && <InputGroupAddon addonType='append'
                                                                  className='bg-white align-items-center justify-content-center'>
                                <i className="fas fa-times p-2" onClick={this.handleOnClearSearch}
                                   style={{cursor: "pointer", color: '#dee2e6'}} title={'clear'}/>
                            </InputGroupAddon>}
                        </InputGroup>
                    </div>
                    {(this.state.isLoading && <div className='select-list'>Loading...</div>) ||
                    <div className='select-list'>
                        {
                            <AutoSizer>
                                {({height, width}) => {
                                    this.mostRecentWidth = width;
                                    if (this.mostRecentWidth && this.mostRecentWidth !== width) {
                                        this.resizeAllFlag = true;
                                        setTimeout(this.resizeAll, 0);
                                    }

                                    return <List
                                        ref={this.setListRef}
                                        deferredMeasurementCache={this.cache}
                                        rowCount={filteredOptions.length}
                                        rowHeight={35}
                                        width={width}
                                        height={height}
                                        overscanRowCount={3}
                                        rowRenderer={({index, style, key, parent, isScrolling}) =>
                                            this.renderRow({
                                                index,
                                                style,
                                                key,
                                                parent,
                                                isScrolling,
                                                filteredOptions,
                                                selectedOptions
                                            })}

                                    />
                                }}
                            </AutoSizer>
                        }
                    </div>
                    }

                    <ErrorMessage name={field.name} component="div" className="input-error"/>
                </div>
            </>
        );
    }

    renderRow = ({index, style, key, isScrolling, parent, filteredOptions, selectedOptions}: any) => {
        const {
            labelField,
            valueField,
            field,
            form,
        } = this.props;
        const item = filteredOptions[index] || null;
        const isChecked = (selectedOptions && selectedOptions.includes(item[valueField]));
        return (
            (item &&
                <CellMeasurer
                    cache={this.cache}
                    columnIndex={0}
                    key={key}
                    parent={parent}
                    rowIndex={index}
                    width={this.mostRecentWidth}
                >
                    <FormGroup key={field.name + '-' + key}
                               style={style}
                               check
                               disabled={this.props.isDisabled}
                               className={'select-item d-flex align-items-center' + (isChecked ? ' checked ' : '')}>
                        <Label check className='text' title={item[labelField]}>
                            <Input
                                type="checkbox"
                                onChange={() => this.handleOnChange(item)}
                                checked={isChecked}
                                disabled={this.props.isDisabled}
                                invalid={Boolean(form.touched[field.name] && form.errors[field.name])}/>

                            {this.props.link && <NavLink
                                style={{
                                    textAlign: 'right',
                                    textDecoration: 'underline',
                                    fontSize: '11px'
                                }}
                                onClick={(e: any) => {
                                    e.preventDefault();
                                    this.props.onLinkClick && this.props.onLinkClick(item)
                                }} to={'#'}>
                                {this.props.link}
                            </NavLink>}{' '}

                            {item[labelField]}

                        </Label>
                    </FormGroup>
                </CellMeasurer>
            )
        )
    };

    resizeAll = () => {
        this.resizeAllFlag = false;
        this.cache.clearAll();
        if (this.list) {
            this.list.recomputeRowHeights();
        }
    };
}
