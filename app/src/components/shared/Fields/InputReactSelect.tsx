import React, {useEffect, useState} from 'react';
import {FormGroup} from "reactstrap";
import {ErrorMessage, useField} from "formik";
import {FieldLabel} from "./FieldLabel";
import Select, {components} from "react-select";

interface IProps {
    label?: string,
    field: any,
    form?: any,
    api?: any,
    options?: any,
    onSelect?: any,
    isRequired?: boolean,
    isClearable: boolean,
    labelField?: any,
    valueField?: any,
    subLabel?: any,
    needObject?: boolean,
    isMulti?: boolean,
    isHidden: boolean,
    getOptionValue: any,
    getOptionLabel: any,
    renderLabel?: any,
    className?: any,
    style?: any,
    isDisabled: any
}

export function InputReactSelect(props: IProps) {
    const [field,, helpers] = useField(props.field.name);
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const {form, api,options, needObject, valueField, labelField, isMulti, ...fieldProps} = props;

    useEffect(() => {
        if (api) {
            setIsLoading(true);
            api().then((response: any) => {
                setData(response.data.data);
                setIsLoading(false);
            })
        } else {
            setData(options);
        }
    }, [api,options]);

    const getValue = (value: any) => {
        if (needObject) {
            return {
                [valueField]: value[valueField],
                [labelField]: value[labelField]
            }
        }
        return value[valueField]
    };

    const handleOnChange = (item: any) => {
        const {field, form, onSelect} = props;

        if (!item) {
            form.setFieldValue(field.name, item);

            if (onSelect)
                props.onSelect(item);

            return;
        }
        const values = (props.isMulti) ? item.map((value: any) => getValue(value)) : getValue(item);
        helpers.setValue(values);

        if (onSelect)
            props.onSelect(item);
    };

    const Option = (reactSelectProps: any) => {
        return (
            <components.Option {...reactSelectProps}>
                {
                    (props.renderLabel && props.renderLabel(reactSelectProps))
                    || <>
                        <div>{reactSelectProps.data[props.labelField]}</div>
                        {(props.subLabel &&
                            <div style={{fontSize: 12}}>{(reactSelectProps.data[props.subLabel]) || ""}</div>) || null
                        }
                    </>
                }
            </components.Option>
        );
    };

    const value = () => {
        return data.filter((item: any) => {
            if (isMulti && field.value) {
                return (needObject) ?
                    field.value.map((val: any) => val[valueField]).includes(item[valueField])
                    : !!field.value.includes(item[valueField])
            } else {
                return (needObject) ? item[valueField] === field.value[valueField] : item[valueField] === field.value;
            }
        });
    };

    const getOptionLabel = (option: any) => (props.getOptionLabel && props.getOptionLabel(option)) || option[props.labelField];
    const getOptionValue = (option: any) => (props.getOptionValue && props.getOptionValue(option)) || option[props.valueField];

    return (
        <FormGroup>
            <FieldLabel {...props}/>
            <div style={props.style}>
                    <Select
                    className={props.className}
                    isClearable={fieldProps.isClearable}
                    options={data}
                    isLoading={isLoading}
                    getOptionLabel={getOptionLabel}
                    getOptionValue={getOptionValue}
                    onChange={handleOnChange}
                    value={value()}
                    isMulti={isMulti}
                    components={{Option}}
                    isDisabled={fieldProps.isDisabled}
                />
            </div>
            <ErrorMessage name={field.name} component="div" className="input-error"/>
        </FormGroup>
    );
}

InputReactSelect.defaultProps = {
    labelField: 'label',
    valueField: 'value',
    needObject: false,
    options: [],
    isHidden: false,
    isClearable: true,
    className: '',
    style: {},
    isDisabled: false,
};
