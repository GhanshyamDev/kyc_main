import {Progress} from "reactstrap";
import React from "react";
import {FileUpload, UploadStatus} from "./Helper";
import './index.scss';

interface IProps {
    filesUploads: FileUpload[],
    removeFile: any,
    preview?: boolean,
}

export function FileList(props: IProps) {
    const {filesUploads, removeFile} = props;

    return (
        <div className="file-list">
            {filesUploads.map((fileUpload: FileUpload, index) => {
                let row_class = "info";
                row_class = fileUpload.status === UploadStatus.uploaded ? "success" : row_class;
                row_class = fileUpload.status === UploadStatus.fail ? "danger" : row_class;
                return (
                    <div className={`file-row ${row_class}`} key={index}>
                        {props.preview && <img src={URL.createObjectURL(fileUpload.fileObj)} alt={fileUpload.fileObj.name}/>}
                        <span className="ml-3">{fileUpload.fileObj.name}</span>
                        <i className="fa fa-trash trash-icon" aria-hidden="true"
                           onClick={() => removeFile(fileUpload)}/>
                        <Progress value={fileUpload.progress} className="mt-1"/>
                    </div>
                )
            })}
        </div>
    )
}
