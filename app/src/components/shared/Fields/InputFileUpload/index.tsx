import React, {useState} from "react";
import {FieldLabel} from "../FieldLabel";
import Dropzone from 'react-dropzone'
import {guid} from "../../../../util/Helpers";
import axios from "axios";
import {FileUpload, UploadStatus} from "./Helper";
import {FileList} from "./FileList";
import {Toastiy} from "../../../../util/Toast";
import {Button} from "reactstrap";

interface IProps {
    field: any,
    form: any,
    title: string,
    url: string,
    label?: string,
    isRequired?: boolean,
    accept?: string,
    multiple?: boolean,
    maxSize?: number,
    disabled: boolean,
    preview: boolean,
}

const CancelToken = axios.CancelToken;

export function InputFileUpload(props: IProps) {
    const [filesUploads, setFileUploads] = useState<FileUpload[]>([]);

    const onDrop = (items: File[]) => {
        const fileUploadsItems: FileUpload[] = items.map((item: File) => {
            return {
                id: guid(),
                fileObj: item,
                progress: 0,
                cancel: {},
                status: UploadStatus.pending
            }
        });

        setFileUploads([...filesUploads, ...fileUploadsItems]);
        uploadFile(fileUploadsItems);
    };

    const removeFile = (file: FileUpload) => {
        if (file?.cancel) {
            file.cancel()
        }
        const items = filesUploads.filter((item: FileUpload) => item.id !== file.id);
        setFileUploads(items);
    };

    const uploadFile = (fileUploadItems: FileUpload[]) => {
        const {field, form} = props;

        fileUploadItems = fileUploadItems.map((fileUpload: FileUpload) => {
            const formData = new FormData();
            fileUpload.status = UploadStatus.inProgress;
            formData.append(props.field.name, fileUpload.fileObj);
            axios.post(props.url, formData, {
                onUploadProgress: (e: any) => {
                    fileUpload.progress = (e.loaded / e.total) * 100;
                    fileUpload.status = UploadStatus.inProgress;
                    const items: FileUpload[] = fileUploadItems.map((item: FileUpload) => {
                        return (item.id === fileUpload.id && fileUpload) || item;
                    });
                    setFileUploads([...filesUploads, ...items]);
                },
                cancelToken: new CancelToken((e: any) => {
                    fileUpload.cancel = e;
                })
            }).then((response: any) => {
                fileUpload.progress = 100;
                fileUpload.status = UploadStatus.uploaded;
                if (props.multiple)
                    form.setFieldValue(field.name, [...field.value, response.data]);
                else
                    form.setFieldValue(field.name, response.data);

            }).catch((error: any) => {
                fileUpload.progress = 0;
                fileUpload.status = UploadStatus.fail;
                form.setFieldValue(field.name, "");
                Toastiy.error("Failed To Upload File.Try Later.");
            });

            return fileUpload;
        });
        setFileUploads([...filesUploads, ...fileUploadItems]);
    };

    return (
        <>
            <FieldLabel label={props.label} isRequired={props.isRequired}/>
            <Dropzone
                onDrop={onDrop}
                accept={props.accept}
                multiple={props.multiple}
                maxSize={props.maxSize}
                disabled={props.disabled}
            >
                {({getRootProps, getInputProps, isDragReject}) => (
                    <div {...getRootProps()}>
                        <input {...getInputProps()} />
                        {(isDragReject && <span className="input-error">File Not Accepted . Sorry!</span>)
                        || <Button color="primary" size="sm">{props.title}</Button>}
                    </div>
                )}
            </Dropzone>
            <FileList filesUploads={filesUploads} removeFile={removeFile} preview={props.preview}/>
        </>
    )
}

InputFileUpload.defaultProps = {
    title: "Upload",
    isRequired: false,
    accept: "image/*",
    multiple: true,
    maxSize: 10 * 1000 * 1000, // 10Mb,
    disabled: false,
    preview: false,
};
