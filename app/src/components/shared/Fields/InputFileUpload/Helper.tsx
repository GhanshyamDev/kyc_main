export enum UploadStatus {
    pending = 0,
    inProgress = 2,
    uploaded = 1,
    fail = -1,
}

export interface FileUpload {
    id:string,
    fileObj:File,
    progress:number,
    cancel:any,
    status:UploadStatus,
}
