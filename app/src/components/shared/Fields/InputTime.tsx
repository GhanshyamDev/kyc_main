import React from 'react';
import moment from 'moment';
import {ErrorMessage, useField} from "formik";
import {FormGroup} from "reactstrap";
import {FieldLabel} from "./FieldLabel";
import DatePicker from "react-datepicker";

interface IProps {
    label?: string,
    field: any,
    form?: any,
    value: any,
    isRequired?: boolean,
    format?: string,
    className?: string,
}

const timeFormat = 'HH:mm:ss';

export function InputTime(props: IProps) {
    const [field, , helpers] = useField(props.field.name);
    const {
        form,
        format,
        ...fieldProps
    } = props;

    return (
        <FormGroup>
            <div>
                <FieldLabel {...props}/>
                <DatePicker
                    {...fieldProps}
                    className={`${props.className} w-100 form-control`}
                    showTimeSelect
                    showTimeSelectOnly
                    selected={(field.value && moment(field.value, timeFormat).toDate()) || null}
                    onChange={(value: any) => helpers.setValue(moment(value).format(timeFormat))}
                />
                <ErrorMessage name={field.name} component="div" className="input-error"/>
            </div>
        </FormGroup>
    );
}

InputTime.defaultProps = {
    format: timeFormat,
    isRequired: false,
    className: ''
};
