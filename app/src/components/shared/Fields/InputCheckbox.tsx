import React from 'react';
import Label from "reactstrap/lib/Label";
import FormGroup from "reactstrap/lib/FormGroup";
import Input from "reactstrap/lib/Input";
import {ErrorMessage, useField} from "formik";
import {FieldLabel} from "./FieldLabel";

interface IProps {
    label?: string,
    field: any,
    form?: any,
    labelField?: any,
    valueField?: any,
    options: any,
    isRequired?: boolean
}

export function InputCheckbox(props: IProps) {
    const [field, meta, helpers] = useField(props.field.name);
    const {
        form,
        isRequired,
        labelField,
        valueField,
        ...fieldProps
    } = props;

    const addOption = (selectedItem: any) => {
        const name = field.name;
        const selectedOptions = [...form.values[name], selectedItem.value];
        helpers.setValue(selectedOptions);
    };

    const deleteOption = (selectedItem: any) => {
        const name = field.name;
        helpers.setValue(form.values[name].filter((option: any) => {
            return option !== selectedItem.value
        }))
    };

    const handleOnChange = (selectedItem: any) => {
        const index = field.value.indexOf(selectedItem.value);
        index < 0 ? addOption(selectedItem) : deleteOption(selectedItem);
    };

    const selectedOptions = field.value || [];

    return (
        <div>
            <FieldLabel {...props}/>
            {
                props.options.map((item: any, index: number) => {
                    return (
                        <FormGroup key={field.name + '-' + index} check>
                            <Label check>
                                <Input
                                    value={item[valueField]}
                                    onChange={() => handleOnChange(item)}
                                    checked={(selectedOptions.includes(item.value))}
                                    invalid={Boolean(meta.touched && meta.error)}
                                    {...fieldProps}
                                />
                                {item[labelField]}
                            </Label>
                        </FormGroup>
                    );
                })
            }
            <ErrorMessage name={field.name} component="div" className="input-error"/>
        </div>
    );
}

InputCheckbox.defaultProps = {
    labelField: 'label',
    valueField: 'value',
    isRequired: false
};
