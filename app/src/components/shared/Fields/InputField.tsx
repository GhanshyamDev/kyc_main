import React from 'react';
import Input from "reactstrap/lib/Input";
import FormGroup from "reactstrap/lib/FormGroup";
import {ErrorMessage, useField} from "formik";
import {FieldLabel} from "./FieldLabel";

interface IProps {
    label?: string,
    field: any,
    form?: any,
    icon?: any,
    isRequired?: boolean
}

export function InputField(props: IProps) {
    const [field] = useField(props.field.name);
    const {
        form,
        isRequired,
        ...fieldProps
    } = props;
    return (
        <FormGroup>
            <FieldLabel {...props} />
            <Input
                name={field.name}
                value={(field.value != null) ? field.value : ''}
                onChange={field.onChange}
                {...fieldProps}
            />
            <ErrorMessage name={field.name} component="div" className="input-error"/>
        </FormGroup>
    );
}

InputField.defaultProps = {
    isRequired: false
};
