import React from 'react';
import {Input} from "reactstrap";
import {api} from "../../../util/Api";
import {FieldLabel} from "./FieldLabel";

interface IProps {
    onSelect?: any,
    options?: any,
    placeholder?: string,
    field: any,
    form?: any,
}

interface IState {
    isOpen: boolean,
    disabled: boolean,
    locations: any,
    focusLocation: any,
}

export class GooglePlaceApiAutoComplete extends React.Component<IProps, IState> {
    constructor(props: any) {
        super(props);
        this.state = {
            isOpen: false,
            locations: [],
            disabled: false,
            focusLocation: -1
        };
    }

    static defaultProps = {
        placeholder: 'Search Location'
    };

    handleOnSearch(search: any) {
        (search && search.length > 2 && api.googlePlaces.getPlaces({
            ...this.props.options,
            input: search
        }).then((response: any) => {
            const resources = response.data.predictions && response.data.predictions;
            resources && this.setState({locations: resources, isOpen: true});
        })) || this.setState({isOpen: false, locations: [], focusLocation: -1});
    };

    handleOnClear = () => {
        const {form, field} = this.props;
        form.setFieldValue(field.name, "");
        this.setState({locations: []});
    };

    onChange = (event: any) => {
        const {form, field} = this.props;
        form.setFieldValue(field.name, event.target.value);
        this.handleOnSearch(event.target.value);
    };

    handleOnSelect = (item: any) => {
        const {form, field, onSelect} = this.props;
        const terms = item.terms.slice(-3);
        const place = {
            placeObj: item,
            description: item.description,
            country: terms.pop().value || "",
            state: terms.pop().value || "",
            city: terms.pop().value || "",
        };
        onSelect(place);
        form.setFieldValue(field.name, item.description);
        this.setState({isOpen: false});
    };

    renderLocation = (locations: any) => {
        return locations.map((item: any, index: any) => {
            const focus = (this.state.focusLocation === index) ? " focus " : "";
            return (
                <div className={'place-row' + focus} key={"row-" + index}
                     onClick={() => this.handleOnSelect(item)}>
                    <div className="p-2">
                        <i className="fas fa-map-marker-alt"/>
                        <span className="address">{item.description}</span>
                    </div>
                </div>
            )
        })
    };

    handleOnKeyPress = (event: any) => {
        const {locations, focusLocation} = this.state;
        if (event && event.key === "ArrowDown") {
            (focusLocation < locations.length) && this.setState({focusLocation: this.state.focusLocation + 1})
        }
        if (event && event.key === "ArrowUp") {
            (focusLocation >= -1) && this.setState({focusLocation: this.state.focusLocation - 1})
        }
        if (event && event.key === "Enter") {
            if (focusLocation >= 0 && locations) {
                this.handleOnSelect(locations[focusLocation]);
                this.setState({focusLocation: -1});
            }
            event.preventDefault();
            return false;
        }
    };

    render() {
        const {field, placeholder} = this.props;
        const {isOpen, locations} = this.state;
        return (
            <div className="google-search-box">
                <FieldLabel {...this.props} />
                <Input
                    type="textarea"
                    placeholder={placeholder}
                    onChange={this.onChange}
                    onKeyDown={this.handleOnKeyPress}
                    value={field.value}
                    className='form-control mb-2'
                />
                <i className='fas fa-times close-icon' onClick={this.handleOnClear}/>
                {isOpen && locations && <div className="rounded border">
                    {this.renderLocation(locations)}
                </div>}
            </div>
        );
    }
}
