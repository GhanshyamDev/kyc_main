import React from 'react';
import {FormGroup, Input, Label} from "reactstrap";
import {ErrorMessage} from "formik";

interface IProps {
    label?: string,
    field: any,
    form?: any,
    isHidden?: boolean
}

export class InputCheckboxSingle extends React.Component<IProps, {}> {
    static defaultProps = {
        isHidden: false
    };

    handleOnChange = (e: any) => {
        const {
            field,
            form
        } = this.props;
        form.setFieldValue(field.name, e.target.checked);
    };

    render() {
        if (this.props.isHidden) {
            return null;
        }

        const {
            field,
        } = this.props;

        return (
            <FormGroup>
                <FormGroup check>
                    <Label check>
                        <Input
                            type="checkbox"
                            style={{opacity: 1, marginTop: "4px"}}
                            checked={!!(field.value)}
                            onChange={this.handleOnChange}
                        />{' '}
                        {this.props.label}
                    </Label>
                </FormGroup>
                {/*{meta.error && <div className="input-error">{meta.error}</div>}*/}
                <ErrorMessage name={field.name} component="div" className="input-error"/>
            </FormGroup>
        );
    }
}