import React from 'react';
import Input from "reactstrap/lib/Input";
import {FormGroup} from "reactstrap";
import {ErrorMessage} from "formik";
import {FieldLabel} from "./FieldLabel";

interface IProps {
    label?: string,
    field: any,
    form?: any,
    options: any,
    isRequired?: boolean,
    api?: any,
    labelField?: any,
    valueField?: any,
    single?: boolean,
    size?: any
}

interface IState {
    options: any,
    isLoading: boolean
}

export class InputMultipleSelect extends React.Component<IProps, IState> {
    static defaultProps = {
        labelField: 'label',
        valueField: 'value',
        isRequired: false,
        single: false
    };

    constructor(props: any) {
        super(props);
        this.state = {
            options: [],
            isLoading: false,
        };
    }

    componentDidMount(): void {
        if (this.props.api) {
            this.setState({isLoading: true});
            this.props.api().then((response: any) => {
                this.setState({
                    isLoading: false,
                    options: response.data.data
                });
            });
        }
    }

    getValue = () => {
        const {field} = this.props;
        return [field.value];
    };

    handleOnChange = (event: any) => {
        const {field, form, single} = this.props;
        let value = event.target.value;
        const oldValues = this.getValue();
        const values = oldValues.filter((item: any) => {
            return item !== value;
        });
        if (value) {
            //currently support only single
            /*if (!oldValues.includes(value)) {
                if (!single) {
                    form.setFieldValue(field.name, [...values, value]);
                } else {
                    form.setFieldValue(field.name, [value]);
                }
            } else {
                form.setFieldValue(field.name, [...values]);
            }*/
            form.setFieldValue(field.name, value);
        } else {
            form.setFieldValue(field.name, "");
        }
    };

    render() {
        const {
            field,
            form,
            isRequired,
            labelField,
            valueField,
            single,
            api,
            ...props
        } = this.props;

        const options = (api) ? this.state.options : props.options;

        return (
            <FormGroup>
                <FieldLabel {...this.props}/>
                <div>
                    <Input
                        {...field}
                        {...props}
                        type="select"
                        multiple={true}
                        value={this.getValue()}
                        onChange={this.handleOnChange}
                    >
                        {
                            (this.state.isLoading) ?
                                <option value="">
                                    Loading...
                                </option>
                                :
                                options.map((item: any, index: number) => {
                                    return (
                                        <option key={(index + '_' + item.value)} value={item[valueField]}>
                                            {item[labelField]}
                                        </option>
                                    )
                                })
                        }
                    </Input>
                </div>
                <ErrorMessage name={field.name} component="div" className="input-error"/>
            </FormGroup>
        );
    }

}