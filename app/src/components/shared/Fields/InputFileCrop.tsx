import React from 'react';

import {ErrorMessage} from "formik";
import FormGroup from "reactstrap/lib/FormGroup";
import {FieldLabel} from "./FieldLabel";
import ModalWrp from "../ModalWrp";
//import CropImageModal from "./CropImageModal";
import {Toastiy} from "../../../util/Toast";

interface IProps {
    label?: string,
    buttonLabel?: string,
    action: string,
    field: any,
    form?: any,
    value: any,
    isRequired?: boolean,
    desc?: string,
    width: number,
    height: number,
    url?: any
}

interface IState {
    fileList: any
    showFormModal: boolean,
    src: any
}

export class InputFileCrop extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);
        this.state = {
            fileList: [],
            showFormModal: false,
            src: ''
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps: Readonly<IProps>, nextContext: any): void {
        if (nextProps.field.value && nextProps.field.value) {
            const src = nextProps.field.value;
            this.setState({
                fileList: [{
                    uid: '-1',
                    name: 'Uploaded Logo',
                    status: 'done',
                    url: src,
                }],
                src: src,
            });
        }
    }

    handleOnRemove = (file: any) => {
        this.props.form.setFieldValue(this.props.field.name, null);
    };

    toggleModal = () => {
        this.setState({
            showFormModal: !this.state.showFormModal
        });
    };

    handleOnChange = (info: any) => {
        this.setState({
            fileList: info.fileList
        });
        if (info.file.status === 'uploading') {
            //console.log(info.file, info.fileList);
        } else if (info.file.status === 'done') {
            Toastiy.success(`${info.file.name} file uploaded successfully`);
            if (info.file.originFileObj) {
                const reader = new FileReader();
                reader.addEventListener('load', () => {
                    this.setState({ src: reader.result });
                    this.props.form.setFieldValue(this.props.field.name, reader.result);
                });
                reader.readAsDataURL(info.file.originFileObj);
            }
        } else if (info.file.status === 'error') {
            Toastiy.error(`${info.file.name} file upload failed.`);
        } else if (info.file.status === 'removed') {
            //debugger;
            this.setState({
                fileList: this.state.fileList.filter((file: any) => file.uid === info.file.uid)
            });
        }
    };

    handleOnCropComplete = (response: any) => {
        if (response && response.blob) {
            const reader = new FileReader();
            reader.addEventListener('load', () => {
                this.setState({ src: reader.result });
                this.props.form.setFieldValue(this.props.field.name, reader.result);
            });
            reader.readAsDataURL(response.blob);
        }
    };

    dummyAction(file: any) {
        return Promise.resolve(file);
    }

    render() {
        const props = {
            name: this.props.field.name,
            headers: {
                authorization: 'authorization-text',
            },
            onRemove: this.handleOnRemove,
            onChange: this.handleOnChange,
            buttonLabel: "Select File",
            ...this.props,
        };

        return (
            <FormGroup>
                <FieldLabel {...props} />
                {/*<Upload
                    fileList={this.state.fileList}
                    className="p-2"
                    showUploadList={{
                        showPreviewIcon: false
                    }}
                    {...props}
                    action={process.env.REACT_APP_API_URL+"upload/dummyFileUpload"}
                    name="file"
                >
                    <Button>
                        <Icon type="upload" /> {props.buttonLabel}
                    </Button>
                </Upload>*/}
                {this.state.src && (this.state.fileList) && (this.state.fileList.length > 0) &&
                    <>
                        {/*<Button onClick={this.toggleModal}>
                            <i className="fas fa-crop" />
                        </Button>
                        <div className="p-2">
                            <img
                                src={this.state.src}
                                alt='not found'
                                width={this.props.width}
                                height={this.props.height}
                            //style={{border: '1px solid'}}
                            />
                        </div>*/}
                    </>}
                {props.desc && <div><span>{props.desc}</span></div>}
                <ErrorMessage name={props.field.name} component="div" className="input-error" />
                <ModalWrp
                    title='Crop Image'
                    showModal={this.state.showFormModal}
                    onCancel={this.toggleModal}>

                    {/*{this.state.src &&
                        <CropImageModal
                            onCancel={this.toggleModal}
                            src={this.state.src}
                            width={this.props.width}
                            height={this.props.height}
                            onCropComplete={this.handleOnCropComplete}
                        />}*/}

                </ModalWrp>
            </FormGroup>
        );
    }
}
