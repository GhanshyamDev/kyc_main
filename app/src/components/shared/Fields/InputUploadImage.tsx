import React, {useEffect, useState} from 'react';
import {ErrorMessage, useField} from "formik";
import FormGroup from "reactstrap/lib/FormGroup";
import {FieldLabel} from "./FieldLabel";
import Label from "reactstrap/lib/Label";
import {StoneUpload} from "../StoneUpload";

interface IProps {
    label?: string,
    postUrl: string,
    previewHeight: string,
    field: any,
    value: any,
    isRequired?: boolean,
    desc?: string,
    cameraPictureWidth?: string,
    captureByCamera?: boolean,
    authRequired?: boolean,
    extraData?: any,
    onImageUpload?: any,
    isDisabled: boolean,
    candidateSelfie: boolean,
    placeholder?: string,
    imageStatus?: any,
}

export function InputUploadImage(props: IProps) {
    const [field, meta, helpers] = useField(props.field.name);

    useEffect(() => {

    }, []);

    const handleOnUploaded = (data: any) => {
        helpers.setValue(data);
        if (props.onImageUpload !== undefined) {
            props.onImageUpload(data);
        }
    };

    return (
        <FormGroup>
            <FieldLabel {...props}/>
            <StoneUpload
                field={field.name}
                postUrl={props.postUrl}
                showPreview={true}
                onUploaded={handleOnUploaded}
                previewHeight={props.previewHeight}
                previewUrl={(field.value && field.value.url) ? field.value.url : null}
                fileTypes={['image/png', 'image/jpeg']}
                cameraPictureWidth={props.cameraPictureWidth}
                captureByCamera={props.captureByCamera}
                extraData={props.extraData}
                authRequired={props.authRequired}
                isDisabled={props.isDisabled}
                candidateSelfie={props.candidateSelfie}
                placeholder={props.placeholder}
                imageStatus={props.imageStatus}
            />
            {props.desc && <div><Label>{props.desc}</Label></div>}
            {meta.error && <div className="input-error">{meta.error}</div>}
            {/*<ErrorMessage name={field.name} component="div" className="input-error"/>*/}
        </FormGroup>
    );
}
