import React from 'react';
import {FormGroup} from "reactstrap";
import {ErrorMessage} from "formik";
import {FieldLabel} from "./FieldLabel";
import AsyncSelect from "react-select";

interface IInputReactAsyncSelectProps {
    label?: any,
    field: any,
    form?: any,
    options: any,
    isRequired?: any,
    labelField?: any,
    valueField?: any,
    isMulti?: boolean
}

interface IInputReactAsyncSelectState {
    isMenuOpen: boolean
}


export class InputAsyncReactSelect extends React.Component<IInputReactAsyncSelectProps, IInputReactAsyncSelectState> {
    static defaultProps = {
        labelField: 'label',
        valueField: 'value',
        isMulti: false
    };

    constructor(props: any) {
        super(props);
        this.state = {
            isMenuOpen: false
        }
    }


    handleInputChange = (newValue: string) => {
        return newValue.replace(/\W/g, '');
    };

    setValues(value: any) {
        return {
            [this.props.valueField]: value[this.props.valueField],
            [this.props.labelField]: value[this.props.labelField]
        }
    }

    handleOnChange = (item: any) => {
        const {
            field,
            form
        } = this.props;

        (this.props.isMulti) ?
            form.setFieldValue(field.name, item.map((value: any) => this.setValues(value)))
            : form.setFieldValue(field.name, this.setValues(item));

        this.setState({
            isMenuOpen: false
        })
    };


    render() {
        const {
            field,
            form,
            ...props
        } = this.props;

        const loadOptions = (inputValue: any, callback: any) => {
            this.setState({
                isMenuOpen: !!(inputValue.length)
            });

            return props.options({search: inputValue}).then((response: any) => {
                callback(response.data);
            });
        };

        const selectedOption = (field.value) ? field.value : null;

        return (
            <FormGroup>
                <FieldLabel {...props}/>
                <div>
                    <AsyncSelect
                        {...field}
                        {...props}
                        value={selectedOption}
                        cacheOptions
                        loadOptions={loadOptions}
                        getOptionValue={(option: any) => option[this.props.valueField]}
                        getOptionLabel={(option: any) => option[props.labelField]}
                        onChange={this.handleOnChange}
                        noOptionsMessage={() => "Search..."}
                        onInputChange={this.handleInputChange}
                        isClearable={true}
                        menuIsOpen={this.state.isMenuOpen}
                        defaultOptions
                    >
                    </AsyncSelect>
                </div>
                <ErrorMessage name={field.name} component="div" className="input-error"/>
            </FormGroup>
        );
    }

}