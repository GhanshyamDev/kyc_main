import React from 'react';
import Label from "reactstrap/lib/Label";

interface IFieldLabel {
    label?: any,
    isRequired?: any
}

export const FieldLabel: React.FC<IFieldLabel> = (props) => {
    return (
        <div>
            {(props.label) ?
                <Label>{props.label}{(props.isRequired) ? (<span className="text-danger">*</span>) : ''}</Label> : ''}
        </div>
    );
};