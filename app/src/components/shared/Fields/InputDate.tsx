import React from 'react';
import moment from 'moment';
import {FormGroup} from "reactstrap";
import {ErrorMessage, useField} from "formik";
import {FieldLabel} from "./FieldLabel";
import DatePicker from "react-datepicker";

interface IProps {
    label?: string,
    field: any,
    form?: any,
    value: any,
    isRequired?: boolean,
    format?: string,
    showTimeInput?: boolean,
    dateTimeFormat: any,
    className?: string,
}

const dateFormat = 'YYYY-MM-DD';
const timeFormat = 'H:mm:ss';
const dateTimeFormat = dateFormat + ' ' + timeFormat;

export function InputDate(props: IProps) {
    const [field, , helpers] = useField(props.field.name);
    const format = (props.showTimeInput) ? props.dateTimeFormat : props.format;

    const getPickerForm = (format: any) => {
        return [...format]
            .map((item: any) => {
                item = (item == "D" && "d") || item;
                item = (item == "Y" && "y") || item;
                return item;
            }).join('');
    };

    return (
        <FormGroup>
            <FieldLabel {...props} />
            <div>
                <DatePicker
                    showTimeInput={props.showTimeInput}
                    selected={(field.value && moment(field.value, format).toDate()) || null}
                    isClearable
                    dateFormat={getPickerForm(format)}
                    onChange={(value: any) => helpers.setValue((value) ? moment(value).format(format) : '')}
                    className={`${props.className} form-control w-100`}
                />
                <ErrorMessage name={field.name} component="div" className="input-error"/>
            </div>
        </FormGroup>
    );
}

InputDate.defaultProps = {
    showTimeInput: false,
    format: dateFormat,
    dateTimeFormat: dateTimeFormat,
    className: ''
};
