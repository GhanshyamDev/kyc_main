import React from 'react';
import {FormGroup} from "reactstrap";
import {ErrorMessage, useField} from "formik";
import {FieldLabel} from "./FieldLabel";
import {default as ReactSwitch} from "react-switch";

interface IProps {
    label?: string,
    field: any,
    form?: any,
    onChange?: (flag: boolean) => void
}

export function InputSwitch(props: IProps) {
    const [field, , helpers] = useField(props.field.name);

    return (
        <FormGroup>
            <FieldLabel {...props} />
            <div>
                <ReactSwitch
                    checked={!!(field.value)}
                    onChange={(value: boolean) => helpers.setValue(value)}
                />
                <ErrorMessage name={field.name} component="div" className="input-error"/>
            </div>
        </FormGroup>
    );
}

InputSwitch.defaultProps = {
    needBoolean: false,
    isHidden: false
};
