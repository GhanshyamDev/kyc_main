import React from 'react';
import Label from "reactstrap/lib/Label";
import FormGroup from "reactstrap/lib/FormGroup";
import Input from "reactstrap/lib/Input";
import {ErrorMessage, useField} from "formik";
import {FieldLabel} from "./FieldLabel";

interface IProps {
    label?: string,
    field: any,
    form?: any,
    options: any,
    labelField?: any,
    valueField?: any,
    isRequired?: boolean,
}

export function InputRadio(props: IProps) {
    const [field,, helpers] = useField(props.field.name);
    const {
        form,
        isRequired,
        labelField,
        valueField,
        ...fieldProps
    } = props;

    return (
        <div>
            <FieldLabel {...props}/>
            {
                props.options.map((item: any, index: number) => {
                    return (
                        <FormGroup key={field.name + '-' + index} check>
                            <Label check>
                                <Input
                                    value={item[valueField]}
                                    onChange={() => helpers.setValue(item[valueField])}
                                    checked={item[valueField] === field.value}
                                    {...fieldProps}
                                />
                                {item[labelField]}
                            </Label>
                        </FormGroup>
                    );
                })
            }
            <ErrorMessage name={field.name} component="div" className="input-error"/>
        </div>
    );

}

InputRadio.defaultProps = {
    labelField: 'label',
    valueField: 'value',
    isRequired: false
};
