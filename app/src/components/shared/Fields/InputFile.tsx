import React, {useEffect, useState} from 'react';
import {ErrorMessage} from "formik";
import FormGroup from "reactstrap/lib/FormGroup";
import {FieldLabel} from "./FieldLabel";
import {ErrorMessage as Em, getUrlFromPath} from "../../../util/Helpers";
import Label from "reactstrap/lib/Label";
import {Toastiy} from "../../../util/Toast";
import {Button} from "reactstrap";

interface IProps {
    label?: string,
    buttonLabel?: string,
    action: string,
    field: any,
    form?: any,
    value: any,
    isRequired?: boolean,
    desc?: string,
}

interface IFile {
    uid: string,
    name: string,
    status: string,
    url: string,
}

export function InputFile(props: IProps) {
    const [fileList, setFileList] = useState<IFile[]>([]);
    const {form, field, label} = props;

    useEffect(() => {
        if (field.value) {
            setFileList([{
                uid: '-1',
                name: 'Uploaded ' + label,
                status: 'done',
                url: getUrlFromPath(field.value),
            }]);
        }
    }, []);


    const handleOnRemove = (file: any) => {
        form.setFieldValue(field.name, null);
    };

    const setFiles = (fileList: any) => {
        let files = [...fileList];
        // 1. Limit the number of uploaded files
        // Only to show two recent uploaded files, and old ones will be replaced by the new
        files = files.slice(-1);

        // 2. Read from response and show file link
        files = files.map((file: any) => {
            file.name = "Uploaded Logo";
            if (file.status === "done") {
                // Component will show file.url as link
                file.url = getUrlFromPath(file.response);
                form.setFieldValue(field.name, file.response);
            }
            if (file.status === "error") {
                form.setFieldValue(field.name, "");
                if (file.response.errors && file.response.errors.length > 0) {
                    Toastiy.error(<Em errors={file.response.errors}/>);
                }
            }
            return file;
        });

        setFileList(files);
    };

    const handleOnChange = (info: any) => {
        setFiles(info.fileList);
        if (info.file.status !== 'uploading') {
            //console.log(info.file, info.fileList);
        }
        if (info.file.status === 'done') {
            Toastiy.success(`${info.file.name} file uploaded successfully`);
        } else if (info.file.status === 'error') {
            Toastiy.error(`${info.file.name} file uploaded successfully`);
        }
    };

    return (
        <FormGroup>
            <FieldLabel {...props}/>
            {/*<Upload {...props} fileList={fileList}>
                    <Button>
                        <Icon type="upload"/> {props.buttonLabel}
                    </Button>
            </Upload>*/}
            {props.desc && <div><Label>{props.desc}</Label></div>}
            <ErrorMessage name={props.field.name} component="div" className="input-error"/>
        </FormGroup>
    );
}
