import React from 'react';
import {ErrorMessage} from "formik";
import {FieldLabel} from "./FieldLabel";


interface IInputFileThumbnail {
    label?: any,
    field: any,
    form?: any,
    value: any,
    fileList: any,
    isRequired?: any
}

export class InputFileThumbnail extends React.Component<IInputFileThumbnail, {}> {
    constructor(props: any) {
        super(props);
        const fieldProps = {
            ...this.props
        };
        this.state = fieldProps.field.value;
    }

    handleCancel = () => {
        this.setState({previewVisible: false});
        this.setProps();
    };

    setProps() {
        const props = {
            ...this.props
        };
        const state = this.state;
        props.form.setFieldValue(props.field.name, state);
    }

    handlePreview = (file: any) => {
        this.setState({
            previewImage: file.url || file.thumbUrl,
            previewVisible: true,
        });
        //this.setProps();
    };

    handleData=(data: any)=>{
        //console.log(data);
    };

    handleChange = (file: any,fileList: any,event: any) => {
        //console.log(file);
    };

    render() {
        const props = {
            ...this.props
        };

        //const {previewVisible, previewImage} = props.field.value;
        return (
            <div>
                <FieldLabel {...props}/>

                <div className="clearfix">
                    {/*<Upload
                        {...props}
                        //fileList={fileList}
                        //data={this.handleData}
                        //onPreview={this.handlePreview}
                        //onChange={this.handleChange}
                    >
                        <Button>
                            <Icon type="upload"/> Upload
                        </Button>
                    </Upload>
                    <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
                        <img alt="example" style={{width: '100%'}} src={previewImage}/>
                    </Modal>*/}
                </div>
                <ErrorMessage name={props.field.name} component="div" className="input-error"/>
            </div>
        );
    }
}