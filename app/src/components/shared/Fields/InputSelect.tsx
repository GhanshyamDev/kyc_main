import React from 'react';
import Input from "reactstrap/lib/Input";
import {FormGroup} from "reactstrap";
import {ErrorMessage, useField} from "formik";
import {FieldLabel} from "./FieldLabel";

interface IProps {
    label?: string,
    field: any,
    form?: any,
    options: any,
    isRequired?: boolean,
    labelField?: any,
    valueField?: any
}

export function InputSelect(props: IProps) {
    const [field,, helpers] = useField(props.field.name);
    const {
        form,
        isRequired,
        labelField,
        valueField,
        ...fieldProps
    } = props;

    const options = [
        {
            'value': '',
            'label': 'Select Option'
        },
        ...props.options
    ];

    return (
        <FormGroup>
            <FieldLabel {...props}/>
            <div>
                <Input
                    type="select"
                    value={(field.value) ? field.value : ''}
                    onChange={(e: any) => {helpers.setValue(e.target.value);}}
                    {...fieldProps}
                >
                    {
                        options.map((item: any, index: number) => {
                            return (
                                <option key={(index + '_' + item.value)} value={item[valueField]}>
                                    {item[labelField]}
                                </option>
                            )
                        })
                    }

                </Input>
            </div>
            <ErrorMessage name={field.name} component="div" className="input-error"/>
        </FormGroup>
    );
}

InputSelect.defaultProps = {
    labelField: 'label',
    valueField: 'value',
    isRequired: false,
};
