import React, {useState} from 'react';

interface INavCollapseProps {
    title: string
    icon?: any,
    className?: string,
    children: any
}
export function CollapseNav(props: INavCollapseProps) {
    const [openMenu, setOpenMenu] = useState(false);

    const openMenuClassLi = (openMenu) ? "active" : "";
    const openMenuClass = (openMenu) ? "menu-expanded" : "";
    const subMenuClass = (openMenu) ? "show" : "";

    return (
        <li className={`nav-item ${openMenuClassLi} ${props.className}`}>
            <div onClick={() => setOpenMenu(!openMenu)} className={`nav-link ${openMenuClass}`}>
                <span className="menu-title">{props.title}</span>
                <i className="menu-arrow"/>
                {props.icon &&
                    <i className={`${props.icon} menu-icon`}/>
                }
            </div>
            <ul className={`nav flex-column sub-menu collapse ${subMenuClass}`}>
                {props.children}
            </ul>
        </li>
    );
}
