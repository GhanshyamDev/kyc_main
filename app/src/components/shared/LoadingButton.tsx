import React from 'react';
import {Button as BootstrapButton, Spinner} from 'reactstrap';

interface IButtonProps {
    color?: string,
    size?: string,
    type?: any,
    disabled: boolean,
    isLoading: boolean,
    onClick?: any,
    title?: any,
    className?: any,

    spinnerColor?: string,
    spinnerType?: string,
    spinnerSize?: string
}

interface IButtonState {
    isLoading: boolean,
    disabled: boolean
}

export class LoadingButton extends React.Component<IButtonProps, IButtonState> {
    static defaultProps = {
        color: 'primary',
        size: 'small',
        type: 'button',
        disabled: false,
        isLoading: false,
        spinnerColor: 'white',
        spinnerType: 'border',
        spinnerSize: 'sm'
    };

    constructor(props: any) {
        super(props);
        this.state = {
            isLoading: false,
            disabled: false
        };
    }

    componentDidMount(): void {
        this.setState({
            ...this.state,
            isLoading: this.props.isLoading,
            disabled: this.props.disabled
        })
    }

    componentWillReceiveProps(nextProps: Readonly<IButtonProps>, nextContext: any): void {
        if(this.props !== nextProps) {
            this.setState({
                ...this.state,
                isLoading: nextProps.isLoading,
                disabled: nextProps.disabled
            });
        }
    }

    render() {
        const {...props} = this.props;
        const buttonProps = {
            color: this.props.color,
            size: this.props.size,
            type: this.props.type
        };

        const {isLoading} = this.state;

        const spinner = (isLoading) ?
            <Spinner size={props.spinnerSize} color={props.spinnerColor} type={props.spinnerType}/> : ' ';
        return (
            <BootstrapButton {...buttonProps} className={this.props.className} disabled={this.state.disabled} onClick={this.props.onClick} title={props.title}>
                {spinner}
                {' '}
                {props.children}
            </BootstrapButton>
        )
    }
}