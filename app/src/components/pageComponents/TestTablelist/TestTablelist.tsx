import {TableWrp} from "../../shared/TableWrp/TableWrp";
import React, {useEffect, useState} from "react";
import { RouteComponentProps } from "react-router-dom";
import qs from "qs";
import { Form, Field, Formik } from "formik";
import {Button, Col, Row} from "reactstrap";
import {InputField} from "../../shared/Fields/InputField";
import {LoadingButton} from "../../shared/LoadingButton";
import {getQueryParams, IQueryParams, ITableData} from "../../shared/TableWrp/Helper";

interface ITestTableListProps {
    loading?: boolean,
    data: ITableData,
    useUrlQueryParams?: boolean | null,
    onEdit?: any,
    loadData: any,
    routeProps?: RouteComponentProps
}

export function TestTableList(props: ITestTableListProps) {
    const [queryParams, setQueryParams] = useState<IQueryParams>(getQueryParams({}));
    const [filters, setFilters] = useState<any>({});
    const [showFilterDrawer, setShowFilterDrawer] = useState(false);

    useEffect(() => {
        props.loadData(getQueryParams({}));
    }, []);

    const loadData = (queryParams: any) => {
        const params: IQueryParams = getQueryParams(queryParams);
        /*if (props.useUrlQueryParams) {
            props.routeProps.history.push({
                pathname: props.routeProps.location.pathname,
                search: qs.stringify(params),
                state: params
            });
        }*/
        setQueryParams(params);
        props.loadData(params);
    };

    const clearAllFilter = () => {
        const params: IQueryParams = getQueryParams({});
        setFilters({});
        setQueryParams(params);
        setShowFilterDrawer(false);
        loadData(params);
    };

    const getFilterForm = (formProps: any) => {
        return (
            <Form>
                <div>
                    <Row>
                        <Col>
                            <Field component={InputField}
                                   name="deviceTypeHashId"
                                   label="Device Type"
                            />
                        </Col>
                    </Row>
                </div>
                <hr />
                <div className="text-right">
                    <LoadingButton color="primary" size="sm" type='submit'
                            disabled={formProps.isSubmitting}
                            isLoading={formProps.isSubmitting}>
                        <i className="fas fa-save" />{' '}Apply
                    </LoadingButton>{' '}
                    <Button color="secondary" size="sm"
                            onClick={clearAllFilter}>Clear All
                    </Button>
                </div>
            </Form>
        )
    };

    const handleOnSubmit = (formData: any, event: any) => {
        const params = {...queryParams, page: 1, filters: formData};
        setQueryParams(params);
        loadData(params);
        event.setSubmitting(false);
        setShowFilterDrawer(false);
    };

    const columns = [
        {
            dataField: 'name',
            text: 'Name',
        }, {
            dataField: 'class',
            text: 'Class',
            sort: true
        }
    ];

    return (
        <TableWrp
            rowKey="id"
            title="Students"
            filterTitle="Filters"
            loading={props.loading}
            data={props.data}
            columns={columns}
            onChange={loadData}
            queryParams={queryParams}
            filterForm={() => {
                return (
                    <>
                        <Formik
                            enableReinitialize={true}
                            initialValues={filters}
                            onSubmit={handleOnSubmit}
                        >{(props: any) => getFilterForm(props)}
                        </Formik>
                    </>
                )
            }}
            showFilterButton
            showFilterPanel={showFilterDrawer}
            onShowFilterPanel={setShowFilterDrawer}
        />
    )
}
