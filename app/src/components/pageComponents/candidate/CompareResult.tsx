import React, {useEffect, useState} from 'react';
import {Button, Card, CardBody, Col, Row} from 'reactstrap';
import {api} from "../../../util/Api";
import {UiAction} from "../../../store/UiState";
import {useDispatch} from "react-redux";
import SimpleTableWrp from "../../shared/SimpleTableWrp";
import {
    displayCandidateStatus,
    displayCandidateStatusColor,
    getBase64FromImageUrl,
    utcDateToLocalDate
} from "../../../util/Helpers";
import AppConstants from "../../../util/AppConstants";
import pdfMake from 'pdfmake/build/pdfmake';
import moment from "moment";

interface IProps {
    id: string | null,
    useUrlQueryParams?: boolean | null,
    onEdit?: any,
    onDelete?: any
    onCancel?: any
}

export function CompareResult(props: IProps) {
    const dispatch = useDispatch();
    const [candidate, setCandidate] = useState<any>(null);
    const [resultData, setResultData] = useState<any>([]);

    useEffect(() => {
        if (props.id) {
            api.candidate.getDetails(props.id).then((response: any) => {
                setCandidate(response.data.data);
            });
            loadData(props.id);
        }
    }, []);

    const loadData = (id: any) => {
        dispatch(UiAction.showPageLoading());
        api.rekognitionResultLog.getResultLogByCandidate(props.id).then((response: any) => {
            setResultData(response.data.data);
        }).finally(() => dispatch(UiAction.hidePageLoading()));
    };

    const markAsCompleted = () => {
        api.candidate.postMarkCompleted({candidate_id: props.id}).then((response: any) => {
        }).finally(() => {
            dispatch(UiAction.hidePageLoading());
        });
    };

    const generatePdf = () => {
        let rowData = resultData.map((item: any) => {
            return [
                {text: utcDateToLocalDate(item.compareDate, AppConstants.SERVER_DATE_FORMAT, AppConstants.AM_PM_FORMAT)},
                {text: item.imageType},
                {stack: [{text: item.status, background: displayCandidateStatusColor(item.status)}]},
                {stack: [{image: item.sourceFileUri, width: 150}]},
                {stack: [{image: item.targetFileUri,  width: 150}]}
            ]
        });
        pdfMake.createPdf({
            pageMargins: 20,
            styles: {
                header: {
                    fontSize: 16,
                    bold: true,
                    margin: [0, 0, 0, 8]
                },
                label: {
                    fontSize: 11,
                    bold: true,
                },
                subheader: {
                    fontSize: 14,
                    bold: true,
                    margin: [0, 10, 0, 3]
                },
                tableHeader: {
                    bold: true,
                    fontSize: 11,
                    color: 'black'
                },
                tableExample: {
                    fontSize: 11,
                    margin: [0, 5, 0, 15]
                },
            },
            content: [
                {
                    text: 'Candidate Identity Verification Report',
                    style: 'header',
                    alignment: 'center'
                },
                {
                    style: 'tableExample',
                    table: {
                        widths: ['*', '*'],
                        body: [
                            [{text: [{text: 'Organization: ', bold: true}, candidate.organization_name]}, {text: [{text: 'Date of Report: ', bold: true}, moment().format(AppConstants.AM_PM_FORMAT)]}],
                            [{text: [{text: 'Candidate ID No.: ', bold: true}, candidate.cid]}, {text: [{text: 'Job ID: ', bold: true}, candidate.job_id]}],
                            [{text: [{text: 'Candidate Name: ', bold: true}, `${candidate.first_name} ${candidate.last_name}`]}, {text: [{text: 'Result: ', bold: true}, {text: candidate.current_status, background: displayCandidateStatusColor(candidate.current_status)}]}],
                            [{text: [{text: 'Comment: ', bold: true}, candidate.comment], colSpan: 2}],
                        ]
                    },
                    layout: 'noBorders'
                },
                {
                    style: 'tableExample',
                    table: {
                        widths: ['*', '*', '*'],
                        body: [
                            [{text: 'Selfie', style: "tableHeader"}, {text: 'Uploaded at', style: "tableHeader"}, {text: 'Location', style: "tableHeader"}],
                            [{text: 'Candidate Selfie'}, {text: utcDateToLocalDate(candidate.selfie_uploaded_on, AppConstants.SERVER_DATE_FORMAT, AppConstants.AM_PM_FORMAT)}, {text: candidate.selfie1_location}],
                            [{text: 'Onboarding Selfie'}, {text: utcDateToLocalDate(candidate.final_selfie_uploaded_on, AppConstants.SERVER_DATE_FORMAT, AppConstants.AM_PM_FORMAT)}, {text: candidate.validation_image_location}],
                        ]
                    },
                },
                {
                    style: "tableExample",
                    table: {
                        widths: ['auto', 'auto', 'auto', '*',  '*'],
                        dontBreakRows: true,
                        body: [
                            [
                                {text: 'Compared at', style: "tableHeader"},
                                {text: 'Step', style: "tableHeader"},
                                {text: 'Result', style: "tableHeader"},
                                {text: 'Source Image', style: "tableHeader"},
                                {text: 'Target Image', style: "tableHeader"},
                            ],
                            ...rowData,
                        ]
                    },
                    layout: {
                        fillColor: function (rowIndex, node, columnIndex) {
                            return (rowIndex % 2 === 0) ? '#CCCCCC' : null;
                        }
                    }
                }
            ]
        }).download(candidate.cid + "-" + moment().format('DD-MM-YYYY') + ".pdf");
    };

    const columns = [
        {
            dataField: 'compareDate',
            text: 'Date',
            sort: false,
            formatter: (cell: any, row: any) => utcDateToLocalDate(cell, AppConstants.SERVER_DATE_FORMAT, AppConstants.AM_PM_FORMAT)
        }, {
            dataField: 'sourceFileName',
            text: 'Source Image',
            sort: false,
            formatter: (cell: any, row: any) =>
                <div className="preview mb-2 d-inline w-100 p-0 text-center bg-light" style={{height: "192px"}}>
                    <img src={row.sourceFileUrl} alt="Source Image" className="img-fluid" style={{maxHeight: "192px"}}/>
                </div>
        }, {
            dataField: 'targetFileName',
            text: 'Target Image',
            sort: false,
            formatter: (cell: any, row: any) =>
                <div className="preview mb-2 d-inline w-100 p-0 text-center bg-light" style={{height: "192px"}}>
                    <img src={row.targetFileUrl} alt="Target Image" className="img-fluid" style={{maxHeight: "192px"}}/>
                </div>
        }, {
            dataField: 'imageType',
            text: 'Image Type',
            sort: false
        }, {
            dataField: 'compareResult',
            text: 'Result',
            sort: false
        }, {
            dataField: 'status',
            text: 'Status',
            sort: false,
            formatter: (cell: any, row: any) => displayCandidateStatus(cell)
        },
    ];

    return (
        <Card className='shadow-sm'>
            <CardBody>
                <Row>
                    <Col md={4}>
                        <label className="font-weight-semi-bold mb-1">Master Image Upload Location:</label>
                        <div>{candidate?.selfie1_location}</div>
                    </Col>
                    <Col md={4}>
                        <label className="font-weight-semi-bold mb-1">Final Validation Image Upload Location:</label>
                        <div>{candidate?.validation_image_location}</div>
                    </Col>
                    <Col md={4} className='text-md-right text-sm-left mt-2'>
                        <Button color="primary" size="sm" onClick={markAsCompleted}>
                            <i className='mdi mdi-check'/> Mark as Completed
                        </Button>
                        <Button color="primary" size="sm" onClick={generatePdf}>
                            <i className='mdi mdi-download'/> Download PDF
                        </Button>
                    </Col>
                </Row>
                <Row>
                    <Col md={12} className="mt-2">
                        <SimpleTableWrp
                            rowKey="name"
                            title="Results"
                            loading={true}
                            showSearch={false}
                            showPagination={false}
                            data={resultData}
                            columns={columns}
                        />
                    </Col>
                </Row>
            </CardBody>
        </Card>
    )
}
