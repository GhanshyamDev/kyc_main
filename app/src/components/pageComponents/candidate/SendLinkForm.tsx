import React, {useEffect, useState} from 'react';
import {Field, Form, Formik} from "formik";
import {Button, Card, CardBody, Col, ModalBody, ModalFooter, Row} from 'reactstrap';
import {LoadingButton} from "../../shared/LoadingButton";
import ContentLoading from "../../shared/ContentLoading";
import {useDispatch} from "react-redux";
import {InputReactSelect} from "../../shared/Fields/InputReactSelect";
import * as Yup from "yup";
import {api} from "../../../util/Api";
import {Toastiy} from "../../../util/Toast";
import {message} from "../../../locale/en/message";
import {UiAction} from "../../../store/UiState";
import {auth, IOrganization} from "../../../util/Auth";
import {getSendWebLinkOptions} from "../../../util/Helpers";

interface IProps {
    candidate: any
    webLinkType: string,
    onCancel: any,
}

export const sendWebLinkValidationSchema = Yup.object().shape({
    send_by: Yup.string().required().nullable(),
});

const setCompareResultFormData = (formData: any = {}) => {
    return {
        send_by: formData.send_by || '',
    }
};

export function SendLinkForm(props: IProps) {
    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState(false);
    const [formData, setFormData] = useState<any>(setCompareResultFormData());
    const [webLinkUrl, setWebLinkUrl] = useState<string>("");

    useEffect(() => {
    }, []);

    const getForm = (formProps: any) => {
        return (
            <Form>
                <ModalBody>
                    <Row>
                        <Col>
                            <Field component={InputReactSelect}
                                   name="send_by"
                                   api={getSendWebLinkOptions}
                                   label="Send By"
                                   isRequired
                                   onSelect={(value: any) => {
                                       if (value && value.key === "generate_link") {
                                           copyWebLink();
                                       }
                                   }}
                                   labelField="value"
                                   valueField="key"
                            />
                        </Col>
                    </Row>
                    {(formProps.values.send_by === "generate_link" && webLinkUrl) &&
                        <div>
                            <div className="bg-white d-inline-block p-2 border-0 text-truncate" style={{width: "90%"}}>
                                {webLinkUrl}
                            </div>
                            <button type="button"
                                    style={{marginTop: "-25px"}}
                                    className="btn btn-primary btn-sm" onClick={() => navigator.clipboard.writeText(webLinkUrl)}>
                                <i className="mdi mdi-content-copy" />
                            </button>
                        </div>
                    }
                </ModalBody>
                <ModalFooter>
                    {(formProps.values.send_by && formProps.values.send_by !== "generate_link") &&
                        <LoadingButton
                            color="primary"
                            size="sm"
                            type='submit'
                            disabled={formProps.isSubmitting}
                            isLoading={formProps.isSubmitting}
                        >
                            <i className="fas fa-save"/>{' '}Send
                        </LoadingButton>
                    }{' '}
                    <Button disabled={formProps.isSubmitting} color="secondary" size="sm" onClick={props.onCancel}>
                        Cancel
                    </Button>
                </ModalFooter>
            </Form>
        )
    };

    const copyWebLink = () => {
        const data = {
            link_type: props.webLinkType,
            send_by: "generate_link",
            candidate_xid: props.candidate.candidate_xid,
        };
        dispatch(UiAction.showPageLoading());
        api.webLink.postSend(data).then((response: any) => {
            const token = response.data.data.link_token;
            const linkUrl = `${process.env.REACT_APP_URL}ri/wl/c/${token}`;
            navigator.clipboard.writeText(linkUrl);
            setWebLinkUrl(linkUrl);
        }).finally(() => {
            dispatch(UiAction.hidePageLoading());
        });
    };

    const handleOnSubmit = (formData: any, event: any) => {
        const data = {
            ...formData,
            link_type: props.webLinkType,
            candidate_xid: props.candidate.candidate_xid,
        };
        dispatch(UiAction.showPageLoading());
        api.webLink.postSend(data).then((response: any) => {
            if (data.send_by === "send_email" || data.send_by === "send_sms") {
                const msg = (data.send_by === "send_email")
                    ? message.webLinkEmailSent.replace('{email}', props.candidate.email)
                    : message.webLinkSMSSent.replace('{phone_number}', props.candidate.phone_number)
                Toastiy.success(msg);
                setWebLinkUrl("");
                props.onCancel();
            }
            if (data.send_by === "generate_link") {
                const token = response.data.data.link_token;
                const linkUrl = `${process.env.REACT_APP_URL}ri/wl/c/${token}`;
                navigator.clipboard.writeText(linkUrl);
                setWebLinkUrl(linkUrl);
            }
        }).finally(() => {
            dispatch(UiAction.hidePageLoading());
            event.setSubmitting(false);
        });
    };

    return (
        <>
            <ContentLoading show={isLoading}/>
            <Formik
                enableReinitialize={true}
                initialValues={formData}
                validationSchema={sendWebLinkValidationSchema}
                onSubmit={handleOnSubmit}
            >
                {(props) => getForm(props)}
            </Formik>
        </>
    );
}
