import React, {useState} from "react";
import {Card, CardBody, Col, Nav, NavItem, NavLink, Row, TabContent, Table, TabPane} from "reactstrap";
import {CandidateForm} from "./CandidateForm";
import {CandidateImageForm} from "./CandidateImageForm";
import {CompareResult} from "./CompareResult";
import classNames from "classnames";

interface IProps {
    id: string | null,
    onCancel: any,
    loadData?: any
}

export default function CandidateTabs(props: IProps) {
    const [activeTab, setActiveTab] = useState<"formTab" |  "imageTab" | "resultTab">("formTab");

    return (
        <>
            <Nav tabs className={'drawer-tabs'}>
                <NavItem>
                    <NavLink className={classNames({ active: activeTab === 'formTab' })} onClick={() => setActiveTab('formTab')}>
                        Details
                    </NavLink>
                </NavItem>
                {props.id &&
                (
                <>
                    <NavItem>
                        <NavLink className={classNames({ active: activeTab === 'imageTab' })} onClick={() => setActiveTab('imageTab')}>
                            Images
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink className={classNames({ active: activeTab === 'resultTab' })} onClick={() => setActiveTab('resultTab')}>
                            Result
                        </NavLink>
                    </NavItem>
                </>
                )
                }
            </Nav>
            <TabContent activeTab={activeTab} className={'drawer-content'}>
                <TabPane tabId='formTab'>
                    {(activeTab === 'formTab') &&
                    <CandidateForm
                        id={props.id}
                        onCancel={props.onCancel}
                        loadData={props.loadData}
                        onSave={() => setActiveTab('imageTab')}
                    />
                    }
                </TabPane>
                <TabPane tabId='imageTab'>
                    {(activeTab === 'imageTab') &&
                    <CandidateImageForm
                        id={props.id}
                        onCancel={props.onCancel}
                        loadData={props.loadData}
                        onCompare={() => setActiveTab('resultTab')}/>
                    }
                </TabPane>
                <TabPane tabId='resultTab'>
                    {(activeTab === 'resultTab') &&
                    <CompareResult id={props.id} onCancel={props.onCancel} useUrlQueryParams={false} />
                    }
                </TabPane>
            </TabContent>
        </>
    );
}
