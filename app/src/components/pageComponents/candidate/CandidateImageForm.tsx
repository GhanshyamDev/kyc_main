import React, {useEffect, useState} from 'react';
import {Field, Form, Formik} from "formik";
import {Badge, Button, Card, CardBody, Col, Row} from 'reactstrap';
import {api} from "../../../util/Api";
import {LoadingButton} from "../../shared/LoadingButton";
import ContentLoading from "../../shared/ContentLoading";
import {UiAction} from "../../../store/UiState";
import {useDispatch} from "react-redux";
import {InputUploadImage} from "../../shared/Fields/InputUploadImage";
import ModalWrp from "../../shared/ModalWrp";
import {SendLinkForm} from "./SendLinkForm";
import {displayCandidateStatus} from "../../../util/Helpers";

interface IProps {
    id: string | null,
    onCancel: any,
    loadData?: any,
    onCompare?: any
}

const emptyImage = {
    fileName: '',
    url: '',
    id: null,
    imageStatus:'',
    result: 0
}

const setCandidateImageFormData = (formData: any = {}) => {
    return {
        candidate_xid: formData.candidate_xid || null,
        completed_status: formData.completed_status || false,
        master_image: formData.master_image || emptyImage,
        step_1_image: formData.step_1_image || emptyImage,
        step_2_image: formData.step_2_image || emptyImage,
        step_3_image: formData.step_3_image || emptyImage,
        final_validation_image: formData.final_validation_image || emptyImage,
        master_image_changed: formData.master_image_changed || 0,
        step_1_image_changed: formData.step_1_image_changed || 0,
        step_2_image_changed: formData.step_2_image_changed || 0,
        step_3_image_changed: formData.step_3_image_changed || 0,
        final_validation_image_changed: formData.final_validation_image_changed || 0
    }
};

export function CandidateImageForm(props: IProps) {
    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState(false);
    const [formData, setFormData] = useState<any>(setCandidateImageFormData());
    const [showSendWebLinkModal, setShowSendWebLinkModal] = useState<boolean>(false);
    const [webLinkType, setWebLinkType] = useState<string>("");
    const [compareStatus, setCompareStatus] = useState(false);
    const [candidate, setCandidate] = useState<any>(null);

    useEffect(() => {
        if (props.id) {
            api.candidate.getDetails(props.id).then((response: any) => {
                setCandidate(response.data.data);
            });
        }
    }, []);

    useEffect(() => {
        if (props.id) {
            dispatch(UiAction.showPageLoading());
            getCandidateImages();
        }
    }, []);

    const onSendWebLink1 = () => {
        setWebLinkType("candidate_selfie_1");
        setShowSendWebLinkModal(true);
    };

    const onSendWebLink2 = () => {
        setWebLinkType("candidate_selfie_2");
        setShowSendWebLinkModal(true);
    };

    const getCandidateImages = () => {
        api.candidate.getImages(props.id).then((response: any) => {
            setFormData(setCandidateImageFormData(response.data));
            setCompareStatus(enableCompare(response.data));
        }).finally(() => dispatch(UiAction.hidePageLoading()));
    }

    const enableCompare = (imageData: any) => {
        return ((imageData.step_1_image.url !== '' || imageData.step_2_image.url !== ''|| imageData.step_3_image.url !== '' || imageData.final_validation_image.url !== '') && imageData.master_image_changed) ||
            (imageData.master_image.url !== '' && imageData.step_1_image_changed) ||
            (imageData.master_image.url !== '' && imageData.step_2_image_changed) ||
            (imageData.master_image.url !== '' && imageData.step_3_image_changed) ||
            (imageData.master_image.url !== '' && imageData.final_validation_image_changed);
    }

    const previewPlaceHolder = () => {
        return <div className="d-flex justify-content-center align-items-center" style={{height: "100%"}}>
            <div className="h3 text-light-gray">Send Weblink</div>
        </div>
    };

    const getForm = (formProps: any) => {
        return (
            <Form>
                <Card className="shadow-sm">
                    <CardBody>
                        <Row>
                            <Col md={3}>
                                <label className="font-weight-semi-bold">Master Image</label>
                                <div className="preview mb-1 w-100 p-2 rounded text-center bg-light" style={{height: (formData.master_image.url) ? "auto" : "192px"}}>
                                    {(formData.master_image.url)
                                        ? <img src={formData.master_image.url} className="img-fluid" alt="" style={{maxHeight: "192px"}} />
                                        : previewPlaceHolder()
                                    }
                                </div>
                                {formData.master_image.url === '' &&
                                    <button type="button" className="btn btn-primary mb-2" onClick={onSendWebLink1}>Send Web Link</button>
                                }
                            </Col>
                            <Col md={3}>
                                <label className="font-weight-semi-bold">Final Validation Image</label>
                                <div className="preview mb-1 w-100 p-2 rounded text-center bg-light" style={{height: (formData.final_validation_image.url) ? "auto" : "192px"}}>
                                {formData.final_validation_image.url
                                    ? <>
                                        <img src={formData.final_validation_image.url} alt="" className="img-fluid" style={{maxHeight: "192px"}}/>
                                      </>
                                    : previewPlaceHolder()
                                }
                                </div>
                                {formData.final_validation_image.url &&
                                    <div className="text-right">
                                        {displayCandidateStatus(formData.final_validation_image.imageStatus)}
                                    </div>
                                }
                                {formData.final_validation_image.url === '' &&
                                    <button type="button" className="btn btn-primary mb-2" onClick={onSendWebLink2}>Send Web Link</button>
                                }
                            </Col>
                            <Col md={6}>
                            </Col>
                            <Col md={3} className="mt-3">
                                <Field component={InputUploadImage}
                                       name="step_1_image"
                                       postUrl={`${process.env.REACT_APP_API_URL}file/upload-image/` + props.id}
                                       previewHeight="192px"
                                       label="Step 1 Image"
                                       isDisabled={formData.completed_status}
                                       imageStatus={displayCandidateStatus(formData.step_1_image.imageStatus)}
                                       onImageUpload={(data: any) => imageUploaded(data, "step_1_image")}
                                />
                            </Col>
                            <Col md={3} className="mt-3">
                                <Field component={InputUploadImage}
                                       name="step_2_image"
                                       postUrl={`${process.env.REACT_APP_API_URL}file/upload-image/` + props.id}
                                       previewHeight="192px"
                                       label="Step 2 Image"
                                       isDisabled={formData.completed_status}
                                       imageStatus={displayCandidateStatus(formData.step_2_image.imageStatus)}
                                       onImageUpload={(data: any) => imageUploaded(data, "step_2_image")}
                                />
                            </Col>
                            <Col md={3} className="mt-3">
                                <Field component={InputUploadImage}
                                       name="step_3_image"
                                       postUrl={`${process.env.REACT_APP_API_URL}file/upload-image/` + props.id}
                                       previewHeight="192px"
                                       label="Step 3 Image"
                                       isDisabled={formData.completed_status}
                                       imageStatus={displayCandidateStatus(formData.step_3_image.imageStatus)}
                                       onImageUpload={(data: any) => imageUploaded(data, "step_3_image")}
                                />
                            </Col>
                        </Row>
                        <hr/>
                    </CardBody>
                </Card>
                <div className="drawer-footer">
                    {!formData.completed_status &&
                    <>
                        <LoadingButton
                            color="primary"
                            size="sm"
                            type='submit'
                            disabled={formProps.isSubmitting || !compareStatus}
                            isLoading={formProps.isSubmitting}
                        >
                            {' '}Compare
                        </LoadingButton>{' '}
                        <Button disabled={formProps.isSubmitting} color="secondary" size="sm" onClick={props.onCancel}>
                            Cancel
                        </Button>
                    </>
                    }
                </div>
            </Form>
        )
    };

    const imageUploaded = (uploadedImage: any, imageType:string) => {
        api.candidate.getImages(props.id).then((response: any) => {
            setFormData(setCandidateImageFormData(response.data));
            setCompareStatus(enableCompare(response.data));
        }).finally(() => dispatch(UiAction.hidePageLoading()));
    }

    const add = (formData: any, event: any) => {

    };

    const edit = (formData: any, event: any) => {

    };

    const handleOnSubmit = (formData: any, event: any) => {
        api.candidate.postCompare(props.id).then((response: any) => {
            getCandidateImages();
            props.onCompare();
        }).finally(() => {
            event.setSubmitting(false);
        });
    };

    return (
        <>
            <ModalWrp title="Send Web Link" size="md" showModal={showSendWebLinkModal} onCancel={() => setShowSendWebLinkModal(false)}>
                <SendLinkForm
                    candidate={{...formData, email: candidate?.email, phone_number: candidate?.phone_number, candidate_xid: props.id}}
                    webLinkType={webLinkType}
                    onCancel={() => setShowSendWebLinkModal(false)}
                />
            </ModalWrp>
            <ContentLoading show={isLoading}/>
            <Formik
                enableReinitialize={true}
                initialValues={formData}
                onSubmit={handleOnSubmit}
            >
                {(props) => getForm(props)}
            </Formik>
        </>
    );
}
