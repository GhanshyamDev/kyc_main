import React, {useEffect, useState} from 'react';
import {auth} from "../../../util/Auth";
import {Field, Form, Formik} from "formik";
import {Card, CardBody, Col, Row} from 'reactstrap';
import {InputField} from "../../shared/Fields/InputField";
import {api} from "../../../util/Api";
import {LoadingButton} from "../../shared/LoadingButton";
import ContentLoading from "../../shared/ContentLoading";
import {Toastiy} from "../../../util/Toast";
import {message} from "../../../locale/en/message";
import {InputReactSelect} from '../../shared/Fields/InputReactSelect';
import {useDispatch} from "react-redux";
import {UiAction} from "../../../store/UiState";
import {profileFormValidateSchema} from "../../../util/Validation";

interface IProps {
    onCancel?: any,
}

const setProfileFormData = (formData: any = {}) => {
    return {
        user_xid: formData.user_xid || null,
        first_name: formData.first_name || '',
        last_name: formData.last_name || '',
        email: formData.email || '',
        phone_number: formData.phone_number || '',
        //timezone_name: formData.timezone_name || '',
        password: '',
        password_confirmation: '',
    }
};

export function ProfileForm(props: IProps) {
    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState(false);
    const [formData, setFormData] = useState<any>(setProfileFormData());

    useEffect(() => {
        setFormData(setProfileFormData(auth.getUser()));
    }, []);

    const getForm = (formProps: any) => {
        return (
            <Form>
                <Card className="shadow-sm">
                    <CardBody>
                        <Row>
                            <Col md={4}>
                                <Field component={InputField}
                                       name="first_name"
                                       label="First Name"
                                       isRequired
                                       placeholder="Enter First Name"/>
                            </Col>
                            <Col md={4}>
                                <Field component={InputField}
                                       name="last_name"
                                       label="Last Name"
                                       isRequired
                                       placeholder="Enter Last Name"/>
                            </Col>
                            <Col md={4}>
                                <Field component={InputField}
                                       name="phone_number"
                                       label="Phone Number"
                                       isRequired
                                       placeholder="Enter Phone Number"/>
                            </Col>
                            <Col md={4}>
                                <Field component={InputField}
                                       isRequired
                                       name="email"
                                       label="Email Address"
                                       placeholder="Enter Email Address"/>
                            </Col>
                            <Col md={4}>
                                <Field component={InputField}
                                       name="password"
                                       label="Password"
                                       type='password'
                                       isRequired={(!formProps.values.user_xid)}
                                       placeholder="Enter Password"/>
                            </Col>
                            <Col md={4}>
                                <Field component={InputField}
                                       name="password_confirmation"
                                       label="Confirm Password"
                                       isRequired={(!formProps.values.user_xid)}
                                       type='password'
                                       placeholder="Enter Confirm Password"/>
                            </Col>
                            {/*<Col md={4}>
                                <Field component={InputReactSelect}
                                       name="timezone_name"
                                       api={api.common.getTimeZoneList}
                                       label="Timezone"
                                       isRequired
                                       labelField="timezone_name"
                                       valueField="timezone_name"
                                />
                            </Col>*/}
                        </Row>
                        <hr/>
                        <div className="text-right">
                            <LoadingButton
                                color="primary"
                                size="sm"
                                type='submit'
                                disabled={formProps.isSubmitting}
                                isLoading={formProps.isSubmitting}
                            >
                                <i className="fas fa-save"/>{' '}Save
                            </LoadingButton>{' '}
                        </div>
                    </CardBody>
                </Card>
            </Form>
        )
    };

    const handleOnSubmit = (formData: any, event: any) => {
        dispatch(UiAction.showPageLoading());
        api.user.postUpdateProfile(formData).then(() => {
            const currentInfo: any = auth.getUser();
            auth.setUser({
                ...currentInfo,
                first_name: formData.first_name,
                last_name: formData.last_name,
                phone_number: formData.phone_number,
                email: formData.email,
                //timezone_name: formData.timezone_name,
            });
            Toastiy.success(message.profileUpdated);
            //window.location.reload();
        }).finally(() => {
            dispatch(UiAction.hidePageLoading());
            event.setSubmitting(false);
        });
    };

    return (
        <>
            <ContentLoading show={isLoading}/>
            <Formik
                enableReinitialize={true}
                initialValues={formData}
                validationSchema={profileFormValidateSchema}
                onSubmit={handleOnSubmit}
            >
                {(props) => getForm(props)}
            </Formik>
        </>
    );
}