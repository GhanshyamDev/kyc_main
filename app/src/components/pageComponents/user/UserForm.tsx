import React, {useEffect, useState} from 'react';
import {auth} from "../../../util/Auth";
import {Field, Form, Formik} from "formik";
import {Button, Card, CardBody, Col, Row} from 'reactstrap';
import {InputField} from "../../shared/Fields/InputField";
import {api} from "../../../util/Api";
import {LoadingButton} from "../../shared/LoadingButton";
import ContentLoading from "../../shared/ContentLoading";
import {userValidateSchema} from "../../../util/Validation";
import {Toastiy} from "../../../util/Toast";
import {message} from "../../../locale/en/message";
import {InputSwitch} from "../../shared/Fields/InputSwitch";
import {InputReactSelect} from '../../shared/Fields/InputReactSelect';
import {UiAction} from "../../../store/UiState";
import {useDispatch} from "react-redux";
import {InputUploadImage} from "../../shared/Fields/InputUploadImage";

interface IProps {
    id: string | null,
    onCancel: any,
    loadData?: any
}

const setUserFormData = (formData: any = {}) => {
    return {
        user_xid: formData.user_xid || null,
        organization_xid: auth.getCurrentOrganizationId(),
        first_name: formData.first_name || '',
        last_name: formData.last_name || '',
        is_active: formData.is_active != null ? formData.is_active : true,
        email: formData.email || '',
        phone_number: formData.phone_number || '',
        timezone_id: 248,
        password: '',
        password_confirmation: '',
    }
};

export function UserForm(props: IProps) {
    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState(false);
    const [formData, setFormData] = useState<any>(setUserFormData());

    useEffect(() => {
        if (props.id) {
            dispatch(UiAction.showPageLoading());
            api.user.getDetails(props.id).then((response: any) => {
                setFormData(setUserFormData(response.data.data));
            }).finally(() => dispatch(UiAction.hidePageLoading()));
        }
    }, []);

    const getForm = (formProps: any) => {
        return (
            <Form>
                <Card className="shadow-sm">
                    <CardBody>
                        <Row>
                            <Col md={4}>
                                <Field component={InputField}
                                       name="first_name"
                                       label="First Name"
                                       isRequired
                                       placeholder="Enter First Name"/>
                            </Col>
                            <Col md={4}>
                                <Field component={InputField}
                                       name="last_name"
                                       label="Last Name"
                                       isRequired
                                       placeholder="Enter Last Name"/>
                            </Col>
                            <Col md={4}>
                                <Field component={InputField}
                                       name="phone_number"
                                       label="Phone Number"
                                       isRequired
                                       placeholder="Enter Phone Number"/>
                            </Col>
                            <Col md={4}>
                                <Field component={InputField}
                                       isRequired
                                       name="email"
                                       label="Email Address"
                                       placeholder="Enter Email Address"/>
                            </Col>
                            <Col md={4}>
                                <Field component={InputField}
                                       name="password"
                                       label="Password"
                                       type='password'
                                       isRequired={(!formProps.values.user_xid)}
                                       placeholder="Enter Password"/>
                            </Col>
                            <Col md={4}>
                                <Field component={InputField}
                                       name="password_confirmation"
                                       label="Confirm Password"
                                       isRequired={(!formProps.values.user_xid)}
                                       type='password'
                                       placeholder="Enter Confirm Password"/>
                            </Col>
                            {/*<Col md={4}>
                                <Field component={InputUploadImage}
                                       name="user_image"
                                       postUrl={`${process.env.REACT_APP_API_URL}file/upload-candidate-image`}
                                       cameraPictureWidth="320"
                                       previewHeight="240px"
                                       captureByCamera={true}
                                       label="Upload Image"
                                       desc="Upload Image file only and max 10 MB."
                                />
                            </Col>*/}
                            {/*<Col md={4}>
                                <Field component={InputReactSelect}
                                       name="timezone_name"
                                       api={api.common.getTimeZoneList}
                                       label="Timezone"
                                       isRequired
                                       labelField="timezone_name"
                                       valueField="timezone_name"
                                />
                            </Col>
                            <Col md={4}>
                                <Field component={InputReactSelect}
                                       name="roleId"
                                       label="Select Role(s)"
                                       api={api.role.getList}
                                       isRequired
                                       placeholder="Select Roles"
                                       labelField="normalizedName"
                                       valueField="normalizedName"
                                />
                            </Col>*/}
                            <Col md={4}>
                                <Field
                                    component={InputSwitch}
                                    name="is_active"
                                    label="Is Active?"
                                />
                            </Col>
                        </Row>
                        <hr/>
                    </CardBody>
                </Card>
                <div className="drawer-footer">
                    <LoadingButton
                        color="primary"
                        size="sm"
                        type='submit'
                        disabled={formProps.isSubmitting}
                        isLoading={formProps.isSubmitting}
                    >
                        <i className="fas fa-save"/>{' '}Save
                    </LoadingButton>{' '}
                    <Button disabled={formProps.isSubmitting} color="secondary" size="sm" onClick={props.onCancel}>
                        Cancel
                    </Button>
                </div>
            </Form>
        )
    };

    const add = (formData: any, event: any) => {
        api.user.postCreate(formData).then(() => {
            Toastiy.success(message.recordAdded);
            props.loadData();
            props.onCancel();
        }).finally(() => event.setSubmitting(false));
    };

    const edit = (formData: any, event: any) => {
        api.user.postUpdate(formData).then(() => {
            Toastiy.success(message.recordEdit);
            props.loadData();
            props.onCancel();
        }).finally(() => event.setSubmitting(false));
    };

    const handleOnSubmit = (formData: any, event: any) => {
        (formData.user_xid) ? edit(formData, event) : add(formData, event);
    };

    return (
        <>
            <ContentLoading show={isLoading}/>
            <Formik
                enableReinitialize={true}
                initialValues={formData}
                validationSchema={userValidateSchema}
                onSubmit={handleOnSubmit}
            >
                {(props) => getForm(props)}
            </Formik>
        </>
    );
}
