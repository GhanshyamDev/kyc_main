import React, {useEffect, useState} from "react";
import qs from "qs";
import {useHistory, useLocation} from "react-router-dom";
import {ShowBoolean} from "../../shared/ShowBoolean";
import {DropDownWrp} from "../../shared/DropDownWrp";
import {DropdownItem} from "reactstrap";
import {ITableData, TableWrp} from "../../shared/TableWrp/TableWrp";
import {getQueryParams, IQueryParams} from "../../shared/TableWrp/Helper";
import {useSelector} from "react-redux";
import {IRootState} from "../../../store/AppStore";

interface IProps {
    data: ITableData,
    useUrlQueryParams?: boolean | null,
    onEdit?: any,
    onDelete?: any,
    loadData: any
}

export function UserList(props: IProps) {
    const [queryParams, setQueryParams] = useState<IQueryParams>(getQueryParams());
    const location = useLocation();
    const history = useHistory();
    const {showPageLoading} = useSelector((state: IRootState) => state.ui);

    useEffect(() => {
        const urlQueryParams = qs.parse(location.search.substr(1));
        if (Object.keys(urlQueryParams).length > 0) {
            setQueryParams(getQueryParams(urlQueryParams));
            props.loadData(getQueryParams(urlQueryParams));
        } else {
            props.loadData(queryParams);
        }
    }, []);

    const loadData = (queryParams: any) => {
        const params: IQueryParams = getQueryParams(queryParams);
        if (props.useUrlQueryParams) {
            history.push({
                pathname: location.pathname,
                search: qs.stringify(params),
                state: params
            });
        }
        setQueryParams(params);
        props.loadData(params);
    };

    const columns = [
        {
            dataField: 'first_name',
            text: 'First Name',
            sort: true,
            formatter: (cell: any, row: any) => <a href="#" onClick={(e: any) => {e.preventDefault(); props.onEdit(row)}}>{cell}</a>
        }, {
            dataField: 'last_name',
            text: 'Last Name',
            sort: true
        }, {
            dataField: 'email',
            text: 'Email',
            sort: true
        }, {
            dataField: 'phone_number',
            text: 'Phone',
            sort: true
        }, {
            dataField: 'is_active',
            text: 'Active',
            formatter: (cell: any, row: any) => {
                return <ShowBoolean value={cell}/>
            }
        }, {
            dataField: 'user_xid',
            text: '',
            align: 'right',
            formatter: (cell: any, row: any) => (
                <>
                    <DropDownWrp className="pr-4">
                        <DropdownItem className="cursor-pointer" tag="a" onClick={() => props.onEdit(row)}>
                            Edit
                        </DropdownItem>
                        {/*<DropdownItem className="cursor-pointer" tag="a" onClick={() => props.onDelete(row)}>
                            Delete
                        </DropdownItem>*/}
                    </DropDownWrp>
                </>
            ),
        }
    ];

    return (
        <TableWrp
            rowKey="user_xid"
            title="Users"
            filterTitle="Filters"
            loading={showPageLoading}
            data={props.data}
            columns={columns}
            onChange={loadData}
            queryParams={queryParams}
        />
    )
}
