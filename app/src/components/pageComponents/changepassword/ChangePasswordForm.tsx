import React, {useEffect, useState} from 'react';
import {auth} from "../../../util/Auth";
import {Field, Form, Formik} from "formik";
import {Card, CardBody, Col, Row} from 'reactstrap';
import {InputField} from "../../shared/Fields/InputField";
import {api} from "../../../util/Api";
import {LoadingButton} from "../../shared/LoadingButton";
import ContentLoading from "../../shared/ContentLoading";
import {Toastiy} from "../../../util/Toast";
import {message} from "../../../locale/en/message";
import {useDispatch} from "react-redux";
import {useHistory} from 'react-router';
import {UiAction} from "../../../store/UiState";
import {changePasswordFormValidateSchema} from "../../../util/Validation";

interface IProps {
    onCancel?: any,
}

const setPasswordFormData = (formData: any = {}) => {
    return {
        user_xid: formData.user_xid || null,
        password: '',
        password_confirmation: '',
    }
};

export function ChangePasswordForm(props: IProps) {
    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState(false);
    const [formData, setFormData] = useState<any>(setPasswordFormData());
    const history = useHistory();

    useEffect(() => {
        setFormData(setPasswordFormData(auth.getUser()));
    }, []);

    const getForm = (formProps: any) => {
        return (
            <Form>
                <Card className="shadow-sm">
                    <CardBody>
                        <h3 className="mb-3">Choose your password</h3>
                        <Row>
                            <Col md={12}>
                                <Field component={InputField}
                                       name="password"
                                       label="Password"
                                       type='password'
                                       isRequired={(!formProps.values.user_xid)}
                                       placeholder="Enter Password"/>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12}>
                                <Field component={InputField}
                                       name="password_confirmation"
                                       label="Confirm Password"
                                       isRequired={(!formProps.values.user_xid)}
                                       type='password'
                                       placeholder="Enter Confirm Password"/>
                            </Col>
                        </Row>
                        <hr/>
                        <div className="text-right">
                            <LoadingButton
                                color="primary"
                                size="sm"
                                type='submit'
                                disabled={formProps.isSubmitting}
                                isLoading={formProps.isSubmitting}
                            >
                                <i className="fas fa-save"/>{' '}Change
                            </LoadingButton>{' '}
                        </div>
                    </CardBody>
                </Card>
            </Form>
        )
    };

    const handleOnSubmit = (formData: any, event: any) => {
        dispatch(UiAction.showPageLoading());
        api.user.postChangePassword(formData).then(() => {
            const currentInfo: any = auth.getUser();
            currentInfo.first_login = 0;
            auth.setUser(currentInfo);
            Toastiy.success(message.passwordChanged);
            // auth.logout();
            history.push({
                pathname: '/',
            });
            window.location.reload();
        }).finally(() => {
            dispatch(UiAction.hidePageLoading());
            event.setSubmitting(false);
        });
    };

    return (
        <>
            <ContentLoading show={isLoading}/>
            <Formik
                enableReinitialize={true}
                initialValues={formData}
                validationSchema={changePasswordFormValidateSchema}
                onSubmit={handleOnSubmit}
            >
                {(props) => getForm(props)}
            </Formik>
        </>
    );
}