import React, {useState} from "react";
import {Nav, NavItem, NavLink, TabContent, TabPane} from "reactstrap";
import classNames from "classnames";
import {CustomerImageForm} from "./CustomerImageForm";
import {CustomerCompareResult} from "./CustomerCompareResult";
import {CustomerForm} from "./CustomerForm";

interface IProps {
    id: string | null,
    onCancel: any,
    loadData?: any
}

export default function CustomerTabs(props: IProps) {
    const [activeTab, setActiveTab] = useState<"formTab" |  "imageTab" | "resultTab">("formTab");

    return (
        <>
            <Nav tabs className={'drawer-tabs'}>
                <NavItem>
                    <NavLink className={classNames({ active: activeTab === 'formTab' })} onClick={() => setActiveTab('formTab')}>
                        Basic Detail
                    </NavLink>
                </NavItem>
                {props.id &&
                (
                <>
                    <NavItem>
                        <NavLink className={classNames({ active: activeTab === 'imageTab' })} onClick={() => setActiveTab('imageTab')}>
                            Documents
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink className={classNames({ active: activeTab === 'resultTab' })} onClick={() => setActiveTab('resultTab')}>
                            Full Detail
                        </NavLink>
                    </NavItem>
                </>
                )
                }
            </Nav>
            <TabContent activeTab={activeTab} className={'drawer-content'}>
                <TabPane tabId='formTab'>
                    {(activeTab === 'formTab') &&
                    <CustomerForm
                        id={props.id}
                        onCancel={props.onCancel}
                        loadData={props.loadData}
                        onSave={() => setActiveTab('imageTab')}
                    />
                    }
                </TabPane>
                <TabPane tabId='imageTab'>
                    {(activeTab === 'imageTab') &&
                    <CustomerImageForm
                        id={props.id}
                        onCancel={props.onCancel}
                        loadData={props.loadData}
                        onCompare={() => setActiveTab('resultTab')}/>
                    }
                </TabPane>
                <TabPane tabId='resultTab'>
                    {(activeTab === 'resultTab') &&
                    <CustomerCompareResult id={props.id} onCancel={props.onCancel} useUrlQueryParams={false} />
                    }
                </TabPane>
            </TabContent>
        </>
    );
}
