import React, {useEffect, useState} from 'react';
import {Badge, Button, Card, CardBody, Col, Row} from 'reactstrap';
import {api} from "../../../util/Api";
import {UiAction} from "../../../store/UiState";
import {useDispatch} from "react-redux";
import SimpleTableWrp from "../../shared/SimpleTableWrp";
import {
    displayCandidateStatus,
    utcDateToLocalDate
} from "../../../util/Helpers";
import AppConstants from "../../../util/AppConstants";
import pdfMake from 'pdfmake/build/pdfmake';
import moment from "moment";

interface IProps {
    id: string | null,
    useUrlQueryParams?: boolean | null,
    onEdit?: any,
    onDelete?: any
    onCancel?: any
}

export function CustomerCompareResult(props: IProps) {
    const dispatch = useDispatch();
    const [customer, setCustomer] = useState<any>(null);
    const [resultData, setResultData] = useState<any>([]);

    useEffect(() => {
        if (props.id) {
            //dispatch(UiAction.showPageLoading());
            api.customer.getDetails(props.id).then((response: any) => {
                setCustomer(response.data.data);
            });
            loadData(props.id);
        }
    }, []);

    const loadData = (id: any) => {
        //dispatch(UiAction.showPageLoading());
        /*api.rekognitionResultLog.getResultLogByCustomer(props.id).then((response: any) => {
            setResultData(response.data.data);
            /!*setResultData([
                ...response.data.data,
                response.data.data[0],
            ]);*!/
        }).finally(() => dispatch(UiAction.hidePageLoading()));*/
    };

    const markAsCompleted = () => {
        api.customer.postMarkCompleted({customer_id: props.id}).then((response: any) => {
        }).finally(() => {
            dispatch(UiAction.hidePageLoading());
        });
    };

    const generatePdf = () => {
        let rowData = resultData.map((item: any) => {
            return [
                {text: utcDateToLocalDate(item.compareDate, AppConstants.SERVER_DATE_FORMAT, AppConstants.AM_PM_FORMAT)},
                {text: item.imageType},
                {stack: [{text: item.status, background: displayCandidateStatus(item.status)}]},
                {stack: [{image: item.sourceFileUri, width: 150}]},
                {stack: [{image: item.targetFileUri,  width: 150}]}
            ]
        });
        pdfMake.createPdf({
            pageMargins: 20,
            styles: {
                header: {
                    fontSize: 16,
                    bold: true,
                    margin: [0, 0, 0, 8]
                },
                label: {
                    fontSize: 11,
                    bold: true,
                },
                subheader: {
                    fontSize: 14,
                    bold: true,
                    margin: [0, 10, 0, 3]
                },
                tableHeader: {
                    bold: true,
                    fontSize: 11,
                    color: 'black'
                },
                tableExample: {
                    fontSize: 11,
                    margin: [0, 5, 0, 15]
                },
            },
            content: [
                {
                    text: 'Customer Identity Verification Report',
                    style: 'header',
                    alignment: 'center'
                },
                {
                    style: 'tableExample',
                    table: {
                        widths: ['*', '*'],
                        body: [
                            [{text: [{text: 'Organization: ', bold: true}, customer.organization_name]}, {text: [{text: 'Date of Report: ', bold: true}, moment().format(AppConstants.AM_PM_FORMAT)]}],
                            [{text: [{text: 'Customer ID No.: ', bold: true}, customer.cid]}, {text: [{text: 'Job ID: ', bold: true}, customer.job_id]}],
                            [{text: [{text: 'Customer Name: ', bold: true}, `${customer.first_name} ${customer.last_name}`]}, {text: [{text: 'Result: ', bold: true}, {text: customer.current_status, background: displayCandidateStatus(customer.current_status)}]}],
                            [{text: [{text: 'Comment: ', bold: true}, customer.comment], colSpan: 2}],
                        ]
                    },
                    layout: 'noBorders'
                },
                {
                    style: 'tableExample',
                    table: {
                        widths: ['*', '*', '*'],
                        body: [
                            [{text: 'Selfie', style: "tableHeader"}, {text: 'Uploaded at', style: "tableHeader"}, {text: 'Location', style: "tableHeader"}],
                            [{text: 'Customer Selfie'}, {text: utcDateToLocalDate(customer.selfie_uploaded_on, AppConstants.SERVER_DATE_FORMAT, AppConstants.AM_PM_FORMAT)}, {text: customer.selfie1_location}],
                            [{text: 'Onboarding Selfie'}, {text: utcDateToLocalDate(customer.final_selfie_uploaded_on, AppConstants.SERVER_DATE_FORMAT, AppConstants.AM_PM_FORMAT)}, {text: customer.validation_image_location}],
                        ]
                    },
                },
                {
                    style: "tableExample",
                    table: {
                        widths: ['auto', 'auto', 'auto', '*',  '*'],
                        dontBreakRows: true,
                        body: [
                            [
                                {text: 'Compared at', style: "tableHeader"},
                                {text: 'Step', style: "tableHeader"},
                                {text: 'Result', style: "tableHeader"},
                                {text: 'Source Image', style: "tableHeader"},
                                {text: 'Target Image', style: "tableHeader"},
                            ],
                            ...rowData,
                        ]
                    },
                    layout: {
                        fillColor: function (rowIndex, node, columnIndex) {
                            return (rowIndex % 2 === 0) ? '#CCCCCC' : null;
                        }
                    }
                }
            ]
        }).download("route-master.pdf");
    };

    const columns = [
        {
            dataField: 'compareDate',
            text: 'Date',
            sort: false,
            formatter: (cell: any, row: any) => utcDateToLocalDate(cell, AppConstants.SERVER_DATE_FORMAT, AppConstants.AM_PM_FORMAT)
        }, {
            dataField: 'sourceFileName',
            text: 'Source Image',
            sort: false,
            formatter: (cell: any, row: any) =>
                <div className="preview mb-2 d-inline w-100 p-0 text-center bg-light" style={{height: "192px"}}>
                    <img src={row.sourceFileUrl} alt="Source Image" className="img-fluid" style={{maxHeight: "192px"}}/>
                </div>
        }, {
            dataField: 'targetFileName',
            text: 'Target Image',
            sort: false,
            formatter: (cell: any, row: any) =>
                <div className="preview mb-2 d-inline w-100 p-0 text-center bg-light" style={{height: "192px"}}>
                    <img src={row.targetFileUrl} alt="Target Image" className="img-fluid" style={{maxHeight: "192px"}}/>
                </div>
        }, {
            dataField: 'imageType',
            text: 'Image Type',
            sort: false
        }, {
            dataField: 'compareResult',
            text: 'Result',
            sort: false
        }, {
            dataField: 'status',
            text: 'Status',
            sort: false,
            formatter: (cell: any, row: any) => displayCandidateStatus(cell)
        },
    ];

    return (
        <Card className='shadow-sm'>
            <CardBody>
                <Row>
                    <Col md={4}>
                        <label className="font-weight-semi-bold mb-1">Upload Location:</label>
                        <div>{customer?.selfie1_location}</div>
                    </Col>
                    <Col md={4}>{''}</Col>
                    <Col md={4} className='text-md-right text-sm-left mt-2'>
                        <Button color="primary" size="sm" onClick={markAsCompleted}>
                            <i className='mdi mdi-check'/> Mark as Completed
                        </Button>
                        <Button color="primary" size="sm" onClick={generatePdf}>
                            <i className='mdi mdi-download'/> Download PDF
                        </Button>
                    </Col>
                </Row>
                <Row>
                    <Col md={12} className="mt-2">
                        <h4 className="mb-0">Comparison</h4>
                        <table className="table table-striped">
                            <thead className="thead-light">
                                <tr>
                                    <th>Date</th>
                                    <th>Customer Photo</th>
                                    <th>PAN Card, Aadhaar Card</th>
                                    <th>Compare Result</th>
                                    <th>Compare Status</th>
                                    <th>Aadhaar Card Verified</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        2020-12-03 11:26:37 PM
                                    </td>
                                    <td>
                                        <div className="preview mb-2 d-inline w-100 p-0 text-center bg-light" style={{height: "150px"}}>
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/0/0e/Lakeyboy_Silhouette.PNG" alt="Source Image" className="img-fluid" style={{maxHeight: "150px"}}/>
                                        </div>
                                    </td>
                                    <td>
                                        <div className="preview mb-2 d-inline w-100 p-0 text-center bg-light" style={{height: "150px"}}>
                                            <img src="https://spiderimg.amarujala.com/assets/images/2017/12/02/750x506/demo-pic_1512220322.jpeg" alt="Source Image" className="img-fluid" style={{maxHeight: "150px"}}/>
                                        </div>
                                    </td>
                                    <td>99.9</td>
                                    <td>
                                        <Badge color="success" className='badge'>
                                            Verified
                                        </Badge>
                                    </td>
                                    <td>
                                        <Badge color="success" className='badge'>
                                            Yes
                                        </Badge>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <h4 className="mt-2 mb-0">Docs</h4>
                        <table className="table table-striped">
                            <thead className="thead-light">
                                <tr>
                                    <th>Date</th>
                                    <th>Doc. Type</th>
                                    <th>Preview</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        2020-12-03 11:26:37 PM
                                    </td>
                                    <td>
                                        Identity Proof
                                    </td>
                                    <td>
                                        <div className="preview mb-2 d-inline w-100 p-0 text-center bg-light" style={{height: "150px"}}>
                                            <img src="https://spiderimg.amarujala.com/assets/images/2017/12/02/750x506/demo-pic_1512220322.jpeg" alt="Source Image" className="img-fluid" style={{maxHeight: "150px"}}/>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        2020-12-03 11:26:37 PM
                                    </td>
                                    <td>
                                        Address Proof
                                    </td>
                                    <td>
                                        <div className="preview mb-2 d-inline w-100 p-0 text-center bg-light" style={{height: "150px"}}>
                                            <img src="https://spiderimg.amarujala.com/assets/images/2017/12/02/750x506/demo-pic_1512220322.jpeg" alt="Source Image" className="img-fluid" style={{maxHeight: "150px"}}/>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        2020-12-03 11:26:37 PM
                                    </td>
                                    <td>
                                        Income Proof
                                    </td>
                                    <td>
                                        <a href="https://spiderimg.amarujala.com/assets/images/2017/12/02/750x506/demo-pic_1512220322.jpeg">Attached File</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        2020-12-03 11:26:37 PM
                                    </td>
                                    <td>
                                        Account Linking
                                    </td>
                                    <td>

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        2020-12-03 11:26:37 PM
                                    </td>
                                    <td>
                                        Signed on Blank Paper
                                    </td>
                                    <td>

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <h4 className="mt-2 mb-0">Questions & Answers</h4>
                        <table className="table table-striped">
                            <thead className="thead-light">
                            <tr>
                                <th>Date</th>
                                <th>Question</th>
                                <th>Answer</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    2020-12-03 11:26:37 PM
                                </td>
                                <td>
                                    What Lorem Ipsum is simply dummy text
                                </td>
                                <td>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    2020-12-03 11:26:37 PM
                                </td>
                                <td>
                                    What Lorem Ipsum is simply dummy text
                                </td>
                                <td>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    2020-12-03 11:26:37 PM
                                </td>
                                <td>
                                    What Lorem Ipsum is simply dummy text
                                </td>
                                <td>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    2020-12-03 11:26:37 PM
                                </td>
                                <td>
                                    What Lorem Ipsum is simply dummy text
                                </td>
                                <td>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    2020-12-03 11:26:37 PM
                                </td>
                                <td>
                                    What Lorem Ipsum is simply dummy text
                                </td>
                                <td>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </Col>
                </Row>
            </CardBody>
        </Card>
    )
}
