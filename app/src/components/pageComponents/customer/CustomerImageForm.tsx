import React, {useEffect, useState} from 'react';
import {Field, Form, Formik} from "formik";
import {Badge, Button, Card, CardBody, Col, Row} from 'reactstrap';
import {api} from "../../../util/Api";
import {LoadingButton} from "../../shared/LoadingButton";
import ContentLoading from "../../shared/ContentLoading";
import {UiAction} from "../../../store/UiState";
import {useDispatch} from "react-redux";
import {InputUploadImage} from "../../shared/Fields/InputUploadImage";
import ModalWrp from "../../shared/ModalWrp";
import {displayCandidateStatus} from "../../../util/Helpers";
import {CustomerSendLinkForm} from "./CustomerSendLinkForm";
import {InputUploadFile} from "../../shared/Fields/InputUploadFile";

interface IProps {
    id: string | null,
    onCancel: any,
    loadData?: any,
    onCompare?: any
}

const emptyImage = {
    fileName: '',
    url: '',
    id: null,
    imageStatus:'',
    result: 0
}

const setCustomerImageFormData = (formData: any = {}) => {
    return {
        customer_xid: formData.customer_xid || null,
        completed_status: formData.completed_status || false,
        master_image: formData.master_image || emptyImage,
        doc_image_1: formData.doc_image_1 || emptyImage,
        doc_image_2: formData.doc_image_2 || emptyImage,
        doc_image_3: formData.doc_image_3 || emptyImage,
        doc_image_4: formData.doc_image_4 || emptyImage,
        doc_image_5: formData.doc_image_5 || emptyImage,
        master_image_changed: formData.master_image_changed || 0,
        doc_image_1_changed: formData.doc_image_1_changed || 0,
        doc_image_2_changed: formData.doc_image_2_changed || 0,
        doc_image_3_changed: formData.doc_image_3_changed || 0,
        final_validation_image_changed: formData.final_validation_image_changed || 0
    }
};

export function CustomerImageForm(props: IProps) {
    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState(false);
    const [formData, setFormData] = useState<any>(setCustomerImageFormData());
    const [showSendWebLinkModal, setShowSendWebLinkModal] = useState<boolean>(false);
    const [webLinkType, setWebLinkType] = useState<string>("");
    const [compareStatus, setCompareStatus] = useState(false);
    const [customer, setCustomer] = useState<any>(null);

    useEffect(() => {
        if (props.id) {
            /*api.customer.getDetails(props.id).then((response: any) => {
                setCustomer(response.data.data);
            });*/
        }
    }, []);

    useEffect(() => {
        if (props.id) {
            /*dispatch(UiAction.showPageLoading());
            getcustomerImages();*/
        }
    }, []);

    const onSendWebLink1 = () => {
        setWebLinkType("customer_selfie_1");
        setShowSendWebLinkModal(true);
    };

    const onSendWebLink2 = () => {
        setWebLinkType("customer_selfie_2");
        setShowSendWebLinkModal(true);
    };


    const getcustomerImages = () => {
        api.customer.getImages(props.id).then((response: any) => {
            setFormData(setCustomerImageFormData(response.data));
            setCompareStatus(enableCompare(response.data));
        }).finally(() => dispatch(UiAction.hidePageLoading()));
    }

    const enableCompare = (imageData: any) => {
        return ((imageData.step_1_image.url !== ''
            || imageData.step_2_image.url !== ''
            || imageData.step_3_image.url !== ''
            || imageData.final_validation_image.url !== '')
            && imageData.master_image_changed) ||
            (imageData.master_image.url !== '' && imageData.step_1_image_changed) ||
            (imageData.master_image.url !== '' && imageData.step_2_image_changed) ||
            (imageData.master_image.url !== '' && imageData.step_3_image_changed) ||
            (imageData.master_image.url !== '' && imageData.final_validation_image_changed);
    }

    const previewPlaceHolder = () => {
        return <div className="d-flex justify-content-center align-items-center" style={{height: "100%"}}>
            <div className="h3 text-light-gray">Send Weblink</div>
        </div>
    };

    const getForm = (formProps: any) => {
        return (
            <Form>
                <Card className="shadow-sm">
                    <CardBody>
                        <div className="text-right">
                            <button type="button" className="btn btn-primary btn-sm" onClick={onSendWebLink1}>Send Web Link</button>
                        </div>
                        <Row>
                            <Col md={3}>
                                <Field component={InputUploadImage}
                                       name="master_image"
                                       postUrl={`${process.env.REACT_APP_API_URL}file/upload-image/` + props.id}
                                       previewHeight="150px"
                                       label="Passport Size Photo"
                                       isDisabled={formData.completed_status}
                                       imageStatus={displayCandidateStatus(formData.master_image.imageStatus)}
                                       onImageUpload={(data: any) => imageUploaded(data, "master_image")}
                                />
                            </Col>
                            <Col md={3}>
                                <Field component={InputUploadFile}
                                       name="doc_image_1"
                                       postUrl={`${process.env.REACT_APP_API_URL}file/upload-image/` + props.id}
                                       showPreview={true}
                                       placeholder="Upload File"
                                       previewHeight="150px"
                                       desc="PAN card, Aadhaar Card"
                                       label="Identity Proof"
                                       isDisabled={formData.completed_status}
                                       imageStatus={displayCandidateStatus(formData.doc_image_1.imageStatus)}
                                       onImageUpload={(data: any) => imageUploaded(data, "doc_image_1")}
                                />
                            </Col>
                            <Col md={3}>
                                <Field component={InputUploadFile}
                                       name="doc_image_2"
                                       postUrl={`${process.env.REACT_APP_API_URL}file/upload-image/` + props.id}
                                       showPreview={true}
                                       placeholder="Upload File"
                                       previewHeight="150px"
                                       label="Address Proof"
                                       desc="Passport, Driving Licence, Aadhaar Card, Bank Statement, Utility bills"
                                       isDisabled={formData.completed_status}
                                       imageStatus={displayCandidateStatus(formData.doc_image_2.imageStatus)}
                                       onImageUpload={(data: any) => imageUploaded(data, "doc_image_2")}
                                />
                            </Col>
                            <Col md={3}>
                                <Field component={InputUploadFile}
                                       name="doc_image_3"
                                       postUrl={`${process.env.REACT_APP_API_URL}file/upload-image/` + props.id}
                                       showPreview={true}
                                       placeholder="Upload File"
                                       previewHeight="150px"
                                       label="Income Proof"
                                       desc="Salary slip (latest), ITR (latest), Bank Statement (6 months)"
                                       isDisabled={formData.completed_status}
                                       imageStatus={displayCandidateStatus(formData.doc_image_3.imageStatus)}
                                       onImageUpload={(data: any) => imageUploaded(data, "doc_image_3")}
                                />
                            </Col>
                            <Col md={3}>
                                <Field component={InputUploadFile}
                                       name="doc_image_4"
                                       postUrl={`${process.env.REACT_APP_API_URL}file/upload-image/` + props.id}
                                       showPreview={true}
                                       placeholder="Upload File"
                                       previewHeight="150px"
                                       label="Account Linking"
                                       desc="Cancelled Cheque"
                                       isDisabled={formData.completed_status}
                                       imageStatus={displayCandidateStatus(formData.doc_image_4.imageStatus)}
                                       onImageUpload={(data: any) => imageUploaded(data, "doc_image_4")}
                                />
                            </Col>
                            <Col md={3}>
                                <Field component={InputUploadFile}
                                       name="doc_image_5"
                                       postUrl={`${process.env.REACT_APP_API_URL}file/upload-image/` + props.id}
                                       showPreview={true}
                                       placeholder="Upload Image"
                                       previewHeight="150px"
                                       label="Signed on Blank Paper"
                                       desc="Signed on Blank Paper and Upload"
                                       isDisabled={formData.completed_status}
                                       imageStatus={displayCandidateStatus(formData.doc_image_4.imageStatus)}
                                       onImageUpload={(data: any) => imageUploaded(data, "doc_image_5")}
                                />
                            </Col>
                        </Row>
                        <hr/>
                    </CardBody>
                </Card>
                <div className="drawer-footer">
                    {!formData.completed_status &&
                    <>
                        <LoadingButton
                            color="primary"
                            size="sm"
                            type='submit'
                            disabled={formProps.isSubmitting || !compareStatus}
                            isLoading={formProps.isSubmitting}
                        >
                            {' '}Compare
                        </LoadingButton>{' '}
                        <Button disabled={formProps.isSubmitting} color="secondary" size="sm" onClick={props.onCancel}>
                            Cancel
                        </Button>
                    </>
                    }
                </div>
            </Form>
        )
    };

    const imageUploaded = (uploadedImage: any, imageType:string) => {
        api.customer.getImages(props.id).then((response: any) => {
            setFormData(setCustomerImageFormData(response.data));
            setCompareStatus(enableCompare(response.data));
        }).finally(() => dispatch(UiAction.hidePageLoading()));
    }

    const add = (formData: any, event: any) => {

    };

    const edit = (formData: any, event: any) => {

    };

    const handleOnSubmit = (formData: any, event: any) => {
        api.customer.postCompare(props.id).then((response: any) => {
            getcustomerImages();
            props.onCompare();
        }).finally(() => {
            event.setSubmitting(false);
        });
    };

    return (
        <>
            <ModalWrp title="Send Web Link" size="md" showModal={showSendWebLinkModal} onCancel={() => setShowSendWebLinkModal(false)}>
                <CustomerSendLinkForm
                    customer={{...formData, email: customer?.email, phone_number: customer?.phone_number, customer_xid: props.id}}
                    webLinkType={webLinkType}
                    onCancel={() => setShowSendWebLinkModal(false)}
                />
            </ModalWrp>
            <ContentLoading show={isLoading}/>
            <Formik
                enableReinitialize={true}
                initialValues={formData}
                onSubmit={handleOnSubmit}
            >
                {(props) => getForm(props)}
            </Formik>
        </>
    );
}
