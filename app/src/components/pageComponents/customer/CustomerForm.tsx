import React, {useEffect, useState} from 'react';
import {auth} from "../../../util/Auth";
import {Field, Form, Formik} from "formik";
import {Button, Card, CardBody, Col, Row} from 'reactstrap';
import {InputField} from "../../shared/Fields/InputField";
import {api} from "../../../util/Api";
import {LoadingButton} from "../../shared/LoadingButton";
import ContentLoading from "../../shared/ContentLoading";
import {candidateValidateSchema, customerValidateSchema} from "../../../util/Validation";
import {Toastiy} from "../../../util/Toast";
import {message} from "../../../locale/en/message";
import {UiAction} from "../../../store/UiState";
import {useDispatch} from "react-redux";

interface IProps {
    id: string | null,
    onCancel: any,
    loadData?: any,
    onSave?: any
}

const setCustomerFormData = (formData: any = {}) => {
    return {
        customer_xid: formData.customer_xid || null,
        organization_xid: auth.getCurrentOrganizationId(),
        first_name: formData.first_name || '',
        last_name: formData.last_name || '',
        email: formData.email || '',
        phone_number: formData.phone_number || '',
        cid: formData.cid || '',
        address: formData.address || '',
        comment: formData.comment || '',
        completed_status: formData.completed_status || false,
        current_status: formData.current_status || '',
        location: formData.location || '',
    }
};

export function CustomerForm(props: IProps) {
    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState(false);
    const [formData, setFormData] = useState<any>(setCustomerFormData());

    useEffect(() => {
        if (props.id) {
            dispatch(UiAction.showPageLoading());
            api.customer.getDetails(props.id).then((response: any) => {
                setFormData(setCustomerFormData(response.data.data));
            }).finally(() => dispatch(UiAction.hidePageLoading()));
        }
    }, []);

    const getForm = (formProps: any) => {
        return (
            <Form>
                <Card className="shadow-sm">
                    <CardBody>
                        <Row>
                            <Col md={4}>
                                <Field component={InputField}
                                       name="cid"
                                       label="Customer Id"
                                       isRequired
                                       disabled={props.id}
                                       placeholder="Enter Candidate Id"/>
                            </Col>
                            <Col md={4}>
                                <Field component={InputField}
                                       name="first_name"
                                       label="First Name"
                                       isRequired
                                       //disabled={props.id}
                                       placeholder="Enter First Name"/>
                            </Col>
                            <Col md={4}>
                                <Field component={InputField}
                                       name="last_name"
                                       label="Last Name"
                                       //disabled={props.id}
                                       placeholder="Enter Last Name"/>
                            </Col>
                            <Col md={4}>
                                <Field component={InputField}
                                       name="phone_number"
                                       label="Phone Number"
                                       isRequired
                                       disabled={formData.completed_status}
                                       placeholder="Enter Phone Number"/>
                            </Col>
                            <Col md={4}>
                                <Field component={InputField}
                                       isRequired
                                       name="email"
                                       label="Email Address"
                                       disabled={formData.completed_status}
                                       placeholder="Enter Email Address"/>
                            </Col>
                            <Col md={4}>
                                <Field component={InputField}
                                       name="address"
                                       label="Address"
                                       disabled={formData.completed_status}
                                       placeholder="Enter Address"/>
                            </Col>
                            <Col md={4}>
                                <Field component={InputField}
                                       type="textarea"
                                       name="comment"
                                       label="Comment"
                                       disabled={formData.completed_status}
                                       placeholder="Enter Comment" />
                            </Col>
                        </Row>
                        <hr/>
                    </CardBody>
                </Card>
                <div className="drawer-footer">
                    {!formData.completed_status &&
                    <>
                        <LoadingButton
                            color="primary"
                            size="sm"
                            type='submit'
                            disabled={formProps.isSubmitting}
                            isLoading={formProps.isSubmitting}
                        >
                            <i className="fas fa-save"/>{' '}Save
                        </LoadingButton>{' '}
                        <Button disabled={formProps.isSubmitting} color="secondary" size="sm" onClick={props.onCancel}>
                            Cancel
                        </Button>
                    </>
                    }
                </div>
            </Form>
        )
    };

    const add = (formData: any, event: any) => {
        api.customer.postCreate(formData).then(() => {
            Toastiy.success(message.recordAdded);
            props.loadData();
            props.onCancel();
        }).finally(() => event.setSubmitting(false));
    };

    const edit = (formData: any, event: any) => {
        api.customer.postUpdate(formData).then(() => {
            Toastiy.success(message.recordEdit);
            props.loadData();
            props.onSave();
        }).finally(() => event.setSubmitting(false));
    };

    const handleOnSubmit = (formData: any, event: any) => {
        (formData.customer_xid) ? edit(formData, event) : add(formData, event);
    };

    return (
        <>
            <ContentLoading show={isLoading}/>
            <Formik
                enableReinitialize={true}
                initialValues={formData}
                validationSchema={customerValidateSchema}
                onSubmit={handleOnSubmit}
            >
                {(props) => getForm(props)}
            </Formik>
        </>
    );
}
