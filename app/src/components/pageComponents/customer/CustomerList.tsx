import React, {useEffect, useState} from "react";
import qs from "qs";
import {useHistory, useLocation} from "react-router-dom";
import {DropDownWrp} from "../../shared/DropDownWrp";
import {Badge, Button, Col, DropdownItem, Row} from "reactstrap";
import {ITableData, TableWrp} from "../../shared/TableWrp/TableWrp";
import {getQueryParams, IQueryParams} from "../../shared/TableWrp/Helper";
import {useSelector} from "react-redux";
import {IRootState} from "../../../store/AppStore";
import {
    candidateStatusOptions,
    isAllPropertiesEmpty,
    displayCandidateStatus,
} from "../../../util/Helpers";
import {Field, Form, Formik} from "formik";
import {InputField} from "../../shared/Fields/InputField";
import {LoadingButton} from "../../shared/LoadingButton";
import {InputReactSelect} from "../../shared/Fields/InputReactSelect";

interface IProps {
    data: ITableData,
    useUrlQueryParams?: boolean | null,
    onEdit?: any,
    onDelete?: any,
    loadData: any
}

export function CustomerList(props: IProps) {
    const [queryParams, setQueryParams] = useState<IQueryParams>(getQueryParams());
    const location = useLocation();
    const history = useHistory();
    const {showPageLoading} = useSelector((state: IRootState) => state.ui);
    const [filters, setFilters] = useState<any>({
        search: null,
        current_status: null
    });

    useEffect(() => {
        const urlQueryParams = qs.parse(location.search.substr(1));
        if (Object.keys(urlQueryParams).length > 0) {
            setQueryParams(getQueryParams(urlQueryParams));
            props.loadData(getQueryParams(urlQueryParams));
        } else {
            props.loadData(queryParams);
        }
    }, []);

    const loadData = (queryParams: any) => {
        const params: IQueryParams = getQueryParams(queryParams);
        if (props.useUrlQueryParams) {
            history.push({
                pathname: location.pathname,
                search: qs.stringify(params),
                state: params
            });
        }
        setQueryParams(params);
        props.loadData(params);
    };

    const handleOnSubmit = (formData: any, event: any) => {
        const params = {...queryParams, status: 1, filters: !isAllPropertiesEmpty(formData) ? formData : null };
        setQueryParams(params);
        loadData(params);
        event.setSubmitting(false);
    };

    const getFilterForm = (formProps: any) => {
        return (
            <Form className="form-inline">
                <Field component={InputReactSelect}
                       options={candidateStatusOptions}
                       name="current_status"
                       className="mr-2"
                       style={{minWidth: "182px"}}
                       isMulti={false}
                       label=""
                       isClearable={false}
                       isRequired="true"
                       labelField="label"
                       valueField="value"
                       onSelect={(value: any) => {
                           if (value) {
                               const data = {
                                   current_status: value.value,
                               }
                               const params = {...queryParams, status: 1, filters: !isAllPropertiesEmpty(data) ? data : null };
                               setQueryParams(params);
                               loadData(params);
                           }
                       }}
                />
                <Field component={InputField}
                       name="search"
                       placeholder="Search by keyword"
                       className="mr-2"
                       label=""
                />
                <div>
                    <LoadingButton color="primary" size="sm" type='submit'
                                   disabled={formProps.isSubmitting}
                                   isLoading={formProps.isSubmitting}>
                        <i className="mdi mdi-magnify" />
                    </LoadingButton>{' '}
                    <Button color="secondary" size="sm"
                            onClick={() => clearAllFilter(formProps)}><i className="mdi mdi-close" />
                    </Button>
                </div>
            </Form>
        )
    };

    const clearAllFilter = (formProps: any) => {
        const params: IQueryParams = getQueryParams({});
        formProps.setFieldValue('search', null);
        formProps.setFieldValue('current_status', null);
        setQueryParams(params);
        loadData(params);
    };

    const columns = [
        {
            dataField: 'cid',
            text: 'Customer Id',
            sort: true,
        }, {
            dataField: 'first_name',
            text: 'First Name',
            sort: true,
            formatter: (cell: any, row: any) => <a href="#" onClick={(e: any) => {e.preventDefault(); props.onEdit(row)}}>{cell}</a>
        }, {
            dataField: 'last_name',
            text: 'Last Name',
            sort: true,
        }, {
            dataField: 'email',
            text: 'Email',
            sort: true
        }, {
            dataField: 'phone_number',
            text: 'Phone',
            sort: true
        }, {
            dataField: 'current_status',
            text: 'Status',
            sort: true,
            formatter: (cell: any, row: any) => displayCandidateStatus(cell)
        }, {
            dataField: 'customer_xid',
            text: '',
            align: 'right',
            formatter: (cell: any, row: any) => (
                <>
                    <DropDownWrp className="pr-4">
                        <DropdownItem className="cursor-pointer" tag="a" onClick={() => props.onEdit(row)}>
                            Edit
                        </DropdownItem>
                        <DropdownItem className="cursor-pointer" tag="a" onClick={() => props.onDelete(row)}>
                            Delete
                        </DropdownItem>
                    </DropDownWrp>
                </>
            ),
        }
    ];

    return (
        <TableWrp
            rowKey="customer_xid"
            title="Customers"
            filterTitle="Filters"
            loading={showPageLoading}
            filterForm={() => {
                return (
                    <>
                        <Formik
                            enableReinitialize={true}
                            initialValues={filters}
                            onSubmit={handleOnSubmit}
                        >{(props: any) => getFilterForm(props)}
                        </Formik>
                    </>
                )
            }}
            data={props.data}
            columns={columns}
            onChange={loadData}
            queryParams={queryParams}
            showSearch={false}
            showInLineFilter={true}
        />
    )
}
