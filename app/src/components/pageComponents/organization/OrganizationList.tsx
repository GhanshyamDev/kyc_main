import React, {useEffect, useState} from "react";
import qs from "qs";
import {useHistory, useLocation} from "react-router-dom";
import {ShowBoolean} from "../../shared/ShowBoolean";
import {DropDownWrp} from "../../shared/DropDownWrp";
import {Card, CardBody, DropdownItem} from "reactstrap";
import {getQueryParams, IQueryParams} from "../../shared/TableWrp/Helper";
import {useSelector} from "react-redux";
import {IRootState} from "../../../store/AppStore";
import SimpleTableWrp from "../../shared/SimpleTableWrp";

interface IProps {
    data: [],
    useUrlQueryParams?: boolean | null,
    onEdit?: any,
    onDelete?: any,
    loadData: any
}

export function OrganizationList(props: IProps) {
    const [queryParams, setQueryParams] = useState<IQueryParams>(getQueryParams({}));
    const location = useLocation();
    const history = useHistory();
    const {showPageLoading} = useSelector((state: IRootState) => state.ui);

    useEffect(() => {
        const urlQueryParams = qs.parse(location.search.substr(1));
        if (Object.keys(urlQueryParams).length > 0) {
            setQueryParams(getQueryParams(urlQueryParams));
            props.loadData(getQueryParams(urlQueryParams));
        } else {
            props.loadData(queryParams);
        }
    }, []);

    const columns = [
        {
            dataField: 'name',
            text: 'Organization',
            sort: true,
            formatter: (cell: any, row: any) => {

                return <>
                    {row.level !== 1 ? "--".repeat(row.level) : ""}{' '}
                    <a href="#" onClick={(e: any) => {e.preventDefault(); props.onEdit(row)}}>{cell}</a>
                </>
            }
        }, {
            dataField: 'parent_organization_name',
            text: 'Parent Organization',
        }, {
            dataField: 'is_active',
            text: 'Active',
            formatter: (cell: any, row: any) => {
                return <ShowBoolean value={cell}/>
            }
        }, {
            dataField: 'organization_xid',
            text: '',
            align: 'right',
            headerStyle: () => {
                return {width: '10%'};
            },
            formatter: (cell: any, row: any) => (
                <>
                    <DropDownWrp className="pr-4">
                        <DropdownItem className="cursor-pointer" tag="a" onClick={() => props.onEdit(row)}>
                            Edit
                        </DropdownItem>
                        <DropdownItem className="cursor-pointer" tag="a" onClick={() => props.onDelete(row)}>
                            Delete
                        </DropdownItem>
                    </DropDownWrp>
                </>
            ),
        }
    ];

    return (
        <Card className="shadow-sm">
            <CardBody>
                <SimpleTableWrp
                    rowKey="organization_xid"
                    title="Organizations"
                    filterTitle="Filters"
                    showSearch={true}
                    loading={showPageLoading}
                    data={props.data}
                    columns={columns}
                    onChange={props.loadData}
                />
            </CardBody>
        </Card>
    )
}
