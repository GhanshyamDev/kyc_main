import React, {useState} from "react";
import {Nav, NavItem, NavLink, TabContent, TabPane} from "reactstrap";
import classNames from 'classnames';
import {OrganizationForm} from "./OranizationForm";

interface IProps {
    id: string | null,
    onCancel: any,
    loadData?: any
}

export function OrganizationTabs(props: IProps) {
    const [activeTab, setActiveTab] = useState<"basicInfo" |  "basicInfo2">("basicInfo");

    return (
        <div>
            <Nav tabs>
                <NavItem>
                    <NavLink href="#"
                        className={classNames({ active: activeTab === 'basicInfo' })}
                        onClick={() => { setActiveTab('basicInfo'); }} >Profile</NavLink>
                </NavItem>
                {/*<NavItem>
                <NavLink href="#"
                         className={classNames({ active: activeTab === 'basicInfo2' })}
                         onClick={() => { setActiveTab('basicInfo'); }} >Profile</NavLink>
                </NavItem>*/}
            </Nav>
            <TabContent activeTab={activeTab}>
                <TabPane tabId="basicInfo">
                    <OrganizationForm
                        id={props.id}
                        onCancel={props.onCancel}
                        loadData={props.loadData}
                    />
                </TabPane>
            </TabContent>
        </div>
    );
}
