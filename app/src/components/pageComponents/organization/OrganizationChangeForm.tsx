import React, {useEffect, useState} from 'react';
import {auth} from "../../../util/Auth";
import {Field, Form, Formik} from "formik";
import {Button, Col, ModalBody, ModalFooter, Row} from 'reactstrap';
import {api} from "../../../util/Api";
import {LoadingButton} from "../../shared/LoadingButton";
import {InputReactSelect} from "../../shared/Fields/InputReactSelect";
import * as Yup from "yup";

interface IProps {
    onCancel: any,
}

export const organizationChangeFormValidation = Yup.object().shape({
    organization_xid: Yup.string().required().nullable(),
});

const setOrganizationChangeFormData = (formData: any = {}) => {
    return {
        organization_xid: auth.getCurrentOrganizationId(),
    }
};

let loadedOrganizations: any = [];

const getOrganizationList = async () => {
    if (loadedOrganizations.length > 0) {
        return {data: {data: loadedOrganizations}};
    }
    const response = await api.organization.getList({all: 1});
    loadedOrganizations = response.data.data;
    response.data.data = response.data.data.map((item: any) => {
        const hyphen = item.level !== 1 ? "--".repeat(item.level) : "";
        return {...item, name: `${hyphen} ${item.name}`}
    });
    return response;
};

export function OrganizationChangeForm(props: IProps) {
    const [formData,] = useState<any>(setOrganizationChangeFormData());

    useEffect(() => {
        return () => {
            loadedOrganizations = [];
        }
    }, []);

    const getForm = (formProps: any) => {
        return (
            <Form>
                <ModalBody>
                    <Row>
                        <Col>
                            <Field component={InputReactSelect}
                                   name="organization_xid"
                                   api={getOrganizationList}
                                   label="Organization"
                                   isRequired
                                   labelField="name"
                                   valueField="organization_xid"
                            />
                        </Col>
                    </Row>
                </ModalBody>
                <ModalFooter>
                    <LoadingButton
                        color="primary"
                        size="sm"
                        type='submit'
                        disabled={formProps.isSubmitting}
                        isLoading={formProps.isSubmitting}
                    >
                        <i className="fas fa-save"/>{' '}Change
                    </LoadingButton>{' '}
                    <Button disabled={formProps.isSubmitting} color="secondary" size="sm" onClick={props.onCancel}>
                        Cancel
                    </Button>
                </ModalFooter>
            </Form>
        )
    };

    const handleOnSubmit = (formData: any, event: any) => {
        const organization_xid = formData.organization_xid;
        const organization = loadedOrganizations.find((item:any) => item.organization_xid === organization_xid);
        auth.setCurrentOrganization(organization);
        props.onCancel();
        event.setSubmitting(false);
        window.location.reload();
    };

    return (
        <>
            <Formik
                enableReinitialize={true}
                initialValues={formData}
                validationSchema={organizationChangeFormValidation}
                onSubmit={handleOnSubmit}
            >
                {(props) => getForm(props)}
            </Formik>
        </>
    );
}