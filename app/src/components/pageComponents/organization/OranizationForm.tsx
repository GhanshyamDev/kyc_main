import React, {useEffect, useState} from 'react';
import {auth} from "../../../util/Auth";
import {Field, Form, Formik} from "formik";
import {Button, Card, CardBody, Col, Row} from 'reactstrap';
import {InputField} from "../../shared/Fields/InputField";
import {InputReactSelect} from "../../shared/Fields/InputReactSelect";
import {api} from "../../../util/Api";
import {LoadingButton} from "../../shared/LoadingButton";
import {InputSwitch} from "../../shared/Fields/InputSwitch";
import ContentLoading from "../../shared/ContentLoading";
import {organizationValidateSchema} from "../../../util/Validation";
import {Toastiy} from "../../../util/Toast";
import {message} from "../../../locale/en/message";
import {useDispatch} from "react-redux";
import {UiAction} from "../../../store/UiState";

interface IProps {
    id: string | null,
    onCancel: any,
    loadData?: any
}

function setOrganizationFormData(formData: any = {}) {
    return {
        organization_xid: formData.organization_xid || null,
        //organizationTypeId: formData.organizationTypeId || '',
        parent_organization_xid: auth.getCurrentOrganizationId(),
        //timezone_id: formData.timezone_name || '',
        name: formData.name || '',
        contact_person: formData.contact_person || '',
        phone_number: formData.phone_number || '',
        email: formData.email || '',
        is_active: formData.is_active != null ? formData.is_active : true,
    }
}

export function OrganizationForm(props: IProps) {
    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState(false);
    const [formData, setFormData] = useState<any>(setOrganizationFormData({}));

    useEffect(() => {
        if (props.id) {
            dispatch(UiAction.showPageLoading());
            api.organization.getDetails(props.id).then((response: any) => {
                setFormData(setOrganizationFormData(response.data.data));
            }).finally(() => dispatch(UiAction.hidePageLoading()))
        }
    }, []);

    const getForm = (formProps: any) => {
        return (
            <Form>
                <Card>
                    <CardBody>
                        <Row>
                            <Col md={4}>
                                <Field component={InputField}
                                       name="name"
                                       label="Organization Name"
                                       isRequired
                                       placeholder="Enter Organization Name"/>
                            </Col>
                            <Col md={4}>
                                <Field component={InputField}
                                       name="contact_person"
                                       label="Contact Person"
                                       placeholder="Enter Contact person"/>
                            </Col>
                            <Col md={4}>
                                <Field component={InputField}
                                       name="phone_number"
                                       label="Phone Number"
                                       placeholder="Enter Phone Number"/>
                            </Col>
                            <Col md={4}>
                                <Field component={InputField}
                                       name="email"
                                       label="Email"
                                       placeholder="Enter Email"/>
                            </Col>
                            {/*<Col md={4}>
                                <Field component={InputReactSelect}
                                       name="parent_organization_xid"
                                       api={api.organization.getList}
                                       label="Parent Organization"
                                       labelField="organization_name"
                                       valueField="organization_xid"
                                />
                            </Col>
                            <Col md={4}>
                                <Field component={InputReactSelect}
                                       name="timezone_name"
                                       api={api.common.getTimeZoneList}
                                       label="Timezone"
                                       isRequired
                                       labelField="timezone_name"
                                       valueField="timezone_name"
                                />
                            </Col>*/}
                            <Col md={4}>
                                <Field
                                    component={InputSwitch}
                                    name="is_active"
                                    label="Is Active?"
                                />
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
                <div className="drawer-footer">
                    <LoadingButton
                        color="primary" size="sm" type='submit'
                        disabled={formProps.isSubmitting}
                        isLoading={formProps.isSubmitting}
                    >
                        <i className="fas fa-save"/>{' '}Save
                    </LoadingButton>{' '}
                    <Button
                        disabled={formProps.isSubmitting}
                        color="secondary"
                        size="sm"
                        onClick={props.onCancel}>
                        Cancel
                    </Button>
                </div>
            </Form>
        )
    };

    const add = (formData: any, event: any) => {
        api.organization.postCreate(formData).then(() => {
            Toastiy.success(message.recordAdded);
            props.loadData();
            props.onCancel();
        }).finally(() => event.setSubmitting(false));
    };

    const edit = (formData: any, event: any) => {
        api.organization.postUpdate(formData).then(() => {
            Toastiy.success(message.recordEdit);
            props.loadData();
            props.onCancel();
        }).finally(() => event.setSubmitting(false));
    };

    const handleOnSubmit = (formData: any, event: any) => {
        (formData.organization_xid) ? edit(formData, event) : add(formData, event);
    };

    return (
        <>
            <ContentLoading show={isLoading}/>
            <Formik
                enableReinitialize={true}
                initialValues={formData}
                validationSchema={organizationValidateSchema}
                onSubmit={handleOnSubmit}
            >
                {(props) => getForm(props)}
            </Formik>
        </>
    );
}
