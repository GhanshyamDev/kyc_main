import React from 'react';
import {auth} from "../../../util/Auth";
import {Card, CardBody, Col, Row} from "reactstrap";
import {Link} from "react-router-dom";
import {OrganizationType} from "../../../util/Enums";

interface IProps{

}

const currentOrganization = auth.getCurrentOrganization();

export function Dashboard(props: IProps) {
    return (
        <>
        <h1 className="mb-3">Hello! Welcome {auth.getFirstName()}</h1>
        <div>
            <Row>
                {(currentOrganization?.organization_type === OrganizationType.SuperAdmin
                    || currentOrganization?.organization_type === OrganizationType.Company) &&
                    <Col md={3} className="mb-3">
                        <Card className="shadow-sm">
                            <CardBody className="text-center pl-3 pr-3">
                                <Link to="/organization/list" className="text-primary">
                                    <i className="mdi mdi-domain menu-icon"/>{' '}
                                    <span>Organization</span>
                                </Link>
                            </CardBody>
                        </Card>
                    </Col>
                }
                <Col md={3} className="mb-3">
                    <Card className="shadow-sm">
                        <CardBody className="text-center pl-3 pr-3">
                            <Link to="/user/list" className="text-primary">
                                <i className="mdi mdi-account menu-icon"/>{' '}
                                <span>User</span>
                            </Link>
                        </CardBody>
                    </Card>
                </Col>
                <Col md={3} className="mb-3">
                    <Card className="shadow-sm">
                        <CardBody className="text-center pl-3 pr-3">
                            <Link to="/candidate/list" className="text-primary">
                                <i className="mdi mdi-account menu-icon"/>{' '}
                                <span>Candidate</span>
                            </Link>
                        </CardBody>
                    </Card>
                </Col>
                <Col md={3} className="mb-3">
                    <Card className="shadow-sm">
                        <CardBody className="text-center pl-3 pr-3">
                            <Link to="/report/candidate" className="text-primary">
                                <i className="mdi mdi-chart-bar menu-icon"/>{' '}
                                <span>Candidate Report</span>
                            </Link>
                        </CardBody>
                    </Card>
                </Col>
            </Row>

        </div>
        </>
    );
}