import React, {useEffect} from 'react';
import {BrowserRouter as Router, useLocation, Switch, Route} from 'react-router-dom';
import {getPageClass} from "../../util/Helpers";
import {loading} from "../shared/Common";
import {Card, CardBody} from "reactstrap";
import {toast} from "react-toastify";

const WebLinkCandidatePage = React.lazy(() => import('../pages/requestInfo/WebLinkCandidatePage'));
const OTPValidatePage = React.lazy(() => import('../pages/requestInfo/OTPValidatePage'));
const CandidateRequestInfo1Page = React.lazy(() => import('../pages/requestInfo/CandidateRequestInfo1Page'));
const CandidateRequestInfo2Page = React.lazy(() => import('../pages/requestInfo/CandidateRequestInfo2Page'));
const CustomerRequestInfoPage = React.lazy(() => import('../pages/requestInfo/CustomerRequestInfoPage'));
const RequestInfoPage = React.lazy(() => import('../pages/requestInfo/RequestInfoPage'));
const RequestInfoThankYouPage = React.lazy(() => import('../pages/requestInfo/RequestInfoThankYouPage'));

interface IProps {

}
let sessionTime = 0;

export default function RequestInfoLayout(props: IProps) {
    useEffect(() => {
        setInterval(() => {
            sessionTime++;
            if (sessionTime === (60 * 60 * 5)) {
                window.location.reload();
            }
        }, 1000);
    }, []);

    return (
        <div id='wrapper'>
            <Router>
                <div className="container-scroller">
                    <div className="container">
                        <div className={`request-layout-container mt-5`}>
                            <Card>
                                <CardBody>
                                    <PageContent>
                                        <React.Suspense fallback={loading()}>
                                            <Switch>
                                                <Route exact path="/ri" component={RequestInfoPage} />
                                                <Route path="/ri/wl/c/:token" component={WebLinkCandidatePage} />
                                                <Route path="/ri/otp/:token" component={OTPValidatePage} />
                                                <Route path="/ri/c/s1/:token" component={CandidateRequestInfo1Page} />
                                                <Route path="/ri/c/s2/:token" component={CandidateRequestInfo2Page} />
                                                <Route path="/ri/c/ci/:token" component={CustomerRequestInfoPage} />
                                                <Route path="/ri/thank-you" component={RequestInfoThankYouPage} />
                                            </Switch>
                                        </React.Suspense>
                                    </PageContent>
                                </CardBody>
                            </Card>
                        </div>
                    </div>
                </div>
            </Router>
        </div>
    );
}

function PageContent(props: any) {
    let location = useLocation();
    const pageClass = getPageClass(location.pathname);

    useEffect(() => {
        toast.dismiss();
    }, [location.pathname]);

    return (
        <div className={`${pageClass}-page page`}>
            <div className="content">
                {props.children}
            </div>
        </div>
    )
}

