import React, {useEffect} from 'react';
import {IRootState} from "../../store/AppStore";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import NotFound from "../pages/NotFound";
import PageLoading from "../shared/PageLoading";
import {loading} from "../shared/Common";
import {ToastContainer} from "react-toastify";
import {useSelector} from "react-redux";

const LoginPage = React.lazy(() => import('../pages/auth/LoginPage'));
const ForgotPasswordPage = React.lazy(() => import('../pages/auth/ForgotPasswordPage'));
const ResetPasswordPage = React.lazy(() => import('../pages/auth/PasswordReset'));
const ChangePasswordPage = React.lazy(() => import('../pages/ChangePasswordPage'));
const DefaultLayout = React.lazy(() => import('../layouts/DefaultLayout'));
const RequestInfoLayout = React.lazy(() => import('../layouts/RequestInfoLayout'));

interface IProps {

}

export interface ISendBy {
    send_sms: boolean,
    send_email: boolean
}

declare global {
    interface Window {
        requestInfoOtpSendBy: ISendBy|null,
    }
}

export default function AppContainer(props: IProps) {
    const {showPageLoading} = useSelector((state: IRootState) => state.ui);
    return (
        <>
            <PageLoading show={showPageLoading}/>
            <Router>
                <React.Suspense fallback={loading()}>
                    <Switch>
                        <Route path="/login" component={LoginPage}/>
                        <Route path="/forgot-password" component={ForgotPasswordPage} />
                        <Route path="/reset-password/:token/:email" component={ResetPasswordPage} />
                        <Route path="/change-password" component={ChangePasswordPage}/>
                        <Route path="/ri" component={RequestInfoLayout}/>
                        <Route path="/" component={DefaultLayout}/>
                        <Route component={NotFound}/>
                    </Switch>
                </React.Suspense>
            </Router>
            <ToastContainer/>
        </>
    );
}
