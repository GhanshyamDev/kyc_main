import React, {useEffect} from 'react';
import Header from "../partials/Header";
import {BrowserRouter as Router, useLocation, Switch, Route, Redirect, useHistory} from 'react-router-dom';
import {useSelector} from "react-redux";
import {IRootState} from "../../store/AppStore";
import classNames from "classnames";
import {getPageClass} from "../../util/Helpers";
import {loading} from "../shared/Common";
import {auth} from "../../util/Auth";
import SideNav from "../partials/SideNav";
import {toast} from "react-toastify";
import {OrganizationType} from "../../util/Enums";

/* Admin */
const DashboardPage = React.lazy(() => import('../pages/DashboardPage'));
const OrganizationPage = React.lazy(() => import('../pages/OrganizationPage'));
const UserPage = React.lazy(() => import('../pages/UserPage'));
const ProfilePage = React.lazy(() => import('../pages/ProfilePage'));
const CandidatePage = React.lazy(() => import('../pages/CandidatePage'));
const CustomerPage = React.lazy(() => import('../pages/CustomerPage'));
const CandidateReportPage = React.lazy(() => import('../pages/reports/CandidateReportPage'));
const SummaryReportPage = React.lazy(() => import('../pages/reports/SummaryReportPage'));

/* Other Page */

interface IProps {

}

const currentOrganization = auth.getCurrentOrganization();

export default function DefaultLayout(props: IProps) {
    const {isSideNavShrinked} = useSelector((state: IRootState) => state.ui);

    if (!auth.isLoggedIn()) {
        return <Redirect to="/login"/>;
    }

    const firstUserLogin = auth.getUser();
    if(firstUserLogin && firstUserLogin.first_login) {
        return <Redirect to="/change-password"/>;
    }

    return (
        <div id='wrapper'>
            <Router>
                <div className="container-scroller">
                    <Header />
                    <div className="container-fluid page-body-wrapper">
                        <SideNav isShrinked={isSideNavShrinked}/>
                        <div className="main-panel">
                            <div className={`content-wrapper ${classNames("shrinked", isSideNavShrinked)}`}>
                                <PageContent>
                                    <React.Suspense fallback={loading()}>
                                        <Switch>
                                            <Route exact path="/" component={DashboardPage} />
                                            <Route path="/dashboard" component={DashboardPage} />
                                            {(currentOrganization?.organization_type === OrganizationType.SuperAdmin
                                                || currentOrganization?.organization_type === OrganizationType.Company) &&
                                                <Route path="/organization" component={OrganizationPage}/>
                                            }
                                            <Route path="/user" component={UserPage} />
                                            <Route path="/profile" component={ProfilePage} />
                                            <Route path="/candidate" component={CandidatePage} />
                                            <Route path="/customer" component={CustomerPage} />
                                            <Route path="/report/candidate" component={CandidateReportPage} />
                                            <Route path="/report/candidate-summary-report" component={SummaryReportPage} />
                                        </Switch>
                                    </React.Suspense>
                                </PageContent>
                            </div>
                        </div>
                    </div>
                </div>
            </Router>
        </div>
    );
}

function PageContent(props: any) {
    let location = useLocation();
    const pageClass = getPageClass(location.pathname);

    useEffect(() => {
        toast.dismiss();
        document?.querySelector('.sidebar-offcanvas')?.classList.remove('active');
    }, [location.pathname]);

    return (
        <div className={`${pageClass}-page page`}>
            <div className="content">
                {props.children}
            </div>
        </div>
    )
}

