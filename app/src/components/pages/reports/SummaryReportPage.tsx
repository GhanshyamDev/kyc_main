import {useDispatch} from "react-redux";
import React, {useEffect, useRef, useState} from "react";
import {api} from "../../../util/Api";
import {UiAction} from "../../../store/UiState";
import {ITableData} from "../../shared/TableWrp/TableWrp";
import {Card, CardBody, Col, Collapse, Row} from "reactstrap";
import {Field, Form, Formik, FormikProps} from "formik";
import {InputField} from "../../shared/Fields/InputField";
import {InputReactSelect} from "../../shared/Fields/InputReactSelect";
import Api from "../../../util/Api";
import AppConstants from "../../../util/AppConstants";
import moment from "moment";
import {candidateStatusOptions, dateToUtcDate, displayCandidateStatus} from "../../../util/Helpers";
import {InputDate} from "../../shared/Fields/InputDate";
import {Button} from "../../shared/Button";
import {fromToDateValidationSchema} from "../../../util/Validation";
const $: any = require('jquery');
$.DataTable = require('datatables.net');

require('datatables.net-buttons');
require('datatables.net-buttons/js/buttons.flash.js');//# Flash file export
require('datatables.net-buttons/js/buttons.html5.js');//# HTML 5 file export
require('datatables.net-buttons/js/buttons.print.js');//# Print view button
require('datatables.net-buttons-dt');
require('../../../util/datetime-moment');

$.DataTable.moment(AppConstants.DEFAULT_DATE_FORMAT);
let queryParams: any = {};

interface IProps {
    data: ITableData,
    useUrlQueryParams?: boolean | null,
    onEdit?: any,
    onDelete?: any,
    loadData: any
}

const initialValues = {
    fromDate: moment().set({
        hours: 0,
        minutes: 0,
        second: 0
    }).subtract(2, 'days').format(AppConstants.DEFAULT_DATE_FORMAT),
    toDate: moment().set({hours: 23, minutes: 59, second: 59}).format(AppConstants.DEFAULT_DATE_FORMAT),
    current_status: '',
    organization: null,
    search: ''
};

let dbTable: any = null;

export default function CandidateReportPage() {
    const reportTitle = "Consolidated Candidate Summary Report";
    const fileName = `SummaryReport_${moment().format(AppConstants.ONLY_DATE_FORMAT)}`;
    const [isFilterOpen, setIsFilterOpen] = useState(false);
    const dispatch = useDispatch();
    const el = useRef(null);
    let $el: any = null;

    useEffect(() => {
        $el = $(el.current);
        dbTable = $el.DataTable(getDataTablesOptions());
        loadData(initialValues);
    }, []);

    const setFilters = (data: any = {}) => {
        return {
            filters: {
                ...data.fromDate && {fromDate: dateToUtcDate(data.fromDate, AppConstants.DEFAULT_DATE_FORMAT)},
                ...data.toDate && {toDate: dateToUtcDate(data.toDate, AppConstants.DEFAULT_DATE_FORMAT)},
            }
        }
    };

    const loadData = (params: any = {}, event: any = undefined) => {
        dispatch(UiAction.showPageLoading());
        api.report.postSummaryReport({filters: params}).then((response: any) => {
            dbTable.clear().rows.add(response.data.data).draw();
        }).finally(() => {
            if (event) {
                event.setSubmitting(false)
            }
            dispatch(UiAction.hidePageLoading())
        });
    };

    const getDataTablesOptions = () => {
        return {
            data: [],
            columns: columns(),
            pageLength: 25,
            order: [],
            dom: 'Bfrtip',
            buttons: [
                {extend: 'excelHtml5', title: reportTitle, filename: fileName},
                {extend: 'csvHtml5', title: reportTitle, filename: fileName},
                {extend: 'pdfHtml5', title: reportTitle, filename: fileName},
            ]
        }
    };

    const getForm = (props: any) => (
        <Form>
            <Row className="mt-2">
                <Col md={3}>
                    <Field component={InputDate}
                           isRequired
                           showTimeInput
                           name="fromDate"
                           label="From Date"
                    />
                </Col>
                <Col md={3}>
                    <Field component={InputDate}
                           isRequired
                           showTimeInput
                           name="toDate"
                           label="To Date"
                    />
                </Col>
            </Row>
            <div className="text-right border-top pt-2">
                <Button color="primary"
                        size="sm"
                        type='submit'
                        disabled={props.isSubmitting}
                        isLoading={props.isSubmitting}>
                    <i className="fas fa-search"/>{' '}Search
                </Button>
            </div>
        </Form>
    );

    const columns = () => {
        return [
            {
                title: 'Organization Name',
                data: 'organizationName',
                orderable:false,
                render: (data:any, type: any, row:any) => {
                    return (row.level !== 1 ? "--".repeat(row.level) : "") + data;
                }
            }, {
                title: 'Verified Count',
                data: 'verifiedCount',
                orderable:false,
            }, {
                title: 'Suspecious Count',
                data: 'suspeciousCount',
                orderable:false,
            }, {
                title: 'Total Count',
                data: 'totalCount',
                orderable:false,
            }
        ]
    };

    return (
        <div className="">
            <Card className={'br-g-300 shadow rounded-2 mb-3'} style={{display: 'block'}}>
                <CardBody className="p-3">
                    <div>
                        <h4 className="mb-0">{reportTitle}
                            <i className="fa fa-filter cursor-pointer text-primary float-right"
                               onClick={() => setIsFilterOpen(!isFilterOpen)}
                            />
                        </h4>
                    </div>
                    <Collapse isOpen={isFilterOpen}>
                        <Formik
                            validationSchema={fromToDateValidationSchema}
                            initialValues={initialValues}
                            onSubmit={((values, event) => loadData(values, event))}
                        >{(props: FormikProps<any>) => getForm(props)}
                        </Formik>
                    </Collapse>
                </CardBody>
            </Card>

            <Card className={'br-g-300 shadow rounded-2 mt-3 mb-3'}>
                <CardBody className="report-viewer">
                    <table className="display table table-responsive-md" style={{width: "100%"}} ref={el}/>
                </CardBody>
            </Card>
        </div>
    );
}
