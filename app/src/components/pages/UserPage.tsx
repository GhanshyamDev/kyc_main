import {useDispatch} from "react-redux";
import React, {useState} from "react";
import {Button} from "reactstrap";
import {api} from "../../util/Api";
import {UiAction} from "../../store/UiState";
import {UserList} from "../pageComponents/user/UserList";
import FormDrawerWrp from "../shared/FormDrawerWrp";
import {UserForm} from "../pageComponents/user/UserForm";
import {message} from "../../locale/en/message";
import {DeleteConfirm} from "../shared/DeleteConfirm";
import {Toastiy} from "../../util/Toast";
import {ITableData} from "../shared/TableWrp/TableWrp";

let queryParams: any = {};

export default function UserPage() {
    const dispatch = useDispatch();
    const [showFormModal, setShowFormModal] = useState(false);
    const [id, setId] = useState<string | null>(null);
    const [formTitle, setFormTitle] = useState<string>("Add User");
    const [data, setData] = useState<ITableData>({ list: [], total: 0 });

    const handleOnAdd = () => {
        setId(null);
        setFormTitle("Add User");
        setShowFormModal(true);
    };

    const handleOnEdit = (record: any) => {
        setId(record.user_xid);
        setFormTitle(`Edit User - ${record.first_name} ${record.last_name}`);
        setShowFormModal(true);
    };

    const handleOnDelete = (record: any) => {
        DeleteConfirm({
            confirmText: message.confirmDelete,
            onConfirm: () => {
                dispatch(UiAction.showPageLoading());
                const param = {
                    user_xid: record.user_xid
                };
                api.user.postDelete(param)
                    .then(response => {
                        Toastiy.success(message.recordDeleted);
                        loadData(queryParams);
                    }).finally(() => dispatch(UiAction.showPageLoading()));
            }
        })
    };

    const loadData = (listQueryParams: any = {}) => {
        queryParams = listQueryParams;
        dispatch(UiAction.showPageLoading());
        api.user.getList(queryParams).then((response: any) => {
            setData({ total: response.data.meta.total, list: response.data.data });
        }).finally(() => {
            dispatch(UiAction.hidePageLoading());
        });
    };

    return (
        <div className="content-inner">
            <div className="mb-4 text-right">
                <Button color="primary" size="sm" onClick={handleOnAdd}>
                    <i className='fas fa-plus' /> Add User
                </Button>
            </div>

            <UserList
                data={data}
                useUrlQueryParams={true}
                onEdit={handleOnEdit}
                onDelete={handleOnDelete}
                loadData={loadData}
            />
            {(showFormModal) &&
            <FormDrawerWrp title={formTitle}
                           width='80%' showDrawer={showFormModal}
                           onClose={() => {
                               setShowFormModal(false);
                               loadData(queryParams);
                           }}
            >
                <UserForm
                    id={id}
                    onCancel={() => setShowFormModal(false)}
                    loadData={() => loadData(queryParams)} />
            </FormDrawerWrp>
            }
        </div>
    );
}
