import React, {useEffect, useState} from 'react';
import {Button, Card, CardBody, CardHeader, Col, Collapse, Row} from "reactstrap";
import {useHistory, useParams} from "react-router";
import {InValidRequest, LoadingContent, useVerifyLinkToken} from "./RequestInfoCustomHooks";
import {Field, Form, Formik} from "formik";
import {displayCandidateStatus, getCookie} from "../../../util/Helpers";
import {InputField} from "../../shared/Fields/InputField";
import {LoadingButton} from "../../shared/LoadingButton";
import {InputUploadImage} from "../../shared/Fields/InputUploadImage";
import * as Yup from "yup";
import {api} from "../../../util/Api";
import {useDispatch} from "react-redux";
import {UiAction} from "../../../store/UiState";
import {InputCheckboxSingle} from "../../shared/Fields/InputCheckboxSingle";
import {selfieSubmitConfirm} from "../../shared/DeleteConfirm";
import {message} from "../../../locale/en/message";
import {Toastiy} from "../../../util/Toast";
import {InputUploadFile} from "../../shared/Fields/InputUploadFile";

interface IProps {

}

const validationSchema = Yup.object().shape({
    master_image: Yup.string().required("Photo is required.").nullable(),
    first_name: Yup.string().required("First Name is required.").nullable(),
    last_name: Yup.string().required("Last Name is required.").nullable(),
    address: Yup.string().required("Address Name is required.").nullable(),
    agree: Yup.bool().oneOf([true], 'Accept Terms & Conditions is required'),
});

const setCustomerData = (formData: any = {}) => {
    return {
        master_image: formData.master_image || '',
        first_name: formData.first_name || '',
        last_name: formData.last_name || '',
        address: formData.address || '',
        agree: formData.agree || false,
    }
};

let currentLocation: any = null;

export default function CustomerRequestInfoPage(props: IProps) {
    const dispatch = useDispatch();
    const history = useHistory();
    const {token} = useParams();
    const [locationAllowed, setLocationAllowed] = useState<boolean>(true);
    const [currentStep, setCurrentStep] = useState<number>(1);
    //const {isChecking, isValidLinkToken, webLinkInfo} = useVerifyLinkToken(token);
    const webLinkInfo = {
        organization_name: "ABC Organization"
    }
    const [formData, setFormData] = useState<any>(setCustomerData({}));
    const vpto: any = getCookie("vpto");

    useEffect(() => {
        navigator.geolocation.getCurrentPosition((position) => {
            //set current location
            currentLocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            }
            setLocationAllowed(true);
        }, () => {
            setLocationAllowed(false);
        });
    }, []);

    /*useEffect(() => {
        setFormData({...formData, ...webLinkInfo});
    }, [webLinkInfo]);*/

    /*if (!window.requestInfoOtpSendBy || !vpto) {
        history.push(`/ri/wl/c/${token}`);
        return <></>;
    }

    if (isChecking) {
        return <LoadingContent/>
    }

    if (!isChecking && !isValidLinkToken) {
        return <InValidRequest/>
    }*/

    if (!locationAllowed) {
        return <div className="text-center text-danger">Allow location access to proceed.</div>
    }

    const getForm = (formProps: any) => {
        return (
            <Form>
                <Card>
                    <CardHeader>
                        <h5 className="mb-0">1. Upload Your Photo & Fill Basic Info</h5>
                    </CardHeader>
                    <Collapse
                        isOpen={currentStep === 1}
                    >
                    <CardBody>
                        <Row>
                            <Col md={6}>
                                <Field component={InputUploadImage}
                                       name="passport_size_photo"
                                       isRequired
                                       postUrl={`${process.env.REACT_APP_API_URL}file/upload-candidate-selfie-image/${token}`}
                                       cameraPictureWidth="300"
                                       previewHeight="225px"
                                       captureByCamera={true}
                                       candidateSelfie={false}
                                       authRequired={false}
                                       label="Upload Your Photo"
                                       desc="Upload Image file only and max 10 MB."
                                />
                            </Col>
                            <Col md={6}>
                                {''}
                            </Col>
                            <Col md={6}>
                                <Field component={InputField}
                                       name="first_name"
                                       label="First Name"
                                       isRequired
                                       placeholder="Enter First Name"/>
                            </Col>
                            <Col md={6}>
                                <Field component={InputField}
                                       name="last_name"
                                       label="Last Name"
                                       isRequired
                                       placeholder="Enter Last Name"/>
                            </Col>
                            <Col md={6}>
                                <Field component={InputField}
                                       name="phone_number"
                                       label="Phone Number"
                                       isRequired
                                       placeholder="Enter Phone Number"/>
                            </Col>
                            <Col md={6}>
                                <Field component={InputField}
                                       name="email"
                                       label="Email"
                                       isRequired
                                       placeholder="Enter Email"/>
                            </Col>
                            <Col md={12}>
                                <Field component={InputField}
                                       name="address"
                                       label="Address"
                                       isRequired
                                       placeholder="Enter Your Address"/>
                            </Col>
                        </Row>
                        <div className="text-right">
                            <LoadingButton
                                color="primary"
                                type='submit'
                                size="sm"
                                disabled={formProps.isSubmitting}
                                isLoading={formProps.isSubmitting}
                            >
                                Next
                            </LoadingButton>
                        </div>
                    </CardBody>
                    </Collapse>
                </Card>
                <Card className="mt-2">
                    <CardHeader>
                        <h5 className="mb-0">2. Upload Documents</h5>
                    </CardHeader>
                    <Collapse
                        isOpen={currentStep === 2}
                    >
                        <CardBody>
                            <Row>
                                <Col md={4}>
                                    <Field component={InputUploadFile}
                                           name="doc_image_1"
                                           postUrl={`${process.env.REACT_APP_API_URL}file/upload-image/` + token}
                                           showPreview={true}
                                           placeholder="Upload File"
                                           previewHeight="150px"
                                           desc="PAN card, Aadhaar Card"
                                           label="Identity Proof"
                                           isDisabled={formData.completed_status}
                                           //imageStatus={displayCandidateStatus(formData.doc_image_1.imageStatus)}
                                           //onImageUpload={(data: any) => imageUploaded(data, "doc_image_1")}
                                    />
                                </Col>
                                <Col md={4}>
                                    <Field component={InputUploadFile}
                                           name="doc_image_2"
                                           postUrl={`${process.env.REACT_APP_API_URL}file/upload-image/` + token}
                                           showPreview={true}
                                           placeholder="Upload File"
                                           previewHeight="150px"
                                           label="Address Proof"
                                           desc="Passport, Driving Licence, Aadhaar Card, Bank Statement, Utility bills"
                                           isDisabled={formData.completed_status}
                                        //imageStatus={displayCandidateStatus(formData.doc_image_2.imageStatus)}
                                        //onImageUpload={(data: any) => imageUploaded(data, "doc_image_2")}
                                    />
                                </Col>
                                <Col md={4}>
                                    <Field component={InputUploadFile}
                                           name="doc_image_3"
                                           postUrl={`${process.env.REACT_APP_API_URL}file/upload-image/` + token}
                                           showPreview={true}
                                           placeholder="Upload File"
                                           previewHeight="150px"
                                           label="Income Proof"
                                           desc="Salary slip (latest), ITR (latest), Bank Statement (6 months)"
                                           isDisabled={formData.completed_status}
                                        //imageStatus={displayCandidateStatus(formData.doc_image_3.imageStatus)}
                                        //onImageUpload={(data: any) => imageUploaded(data, "doc_image_3")}
                                    />
                                </Col>
                                <Col md={4}>
                                    <Field component={InputUploadFile}
                                           name="doc_image_4"
                                           postUrl={`${process.env.REACT_APP_API_URL}file/upload-image/` + token}
                                           showPreview={true}
                                           placeholder="Upload File"
                                           previewHeight="150px"
                                           label="Account Linking"
                                           desc="Cancelled Cheque"
                                           isDisabled={formData.completed_status}
                                        //imageStatus={displayCandidateStatus(formData.doc_image_4.imageStatus)}
                                            //onImageUpload={(data: any) => imageUploaded(data, "doc_image_4")}
                                    />
                                </Col>
                                <Col md={4}>
                                    <Field component={InputUploadFile}
                                           name="doc_image_5"
                                           postUrl={`${process.env.REACT_APP_API_URL}file/upload-image/` + token}
                                           showPreview={true}
                                           placeholder="Upload Image"
                                           previewHeight="150px"
                                           label="Signed on Blank Paper"
                                           desc="Signed on Blank Paper and Upload"
                                           isDisabled={formData.completed_status}
                                        //imageStatus={displayCandidateStatus(formData.doc_image_4.imageStatus)}
                                            //onImageUpload={(data: any) => imageUploaded(data, "doc_image_5")}
                                    />
                                </Col>
                            </Row>
                            <div className="text-right">
                                <Button disabled={formProps.isSubmitting} color="secondary" size="sm" onClick={onPrevious}>
                                    Previous
                                </Button>
                                <LoadingButton
                                    color="primary"
                                    type='submit'
                                    size="sm"
                                    disabled={formProps.isSubmitting}
                                    isLoading={formProps.isSubmitting}
                                >
                                    Next
                                </LoadingButton>
                            </div>
                        </CardBody>
                    </Collapse>
                </Card>
                <Card className="mt-2">
                    <CardHeader>
                        <h5 className="mb-0">3. Answer the questions</h5>
                    </CardHeader>
                    <Collapse
                        isOpen={currentStep === 3}
                    >
                        <CardBody>
                            <Row>
                                <Col md={12}>
                                    <Field component={InputField}
                                           type="textarea"
                                           name="question_1"
                                           label="What Lorem Ipsum is simply dummy text?"
                                           disabled={formData.completed_status}
                                           placeholder="Enter Comment" />
                                </Col>
                                <Col md={12}>
                                    <Field component={InputField}
                                           type="textarea"
                                           name="question_2"
                                           label="What Lorem Ipsum is simply dummy text?"
                                           disabled={formData.completed_status}
                                           placeholder="Enter Comment" />
                                </Col>
                                <Col md={12}>
                                    <Field component={InputField}
                                           type="textarea"
                                           name="question_3"
                                           label="What Lorem Ipsum is simply dummy text?"
                                           disabled={formData.completed_status}
                                           placeholder="Enter Comment" />
                                </Col>
                                <Col md={12}>
                                    <Field component={InputField}
                                           type="textarea"
                                           name="question_4"
                                           label="What Lorem Ipsum is simply dummy text?"
                                           disabled={formData.completed_status}
                                           placeholder="Enter Comment" />
                                </Col>
                                <Col md={12}>
                                    <Field component={InputField}
                                           type="textarea"
                                           name="question_5"
                                           label="What Lorem Ipsum is simply dummy text?"
                                           disabled={formData.completed_status}
                                           placeholder="Enter Comment" />
                                </Col>
                            </Row>
                            <div className="text-right">
                                <Button disabled={formProps.isSubmitting} color="secondary" size="sm" onClick={onPrevious}>
                                    Previous
                                </Button>
                                <LoadingButton
                                    color="primary"
                                    size="sm"
                                    type='submit'
                                    disabled={formProps.isSubmitting}
                                    isLoading={formProps.isSubmitting}
                                >
                                    Next
                                </LoadingButton>
                            </div>
                        </CardBody>
                    </Collapse>
                </Card>
                <Card className="mt-2">
                    <CardHeader>
                        <h5 className="mb-0">4. Finish</h5>
                    </CardHeader>
                    <Collapse
                        isOpen={currentStep === 4}
                    >
                        <CardBody>
                            <Row>
                                <Col md={12}>
                                    <Field component={InputCheckboxSingle}
                                           name="agree"
                                           label="Agree to provide my consent to CelebratingLife to use my personal information and Images for assessment and verification of my identity."
                                    />
                                </Col>
                            </Row>
                            <div className="text-center mt-3">
                                <Button disabled={formProps.isSubmitting} color="secondary" onClick={onPrevious}>
                                    Previous
                                </Button>
                                <LoadingButton
                                    color="primary"
                                    type='submit'
                                    disabled={formProps.isSubmitting}
                                    isLoading={formProps.isSubmitting}
                                >
                                    Submit
                                </LoadingButton>
                            </div>
                        </CardBody>
                    </Collapse>
                </Card>

            </Form>
        )
    };

    const onPrevious = () => {
        if (currentStep !== 1) {
            setCurrentStep(currentStep - 1);
        }
    }

    const handleOnSubmit = (formData: any, event: any) => {
        if (currentStep === 4) {
            //last step
        } else {
            setCurrentStep(currentStep + 1);
        }
        event.setSubmitting(false);
        return;

        //submit candidate form data and redirect to thank you page
        /*const data = {
            master_image: formData.master_image,
            first_name: formData.first_name,
            last_name: formData.last_name,
            address: formData.address,
            vpto: vpto,
            agree: formData.agree,
            location: currentLocation,
        };

        selfieSubmitConfirm({
            onConfirm: () => {
                dispatch(UiAction.showPageLoading());
                /!*api.requestInfo.postCustomerSelfie1(data).then(() => {
                    history.push(`/ri/thank-you`);
                    return <></>;
                }).finally(() => {
                    dispatch(UiAction.hidePageLoading());
                    event.setSubmitting(false)
                })*!/
            },
            onCancel: () => {
                event.setSubmitting(false)
            }
        });*/
    };

    return (
        <div>
            <div className="text-center">
                <h2>{webLinkInfo.organization_name}</h2>
            </div>
            <h4 className="mt-3 text-center">Fill all the requested information, require by organization.</h4>
            <div className="mt-5 d-flex justify-content-center">
                <div style={{width: "100%", maxWidth: "1000px"}}>
                    <Formik
                        enableReinitialize={true}
                        initialValues={formData}
                        //validationSchema={validationSchema}
                        onSubmit={handleOnSubmit}
                    >
                        {(props) => getForm(props)}
                    </Formik>
                </div>
            </div>
        </div>
    );
}