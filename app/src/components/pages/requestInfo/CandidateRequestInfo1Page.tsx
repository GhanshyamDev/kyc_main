import React, {useEffect, useState} from 'react';
import {Card, CardBody, Col, Row} from "reactstrap";
import {useHistory, useParams} from "react-router";
import {InValidRequest, LoadingContent, useVerifyLinkToken} from "./RequestInfoCustomHooks";
import {Field, Form, Formik} from "formik";
import {getCookie} from "../../../util/Helpers";
import {InputField} from "../../shared/Fields/InputField";
import {LoadingButton} from "../../shared/LoadingButton";
import {InputUploadImage} from "../../shared/Fields/InputUploadImage";
import * as Yup from "yup";
import {api} from "../../../util/Api";
import {useDispatch} from "react-redux";
import {UiAction} from "../../../store/UiState";
import {InputCheckboxSingle} from "../../shared/Fields/InputCheckboxSingle";
import {selfieSubmitConfirm} from "../../shared/DeleteConfirm";
import {message} from "../../../locale/en/message";
import {Toastiy} from "../../../util/Toast";

interface IProps {

}

const validationSchema = Yup.object().shape({
    master_image: Yup.string().required("Photo is required.").nullable(),
    first_name: Yup.string().required("First Name is required.").nullable(),
    last_name: Yup.string().required("Last Name is required.").nullable(),
    address: Yup.string().required("Address Name is required.").nullable(),
    agree: Yup.bool().oneOf([true], 'Accept Terms & Conditions is required'),
});

const setCandidateData = (formData: any = {}) => {
    return {
        master_image: formData.master_image || '',
        first_name: formData.first_name || '',
        last_name: formData.last_name || '',
        address: formData.address || '',
        agree: formData.agree || false,
    }
};

let currentLocation: any = null;

export default function CandidateRequestInfo1Page(props: IProps) {
    const dispatch = useDispatch();
    const history = useHistory();
    const {token} = useParams();
    const [locationAllowed, setLocationAllowed] = useState<boolean>(true);
    const {isChecking, isValidLinkToken, webLinkInfo} = useVerifyLinkToken(token);
    const [formData, setFormData] = useState<any>(setCandidateData({}));
    const vpto: any = getCookie("vpto");

    useEffect(() => {
        navigator.geolocation.getCurrentPosition((position) => {
            //set current location
            currentLocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            }
            setLocationAllowed(true);
        }, () => {
            setLocationAllowed(false);
        });
    }, []);

    useEffect(() => {
        setFormData({...formData, ...webLinkInfo});
    }, [webLinkInfo]);

    if (!window.requestInfoOtpSendBy || !vpto) {
        history.push(`/ri/wl/c/${token}`);
        return <></>;
    }

    if (isChecking) {
        return <LoadingContent/>
    }

    if (!isChecking && !isValidLinkToken) {
        return <InValidRequest/>
    }

    if (!locationAllowed) {
        return <div className="text-center text-danger">Allow location access to proceed.</div>
    }

    const getForm = (formProps: any) => {
        return (
            <Form>
                <Row>
                    <Col md={6}>
                        <Field component={InputUploadImage}
                               name="master_image"
                               isRequired
                               postUrl={`${process.env.REACT_APP_API_URL}file/upload-candidate-selfie-image/${token}`}
                               cameraPictureWidth="300"
                               previewHeight="225px"
                               captureByCamera={true}
                               candidateSelfie={false}
                               authRequired={false}
                               label="Upload Your Photo"
                               desc="Upload Image file only and max 10 MB."
                        />
                    </Col>
                    <Col md={6}>
                        <div className="h4 mb-2">Application Information</div>
                        <div className="mb-2"><label className="h5">Job Id</label>: {webLinkInfo.job_id}</div>
                        <div className="mb-2"><label className="h5">Designation</label>: {webLinkInfo.designation}</div>
                        {/*<div className="mb-2"><label className="h5">First Name</label>: {webLinkInfo.first_name}</div>
                        <div className="mb-2"><label className="h5">Last Name</label>: {webLinkInfo.first_name}</div>
                        <div className="mb-2"><label className="h5">Address</label>: {webLinkInfo.address}</div>*/}
                    </Col>
                </Row>
                <Row>
                    <Col md={6}>
                        <Field component={InputField}
                               name="first_name"
                               label="First Name"
                               isRequired
                               placeholder="Enter First Name"/>
                    </Col>
                    <Col md={6}>
                        <Field component={InputField}
                               name="last_name"
                               label="Last Name"
                               isRequired
                               placeholder="Enter Last Name"/>
                    </Col>
                    <Col md={12}>
                        <Field component={InputField}
                               name="address"
                               label="Address"
                               isRequired
                               placeholder="Enter Your Address"/>
                    </Col>
                    <Col md={12}>
                        <Field component={InputCheckboxSingle}
                               name="agree"
                               label="Agree to provide my consent to CelebratingLife to use my personal information and Images for assessment and verification of my identity."
                        />
                    </Col>
                </Row>
                <div className="text-center">
                <LoadingButton
                    color="primary"
                    type='submit'
                    disabled={formProps.isSubmitting}
                    isLoading={formProps.isSubmitting}
                >
                    Submit
                </LoadingButton>
                </div>
            </Form>
        )
    };

    const handleOnSubmit = (formData: any, event: any) => {
        //submit candidate form data and redirect to thank you page
        const data = {
            master_image: formData.master_image,
            first_name: formData.first_name,
            last_name: formData.last_name,
            address: formData.address,
            vpto: vpto,
            agree: formData.agree,
            location: currentLocation,
        };

        selfieSubmitConfirm({
            onConfirm: () => {
                dispatch(UiAction.showPageLoading());
                api.requestInfo.postCandidateSelfie1(data).then(() => {
                    history.push(`/ri/thank-you`);
                    return <></>;
                }).finally(() => {
                    dispatch(UiAction.hidePageLoading());
                    event.setSubmitting(false)
                })
            },
            onCancel: () => {
                event.setSubmitting(false)
            }
        });
    };

    return (
        <div>
            <div className="text-center">
                <h2>{webLinkInfo.organization_name}</h2>
            </div>
            <h4 className="mt-3 text-center">Fill all the requested information, require by organization.</h4>
            <div className="mt-5 d-flex justify-content-center">
                <div style={{width: "100%", maxWidth: "1000px"}}>
                    <Formik
                        enableReinitialize={true}
                        initialValues={formData}
                        validationSchema={validationSchema}
                        onSubmit={handleOnSubmit}
                    >
                        {(props) => getForm(props)}
                    </Formik>
                </div>
            </div>
        </div>
    );
}