import React, {useEffect, useState} from 'react';
import {Card, CardBody} from "reactstrap";
import {LoadingButton} from "../../shared/LoadingButton";
import { useHistory, useParams } from 'react-router-dom';
import {useVerifyLinkToken, LoadingContent, InValidRequest} from "./RequestInfoCustomHooks";
import {api} from "../../../util/Api";
import {Toastiy} from "../../../util/Toast";
import {ISendBy} from "../../layouts/AppContainer";

interface IProps {

}

window.requestInfoOtpSendBy = null;

export default function WebLinkCandidatePage(props: IProps) {
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const history = useHistory();
    const {token} = useParams();
    const {isChecking, isValidLinkToken, webLinkInfo} = useVerifyLinkToken(token);
    const [sendBy, setSendBy] = useState<ISendBy|null>(null);

    const generateOtp = (event: any) => {
        if (!sendBy) {
            Toastiy.error("Select where would you like to send otp.");
            return;
        }

        setIsLoading(true);
        api.otp.postGenerate({
           link_token: token,
           send_by: sendBy,
        }).then(() => {
            window.requestInfoOtpSendBy = sendBy;
            history.push(`/ri/otp/${token}`);
        }).finally(() => setIsLoading(false));
    };

    const handleOnCaptureChange = (field: string, e: any) => {
        const by: any = {send_sms: false, send_email: false};
        setSendBy({...by, ...sendBy, [field]: e.target.checked});
    };

    if (isChecking) {
        return <LoadingContent/>
    }

    if (!isChecking && !isValidLinkToken) {
        return <InValidRequest/>
    }

    return (
        <div>
            <div className="text-center">
                <h2>{webLinkInfo.organization_name}</h2>
            </div>
            <h4 className="text-bold mt-5">{webLinkInfo.organization_name} has requested information.</h4>
            <div className="mt-3">
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                    It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                    and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                </p>
            </div>
            <div className="mt-5 d-flex justify-content-center">
                <div>
                    <h5 className="mb-4">Where would you like to send otp?</h5>
                    {webLinkInfo.allow_send_sms === 1 &&
                    <div className="form-check ml-3">
                        <label className="">
                            <input className="form-check-input" value="1" checked={sendBy?.send_sms} type="checkbox" onChange={(e: any) => handleOnCaptureChange("send_sms", e)}/>
                            {webLinkInfo.phone_number}
                        </label>
                    </div>
                    }
                    {webLinkInfo.allow_send_email === 1 &&
                    <div className="form-check  ml-3">
                        <label className="">
                            <input className="form-check-input" value="1" checked={sendBy?.send_email} type="checkbox" onChange={(e: any) => handleOnCaptureChange("send_email", e)}/>
                            {webLinkInfo.email}
                        </label>
                    </div>
                    }
                </div>
            </div>
            <div className="text-center mt-5">
                <LoadingButton
                    color="primary"
                    type='submit'
                    disabled={isLoading}
                    isLoading={isLoading}
                    onClick={generateOtp}
                >
                    Generate OTP
                </LoadingButton>
            </div>
        </div>
    );
}