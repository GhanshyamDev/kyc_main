import React, {useEffect, useState} from 'react';
import {Spinner} from "reactstrap";
import {useDispatch} from "react-redux";
import {UiAction} from "../../../store/UiState";
import {api} from "../../../util/Api";

export function useVerifyLinkToken(linkToken: string) {
    //const dispatch = useDispatch();
    const [isChecking, setIsChecking] = useState<boolean>(false);
    const [isValidLinkToken, setIsValidLinkToken] = useState<boolean>(false);
    const [webLinkInfo, setWebLinkInfo] = useState<any>(null);

    useEffect(() => {
        setIsChecking(true);
        api.webLink.getDetail(linkToken).then((response: any) => {
            setIsValidLinkToken(true);
            setWebLinkInfo(response.data.data);
        }).finally(() => {
            setIsChecking(false);
        });
    }, []);

    return {isChecking, isValidLinkToken, webLinkInfo};
}

export function LoadingContent() {
    return <div className="content-inner d-flex justify-content-center h-75">
        <div className="align-self-center"><Spinner color="primary" /> Loading....</div>
    </div>
}

export function InValidRequest() {
    return <div className="text-center text-danger">Request Not Valid.</div>
}