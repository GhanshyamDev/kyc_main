import React, {useState} from 'react';
import {InValidRequest, LoadingContent, useVerifyLinkToken} from "./RequestInfoCustomHooks";
import {useHistory, useParams} from "react-router";
import {LoadingButton} from "../../shared/LoadingButton";
import {api} from "../../../util/Api";
import {Toastiy} from "../../../util/Toast";
import {setCookie} from "../../../util/Helpers";

interface IProps {

}

export default function OTPValidatePage(props: IProps) {
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [otp, setOtp] = useState<string>("");
    //const [errors, setErrors] = useState<any>(null);
    const history = useHistory();
    const {token} = useParams();
    const {isChecking, isValidLinkToken, webLinkInfo} = useVerifyLinkToken(token);

    if (!window.requestInfoOtpSendBy) {
        history.push(`/ri/wl/c/${token}`);
        return <></>;
    }

    const resendOtp = (event: any) => {
        if (window.requestInfoOtpSendBy) {
            api.otp.postGenerate({
                link_token: token,
                send_by: window.requestInfoOtpSendBy,
            }).then(() => {
                Toastiy.success("OTP resent.");
            }).finally(() => setIsLoading(false));
        }
    };

    const validateOtp = () => {
        if (!otp) {
            Toastiy.error("Please enter OTP.")
            return;
        }

        setIsLoading(true);
        api.otp.postValidate({
            otp_number: otp,
            link_token: token
        }).then((response: any) => {
            setCookie("vpto", response.data.access_token, 1);
            if (webLinkInfo.link_type === "candidate_selfie_1") {
                history.push(`/ri/c/s1/${token}`);
            }
            if (webLinkInfo.link_type === "candidate_selfie_2") {
                history.push(`/ri/c/s2/${token}`);
            }
            if (webLinkInfo.link_type === "customer_info") {
                history.push(`/ri/c/ci/${token}`);
            }
        }).finally(() => setIsLoading(false));
    };

    if (isChecking) {
        return <LoadingContent/>
    }

    if (!isChecking && !isValidLinkToken) {
        return <InValidRequest/>
    }

    return (
        <div>
            <div className="text-center">
                <h2>{webLinkInfo.organization_name}</h2>
            </div>
            <h4 className="mt-5 text-center">OTP</h4>
            <div className="mt-3 d-flex justify-content-center">
                <div>
                    <div className="form-group mb-2">
                        <label htmlFor="exampleInputEmail1">Enter OTP</label>
                        <input type="text" className="form-control" required
                               autoComplete="false"
                               onChange={(e: any) => setOtp(e.target.value)}
                               maxLength={6} placeholder="XXXX"
                        />
                    </div>
                    <div className="text-right text-primary">
                        <span className="cursor-pointer" onClick={resendOtp}>Resend OTP</span>
                    </div>
                </div>
            </div>
            <div className="text-center mt-5">
                <LoadingButton
                    color="primary"
                    type='submit'
                    disabled={isLoading}
                    isLoading={isLoading}
                    onClick={validateOtp}
                >
                    Validate OTP
                </LoadingButton>
            </div>
        </div>
    );
}