import React from 'react';
import {Card, CardBody} from "reactstrap";

interface IProps {

}

export default function RequestInfoThankYouPage(props: IProps) {

    return (
        <div className="mt-5 mb-5">
            <h4 className="text-center">Thank You!</h4>
            <div className="mt-3 d-flex justify-content-center">
                <div className="text-center">
                    Your information has been successfully submitted.
                </div>
            </div>
        </div>
    );
}