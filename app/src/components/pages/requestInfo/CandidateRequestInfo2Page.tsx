import React, {useEffect, useState} from 'react';
import {Card, CardBody, Col, Row} from "reactstrap";
import {useHistory, useParams} from "react-router";
import {InValidRequest, LoadingContent, useVerifyLinkToken} from "./RequestInfoCustomHooks";
import {getCookie} from "../../../util/Helpers";
import {Field, Form, Formik} from "formik";
import {InputUploadImage} from "../../shared/Fields/InputUploadImage";
import {LoadingButton} from "../../shared/LoadingButton";
import {userValidateSchema} from "../../../util/Validation";
import {api} from "../../../util/Api";
import * as Yup from "yup";
import {InputCheckboxSingle} from "../../shared/Fields/InputCheckboxSingle";
import {selfieSubmitConfirm} from "../../shared/DeleteConfirm";
import {UiAction} from "../../../store/UiState";
import {useDispatch} from "react-redux";

interface IProps {

}

const validationSchema = Yup.object().shape({
    final_validation_image: Yup.string().required("Photo is required.").nullable(),
    agree: Yup.bool().oneOf([true], 'Accept Terms & Conditions is required'),

});

const setCandidateData = (formData: any = {}) => {
    return {
        final_validation_image: formData.final_validation_image || null,
        agree: formData.agree || false,
    }
};

let currentLocation: any = null;

export default function CandidateRequestInfo2Page(props: IProps) {
    const dispatch = useDispatch();
    const history = useHistory();
    const {token} = useParams();
    const [locationAllowed, setLocationAllowed] = useState<boolean>(true);
    const {isChecking, isValidLinkToken, webLinkInfo} = useVerifyLinkToken(token);
    const [formData, setFormData] = useState<any>(setCandidateData({}));
    const vpto: any = getCookie("vpto");

    useEffect(() => {
        navigator.geolocation.getCurrentPosition((position) => {
            //set current location
            currentLocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            }
            setLocationAllowed(true);
        }, () => {
            setLocationAllowed(false);
        });
    }, []);

    if (!window.requestInfoOtpSendBy || !vpto) {
        history.push(`/ri/wl/c/${token}`);
        return <></>;
    }

    if (isChecking) {
        return <LoadingContent/>
    }

    if (!isChecking && !isValidLinkToken) {
        return <InValidRequest/>
    }

    if (!locationAllowed) {
        return <div className="text-center text-danger">Allow location access to proceed.</div>
    }

    const getForm = (formProps: any) => {
        return (
            <Form>
                <Row>
                    <Col md={6}>
                        <Field component={InputUploadImage}
                               name="final_validation_image"
                               isRequired
                               postUrl={`${process.env.REACT_APP_API_URL}file/upload-candidate-selfie-image/${token}`}
                               cameraPictureWidth="300"
                               previewHeight="225px"
                               captureByCamera={true}
                               candidateSelfie={false}
                               authRequired={false}
                               label="Upload Your Photo"
                               desc="Upload Image file only and max 10 MB."
                        />
                    </Col>
                    <Col md={6}>
                        <div className="h4 mb-2">Application Information</div>
                        <div className="mb-2"><label className="h5">Job Id</label>: {webLinkInfo.job_id}</div>
                        <div className="mb-2"><label className="h5">Designation</label>: {webLinkInfo.designation}</div>
                    </Col>
                    <Col md={12}>
                        <Field component={InputCheckboxSingle}
                               name="agree"
                               label="Agree to provide my consent to CelebratingLife to use my personal information and Images for assessment and verification of my identity."
                        />
                    </Col>
                </Row>
                <div className="text-center">
                    <LoadingButton
                        color="primary"
                        type='submit'
                        disabled={formProps.isSubmitting}
                        isLoading={formProps.isSubmitting}
                    >
                        Submit
                    </LoadingButton>
                </div>
            </Form>
        )
    };

    const handleOnSubmit = (formData: any, event: any) => {
        const data = {
            ...formData,
            vpto: vpto,
            location: currentLocation,
        };
        selfieSubmitConfirm({
            onConfirm: () => {
                dispatch(UiAction.showPageLoading());
                api.requestInfo.postCandidateSelfie2(data).then(() => {
                    history.push(`/ri/thank-you`);
                    return <></>;
                }).finally(() => {
                    dispatch(UiAction.hidePageLoading());
                    event.setSubmitting(false);
                })
            },
            onCancel: () => {
                event.setSubmitting(false)
            }
        });
    };

    return (
        <div>
            <div className="text-center">
                <h2>{webLinkInfo.organization_name}</h2>
            </div>
            <h4 className="mt-3 text-center">Fill all the requested information, require by organization.</h4>
            <div className="mt-3 d-flex justify-content-center">
                <div style={{width: "100%", maxWidth: "1000"}}>
                    <Formik
                        enableReinitialize={true}
                        initialValues={formData}
                        validationSchema={validationSchema}
                        onSubmit={handleOnSubmit}
                    >
                        {(props) => getForm(props)}
                    </Formik>
                </div>
            </div>
        </div>
    );
}