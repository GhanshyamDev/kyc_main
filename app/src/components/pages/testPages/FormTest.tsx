import React from 'react';
import Col from "reactstrap/lib/Col";
import Button from "reactstrap/lib/Button";
import {InputField} from "../../shared/Fields/InputField";
import {InputRadio} from "../../shared/Fields/InputRadio";
import {InputCheckbox} from "../../shared/Fields/InputCheckbox";
import {InputSelect} from "../../shared/Fields/InputSelect";
import {InputSwitch} from "../../shared/Fields/InputSwitch";
import {InputDate} from "../../shared/Fields/InputDate";
import {InputTime} from "../../shared/Fields/InputTime";
import {ErrorMessage, Field, FieldArray, Form, Formik} from "formik";
import * as Yup from 'yup';
import {string} from 'yup';
import {Row} from "reactstrap";
import {InputReactSelect} from "../../shared/Fields/InputReactSelect";
import Api from "../../../util/Api";
import {IRootState} from "../../../store/AppStore";
import {RouteComponentProps} from 'react-router-dom';
import {connect} from "react-redux";
import {InputFileUpload} from "../../shared/Fields/InputFileUpload";
import InputVirtualTransfer from "../../shared/Fields/InputVirtualTransfer";
import {ITableData} from "../../shared/TableWrp/Helper";

const ValidationSchema = Yup.object().shape({
    name: Yup.string().required(),
    city: Yup.string().required(),
    /*age: Yup.number().required()
        .test('isChild', 'Age Should Above 18', function (value) {
            return (value > 18)
        }),*/
    friends: Yup.array().of(string().required()).required(),
    gender: Yup.string().required().nullable(),
    hobbies: Yup.array().required().nullable(),
    hobby: Yup.string().required().nullable(),
    colour: Yup.string().required().nullable(),
    class: Yup.string().required().nullable(),
    isMarried: Yup.boolean().required().nullable(),
    time: Yup.string().required().nullable(),
    vacation: Yup.array().required().nullable(),
});


const genderButtons = [
    {
        'value': 'Male',
        'label': 'Male'
    },
    {
        'value': 'Female',
        'label': 'Female'
    }
];
const hobbiesButtons = [
    {
        'value': 'Singing',
        'label': 'Singing'
    },
    {
        'value': 'Reading',
        'label': 'Reading'
    }
];
const classes = [
    {
        value1: 'BCA',
        label1: 'BCA'
    },
    {
        value1: 'MCA',
        label1: 'MCA'
    }
];

const mapStateToProps = ({ui, page}: IRootState, ownProps: RouteComponentProps) => {
    return {ui, page, ownProps};
};

type ReduxType = ReturnType<typeof mapStateToProps>

interface IFormTestState {
    student: any,
    tableData: ITableData,
    loading: boolean,
}

interface IProps extends ReduxType {

}

export class FormTest extends React.Component<IProps, IFormTestState> {
    api: Api = new Api();
    state = {
        student: '',
        tableData: {list: [], total: 0},
        loading: false,
    };

    formData = {
        name: '',
        age: '',
        city: '',
        gender: '',
        floorPlanImage: [],
        hobbies: ['Singing'],
        hobby: false,
        isMarried: true,
        dob: '',
        time: '',
        logo: '',
        class: ['MCA'],
        colour: '',
        friends: [],
        userLocation: {
            lat: 51.51154695449996,
            lng: -0.11016368865966798
        },
        imgfile: {
            previewVisible: false,
            previewImage: '',
            fileList: [{
                uid: '-1',
                name: 'abc.png',
                status: 'done',
                url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
            }]
        },
        dataSource: [],
        vacation: [],
        students: [],
    };

    getStudents = () => {
        const data = [...new Array(100).keys()].map((item: any, index: number) => {
            return {name: `Student${index}`, id: index}
        });
        return Promise.resolve({data: {data: data}})
    };

    getForm = (props: any) => {
        return (
            <Form>
                <div className="container-fluid">
                    <Row>
                        <Col sm={8}>

                            <Field component={InputField}
                                   name="name"
                                   label="Name"
                                   isRequired="true"
                                   placeholder="Enter Employee Name"/>

                            <FieldArray name="friends"
                                        render={arrayHelpers => (
                                            <div>
                                                {(props.values.friends && props.values.friends.length > 0) ? (
                                                    props.values.friends.map((friend: any, index: number) => (
                                                            <div key={index} className="row">
                                                                <Col sm={6}>
                                                                    <Field component={InputField}
                                                                           name={`friends.${index}`}
                                                                           isRequired="true"/>
                                                                    <Button
                                                                        className="btn-danger"
                                                                        type="button"
                                                                        onClick={() => arrayHelpers.remove(index)}
                                                                    >
                                                                        -
                                                                    </Button>
                                                                    <Button
                                                                        className="btn-primary"
                                                                        type="button"
                                                                        onClick={() => arrayHelpers.insert(index, '')}
                                                                    >
                                                                        +
                                                                    </Button>
                                                                </Col>
                                                            </div>
                                                        )
                                                    )
                                                ) : (
                                                    <Button type="button"
                                                            className="btn-success"
                                                            onClick={() => arrayHelpers.push('')}>
                                                        Add a friend
                                                    </Button>
                                                )}
                                            </div>
                                        )}

                            />

                            <ErrorMessage name="friends" component="div" className="input-error"/>


                            <Field component={InputRadio}
                                   options={genderButtons}
                                   name="gender"
                                   label="Select Gender"
                                   isRequired="true"
                                   value={props.values.gender}
                                   type="radio"
                            />

                            <Field component={InputCheckbox}
                                   options={hobbiesButtons}
                                   name="hobbies"
                                   value={props.values.hobbies || []}
                                   label="Select Hobbies"
                                   isRequired="true"
                                   type="checkbox"
                            />

                            <Field component={InputSelect}
                                   options={hobbiesButtons}
                                   name="hobby"
                                   value={props.values.hobby}
                                   label="Select Hobbies"
                                   isRequired="true"
                                   type="select"
                            />

                            <Field component={InputReactSelect}
                                   options={classes}
                                //api={this.api.employee.getList}
                                   name="class"
                                   isMulti={true}
                                   label="Select Class"
                                   isRequired="true"
                                   labelField="label1"
                                   valueField="value1"
                            />

                            {/*<Field component={InputAsyncReactSelect}
                                           name="colour"
                                           options={() => {
                                               return Promise.resolve([])
                                           }}
                                        //options={this.api.employee.getList}
                                           label="Select Colours"
                                           isMulti={true}
                                           isRequired="true"
                                           labelField="employee_name"
                                           valueField="id"
                                    />*/}

                            <Field
                                component={InputSwitch}
                                name="isMarried"
                                isRequired="true"
                                label="Is Married"
                            />

                            <Field
                                component={InputDate}
                                name="dob"
                                isRequired="true"
                                /*showTime
                                showTimeSelect
                                timeFormat="H:mm:ss"
                                timeIntervals={10}*/
                                className="form-control"
                                label="Enter BirthDate"
                                value={props.values.dob}
                            />

                            <Field component={InputTime}
                                   name="time"
                                   className="form-control"
                                   timeFormat="H:mm:ss"
                                   timeIntervals={10}
                                   isRequired="true"
                                   label="Enter Time"
                                   value={props.values.time}
                            />

                            {/*<Field component={InputFile}
                                   name="logo"
                                   action={process.env.REACT_APP_API_URL + "upload/organizationLogo"}
                                   isRequired="true"
                                   label="Upload File"
                                   multiple={false}
                                   accept=".png,.jpg"
                            />

                            <Field component={InputFileThumbnail}
                                   name="imgfile"
                                   isRequired="true"
                                   label="Upload Image file "
                                   action='https://www.mocky.io/v2/5cc8019d300000980a055e76'
                                   listType="picture"
                                   multiple
                                   defaultFileList={[...this.formData.imgfile.fileList]}
                            />*/}

                            <div className="mt-2">
                                <ul>
                                    <li>Location : {JSON.stringify(props.values.location) || ""}</li>
                                    <li>Address : {props.values.address || ""}</li>
                                    <li>city : {props.values.city || ""}</li>
                                    <li>state : {props.values.state || ""}</li>
                                    <li>country : {props.values.country || ""}</li>
                                    <li>postalCode : {props.values.postalCode || ""}</li>
                                </ul>
                            </div>


                            <Field component={InputVirtualTransfer}
                                   label="Select Students"
                                   name="students"
                                   api={this.getStudents}
                                   labelField="name"
                                   valueField="id"
                            />

                            <Field component={InputFileUpload}
                                   name="floorPlanImage"
                                   label="Select Files"
                                   url="https://dev2api.eztraxs.com/api/upload/floorPlanImage"
                            />

                            <br/>
                            <Button color="primary" type="submit" className="mt-3">Submit</Button>

                        </Col>
                        <Col sm={4}>
                            <pre>
                                {JSON.stringify(props, null, 4)}
                            </pre>
                        </Col>
                    </Row>
                </div>
            </Form>)
    };

    render() {
        return (
            <div className='container-fluid'>
                <br/><br/><br/>

                <Col sm={12}>
                    <h2>Field Examples</h2>
                </Col>
                <Formik
                    enableReinitialize={true}
                    initialValues={this.formData}
                    validationSchema={ValidationSchema}
                    onSubmit={
                        (values, action) => {
                            //console.log(values);
                        }
                    }
                >{(props: any) => this.getForm(props)}
                </Formik>
            </div>
        )
    }
}

export default connect(mapStateToProps)(FormTest);
