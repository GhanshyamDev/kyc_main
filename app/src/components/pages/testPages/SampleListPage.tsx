import React from 'react';
import {SampleList} from "./SampleList";

export default function SampleListPage() {

    return (
        <div className="container-fluid mt-3">
            <h1 className="h3 mb-4 text-gray-800">Sample List Page</h1>
            <SampleList />
        </div>
    );
}
