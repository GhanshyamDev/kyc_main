import React, {useState} from 'react';
import {TestTableList} from "../../pageComponents/TestTablelist/TestTablelist";
import {guid} from "../../../util/Helpers";

let data: any = [...new Array(100).keys()].map((item: any, index) => {
    return {id: guid(), name: "Student" + index, class: "Class" + index}
});

export default function TableListPage(props: any) {
    const [isLoading, setIsLoading] = useState(false);
    const [filteredData, setFilteredData] = useState<any>({
        list: data,
        total: data.length
    });

    const loadData = (event: any = {page: 1, pageSize: 10}) => {
        setIsLoading(true);

        setTimeout(() => {
            setIsLoading(false);
            const offset = ((event.page - 1) * event.pageSize) + 1;
            setFilteredData({
                list: data.slice(offset, offset + event.pageSize),
                total: data.length,
            })
        }, 500);
    };

    return (
        <div className="container-fluid mt-3">
            <h1 className="h3 mb-4 text-gray-800">Sample List Page</h1>
            <TestTableList
                data={filteredData}
                loadData={loadData}
                loading={isLoading}
            />
        </div>
    );
}
