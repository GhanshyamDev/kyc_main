import React, {useEffect, useState} from "react";
import {api} from "../../../util/Api";
import SimpleTableWrp from "../../shared/SimpleTableWrp";

interface IProps {
    loading?: boolean,
    data?: any,
    onEdit?: any,
    onDelete?: any,
    loadData?: any
}

let data: any = [];
let query: string = "";

export function SampleList(props: IProps) {
    const [filteredData, setFilteredData] = useState(data);

    useEffect(() => {
        fetchData();
    }, []);

    const fetchData = () => {
        api.dummyApi.students().then((response: any) => {
            data = response.data.data;
            setFilteredData(filterData(query))
        });
    };

    /* For Custom Search (Optional)  */
    const filterData = (value: string) => {
        if (value === "") {
            return data;
        }
        return data.filter((item: any) => item.name.match(new RegExp(value, "i")));
    };

    const onSearch = (value: string) => {
        query = value;
        setFilteredData(filterData(value));
    };

    const columns = [
        {
            dataField: 'name',
            text: 'Name',
            sort: true
        }, {
            dataField: 'class',
            text: 'Class',
            sort: true
        }
    ];

    return (
        <SimpleTableWrp
            rowKey="name"
            title="Students"
            loading={true}
            data={filteredData}
            columns={columns}
            onChange={fetchData}
            onSearch={onSearch} //Optional
        />
    )
}
