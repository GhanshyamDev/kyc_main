import React, {useEffect, useRef, useState} from "react";
import {Card, CardBody, Col, Collapse, Row} from "reactstrap";
import {Field, Form, Formik, FormikProps} from "formik";
import moment from "moment";
import AppConstants from "../../../util/AppConstants";
import {Button} from "../../shared/Button";
import {InputDate} from "../../shared/Fields/InputDate";
import Api from "../../../util/Api";
import {useDispatch} from "react-redux";
import {UiAction} from "../../../store/UiState";
import "./../../../assets/scss/datatable/custom.scss";
import {fromToDateValidationSchema} from "../../../util/Validation";
import {dateToUtcDate} from "../../../util/Helpers";

const $: any = require('jquery');
$.DataTable = require('datatables.net');
require('datatables.net-buttons');
require('datatables.net-buttons/js/buttons.flash.js');//# Flash file export
require('datatables.net-buttons/js/buttons.html5.js');//# HTML 5 file export
require('datatables.net-buttons/js/buttons.print.js');//# Print view button
require('datatables.net-buttons-dt');
require('../../../util/datetime-moment');
$.DataTable.moment(AppConstants.DEFAULT_DATE_FORMAT);

const initialValues = {
    fromDate: moment().set({
        hours: 0,
        minutes: 0,
        second: 0
    }).subtract(2, 'days').format(AppConstants.DEFAULT_DATE_FORMAT),
    toDate: moment().set({hours: 23, minutes: 59, second: 59}).format(AppConstants.DEFAULT_DATE_FORMAT),
};

let dbTable: any = null;
export default function SampleReport(props: any) {
    const reportTitle = "Sample Report";
    const fileName = `SampleReport_${moment().format(AppConstants.ONLY_DATE_FORMAT)}`;
    const [isFilterOpen, setIsFilterOpen] = useState(false);
    const dispatch = useDispatch();
    const el = useRef(null);
    let $el: any = null;
    const api: Api = new Api();

    useEffect(() => {
        $el = $(el.current);
        dbTable = $el.DataTable(getDataTablesOptions());
        loadData(initialValues);
    }, []);

    const setFilters = (data: any = {}) => {
        return {
            filters: {
                ...data.fromDate && {fromDate: dateToUtcDate(data.fromDate, AppConstants.DEFAULT_DATE_FORMAT)},
                ...data.toDate && {toDate: dateToUtcDate(data.toDate, AppConstants.DEFAULT_DATE_FORMAT)},
            }
        }
    };

    const loadData = (params: any = {}, event: any = undefined) => {
        dispatch(UiAction.showPageLoading());
        api.dummyApi.students().then((response: any) => {
            dbTable.clear().rows.add(response.data.data).draw();
        }).finally(() => {
            if (event) {
                event.setSubmitting(false)
            }
            dispatch(UiAction.hidePageLoading())
        });
    };

    const getDataTablesOptions = () => {
        return {
            data: [],
            columns: columns(),
            pageLength: 25,
            order: [0, 'desc'],
            dom: 'Bfrtip',
            buttons: [
                {extend: 'excelHtml5', title: reportTitle, filename: fileName},
                {extend: 'csvHtml5', title: reportTitle, filename: fileName},
                {extend: 'pdfHtml5', title: reportTitle, filename: fileName},
            ]
        }
    };

    const getForm = (props: any) => (
        <Form>
            <Row className="mt-2">
                <Col md={3}>
                    <Field component={InputDate}
                           isRequired
                           showTimeInput
                           name="fromDate"
                           label="From Date"
                    />
                </Col>
                <Col md={3}>
                    <Field component={InputDate}
                           isRequired
                           showTimeInput
                           name="toDate"
                           label="To Date"
                    />
                </Col>
            </Row>
            <div className="text-right border-top pt-2">
                <Button color="primary"
                        size="sm"
                        type='submit'
                        disabled={props.isSubmitting}
                        isLoading={props.isSubmitting}>
                    <i className="fas fa-search"/>{' '}Search
                </Button>
            </div>
        </Form>
    );

    const columns = () => {
        return [
            {
                title: 'Name',
                data: 'name',
            }, {
                title: 'Class',
                data: 'class',
            }
        ]
    };

    return (
        <div className="container-fluid">
            <Card className={'br-g-300 shadow rounded-2 mt-3 mb-3'} style={{display: 'block'}}>
                <CardBody className="p-3">
                    <div>
                        <h5 className="no-margin">{reportTitle}
                            <i className="fa fa-filter cursor-pointer text-primary float-right"
                               onClick={() => setIsFilterOpen(!isFilterOpen)}
                            />
                        </h5>
                    </div>
                    <Collapse isOpen={isFilterOpen}>
                        <Formik
                            validationSchema={fromToDateValidationSchema}
                            initialValues={initialValues}
                            onSubmit={((values, event) => loadData(values, event))}
                        >{(props: FormikProps<any>) => getForm(props)}
                        </Formik>
                    </Collapse>
                </CardBody>
            </Card>

            <Card className={'br-g-300 shadow rounded-2 mt-3 mb-3'}>
                <CardBody className="report-viewer">
                    <table className="display table table-responsive-md" style={{width: "100%"}} ref={el}/>
                </CardBody>
            </Card>
        </div>
    )
}
