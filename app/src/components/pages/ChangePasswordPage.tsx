import React from 'react';
import {ChangePasswordForm} from "../pageComponents/changepassword/ChangePasswordForm";

interface IProps {

}

export default function ProfilePage(props: IProps) {

    return (
        <>
            <div className="container">
                <div className="d-flex justify-content-center align-items-center" style={{height: '100vh'}}>
                    <div style={{maxWidth: '400px', width: '100%'}}>
                        <ChangePasswordForm/>
                    </div>
                </div>
            </div>
        </>
    );
}