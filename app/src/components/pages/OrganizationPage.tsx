import {useDispatch} from "react-redux";
import React, {useState} from "react";
import {Button} from "reactstrap";
import {api} from "../../util/Api";
import {UiAction} from "../../store/UiState";
import {OrganizationList} from "../pageComponents/organization/OrganizationList";
import FormDrawerWrp from "../shared/FormDrawerWrp";
import {OrganizationTabs} from "../pageComponents/organization/OrganizationTabs";
import {DeleteConfirm} from "../shared/DeleteConfirm";
import {Toastiy} from "../../util/Toast";
import {message} from "../../locale/en/message";
import {ITableData} from "../shared/TableWrp/Helper";

let queryParams: any = {};

export default function OrganizationPage() {
    const dispatch = useDispatch();
    const [showFormModal, setShowFormModal] = useState(false);
    const [formTitle, setFormTitle] = useState<string>("Add Organization");
    const [id, setId] = useState<string | null>(null);
    //const [data, setData] = useState<ITableData>({list: [], total: 0});
    const [data, setData] = useState<[]>([]);

    const handleOnAdd = () => {
        setId(null);
        setFormTitle("Add Organization");
        setShowFormModal(true);
    };

    const handleOnEdit = (record: any) => {
        setId(record.organization_xid);
        setFormTitle("Edit Organization");
        setShowFormModal(true);
    };

    const handleOnDelete = (record: any) => {
        DeleteConfirm({
            confirmText: message.confirmDelete,
            onConfirm: () => {
                dispatch(UiAction.showPageLoading());
                api.organization.postDelete({organization_xid: record.organization_xid})
                    .then(response => {
                        Toastiy.success(message.recordDeleted);
                        loadData(queryParams);
                    }).finally(() => dispatch(UiAction.showPageLoading()));
            }
        })
    };

    const loadData = (listQueryParams: any = {}) => {
        queryParams = listQueryParams;
        dispatch(UiAction.showPageLoading());
        api.organization.getList({my: 1}).then((response: any) => {
            //setData({total: response.data.meta.total, list: response.data.data});
            setData(response.data.data);
        }).finally(() => {
            dispatch(UiAction.hidePageLoading());
        });
    };

    return (
        <div className="content-inner">
            <div className="mb-4 text-right">
                <Button color="primary" size="sm" onClick={() => handleOnAdd()}>
                    <i className='fas fa-plus'/> Add Organization
                </Button>
            </div>

            <OrganizationList
                data={data}
                useUrlQueryParams={true}
                onEdit={handleOnEdit}
                onDelete={handleOnDelete}
                loadData={loadData}
            />

            {(showFormModal) &&
            <FormDrawerWrp
                title={formTitle}
                width='80%'
                showDrawer={showFormModal}
                onClose={() => setShowFormModal(false)}
            >
                <OrganizationTabs
                    id={id}
                    onCancel={() => setShowFormModal(false)}
                    loadData={() => loadData(queryParams)}
                />

            </FormDrawerWrp>
            }
        </div>
    );
}
