import React, {useEffect} from 'react';
import {auth} from "../../../util/Auth";
import {Field, Form, Formik} from "formik";
import {Button, Col, Row} from "reactstrap";
import {InputField} from "../../shared/Fields/InputField";
import * as Yup from 'yup';
import {validation} from "../../../locale/en/validation";
import {api} from "../../../util/Api";
import {useHistory, useLocation} from "react-router";
import {LoadingButton} from "../../shared/LoadingButton";
import {Link} from "react-router-dom";
import {Toastiy} from "../../../util/Toast";
import {message} from "../../../locale/en/message";

const image = require('../../../assets/images/logo.png');

Yup.setLocale(validation);

const validationSchema = Yup.object().shape({
    email: Yup.string().required(),
});

const initValues = {
    email: "",
};

export default function ForgotPasswordPage(props: any) {
    const location: any = useLocation();
    const history = useHistory();

    useEffect(() => {
        if (auth.isLoggedIn()) {
            const {from} = location.state || {from: {pathname: "/"}};
            history.push(from);
        }
    }, []);

    const getForm = (props: any) => {
        return (
            <Form>
                <div>
                    <Row>
                        <Col md={12}>
                            <Field component={InputField}
                                   name="email"
                                   label="Email"
                                   isRequired
                                   placeholder="Enter user_name"/>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={12} className="mb-2">
                            <Link to="/login">Back to login</Link>
                        </Col>
                    </Row>
                </div>
                <div className="text-center">
                    <LoadingButton
                        color="primary"
                        className="btn-block"
                        type='submit'
                        disabled={props.isSubmitting}
                        isLoading={props.isSubmitting}
                    >
                        Send Reset Password Link
                    </LoadingButton>{' '}
                </div>
            </Form>
        )
    };

    const handleOnSubmit = (values: any, event: any): void => {
        api.login.postForgotPassword(values).then((response: any) => {
            Toastiy.success(response.data.message);
        }).finally(() => {
            event.setSubmitting(false)
        });
    };

    return (
        <div className="container-scroller">
            <div className="container-fluid page-body-wrapper full-page-wrapper">
                <div className="main-panel">
                    <div className="content-wrapper">
                        <div>
                            <div className="d-flex align-items-center auth px-0">
                                <div className="row w-100 mx-0">
                                    <div className="col-lg-4 mx-auto">
                                        <div className="auth-form-light text-left py-5 px-4 px-sm-5">
                                            <div className="brand-logo text-center">
                                                <img src={image} alt="logo" />
                                            </div>
                                            <Formik
                                                enableReinitialize={true}
                                                initialValues={initValues}
                                                validationSchema={validationSchema}
                                                onSubmit={handleOnSubmit}
                                            >{(props: any) => getForm(props)}
                                            </Formik>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
