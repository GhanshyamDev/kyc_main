import React, {useEffect} from 'react';
import {auth} from "../../../util/Auth";
import {Field, Form, Formik} from "formik";
import {Button, Col, Row} from "reactstrap";
import {InputField} from "../../shared/Fields/InputField";
import * as Yup from 'yup';
import {validation} from "../../../locale/en/validation";
import {api} from "../../../util/Api";
import {useHistory, useLocation} from "react-router";
import {useParams} from 'react-router-dom';
import {LoadingButton} from "../../shared/LoadingButton";
import {Link} from "react-router-dom";
import {Toastiy} from "../../../util/Toast";
import {message} from "../../../locale/en/message";

const image = require('../../../assets/images/logo.png');

Yup.setLocale(validation);

const validationSchema = Yup.object().shape({
    password:Yup.string().required(),
    password_confirmation:Yup.string().required().oneOf([Yup.ref('password')], 'Confirm Password Should match Password'),
});

const initValues = {
    password:"",
    token:"",
    email:"",
    password_confirmation:""
};

export default function ForgotPasswordPage(props: any) {
    const {token, email} = useParams();
    useEffect(() => {
        if(auth.isLoggedIn() || !token) {
            const {from} = props.location.state || {from:{pathname:"/"}};
            props.history.push(from);
        }
    }, []);

    const getForm = (props: any) => {
        return (
            <Form>
                <div>
                    <Row>
                        <Col md={12}>
                            <Field component={InputField}
                                   type="password"
                                   name="password"
                                   label="Password"
                                   isRequired
                                   placeholder="Enter Password"/>
                        </Col>
                        <Col md={12}>
                            <Field component={InputField}
                                   type="password"
                                   name="password_confirmation"
                                   label="Confirm Password"
                                   isRequired
                                   placeholder="Confirm Password"/>
                        </Col>
                    </Row>
                </div>
                <div className="text-center">
                    <LoadingButton
                        color="primary"
                        className="btn-block"
                        type='submit'
                        disabled={props.isSubmitting}
                        isLoading={props.isSubmitting}
                    >
                        Reset Password
                    </LoadingButton>{' '}
                </div>
            </Form>
        )
    };

    const handleOnSubmit = (values: any, event: any): void => {
        values = {
            ...values,
            token:token,
            email:email
        }
    
        api.login.postResetPassword(values).then((response: any) => {
            Toastiy.success(response.data.message);
            props.history.push({pathname:"/"});
        }).finally(() => {
            event.setSubmitting(false)
        });
    };

    return (
        <div className="container-scroller">
            <div className="container-fluid page-body-wrapper full-page-wrapper">
                <div className="main-panel">
                    <div className="content-wrapper">
                        <div>
                            <div className="d-flex align-items-center auth px-0">
                                <div className="row w-100 mx-0">
                                    <div className="col-lg-4 mx-auto">
                                        <div className="auth-form-light text-left py-5 px-4 px-sm-5">
                                            <div className="brand-logo text-center">
                                                <img src={image} alt="logo" />
                                            </div>
                                            <Formik
                                                enableReinitialize={true}
                                                initialValues={initValues}
                                                validationSchema={validationSchema}
                                                onSubmit={handleOnSubmit}
                                            >{(props: any) => getForm(props)}
                                            </Formik>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}