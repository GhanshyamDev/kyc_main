import React, {useEffect} from 'react';
import {auth} from "../../../util/Auth";
import {Field, Form, Formik} from "formik";
import {Button, Col, Row} from "reactstrap";
import {InputField} from "../../shared/Fields/InputField";
import * as Yup from 'yup';
import {validation} from "../../../locale/en/validation";
import {api} from "../../../util/Api";
import {useHistory, useLocation} from "react-router";
import {LoadingButton} from "../../shared/LoadingButton";
import {Link} from "react-router-dom";

const image = require('../../../assets/images/logo.png');

Yup.setLocale(validation);

const validationSchema = Yup.object().shape({
    email: Yup.string().required(),
    password: Yup.string().required(),
});

const initValues = {
    email: "",
    password: "",
};

export default function LoginPage(props: any) {
    const location: any = useLocation();
    const history = useHistory();

    useEffect(() => {
        if (auth.isLoggedIn()) {
            const {from} = location.state || {from: {pathname: "/"}};
            history.push(from);
        }
    }, []);

    const getForm = (props: any) => {
        return (
            <Form>
                <div>
                    <Row>
                        <Col md={12}>
                            <Field component={InputField}
                                   name="email"
                                   label="Email"
                                   isRequired
                                   placeholder="Enter user_name"/>
                        </Col>
                        <Col md={12}>
                            <Field component={InputField}
                                   type="password"
                                   name="password"
                                   label="Password"
                                   isRequired
                                   placeholder="Enter Password"/>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={12} className="mb-2">
                            <Link to="/forgot-password">Forgot Password?</Link>
                        </Col>
                    </Row>
                </div>
                <div className="text-center">
                    <LoadingButton
                        color="primary"
                        className="btn-block"
                        type='submit'
                        disabled={props.isSubmitting}
                        isLoading={props.isSubmitting}
                    >
                        Sign in
                    </LoadingButton>{' '}
                    {/*<Button className="btn-block" color="primary" type="submit">Sign in</Button>*/}
                </div>
                {/*<div className="mt-2 text-right">
                <a href="!#" onClick={event => event.preventDefault()} className="auth-link text-black">Forgot password?</a>
                </div>*/}
            </Form>
        )
    };

    const handleLoginSubmit = (values: any, event: any): void => {
        api.login.postLogin(values).then((response: any) => {
            let user = response.data.data;
            auth.setAuth(user);
            if(user.user.first_login) {
                const {from} = {from: {pathname: "/change-password"}}
                history.push(from);
            } else {
                const {from} = location.state || {from: {pathname: "/dashboard"}};
                history.push(from);
            }
        })/*.catch((errors: any) => {
            if (errors.response.status === 401) {
                Toastiy.error('user_name or Password Incorrect')
            } else {
                Toastiy.error(errors.message);
            }
        })*/.finally(() => {
            event.setSubmitting(false)
        });
    };

    return (
        <div className="container-scroller">
            <div className="container-fluid page-body-wrapper full-page-wrapper">
                <div className="main-panel">
                    <div className="content-wrapper">
                        <div>
                            <div className="d-flex align-items-center auth px-0">
                                <div className="row w-100 mx-0">
                                    <div className="col-lg-4 mx-auto">
                                        <div className="auth-form-light text-left py-5 px-4 px-sm-5">
                                            <div className="brand-logo text-center">
                                                <img src={image} alt="logo" />
                                            </div>
                                            <Formik
                                                enableReinitialize={true}
                                                initialValues={initValues}
                                                validationSchema={validationSchema}
                                                onSubmit={handleLoginSubmit}
                                            >{(props: any) => getForm(props)}
                                            </Formik>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {/*<div className="d-flex justify-content-center" style={{height: "100vh"}}>
                <div className="align-self-center">
                    <div className="text-center">
                        <img className="mb-4" src={image} alt=""/>
                        <h1 className="h3 mb-3 font-weight-normal">sign in</h1>
                    </div>
                    <Formik
                        enableReinitialize={true}
                        initialValues={initValues}
                        validationSchema={validationSchema}
                        onSubmit={handleLoginSubmit}
                    >{(props: any) => getForm(props)}
                    </Formik>
                </div>
            </div>*/}
        </div>
    );
}
