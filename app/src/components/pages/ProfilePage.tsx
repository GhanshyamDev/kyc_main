import React from 'react';
import {ProfileForm} from "../pageComponents/profile/ProfileForm";

interface IProps {

}

export default function ProfilePage(props: IProps) {

    return (
        <>
            <div className="container">
                <h3 className="mb-3">My Account</h3>
                <ProfileForm/>
            </div>
        </>
    );
}