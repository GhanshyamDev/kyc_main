import qs from "qs";
import { validation } from "../locale/en/validation";
import { Toastiy } from "./Toast";
import moment from "moment-timezone";
import React from 'react';
import {auth, IOrganization} from "./Auth";
import AppConstants  from "./AppConstants";
import {Badge, Spinner} from "reactstrap";
import {api} from "./Api";

export function FullPageLoading() {
    return <div className='container'>
        <div className="content-inner d-flex justify-content-center" style={{ height: "100vh" }}>
            <div className="align-self-center"><Spinner color="primary" /> loading....</div>
        </div>
    </div>;
}

export function getPageClass(path: string) {
    if (!path) {
        return "";
    }
    return path.replace("/", "").substr(0);
}

export function getSendWebLinkOptions() {
    return api.organization.getDetails(auth.getCurrentOrganizationId()).then((response: any) => {
        const organization = response.data.data;
        let sendWebLinkOptions = [{key: "generate_link", value: "Copy Link"}];
        if (organization.allow_send_email) {
            sendWebLinkOptions = [...sendWebLinkOptions, {key: "send_email", value: "Email"}]
        }
        if (organization.allow_send_sms) {
            sendWebLinkOptions = [...sendWebLinkOptions, {key: "send_sms", value: "SMS"}]
        }
        return Promise.resolve({data: {data: sendWebLinkOptions}});
    });
}

export function displayCandidateStatus(status: string) {
    let statusColor = "";
    switch (status) {
        case "New":
            statusColor = "primary";
            break;
        case "Verified":
            statusColor = "success";
            break;
        case "Suspicious":
            statusColor = "danger";
            break;
        case "In Progress":
            statusColor = "info";
            break;
    }
    if (!statusColor) {
        status = "Pending";
        statusColor = "warning"
    }
    return (
        <Badge color={statusColor} className='badge'>
            {status}
        </Badge>
    )
}

export function displayCandidateStatusColor(status: string) {
    let statusColor = "";
    switch (status) {
        case "New":
            statusColor = "#2c349c";
            break;
        case "Verified":
            statusColor = "#1bcfb4";
            break;
        case "Suspicious":
            statusColor = "#fe7c96";
            break;
        case "In Progress":
            statusColor = "#198ae3";
            break;
    }
    if (!statusColor) {
        statusColor = "#fed713"
    }
    return statusColor;
}

export function getListData(getList: any, listQueryParams: any, props: any) {
    props.showPageLoading();
    return getList(listQueryParams).then((data: any) => {
        props.hidePageLoading();
        return Promise.resolve(data);
    }).catch((response: any) => {
        Toastiy.error('Can not load Error :' + response);
        return Promise.reject(response);
    });
}

export function b64toBlob(b64Data: string, contentType: string, sliceSize: number|null = null) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;
    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        const slice = byteCharacters.slice(offset, offset + sliceSize);
        let byteNumbers = new Array(slice.length);
        for (let i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }
        const byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, {type: contentType});
    return blob;
}

export function exportDataToCsv(getList: any, params: any = {}, listQueryParams: any = {}, callback: any = null) {
    delete listQueryParams.page;
    delete listQueryParams.pageSize;
    return getList(params, listQueryParams).then((response: any) => {
        let data = response.data.data;
        let removeColumns: any = ["createDate", "modifyDate", "modifyDate", "createUser", "modifySystem", "modifyUser", "timeZoneId"];
        let columns: any = [];
        if (data.length > 0) {
            columns = Object.keys(data[0]);
            removeColumns = [...columns.filter((item: string) => {
                return item.indexOf('HashId') !== -1;
            }), ...removeColumns];
        }

        data = data.map((item: any) => {
            const row = (callback) ? { ...callback(item) } : { ...item };
            const keys = Object.keys(row);
            keys.forEach((key: any) => {
                if (removeColumns.includes(key)) {
                    delete row[key];
                    return;
                }
                row[key] = ((row[key] === null || row[key] === undefined)) ? `""` : `"${row[key]}"`;
            });

            return {
                ...row,
                ...item.startDate && { startDate: utcDateToLocalDate(item.startDate) },
                ...item.endDate && { endDate: utcDateToLocalDate(item.endDate) },
                ...item.lastOdometerDate && { lastOdometerDate: utcDateToLocalDate(item.lastOdometerDate) },
                ...item.lastOdometerDate && { lastOdometerDate: utcDateToLocalDate(item.lastOdometerDate) },
            };
        });
        exportToCsvFile(data);
        return Promise.resolve({});
    }).catch((response: any) => {
        Toastiy.error('Cannot Export Data' + response);
        return Promise.reject(response);
    });
}

export function downloadFile(fileName: string, response: any) {
    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', fileName); //or any other extension
    document.body.appendChild(link);
    link.click();
}

export function getName(first_name: string, last_name: string) {
    if (last_name) {
        return first_name + " " + last_name;
    }
    return first_name;
}

export function pushListQuery(history: any, location: any, filters: any) {
    const queryParams = qs.parse(location.search.substr(1));
    history.push({
        pathname: location.pathname,
        search: qs.stringify({
            ...queryParams,
            filters: filters,
            page: 1,
            pageSize: 10,
        },
        ),
        state: queryParams
    });
}

export function clearListQuery(history: any, location: any) {
    history.push({
        pathname: location.pathname,
        search: '',
        state: ''
    });
}

export const groupBy = (items: any, key: any) => items.reduce(
    (result: any, item: any) => ({
        ...result,
        [item[key]]: [
            ...(result[item[key]] || []),
            item,
        ],
    }),
    {},
);

export function sortByDateTime(datetimeA: string | null, dateTimeB: string | null) {
    if (datetimeA && dateTimeB) {
        return moment(datetimeA).unix() - moment(dateTimeB).unix();
    }
    return null;
}

export function sortByDateTimeWithLocal(datetimeA: string | null, dateTimeB: string | null) {
    if (datetimeA && dateTimeB) {
        const valueA: any = utcDateToLocalDate(datetimeA, AppConstants.SERVER_DATE_FORMAT, AppConstants.DEFAULT_DATE_FORMAT);
        const valueB: any = utcDateToLocalDate(dateTimeB, AppConstants.SERVER_DATE_FORMAT, AppConstants.DEFAULT_DATE_FORMAT);
        return sortByDateTime(valueA, valueB);
    }
    return null;
}

export function sortByString(valueA: string, valueB: string) {
    return valueA.length - valueB.length;
}

export function utcDateToLocalDate(date: string, fromFormat: string = AppConstants.SERVER_DATE_FORMAT, toFormat: string = AppConstants.DEFAULT_DATE_FORMAT) {
    if (!date) {
        return '';
    }

    const utcDate = moment.utc(date, fromFormat);
    return utcDate.clone().tz(auth.getTimezone()).format(toFormat);
}

export function dateToUtcDate(date: string | null, fromFormat: string = 'YYYY-MM-DD[T]HH:mm:ss') {
    if (!date) {
        return '';
    }
    return moment.tz(date, fromFormat, auth.getTimezone()).clone().utc().format(fromFormat);
}

export function startOfDayToUtcDate(date: string | null, fromFormat: string = 'YYYY-MM-DD[T]HH:mm:ss') {
    if (!date) {
        return '';
    }
    const localDate = moment.tz(date, fromFormat, auth.getTimezone()).set({ hours: 0, minutes: 0, second: 0 });
    return localDate.clone().utc().format('YYYY-MM-DD[T]HH:mm:ss');
}

export function endOfDayToUtcDate(date: string | null, fromFormat: string = 'YYYY-MM-DD[T]HH:mm:ss') {
    if (!date) {
        return '';
    }
    const localDate = moment.tz(date, fromFormat, auth.getTimezone()).set({ hours: 23, minutes: 59, second: 59 });
    return localDate.clone().utc().format('YYYY-MM-DD[T]HH:mm:ss');
}

export function getValidationLocale(locale = 'en') {
    return validation;
}

export const chunk = (arr: any, size: number) =>
    Array.from({ length: Math.ceil(arr.length / size) }, (v, i) =>
        arr.slice(i * size, i * size + size)
    );

export class ErrorMessage extends React.Component<{ errors: any }> {
    render() {
        const errors = this.props.errors;
        if (Array.isArray(errors)) {
            return (
                <ul className="pl-4">
                    {errors.map((item: any, index: any) => {
                        const messageText = item.message || item.Message || item || '';
                        return (
                            <li key={index}>{messageText}</li>
                        )
                    })}
                </ul>
            )
        } else if(typeof errors === 'object' && Object.keys(errors).length > 0) {
            return (
                <ul className="pl-4">
                    {Object.keys(errors).map((value: string, index: any) => {
                        const message = errors[value];
                        const messageText = (Array.isArray(message) && message.length > 0) ? message[0] : message;
                        return (
                            <li key={index}>{messageText}</li>
                        );
                    })}
                </ul>
            );
        } else if (typeof errors === 'string') {
            return (
                <ul className="pl-4">
                    <li>{errors}</li>
                </ul>
            );
        } else {
            return "Unknown error.";
        }
    }
}

export function showErrors(error: any) {
    if (error.response) {
        const response = JSON.parse(JSON.stringify(error.response));
        const errors = response.data.Errors || response.data.errors || response.data.message;

        if (errors) {
            if (errors === "Unauthenticated.") {
                auth.logout();
                window.location.reload();
            } else if(errors && Array.isArray(errors) && errors.includes("Error executing \"CompareFaces\"")) {
                Toastiy.error("Please check the picture or upload the picture with human face.");
            } else {
                Toastiy.error(<ErrorMessage errors={errors} />);
            }
        }
    } else if (error.request) {
        // The request was made but no response was received
        //console.log(error.message);
        Toastiy.error(error.message || error.messageText);
    } else {
        // Something happened in setting up the request that triggered an Error
        //console.log(error.message);
        Toastiy.error(error.message || error.messageText);
    }
}

export function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

export function startCase(text: string) {
    const result = text.replace(/([A-Z])/g, " $1");
    return result.charAt(0).toUpperCase() + result.slice(1);
}

function convertToCSV(objArray: any) {
    if (objArray.length === 0) {
        return '';
    }

    objArray.unshift(Object.keys(objArray[0]).map(item => startCase(item)));

    const array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    let str = '';

    for (let i = 0; i < array.length; i++) {
        let line = '';
        for (let index in array[i]) {
            if (line !== '') line += ',';

            line += array[i][index];
        }

        str += line + '\r\n';
    }

    return str;
}

export function exportToCsvFile(jsonData: any) {
    const csvStr = convertToCSV(jsonData);
    const blob = new Blob([csvStr], { type: 'data:text/csv;charset=utf-8;' });
    const exportedFileName = "data.csv";
    if (navigator.msSaveBlob) { // IE 10+
        navigator.msSaveBlob(blob, exportedFileName);
    } else {
        const link = document.createElement("a");
        if (link.download !== undefined) { // feature detection
            // Browsers that support HTML5 download attribute
            const url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", exportedFileName);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
}

export function isValidJsonString(str: string) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

export function getUrlFromPath(string: any) {
    if (!string) {
        return '';
    }
    let path = string.replace(/\\/g, "/");
    path = process.env.REACT_APP_HOST_URL + path;
    return path;
}

export function copyToClipboard(str: string) {
    const el = document.createElement('textarea');
    el.value = str;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
}

export function isAllElementsExistsInArray(array: any, subArray: any) {
    return subArray.every((v: any) => array.includes(v));
}

export const isValidDate = (date: any) => {
    if (!date) return false;
    else return !(date && date === "0001-01-01T00:00:00");
};

export function getBase64FromImageUrl(url: string) {
    const img: any = new Image();

    img.setAttribute('crossOrigin', 'anonymous');

    img.onload = function () {
        let canvas = document.createElement("canvas");
        canvas.width = this.width;
        canvas.height = this.height;

        let ctx: any = canvas.getContext("2d");
        ctx.drawImage(this, 0, 0);
        console.log(canvas);

        let dataURL = canvas.toDataURL("image/png");
        alert(dataURL.replace(/^data:image\/(png|jpg);base64,/, ""));
    };

    img.src = url;
}

export function toObjectUrl(url: any) {
    return fetch(url)
        .then((response)=> {
            return response.blob();
        })
        .then(blob=> {
            return URL.createObjectURL(blob);
        });
}

export const arrayMoveMutate = (array: any, from: any, to: any) => {
    const startIndex = to < 0 ? array.length + to : to;

    if (startIndex >= 0 && startIndex < array.length) {
        const item = array.splice(from, 1)[0];
        array.splice(startIndex, 0, item);
    }
};

export const arrayMove = (array: any, from: any, to: any) => {
    array = [...array];
    arrayMoveMutate(array, from, to);
    return array;
};

export function setCookie(cname: string, cvalue: string, exphours: number) {
    let d = new Date();
    d.setTime(d.getTime() + (exphours*60*60*1000));
    const expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

export function getCookie(cname: string) {
    const name = cname + "=";
    const decodedCookie = decodeURIComponent(document.cookie);
    const ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

export function isAllPropertiesEmpty(object: any) {
    return !Object.values(object).some(x => (x !== null && x !== ''));
}

export function candidateStatusOptions() {
    return [
        {
            'value': null,
            'label': 'All'
        },
        {
            'value': 'New',
            'label': 'New'
        },
        {
            'value': 'In Progress',
            'label': 'In Progress'
        },
        {
            'value': 'Suspicious',
            'label': 'Suspicious'
        },
        {
            'value': 'Verified',
            'label': 'Verified'
        }
    ];
}