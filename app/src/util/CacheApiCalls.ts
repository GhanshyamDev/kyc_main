import {api} from "./Api";

type entityName = "organizations";

export class CacheApiCalls {
    entity = {
        organizations: [],
    };

    async getEntity(name: entityName, params: any = {}) {
        if (this.entity[name].length > 0) {
            return {
                data: {data: this.entity[name]}
            };
        }

        let response: any = null;

        switch (name) {
            case "organizations":
                response = await api.organization.getList(params);
                break;
        }

        this.entity[name] = (response) ? response.data.data : [];
        return response;
    }
}

export const cacheApiCalls = new CacheApiCalls();

