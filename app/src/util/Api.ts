import {getMockRequest, getRequest, postRequest} from "./Network";
import customerData from "./data/customers.json";

export default class Api {
    public demo: Demo;
    public login: Login;
    public organization: Organization;
    public dummyApi: DummyApi;
    public googlePlaces: GooglePlaces;
    public user: User;
    public role: Role;
    public common: Common;
    public candidate: Candidate;
    public customer: Customer;
    public webLink: WebLink;
    public otp: OTP;
    public requestInfo: RequestInfo;
    public rekognitionResultLog: RekognitionResultLog;
    public report: Report;

    constructor() {
        this.common = new Common();
        this.organization = new Organization();
        this.googlePlaces = new GooglePlaces();
        this.demo = new Demo();
        this.login = new Login();
        this.dummyApi = new DummyApi();
        this.user = new User();
        this.role = new Role();
        this.organization = new Organization();
        this.candidate = new Candidate();
        this.customer = new Customer();
        this.webLink = new WebLink();
        this.otp = new OTP();
        this.requestInfo = new RequestInfo();
        this.rekognitionResultLog = new RekognitionResultLog();
        this.report = new Report();
    }
}

class User {
    getList = (params: any = {}) => {
        return getRequest('user/list', params);
    };

    getDetails = (id: string) => {
        return getRequest('user/detail/' + id);
    };

    postCreate = (data: any) => {
        return postRequest('user/create', data);
    };

    postUpdate = (data: any) => {
        return postRequest('user/update', data);
    };

    postDelete = (data: any) => {
        return postRequest('user/delete', data);
    };

    postUpdateProfile = (data: any) => {
        return postRequest('user/updateProfile', data);
    };

    postChangePassword = (data: any) => {
        return postRequest('user/change-password', data);
    }
}

class Role {
    getList = () => {
        return getRequest(`role/list`);
    };
}

class Common {
    getOrganizationTypeList = (params: any = {}) => {
        return getRequest("organizationType/list", params);
    };

    getTimeZoneList = (params: any = {}) => {
        return getRequest('common/timezone/list', params);
    };
}

class RequestInfo {
    postCandidateSelfie1 = (data: any) => {
        return postRequest(`request-info/candidate-selfie1`, data, false);
    };
    postCandidateSelfie2 = (data: any) => {
        return postRequest(`request-info/candidate-selfie2`, data, false);
    };
}

class WebLink {
    getDetail = (token: string) => {
        return getRequest(`web-link/detail/${token}`, undefined, false);
    };

    postSend = (data: any) => {
        return postRequest('web-link/send', data);
    };

    postVerify = (data: any) => {
        return postRequest('web-link/verify', data, false);
    }
}

class OTP {
    postGenerate = (data: any = {}) => {
        return postRequest("otp/generate", data, false);
    };

    postValidate = (data: any = {}) => {
        return postRequest("otp/validate", data, false);
    };
}


class Login {
    postLogin = (data: any) => {
        return postRequest('auth/sign/in', data, false);
    }
    postForgotPassword = (data: any) => {
        return postRequest('auth/password/forgot', data, false);
    }
    postResetPassword = (data: any) => {
        return postRequest('auth/password/reset', data, false);
    }
}

class GooglePlaces {
    getPlaces = (params: any = {}) => {
        params = {
            ...params,
            key: "AIzaSyAqa66rfgdLoE2yChh6yMnzt6QVXMrnQGI"
        };
        const url = "https://maps.googleapis.com/maps/api/place/autocomplete/json";
        return getMockRequest(url, params);
    };
}

class Organization {
    getList = (params: any = {}) => {
        return getRequest('organization/list', params);
    };
    getDetails = (id: string) => {
        return getRequest('organization/detail/' + id);
    };
    postCreate = (data: any) => {
        return postRequest('organization/create', data);
    };
    postUpdate = (data: any) => {
        return postRequest('organization/update', data);
    };
    postDelete = (data: any) => {
        return postRequest('organization/delete', data);
    };
}

class Demo {
    getList = (params: any = {}) => {
        return getRequest('https://reqres.in/api/users/', params);
    };
}

class DummyApi {
    data: any = [];
    constructor() {
        this.data = [];
        for (let i = 0; i < 100; i++) {
            this.data.push({ id:i,name: `student${i}`, class: `class${i}` })
        }
    }
    students = () => {
        return Promise.resolve({ data: { data: this.data } })
    }
}

class Candidate {
    getList = (params: any = {}) => {
        return getRequest('candidate/list', params);
    };
    getDetails = (id: string) => {
        return getRequest('candidate/detail/' + id);
    };
    getImages = (id: string | null) => {
        return getRequest('candidate/images/' + id);
    };
    postCreate = (data: any) => {
        return postRequest('candidate/create', data);
    };
    postUpdate = (data: any) => {
        return postRequest('candidate/update', data);
    };
    postDelete = (data: any) => {
        return postRequest('candidate/delete', data);
    };
    postCompare = (id: any) => {
        return postRequest('candidate/compare', {candidate_id: id});
    }
    postMarkCompleted = (data: any) => {
        return postRequest('candidate/mark-completed', data)
    }
}

class Customer {
    getList = (params: any = {}) => {
        return Promise.resolve(customerData);
        //return getRequest('customer/list', params);
    };
    getDetails = (id: string) => {
        return Promise.resolve({data: {data: customerData.data.data[0]}});
        //return getRequest('customer/detail/' + id);
    };
    getImages = (id: string | null) => {
        return Promise.resolve({data: {data: []}});
        //return getRequest('customer/images/' + id);
    };
    postCreate = (data: any) => {
        return postRequest('customer/create', data);
    };
    postUpdate = (data: any) => {
        return postRequest('customer/update', data);
    };
    postDelete = (data: any) => {
        return postRequest('customer/delete', data);
    }
    postCompare = (id: any) => {
        return postRequest('customer/compare', {candidate_id: id});
    }
    postMarkCompleted = (data: any) => {
        return postRequest('customer/mark-completed', data)
    }
}

class RekognitionResultLog {
    getResultLogByCandidate = (id: string | null) => {
        return getRequest('candidate/rekognition-result/' + id)
    }
}

class Report {
    postCandidateReport = (data: any) => {
        return postRequest('report/candidate-report', data);
    }
    postSummaryReport = (data: any) => {
        return postRequest('report/candidate-summary-report', data);
    }
}

export const api = new Api();
