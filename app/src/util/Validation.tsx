import * as Yup from "yup";
import {validation} from "../locale/en/validation";
import {message} from "../locale/en/message";

Yup.setLocale(validation);

const passWordRegex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;
const InvalidPasswordMessage = `Password must be minimum 8 character and should contain at least one uppercase latter, one lowercase latter, one special character, one numeral.`;

export const organizationValidateSchema = Yup.object().shape({
    name: Yup.string().required().nullable(),
    phone_number: Yup.string().matches(/(^[\d]{10}$)/, {message: 'Max 10 digit and only number.'}).nullable(),
    email: Yup.string().email().nullable(),
});

export function fromToDateValidationSchema() {
    return Yup.object().shape({
        fromDate: Yup.date().required().nullable(),
        toDate: Yup.date().required().min(Yup.ref('fromDate'), message.toGreaterThanFromDate).nullable()
    });
}

export const userValidateSchema = Yup.object().shape({
    //roleId: Yup.string().required().nullable(),
    last_name: Yup.string().required().nullable(),
    first_name: Yup.string().required().nullable(),
    email: Yup.string().required().email().nullable(),
    phone_number: Yup.string().required().matches(/(^[\d]{10}$)/, {message: 'Invalid Phone Number'}).nullable(),
    //timezone_name: Yup.string().required().nullable(true),
    password: Yup.string().when('user_xid', {
        is: undefined,
        then: Yup.string().matches(passWordRegex, {message: InvalidPasswordMessage}).required(),
        otherwise: Yup.string().matches(passWordRegex, {message: InvalidPasswordMessage}).notRequired(),
    }),
    password_confirmation: Yup.string().when('password', {
        is: ((val: any) => {
            return (val !== '' && val !== undefined)
        }),
        then: Yup.string().required().oneOf([Yup.ref('password')], 'Password Not Match'),
        otherwise: Yup.string().notRequired().oneOf([Yup.ref('password')], 'Password Not Match'),
    })
});

export const candidateValidateSchema = Yup.object().shape({
    cid: Yup.string().required(),
    first_name: Yup.string().required().nullable(),
    email: Yup.string().required().email().nullable(),
    phone_number: Yup.string().required().matches(/(^[\d]{10}$)/, {message: 'Invalid Phone Number'}).nullable(),
});

export const customerValidateSchema = Yup.object().shape({
    cid: Yup.string().required(),
    first_name: Yup.string().required().nullable(),
    last_name: Yup.string().required().nullable(),
    email: Yup.string().required().email().nullable(),
    phone_number: Yup.string().required().matches(/(^[\d]{10}$)/, {message: 'Invalid Phone Number'}).nullable(),
});


export const profileFormValidateSchema = Yup.object().shape({
    last_name: Yup.string().required().nullable(),
    first_name: Yup.string().required().nullable(),
    email: Yup.string().required().email().nullable(),
    phone_number: Yup.string().required().matches(/(^[\d]{10}$)/, {message: 'Invalid Phone Number'}).nullable(),
    //timezone_name: Yup.string().required().nullable(true),
    password: Yup.string().when('user_xid', {
        is: undefined,
        then: Yup.string().matches(passWordRegex, {message: InvalidPasswordMessage}).required(),
        otherwise: Yup.string().matches(passWordRegex, {message: InvalidPasswordMessage}).notRequired(),
    }),
    password_confirmation: Yup.string().when('password', {
        is: ((val: any) => {
            return (val !== '' && val !== undefined)
        }),
        then: Yup.string().required().oneOf([Yup.ref('password')], 'Password Not Match'),
        otherwise: Yup.string().notRequired().oneOf([Yup.ref('password')], 'Password Not Match'),
    })
});
export const changePasswordFormValidateSchema = Yup.object().shape({
    password: Yup.string().when('user_xid', {
        is: undefined,
        then: Yup.string().matches(passWordRegex, {message: InvalidPasswordMessage}).required(),
        otherwise: Yup.string().matches(passWordRegex, {message: InvalidPasswordMessage}).notRequired(),
    }),
    password_confirmation: Yup.string().when('password', {
        is: ((val: any) => {
            return (val !== '' && val !== undefined)
        }),
        then: Yup.string().required().oneOf([Yup.ref('password')], 'Password Not Match'),
        otherwise: Yup.string().notRequired().oneOf([Yup.ref('password')], 'Password Not Match'),
    })
});