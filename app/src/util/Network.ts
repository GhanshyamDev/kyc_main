import axios from "axios";
import qs from "qs";
import Auth from "./Auth";
import {showErrors} from "./Helpers";


interface IHeaders {
    Authorization?: string,
    oarsgf?: string,
    'Content-Type': string
}

export async function getRequest(url: string, params: any = {}, authorization = true) {
    const headers: IHeaders = {
        'Content-Type': 'application/json'
    };

    const auth = new Auth();
    const authToken = auth.getToken();
    const organization = auth.getCurrentOrganization();
    if (authToken) {
        headers.Authorization = `Bearer ${authToken}`;
        if (organization && organization.organization_xid) {
            headers.oarsgf = organization.organization_xid;
        } else {
            //logout
            //auth.logout();
        }
    }

    if (!authorization) {
        delete headers.Authorization;
    }

    return axios({
        method: 'get',
        url: process.env.REACT_APP_API_URL + url,
        params: params,
        paramsSerializer: ((params: any) => qs.stringify(params)),
        headers: headers
    }).then((response: any) => {
        return Promise.resolve(response);
    }).catch((error) => {
        showErrors(error);
        return Promise.reject(error);
    });
}

export async function getMockRequest(url: string, params: any = {}, authorization = true) {
    const headers: IHeaders = {
        'Content-Type': 'application/json'
    };

    const auth = new Auth();
    const authToken = auth.getToken();
    const organization = auth.getCurrentOrganization();

    if (authToken) {
        headers.Authorization = `Bearer ${authToken}`;
        if (organization && organization.organization_xid) {
            headers.oarsgf = organization.organization_xid;
        } else {
            //logout
            //auth.logout();
        }
    }

    if (!authorization) {
        delete headers.Authorization;
    }

    return axios({
        method: 'get',
        url: url,
        params: params,
        paramsSerializer: ((params: any) => qs.stringify(params)),
        headers: headers
    }).then((response: any) => {
        return Promise.resolve(response);
    }).catch((error) => {
        showErrors(error);
        return Promise.reject(error);
    });
}

export async function downloadRequest(url: string, params: any = {}, authorization = true) {
    const headers: IHeaders = {
        'Content-Type': 'application/json'
    };

    const auth = new Auth();
    const authToken = auth.getToken();
    const organization = auth.getCurrentOrganization();

    if (authToken) {
        headers.Authorization = `Bearer ${authToken}`;
        if (organization && organization.organization_xid) {
            headers.oarsgf = organization.organization_xid;
        } else {
            //logout
            auth.logout();
        }
    }

    if (!authorization) {
        delete headers.Authorization;
    }

    return axios({
        method: 'get',
        url: url,
        params: params,
        paramsSerializer: ((params: any) => qs.stringify(params)),
        headers: headers,
        responseType: 'blob'
    }).then((response: any) => {
        return Promise.resolve(response);
    }).catch((error) => {
        showErrors(error);
        return Promise.reject(error);
    });
}

export function postRequest(url: string, data: any = {}, authorization = true, skipHandleError = false) {
    const headers: IHeaders = {
        'Content-Type': 'application/json'
    };

    const auth = new Auth();
    const authToken = auth.getToken();
    const organization = auth.getCurrentOrganization();


    if (authToken) {
        headers.Authorization = `Bearer ${authToken}`;
        if (organization && organization.organization_xid) {
            headers.oarsgf = organization.organization_xid.toString();
        } else {
            //auth.logout();
        }
    }

    if (!authorization) {
        delete headers.Authorization;
    }

    return axios({
        method: 'post',
        url: process.env.REACT_APP_API_URL + url,
        data: JSON.stringify(data),
        headers: headers
    }).then((response: any) => {
        return Promise.resolve(response);
    }).catch((error) => {
        if (!skipHandleError) {
            showErrors(error);
        }
        return Promise.reject(error);
    });
}


