import { IPageState } from "../store/PageState";

/*export interface IOrganization {
    id: string | null,
    name: string | null,
    created_at: string | null,
    is_active: boolean,
    is_admin: boolean,
    timezone_id: string,
}

export interface IRole {
    id: number,
    name: string,
    is_active: boolean,
    created_at: string | null,
    updated_at: string | null,
}

export interface IGeoLocation {
    lat: number,
    lng: number
}*/

export interface ListProps {
    data: any,
    page: IPageState,
    openFilterDrawer?: any,
    closeFilterDrawer?: any,
    toggleFilterDrawer?: any,
    useUrlQueryParams?: boolean | null,
    onView?: any,
    onEdit: any,
    loadData?: any,
    location?: any,
    history?: any,
    showPageLoading?: any,
    hidePageLoading?: any,
    onChangeOrganization?: any,
}

export interface Organization {
    organization_xid?: null | string,
    parentHashId: string,
    timeZoneId: string,
    organization_name: string,
    is_admin: boolean,
    addUser: boolean,
    first_name?: string,
    email?: string,
    phoneNumber?: string,
    password?: string,
    reenteredPassword?: string
}
