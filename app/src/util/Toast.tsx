import {toast, ToastOptions} from "react-toastify";
import React from "react";

const toastConfig: ToastOptions = {
    position: "top-right",
    autoClose: 3000,
};

export class Toastiy {
    static info(text: any) {
        toast.info(text, toastConfig);
    }

    static success(text: any) {
        toast.success(text, toastConfig);
    }

    static error(text: any) {
        toast.error(text, toastConfig);
    }

    static warning(text: any) {
        toast.warning(text, toastConfig);
    }

    static white(text: any) {
        toast(text, toastConfig);
    }
}

export const CustomMessage: React.FC<{title: string, message: any}> = (props) => {
    return (
        <>
            <div className="message-title">{props.title}</div>
            <div className="message-body">{props.message}</div>
        </>
    );
};