export default {
    DEFAULT_DATE_FORMAT: "YYYY-MM-DD HH:mm:ss",
    SERVER_DATE_FORMAT: "YYYY-MM-DD[T]HH:mm:ss",
    ONLY_DATE_FORMAT_MONTH_NAME: "MMMM D, YYYY",
    AM_PM_FORMAT: "YYYY-MM-DD hh:mm:ss A",
    ONLY_TIME_AM_PM_FORMAT: "h:mm A",
    TIME_FORMAT: "HH:mm:ss",
    TIME_FORMAT_HH_MM: "HH:MM",
    ONLY_DATE_FORMAT: "YYYY-MM-DD",
    IMAGES:{
        LOGO: process.env.PUBLIC_URL + '/images/logo.png',
        LOGOMINI: process.env.PUBLIC_URL + '/images/favicon.png',
        DEFAULT_PERSON: process.env.PUBLIC_URL + '/images/default-person.png'
    },
    ROLES : {
        SUPER_ADMIN: "51561f27-6f87-4ace-bb46-0d104d465c1c",
        COMPANY_ADMIN: "685c29b5-acca-4ea1-9850-7231b2ac6d05",
        VENDOR_ADMIN: "6cf06d2a-0770-4269-a7ee-ff6f7a1cf0af",
    }
};
