-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 19, 2021 at 12:37 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fcm`
--

-- --------------------------------------------------------

--
-- Table structure for table `action_logs`
--

CREATE TABLE `action_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `organization_id` bigint(20) DEFAULT NULL,
  `candidate_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `service_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_id` int(10) UNSIGNED DEFAULT NULL,
  `media_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL
) ;

--
-- Dumping data for table `action_logs`
--

INSERT INTO `action_logs` (`id`, `organization_id`, `candidate_id`, `user_id`, `service_type`, `media_id`, `media_type`, `file_name`, `location`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 101, 1, 'Upload', 1, 'step_1_image', NULL, NULL, '2020-12-20 08:25:58', '2020-12-20 08:25:58', NULL),
(2, 1, 101, NULL, 'Upload', 2, 'master_image', NULL, '{\"lat\": 22.2887936, \"lng\": 70.77560319999999}', '2020-12-20 08:32:53', '2020-12-20 08:32:53', NULL),
(3, 1, 101, NULL, 'Compare', 1, 'step_1_image', 'a0e33e18-8b3e-48f3-bf02-a717dee31b61.jpg', NULL, '2020-12-20 08:33:27', '2020-12-20 08:33:27', NULL),
(4, 1, 101, 1, 'Upload', 3, 'step_1_image', NULL, NULL, '2020-12-21 13:27:35', '2020-12-21 13:27:35', NULL),
(5, 1, 102, 1, 'Upload', 4, 'step_1_image', NULL, NULL, '2020-12-21 13:28:28', '2020-12-21 13:28:28', NULL),
(6, 1, 102, NULL, 'Upload', 5, 'master_image', NULL, '{\"lat\": 22.2920704, \"lng\": 70.7821568}', '2020-12-21 13:29:34', '2020-12-21 13:29:34', NULL),
(7, 1, 102, NULL, 'Compare', 4, 'step_1_image', '4afbc9ce-26a8-4998-9f82-214f8378b09e.jpg', NULL, '2020-12-21 13:29:50', '2020-12-21 13:29:50', NULL),
(8, 8, 103, NULL, 'Upload', 7, 'master_image', NULL, '{\"lat\": 19.1957948, \"lng\": 72.872501}', '2020-12-23 02:07:39', '2020-12-23 02:07:39', NULL),
(9, 8, 103, 7, 'Upload', 8, 'step_1_image', NULL, NULL, '2020-12-23 02:10:39', '2020-12-23 02:10:39', NULL),
(10, 8, 103, 7, 'Upload', 9, 'step_2_image', NULL, NULL, '2020-12-23 02:10:51', '2020-12-23 02:10:51', NULL),
(11, 8, 103, NULL, 'Compare', 8, 'step_1_image', 'e7d24ec6-6d87-44ef-aeb7-7566c4ce1ad6.jpg', NULL, '2020-12-23 02:10:55', '2020-12-23 02:10:55', NULL),
(12, 8, 103, NULL, 'Compare', 9, 'step_2_image', 'ce975098-6bde-404a-8987-37e34062dacc.jpg', NULL, '2020-12-23 02:10:55', '2020-12-23 02:10:55', NULL),
(13, 8, 103, 7, 'Upload', 10, 'step_3_image', NULL, NULL, '2020-12-23 02:11:43', '2020-12-23 02:11:43', NULL),
(14, 8, 103, NULL, 'Compare', 10, 'step_3_image', 'c5b7b770-af0e-4453-b18c-581064636901.jpg', NULL, '2020-12-23 02:11:52', '2020-12-23 02:11:52', NULL),
(15, 8, 103, 7, 'Upload', 11, 'step_3_image', NULL, NULL, '2020-12-23 02:12:49', '2020-12-23 02:12:49', NULL),
(16, 8, 103, NULL, 'Compare', 11, 'step_3_image', 'cd6538bc-5880-4810-b2c7-ba0a06dfc39d.jfif', NULL, '2020-12-23 02:12:56', '2020-12-23 02:12:56', NULL),
(17, 8, 103, 7, 'Upload', 12, 'step_3_image', NULL, NULL, '2020-12-23 02:15:12', '2020-12-23 02:15:12', NULL),
(18, 8, 103, NULL, 'Compare', 12, 'step_3_image', '794033f9-0014-4b65-995f-e7e20df548d1.jfif', NULL, '2020-12-23 02:15:20', '2020-12-23 02:15:20', NULL),
(19, 8, 103, 7, 'Upload', 13, 'step_3_image', NULL, NULL, '2020-12-23 02:18:23', '2020-12-23 02:18:23', NULL),
(20, 10, 104, NULL, 'Upload', 15, 'master_image', NULL, '{\"lat\": 18.4862468, \"lng\": 73.9526658}', '2020-12-23 03:22:04', '2020-12-23 03:22:04', NULL),
(21, 10, 104, 1, 'Upload', 16, 'step_1_image', NULL, NULL, '2020-12-23 03:22:51', '2020-12-23 03:22:51', NULL),
(22, 10, 104, NULL, 'Compare', 16, 'step_1_image', '3393d73b-e01d-4c99-87d2-634b5c82f8e9.jpg', NULL, '2020-12-23 03:22:56', '2020-12-23 03:22:56', NULL),
(23, 10, 104, 1, 'Upload', 17, 'step_1_image', NULL, NULL, '2020-12-23 03:23:46', '2020-12-23 03:23:46', NULL),
(24, 10, 104, NULL, 'Compare', 17, 'step_1_image', '1f05032d-379f-4c7a-841e-6da471d19dd2.jpg', NULL, '2020-12-23 03:23:49', '2020-12-23 03:23:49', NULL),
(25, 10, 104, 1, 'Upload', 18, 'step_1_image', NULL, NULL, '2020-12-23 03:24:12', '2020-12-23 03:24:12', NULL),
(26, 10, 104, NULL, 'Compare', 18, 'step_1_image', 'e667b8d8-cbcf-4360-8f26-9d4769dce372.jpg', NULL, '2020-12-23 03:24:17', '2020-12-23 03:24:17', NULL),
(27, 10, 104, NULL, 'Upload', 20, 'final_validation_image', NULL, '{\"lat\": 18.4858228, \"lng\": 73.9523667}', '2020-12-23 03:36:12', '2020-12-23 03:36:12', NULL),
(28, 10, 104, NULL, 'Compare', 20, 'final_validation_image', 'd92a95c3-bef0-480c-8b45-6409142508e9.jpg', NULL, '2020-12-23 03:37:05', '2020-12-23 03:37:05', NULL),
(29, 10, 105, NULL, 'Upload', 22, 'master_image', NULL, '{\"lat\": 18.4858228, \"lng\": 73.9523667}', '2020-12-23 03:40:34', '2020-12-23 03:40:34', NULL),
(30, 10, 105, 1, 'Upload', 23, 'step_1_image', NULL, NULL, '2020-12-23 03:41:12', '2020-12-23 03:41:12', NULL),
(31, 10, 105, NULL, 'Compare', 23, 'step_1_image', '6f611664-9938-406c-b8d1-b3cf56b14fc5.jpg', NULL, '2020-12-23 03:41:14', '2020-12-23 03:41:14', NULL),
(32, 8, 103, 1, 'Upload', 24, 'step_3_image', NULL, NULL, '2020-12-23 03:47:16', '2020-12-23 03:47:16', NULL),
(33, 8, 103, NULL, 'Compare', 24, 'step_3_image', 'cfa6513d-f21d-4bbd-9188-13b3f3ab7f79.jpg', NULL, '2020-12-23 03:47:19', '2020-12-23 03:47:19', NULL),
(34, 1, 102, 1, 'Upload', 25, 'step_2_image', NULL, NULL, '2020-12-24 12:56:27', '2020-12-24 12:56:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `candidates`
--

CREATE TABLE `candidates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `candidate_xid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cid` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL
) ;

--
-- Dumping data for table `candidates`
--

INSERT INTO `candidates` (`id`, `candidate_xid`, `first_name`, `last_name`, `email`, `phone_number`, `cid`, `job_id`, `organization_id`, `address`, `designation`, `comment`, `location`, `validation_image_location`, `current_status`, `status_date`, `completed_status`, `master_image_changed`, `validation_image_changed`, `step_1_image_changed`, `step_2_image_changed`, `step_3_image_changed`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '6f7fcdf3-8367-3577-86ad-0de4d313bcfa', 'Jennyfer', 'Swift', 'candidate001@test.test', '78XXXXX001', 'cid_001', NULL, 4, '3347 Edwin Dam\nMikehaven, TX 45826', 'Clerk', 'Qui asperiores quas harum quia saepe.', NULL, NULL, 'Suspicious', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(2, '7adf6178-c9e9-35c2-9aa0-e5dceb2d167e', 'Ewell', 'Dickinson', 'candidate002@test.test', '78XXXXX002', 'cid_002', NULL, 3, '76449 Jessyca Plaza\nErnestinaland, HI 08060', 'Professor', 'Nulla doloremque deserunt dolorum ut sed.', NULL, NULL, 'Verified', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(3, '6cdd43ee-f16f-3c57-9d7b-aef982b28296', 'Carmel', 'Nienow', 'candidate003@test.test', '78XXXXX003', 'cid_003', NULL, 5, '6180 Stacey Field Apt. 462\nLisaberg, VT 78851', 'Professor', 'Aut eum incidunt itaque est sit corrupti.', NULL, NULL, 'New', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(4, 'd16b33b9-f8e1-3cae-8b31-3a6c9cc53663', 'Flossie', 'Upton', 'candidate004@test.test', '78XXXXX004', 'cid_004', NULL, 4, '8670 Gutmann Drive Apt. 924\nWest Kody, OK 98159-2279', 'Guard', 'Harum voluptates deserunt eveniet et.', NULL, NULL, 'In Progress', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(5, '74a954b9-3471-3607-8c60-a5f9f091c726', 'Cassandra', 'Bogan', 'candidate005@test.test', '78XXXXX005', 'cid_005', NULL, 1, '1229 Karli Radial\nMcLaughlinborough, MO 88479', 'Team Lead', 'Atque minima quam ad.', NULL, NULL, 'In Progress', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(6, '6136a8b2-7ed4-3225-8151-860f2107e647', 'Rogelio', 'Kub', 'candidate006@test.test', '78XXXXX006', 'cid_006', NULL, 5, '50212 Sunny Crest\nWuckerthaven, SD 30622-6639', 'CEO', 'Iure tenetur omnis provident a dolorem.', NULL, NULL, 'New', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(7, 'efc29d8d-01fe-348d-a21a-caeaad497427', 'Hattie', 'McCullough', 'candidate007@test.test', '78XXXXX007', 'cid_007', NULL, 5, '7691 Kenyatta Ville Suite 440\nNew Kamryn, WI 74787', 'Professor', 'Dolor nulla sunt consectetur qui.', NULL, NULL, 'In Progress', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(8, '7ac8c7d8-4589-3bec-8aaa-3860a639d47c', 'Fiona', 'Cartwright', 'candidate008@test.test', '78XXXXX008', 'cid_008', NULL, 1, '3819 VonRueden Station\nAstridside, OH 09150', 'Manager', 'Cupiditate debitis eum modi.', NULL, NULL, 'In Progress', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(9, 'bc8ad9a0-3909-3dbd-beec-29bef5bac9b3', 'Dedric', 'Brown', 'candidate009@test.test', '78XXXXX009', 'cid_009', NULL, 3, '28290 Kieran Green\nLake Nannie, ME 32291', 'CEO', 'Aperiam error nobis et itaque tenetur.', NULL, NULL, 'New', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(10, 'b6d8a164-808f-308f-96f5-eebdccadd624', 'Liam', 'Wintheiser', 'candidate010@test.test', '78XXXXX010', 'cid_010', NULL, 5, '8738 Krystal Rue\nUptonview, DC 86926', 'Professor', 'Ab voluptas autem amet corporis voluptatem.', NULL, NULL, 'In Progress', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(11, '690beda0-5059-3db8-b875-e783a678c4b3', 'Bernita', 'Kulas', 'candidate011@test.test', '78XXXXX011', 'cid_011', NULL, 2, '79957 Myles Hills Suite 586\nNorth Annabellton, HI 08495', 'Guard', 'Placeat consequatur maiores ut.', NULL, NULL, 'In Progress', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(12, '3de1fafa-2d7b-39b5-8830-e1fa756978fc', 'Lemuel', 'Murray', 'candidate012@test.test', '78XXXXX012', 'cid_012', NULL, 3, '225 Gregory Freeway Apt. 175\nMaiyaport, MN 24759-4591', 'Clerk', 'Dolor vitae autem ex non beatae ut.', NULL, NULL, 'New', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(13, 'e4cf2ca6-330e-36f5-9210-86946ba262cc', 'Beth', 'Crist', 'candidate013@test.test', '78XXXXX013', 'cid_013', NULL, 1, '640 Marks Heights Apt. 290\nNew Roy, MI 04087-0511', 'Professor', 'Provident eius culpa perferendis suscipit.', NULL, NULL, 'Suspicious', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(14, 'd3748031-3f61-3d78-a8bb-51ee9f216bca', 'Maddison', 'Conn', 'candidate014@test.test', '78XXXXX014', 'cid_014', NULL, 4, '994 Halvorson Glen\nLednerfort, KY 37690', 'Manager', 'Unde asperiores debitis non voluptatibus.', NULL, NULL, 'New', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(15, '2be5de63-637d-3c55-a823-42f48294b5b1', 'Erling', 'Christiansen', 'candidate015@test.test', '78XXXXX015', 'cid_015', NULL, 3, '699 Cole Coves Suite 487\nNolanville, KY 22649-3340', 'Teacher', 'Commodi dolorum cum mollitia a et quia.', NULL, NULL, 'New', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(16, '5353f4a4-9232-3289-8fae-b835fa896112', 'Margarita', 'Harber', 'candidate016@test.test', '78XXXXX016', 'cid_016', NULL, 2, '1216 Luettgen Fork\nLake Daronfort, VT 80687', 'Clerk', 'Quia doloribus ut aut.', NULL, NULL, 'Suspicious', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(17, '5d4c5429-f9cc-3e14-b560-a64cd556b12b', 'Elroy', 'Bergnaum', 'candidate017@test.test', '78XXXXX017', 'cid_017', NULL, 5, '2968 Turner Corner\nNorth Audie, ID 34407', 'Team Lead', 'Rerum est odit pariatur eveniet velit quo.', NULL, NULL, 'In Progress', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(18, '5da0a192-61ce-38a2-80f6-c0edb13d3f63', 'Taylor', 'Fay', 'candidate018@test.test', '78XXXXX018', 'cid_018', NULL, 2, '51478 Marquardt Freeway\nNorth Carmenside, OH 06367-1673', 'Guard', 'Temporibus unde vel reprehenderit.', NULL, NULL, 'Verified', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(19, '09205e67-8f42-3916-93cd-ea4ad5c65786', 'Harvey', 'Rippin', 'candidate019@test.test', '78XXXXX019', 'cid_019', NULL, 3, '466 Pagac Field Apt. 990\nLake Deshaun, MO 26196-0530', 'Teacher', 'Iste velit iste quam nihil minima.', NULL, NULL, 'New', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(20, 'cea97305-4426-3e8d-b06e-6b050d6a055f', 'Hayden', 'Denesik', 'candidate020@test.test', '78XXXXX020', 'cid_020', NULL, 4, '87505 Ova Islands Suite 127\nMarshalltown, SD 41664', 'CTO', 'Sit non non quia qui sunt id.', NULL, NULL, 'New', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(21, 'aa973f76-700a-3deb-a597-250fbf55dfd6', 'Leonardo', 'Muller', 'candidate021@test.test', '78XXXXX021', 'cid_021', NULL, 2, '117 Anderson Square\nEast Cesarmouth, SD 48435', 'CTO', 'Aut qui rerum harum.', NULL, NULL, 'New', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(22, '749ba64f-70f5-3cbc-b193-2ec3daba62e7', 'Chyna', 'Beer', 'candidate022@test.test', '78XXXXX022', 'cid_022', NULL, 5, '231 Jermaine Mews Apt. 533\nPort Elyssa, NJ 25962-2257', 'Clerk', 'Ab enim fugiat neque.', NULL, NULL, 'In Progress', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(23, '8023f353-e2ca-387d-bd8a-563a1b646252', 'Bella', 'Goyette', 'candidate023@test.test', '78XXXXX023', 'cid_023', NULL, 4, '1102 Sophia Creek Suite 459\nKobeburgh, RI 17812-0862', 'Professor', 'Non quibusdam commodi error occaecati.', NULL, NULL, 'In Progress', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(24, '96c25d74-ecc4-3c56-b8b9-4a98b47f0751', 'Jaylen', 'White', 'candidate024@test.test', '78XXXXX024', 'cid_024', NULL, 4, '5532 O\'Conner Summit Suite 146\nEast Reillyview, MS 77757', 'Teacher', 'Non laboriosam velit amet quidem quae et.', NULL, NULL, 'Verified', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(25, 'e1c13b75-5c9d-3e54-8749-e099965d364d', 'Irving', 'Schmidt', 'candidate025@test.test', '78XXXXX025', 'cid_025', NULL, 1, '517 Bartoletti Stream Apt. 535\nEast Remington, MS 72758', 'Team Lead', 'Quos itaque incidunt nihil sed enim ea sunt.', NULL, NULL, 'Suspicious', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(26, 'bd33a4b7-2b56-3c75-81cf-354aa9299acc', 'Macey', 'Parker', 'candidate026@test.test', '78XXXXX026', 'cid_026', NULL, 1, '398 Darlene Key Apt. 313\nEast Horace, NC 33357', 'CEO', 'Dolores sit ut sunt ut aperiam.', NULL, NULL, 'Verified', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(27, '9e2702ab-3a57-38a6-a075-93963219c412', 'Tyrique', 'Jakubowski', 'candidate027@test.test', '78XXXXX027', 'cid_027', NULL, 4, '33853 Kuvalis Bridge\nFraneckiborough, TN 37186', 'CTO', 'Quasi non omnis rerum facere odio.', NULL, NULL, 'Suspicious', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(28, 'b74c868b-5647-3826-a354-0e9b218dbb3c', 'Alicia', 'Schumm', 'candidate028@test.test', '78XXXXX028', 'cid_028', NULL, 5, '97927 Nigel Freeway\nPort Rubentown, NM 80432', 'Team Lead', 'Voluptate ut laudantium quia laboriosam.', NULL, NULL, 'Suspicious', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(29, '522783ce-cd52-37ed-acea-63c4ca2080d9', 'Cathryn', 'Harris', 'candidate029@test.test', '78XXXXX029', 'cid_029', NULL, 5, '2467 Deangelo Plains Apt. 133\nSouth Candidofort, DC 62955', 'Manager', 'Et minus eaque corrupti molestiae numquam.', NULL, NULL, 'Verified', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(30, '932fe0f6-50df-3bd0-b7cc-58fb2abd50fc', 'Myrtie', 'Moen', 'candidate030@test.test', '78XXXXX030', 'cid_030', NULL, 3, '20796 Weissnat Village\nPort Eldredfort, ID 68909', 'Teacher', 'Soluta iste ipsa enim ipsam odio dolor.', NULL, NULL, 'In Progress', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(31, '75359d86-8d3b-3b03-be29-3ceace63bdc3', 'Wade', 'Kulas', 'candidate031@test.test', '78XXXXX031', 'cid_031', NULL, 5, '194 Koch Freeway\nOrionview, MS 40077-7494', 'Team Lead', 'Fugit ab ea et veritatis temporibus.', NULL, NULL, 'In Progress', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(32, '259d2aa5-3640-3ef7-b9bc-b13b3626045d', 'Lexus', 'Hane', 'candidate032@test.test', '78XXXXX032', 'cid_032', NULL, 2, '441 Thiel Stream Apt. 274\nO\'Connellborough, IL 27493-3121', 'CEO', 'Quo aliquam a qui vero impedit officiis sit.', NULL, NULL, 'Verified', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(33, '7f6ff670-733d-3af6-86fd-9e5e8b0ab651', 'Helmer', 'Hermann', 'candidate033@test.test', '78XXXXX033', 'cid_033', NULL, 1, '3491 Gleason Corners Apt. 186\nSouth Kobe, ME 94334', 'Manager', 'Magni doloribus molestias est eos.', NULL, NULL, 'Suspicious', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(34, 'bb53d2da-5afe-3886-b119-4cd205850765', 'Samir', 'Boehm', 'candidate034@test.test', '78XXXXX034', 'cid_034', NULL, 2, '11123 Eichmann Extension Suite 274\nGulgowskiview, KY 62311-6646', 'Professor', 'Officia nihil totam est molestiae quia.', NULL, NULL, 'In Progress', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(35, '8b27bf07-9427-3793-8b55-246cdffe30d8', 'Kaya', 'Boehm', 'candidate035@test.test', '78XXXXX035', 'cid_035', NULL, 2, '58948 McClure River Apt. 230\nTonytown, NC 55960', 'CEO', 'Enim et a neque ut.', NULL, NULL, 'Verified', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(36, '4a2b9abb-9b81-3e3f-b7fd-3819285156f2', 'Lorenza', 'Gerlach', 'candidate036@test.test', '78XXXXX036', 'cid_036', NULL, 4, '701 Rau Parkways\nSouth Asha, AR 89403-4670', 'CTO', 'Magnam alias mollitia veritatis assumenda.', NULL, NULL, 'Verified', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(37, '6e41836c-d0d1-31e4-a205-bde302faa957', 'Santiago', 'Gleason', 'candidate037@test.test', '78XXXXX037', 'cid_037', NULL, 3, '2312 Eichmann Mill\nWest Makennaview, WY 36967-2122', 'Guard', 'Debitis et culpa qui voluptas architecto.', NULL, NULL, 'Verified', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(38, '4ec00cd8-6ad7-370c-a854-4a683ce539a6', 'Vaughn', 'Russel', 'candidate038@test.test', '78XXXXX038', 'cid_038', NULL, 4, '955 Corwin Cape\nBorertown, IL 43778', 'CEO', 'Earum repudiandae deleniti ipsum velit.', NULL, NULL, 'Verified', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(39, '58177520-d9d7-36e1-b3e9-35e24077f6c5', 'Emilie', 'Heaney', 'candidate039@test.test', '78XXXXX039', 'cid_039', NULL, 5, '7500 Little Circles\nNorth Ulises, MT 64787-2552', 'Manager', 'Id eligendi et voluptatibus sint nam.', NULL, NULL, 'New', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(40, 'bbc2c500-f70b-3862-9671-0333dc42cbc5', 'Darien', 'Gulgowski', 'candidate040@test.test', '78XXXXX040', 'cid_040', NULL, 1, '98809 Howell Forks\nWindlerberg, IA 26873-8444', 'Clerk', 'Eaque eligendi quasi tenetur.', NULL, NULL, 'New', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(41, '430164ef-961b-3b2f-8c7b-6a8f973ee32c', 'Eileen', 'Champlin', 'candidate041@test.test', '78XXXXX041', 'cid_041', NULL, 3, '8224 Billie Circle\nNorth Erling, AZ 88765', 'Professor', 'Aliquid non non sint molestiae dolores sit.', NULL, NULL, 'Verified', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(42, 'e0d05d95-f5c4-34c7-a721-ea1f79eae932', 'Liliana', 'Hermiston', 'candidate042@test.test', '78XXXXX042', 'cid_042', NULL, 5, '349 Julian Ranch\nWest Lomaberg, HI 50765-8718', 'Manager', 'Eum odit tenetur qui consectetur.', NULL, NULL, 'New', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(43, 'ee7b0138-698c-3ad1-936b-8f203cc992ed', 'Jamey', 'Yundt', 'candidate043@test.test', '78XXXXX043', 'cid_043', NULL, 3, '569 Madelyn Court\nLueilwitzhaven, MD 66282-4621', 'Guard', 'Facere mollitia laboriosam ea quod.', NULL, NULL, 'Verified', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(44, '932e17e7-4a82-3cd1-a3b8-2a2c680a6b4e', 'Nedra', 'Lebsack', 'candidate044@test.test', '78XXXXX044', 'cid_044', NULL, 3, '9490 Weimann Lights\nPourosport, VT 40181-6518', 'Professor', 'Aut mollitia odit magnam consequatur.', NULL, NULL, 'Suspicious', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(45, '994eceac-02b2-311b-be31-6acfbf950104', 'Christelle', 'Koelpin', 'candidate045@test.test', '78XXXXX045', 'cid_045', NULL, 1, '534 Emard Row\nLake Annabellebury, KS 74991', 'CTO', 'Aliquam voluptatem porro nihil totam.', NULL, NULL, 'Verified', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(46, '55f187dc-c370-363a-b8da-40f36cc3a8ca', 'Lorna', 'Padberg', 'candidate046@test.test', '78XXXXX046', 'cid_046', NULL, 3, '127 Oberbrunner Falls Suite 254\nPrudenceland, TN 57254', 'CTO', 'Ullam cum vero eaque ut.', NULL, NULL, 'Suspicious', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(47, 'c63dc1f9-56f8-39cb-ba97-6872a3ab5627', 'Jamarcus', 'Durgan', 'candidate047@test.test', '78XXXXX047', 'cid_047', NULL, 1, '294 Barrows Plain Apt. 703\nNorth Cathrineton, MS 17589-3523', 'Team Lead', 'Quasi consequuntur occaecati ipsam corporis.', NULL, NULL, 'In Progress', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(48, '85292769-5012-357b-86df-64f9d2366352', 'Madyson', 'Ledner', 'candidate048@test.test', '78XXXXX048', 'cid_048', NULL, 2, '84757 Collins Path\nNew Sarinaborough, SC 56953-7431', 'Manager', 'Saepe in molestiae molestias non.', NULL, NULL, 'New', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(49, 'd05091ce-1bfc-3196-8a61-7177c9d4b15c', 'Rosalia', 'Murray', 'candidate049@test.test', '78XXXXX049', 'cid_049', NULL, 3, '9361 Collins Vista\nKingside, ID 11472-2830', 'CTO', 'Minus quidem quam quidem.', NULL, NULL, 'In Progress', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(50, '736a3dfc-d99e-374a-bf4b-dd450a28296e', 'Ellsworth', 'Stracke', 'candidate050@test.test', '78XXXXX050', 'cid_050', NULL, 1, '21835 Schultz Stream\nKaydenport, ME 18718', 'CTO', 'Qui soluta quaerat dicta accusamus nobis.', NULL, NULL, 'In Progress', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(51, '96eea5b1-d755-3044-83c7-518c533d3b10', 'Kim', 'Mayer', 'candidate051@test.test', '78XXXXX051', 'cid_051', NULL, 2, '616 Gleichner Views Apt. 416\nCrystelmouth, NV 99320-7741', 'Clerk', 'Saepe a ullam fuga omnis quo maiores.', NULL, NULL, 'Suspicious', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(52, '8ff7f421-dd6d-318e-b45f-08ab51f426a9', 'Elmo', 'Rempel', 'candidate052@test.test', '78XXXXX052', 'cid_052', NULL, 2, '3683 Hermann Shores\nEmmaleebury, MA 77240-5812', 'Teacher', 'Occaecati ea in expedita aut.', NULL, NULL, 'New', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(53, 'f425d5c2-9700-31d2-94f0-e955ad555d9c', 'Gaetano', 'Morar', 'candidate053@test.test', '78XXXXX053', 'cid_053', NULL, 2, '83431 Bernard Squares Apt. 333\nSouth Jaydeville, VT 04161', 'Teacher', 'Error recusandae hic reprehenderit ex.', NULL, NULL, 'Suspicious', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(54, '789ed271-6422-3eed-b2f6-ada68f519114', 'Jaylen', 'Weber', 'candidate054@test.test', '78XXXXX054', 'cid_054', NULL, 2, '2962 Wyman Road Apt. 521\nSouth Bryceborough, KY 61491', 'Teacher', 'Nesciunt ut alias vel quaerat sit autem.', NULL, NULL, 'New', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(55, 'f252b30c-e888-3a08-b2ca-cd3e4d9dbe83', 'Garfield', 'Fay', 'candidate055@test.test', '78XXXXX055', 'cid_055', NULL, 4, '62368 Edythe Square\nNorth Ryleigh, FL 54861', 'Guard', 'Enim qui ut et non non.', NULL, NULL, 'New', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(56, 'b0d7a35f-a295-335d-9104-dfcf23fdaae0', 'Stefan', 'Sipes', 'candidate056@test.test', '78XXXXX056', 'cid_056', NULL, 1, '522 Gaylord Centers Suite 123\nSouth Suzanneville, WA 04436-7534', 'Manager', 'In est quo natus ducimus quod magni.', NULL, NULL, 'In Progress', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(57, '3697ee7c-4e47-367c-b90a-78813960365a', 'Jose', 'Ankunding', 'candidate057@test.test', '78XXXXX057', 'cid_057', NULL, 5, '49303 Langworth Prairie Suite 147\nLornabury, AR 35692', 'Team Lead', 'Laboriosam qui velit molestias eaque.', NULL, NULL, 'Suspicious', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(58, '19eb6772-20a8-3f5c-a376-36a094a5635a', 'Zachariah', 'Hettinger', 'candidate058@test.test', '78XXXXX058', 'cid_058', NULL, 5, '51977 Conroy Trail Apt. 682\nAuroreshire, NH 36120', 'CTO', 'Nostrum quasi commodi repellat porro.', NULL, NULL, 'Verified', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(59, '40b557d2-8883-3f5f-93e8-7503af47d1c2', 'Alessandra', 'Feil', 'candidate059@test.test', '78XXXXX059', 'cid_059', NULL, 3, '6353 Pierce Centers Suite 800\nMillsport, VA 24230', 'Team Lead', 'Aut voluptas quibusdam sit quia.', NULL, NULL, 'In Progress', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(60, '9fb8844b-ca33-3758-9590-ce35f6b4038c', 'Diana', 'Marquardt', 'candidate060@test.test', '78XXXXX060', 'cid_060', NULL, 2, '67925 Estevan Locks Apt. 075\nRexside, ID 54153', 'Clerk', 'Fugit vel qui ipsam molestiae blanditiis et.', NULL, NULL, 'Suspicious', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(61, 'd3239634-65d1-3756-947c-71ce07777d80', 'Kade', 'Kohler', 'candidate061@test.test', '78XXXXX061', 'cid_061', NULL, 2, '861 Romaine Lane Suite 296\nWilburnborough, IN 15341', 'Team Lead', 'Aut excepturi ea asperiores est corporis.', NULL, NULL, 'Verified', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(62, 'f1151cce-5580-321b-9483-615919deb4ea', 'Telly', 'Harber', 'candidate062@test.test', '78XXXXX062', 'cid_062', NULL, 3, '93955 Schinner Skyway Apt. 147\nWehnerland, MN 29075', 'Team Lead', 'Reiciendis beatae et ducimus ut molestias.', NULL, NULL, 'In Progress', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(63, '8710b20b-4250-3ac2-b1fe-9dfc627aa94b', 'Terence', 'Sipes', 'candidate063@test.test', '78XXXXX063', 'cid_063', NULL, 3, '8491 Sven Heights\nSouth Minnie, VA 52163-2884', 'Teacher', 'Repellendus vel maiores aperiam labore at.', NULL, NULL, 'Suspicious', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(64, '59340b6e-1685-3aa2-ae8d-3756940121d1', 'Carole', 'Rosenbaum', 'candidate064@test.test', '78XXXXX064', 'cid_064', NULL, 2, '6358 Nikolaus Bridge\nPort Trudie, MN 98120', 'Guard', 'Quas voluptatum facere et.', NULL, NULL, 'Suspicious', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(65, '2ffcef32-ad5b-3ece-9a29-b867196e87d1', 'Troy', 'Schumm', 'candidate065@test.test', '78XXXXX065', 'cid_065', NULL, 5, '68832 O\'Keefe Lodge Apt. 966\nEast Margotborough, OR 07602', 'Guard', 'Iste sint sit eum sunt a.', NULL, NULL, 'Verified', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(66, 'f4b34cba-9866-3f3a-b713-e626f7c7b05e', 'Devonte', 'Shanahan', 'candidate066@test.test', '78XXXXX066', 'cid_066', NULL, 4, '50508 Krajcik Station Suite 388\nLake Lola, AR 13132', 'CTO', 'Ullam earum placeat nostrum qui.', NULL, NULL, 'New', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(67, '026fd7e4-2116-3875-aa93-6d06297175c7', 'Jedediah', 'O\'Kon', 'candidate067@test.test', '78XXXXX067', 'cid_067', NULL, 1, '5484 Vita Vista\nNorth Karleeborough, GA 66268', 'Manager', 'Odit nisi autem ut qui.', NULL, NULL, 'Suspicious', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(68, 'bffc651e-1b3c-3a8a-a4dd-e81208311272', 'Adrienne', 'Medhurst', 'candidate068@test.test', '78XXXXX068', 'cid_068', NULL, 4, '5852 Chyna Glen\nNew Pat, NH 23449', 'Teacher', 'Porro laboriosam eum ea modi voluptas.', NULL, NULL, 'Suspicious', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(69, 'c53ed4db-d0c1-3952-8637-9286d1c307c2', 'Amos', 'Murphy', 'candidate069@test.test', '78XXXXX069', 'cid_069', NULL, 5, '58187 Okuneva Parkways\nJackyside, IL 69221-8173', 'Clerk', 'Sit repellat modi rem a.', NULL, NULL, 'New', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(70, '2ee78b5a-c4b3-32b5-9142-9837f139379f', 'Kenna', 'Green', 'candidate070@test.test', '78XXXXX070', 'cid_070', NULL, 4, '52297 Mohamed Ranch Apt. 228\nGailfort, UT 69045-8157', 'Team Lead', 'Possimus esse quae odio.', NULL, NULL, 'Suspicious', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(71, '53d7ece2-6f48-37dd-bffc-6d41ed231a49', 'Demarco', 'Hill', 'candidate071@test.test', '78XXXXX071', 'cid_071', NULL, 2, '8937 Wilson Underpass\nOrtizchester, MO 46065-4938', 'Team Lead', 'Aliquid consectetur quia sed ut soluta.', NULL, NULL, 'In Progress', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(72, '390a9481-cf8d-3f65-91a4-3f3f8d8b825e', 'Daphnee', 'McDermott', 'candidate072@test.test', '78XXXXX072', 'cid_072', NULL, 3, '13231 Annalise Summit Suite 705\nEast Melissa, RI 92821', 'Clerk', 'Pariatur molestiae quis voluptas quia eum.', NULL, NULL, 'Suspicious', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(73, 'de27959f-18a5-3771-b52c-77cb93b3eddb', 'Baron', 'Gorczany', 'candidate073@test.test', '78XXXXX073', 'cid_073', NULL, 2, '352 Chadrick Station Apt. 428\nReaganmouth, MN 15493-6713', 'Professor', 'Qui molestias harum totam earum dolorum.', NULL, NULL, 'Verified', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(74, '10a7a911-13a2-31d1-b88c-b997a2e605b6', 'Ottis', 'White', 'candidate074@test.test', '78XXXXX074', 'cid_074', NULL, 3, '2528 Orn Park\nMoenmouth, NY 01426-2130', 'Guard', 'Sit voluptatibus sequi id odit fugit et.', NULL, NULL, 'Suspicious', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(75, 'f4ea2fe2-af48-3265-b92f-8fc54e561f4e', 'Travis', 'Kirlin', 'candidate075@test.test', '78XXXXX075', 'cid_075', NULL, 1, '7298 Chyna Harbors\nRosellaport, MS 47418', 'CEO', 'Facilis ut magnam consequatur.', NULL, NULL, 'New', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(76, '31adb684-11c7-3173-bb0f-74824636fbaf', 'Stephan', 'Flatley', 'candidate076@test.test', '78XXXXX076', 'cid_076', NULL, 3, '11139 Garnett Key\nNew Terrill, TX 71550', 'Clerk', 'Est deleniti fugit esse est in.', NULL, NULL, 'Verified', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(77, 'eb3feace-ed91-3f4f-983b-b265f7876971', 'Neha', 'Bogisich', 'candidate077@test.test', '78XXXXX077', 'cid_077', NULL, 5, '25068 Crawford Avenue Apt. 407\nPort Kevin, WV 86835-1413', 'Teacher', 'Fuga et quis et nihil sint.', NULL, NULL, 'New', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(78, '7b2b1de8-4a2f-3fdb-8062-c27b478165fa', 'Heber', 'Walsh', 'candidate078@test.test', '78XXXXX078', 'cid_078', NULL, 5, '874 Muller Walk\nDibbertstad, FL 21083', 'Manager', 'A in dolore aut consequuntur.', NULL, NULL, 'Suspicious', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(79, '262c9bf8-ad52-3861-bd34-18c02c651dcb', 'Emilia', 'Hickle', 'candidate079@test.test', '78XXXXX079', 'cid_079', NULL, 5, '7303 Sterling Manor\nEast Anitachester, NM 28400-1372', 'CTO', 'Nemo iure non minus et tenetur ipsa aut ut.', NULL, NULL, 'In Progress', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(80, '2b50f463-3e67-356a-bb4b-866a06b9057b', 'Bulah', 'Schmeler', 'candidate080@test.test', '78XXXXX080', 'cid_080', NULL, 1, '42127 Emmalee Neck Suite 750\nNew Isidroport, NY 97183-8065', 'Team Lead', 'Ipsa qui fugiat dignissimos placeat.', NULL, NULL, 'Suspicious', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(81, 'dc736a7d-0262-3f25-be46-9759b0ccc590', 'Bulah', 'Roob', 'candidate081@test.test', '78XXXXX081', 'cid_081', NULL, 3, '442 Missouri Park Apt. 748\nCorkerymouth, NE 52440-6926', 'CEO', 'Consequuntur placeat perferendis minima id.', NULL, NULL, 'In Progress', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(82, 'f12cc2b5-c06a-37c6-9361-639c099f6cbc', 'Modesta', 'Lemke', 'candidate082@test.test', '78XXXXX082', 'cid_082', NULL, 1, '32072 Mattie Crossroad Suite 802\nBauchport, AL 48787', 'CTO', 'Sed quae nisi cumque enim modi et rerum.', NULL, NULL, 'In Progress', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(83, 'b500aab8-bfc2-3690-8bae-4ef5fc17bac8', 'Ahmed', 'Quigley', 'candidate083@test.test', '78XXXXX083', 'cid_083', NULL, 5, '318 Sporer Plain\nSouth Brendenmouth, AL 26329-8156', 'Manager', 'Eum qui expedita nihil odio.', NULL, NULL, 'In Progress', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(84, '4246e583-9b27-375c-acab-e362175b35c2', 'Keaton', 'Heaney', 'candidate084@test.test', '78XXXXX084', 'cid_084', NULL, 1, '382 Celia Throughway\nNew Mortonfurt, IN 26972', 'Manager', 'Optio labore enim dolorem assumenda.', NULL, NULL, 'Suspicious', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(85, '68fa0ce3-a16a-3020-8ca1-657d18b66682', 'Tessie', 'Stiedemann', 'candidate085@test.test', '78XXXXX085', 'cid_085', NULL, 5, '41999 Linwood Landing Apt. 487\nEast Agnes, OR 86633', 'Manager', 'Soluta quasi quo rerum sequi dolore et hic.', NULL, NULL, 'In Progress', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(86, '8873aa16-05d6-3731-bfe3-73c79f00f8df', 'Mitchell', 'Zemlak', 'candidate086@test.test', '78XXXXX086', 'cid_086', NULL, 3, '941 Elroy Fork Suite 145\nNew Vivien, TX 90099', 'Guard', 'Laborum voluptates culpa quia eaque.', NULL, NULL, 'Verified', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(87, '8c287c79-9871-353b-8289-4ae6796e4689', 'Alexie', 'Ruecker', 'candidate087@test.test', '78XXXXX087', 'cid_087', NULL, 2, '2689 Brown Row Apt. 090\nNew Ernestina, MN 17783-2078', 'Team Lead', 'Qui recusandae repellat quam.', NULL, NULL, 'In Progress', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(88, '999e8c19-e164-31b2-bdd1-0c9211beec5f', 'Nona', 'Gulgowski', 'candidate088@test.test', '78XXXXX088', 'cid_088', NULL, 5, '221 Rosanna Center\nLake Hellenburgh, RI 53579', 'Team Lead', 'Aperiam non dolor voluptatem quis optio.', NULL, NULL, 'New', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(89, '850f5e08-f01d-33cb-8728-9c0939e62860', 'Opal', 'Zieme', 'candidate089@test.test', '78XXXXX089', 'cid_089', NULL, 1, '716 Marks Crossing\nShermanstad, KS 17753', 'Professor', 'Et eum veniam dolorem.', NULL, NULL, 'Suspicious', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(90, '7ebb6d90-b2c3-3858-a1fb-75fcdda840d2', 'Ashley', 'Hoppe', 'candidate090@test.test', '78XXXXX090', 'cid_090', NULL, 1, '127 Lynch Ways\nLexieview, MN 25447', 'Teacher', 'Molestiae pariatur rerum rem aut ut vel et.', NULL, NULL, 'Verified', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(91, '0d69b692-7b60-3e90-979d-bd59093f9231', 'Brendon', 'Cartwright', 'candidate091@test.test', '78XXXXX091', 'cid_091', NULL, 3, '6011 Keara Mountain Suite 806\nNorth Lauraton, AR 54996-0138', 'Professor', 'Deserunt quis totam similique voluptatibus.', NULL, NULL, 'New', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(92, '73e2b590-8d80-3e5b-8ba2-c370c49f37d7', 'Bailey', 'Schimmel', 'candidate092@test.test', '78XXXXX092', 'cid_092', NULL, 3, '847 Roob Mount Suite 259\nTeagantown, NV 88457-8416', 'CEO', 'Libero ea quas porro est consequatur.', NULL, NULL, 'New', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(93, 'a79c0910-22cc-3a9d-9688-e1b0cd6f612c', 'Dawn', 'McDermott', 'candidate093@test.test', '78XXXXX093', 'cid_093', NULL, 5, '1173 Audrey Stravenue Apt. 455\nConsidineview, AL 49443-3045', 'Guard', 'Nulla qui dolore molestias eos voluptatum.', NULL, NULL, 'In Progress', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(94, 'ca9f7554-7d97-373d-93be-12965c27e396', 'Coralie', 'Medhurst', 'candidate094@test.test', '78XXXXX094', 'cid_094', NULL, 3, '226 Trystan Flat\nEast Raquelchester, KY 24874-4738', 'Team Lead', 'Nam voluptas dicta saepe.', NULL, NULL, 'In Progress', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(95, '9d5522a1-cf65-3bf0-a2b5-b8a593679db9', 'Vivianne', 'Boyle', 'candidate095@test.test', '78XXXXX095', 'cid_095', NULL, 4, '786 Shane Stravenue Apt. 680\nTillmanfurt, NY 09060', 'Manager', 'Nihil magnam culpa enim dolor.', NULL, NULL, 'Suspicious', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(96, '97499977-de56-3735-bce4-fa1afec5b6fc', 'Virginie', 'Orn', 'candidate096@test.test', '78XXXXX096', 'cid_096', NULL, 4, '3245 Rohan Terrace\nEast Rosalynberg, TN 75561', 'CTO', 'Fugit magnam sit aut voluptas delectus aut.', NULL, NULL, 'New', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(97, 'bde1f0d7-99fc-3572-8490-103a0ec0b903', 'Rudolph', 'Little', 'candidate097@test.test', '78XXXXX097', 'cid_097', NULL, 3, '32936 Gutkowski Rapids Apt. 384\nFrancisberg, NJ 48914-0842', 'Guard', 'Voluptas dolorem ut eius porro non.', NULL, NULL, 'In Progress', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(98, 'a0728006-4952-35f3-8dec-b1bdeb44e6f2', 'Lacy', 'Little', 'candidate098@test.test', '78XXXXX098', 'cid_098', NULL, 4, '428 Kathleen Village\nKarenberg, FL 46191-6195', 'Clerk', 'Aliquam ipsam earum molestias.', NULL, NULL, 'New', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(99, '4bf3d0c0-afd5-3157-87a7-403e7dc7dcda', 'Abbey', 'Cronin', 'candidate099@test.test', '78XXXXX099', 'cid_099', NULL, 3, '5530 Wisoky Parkway\nKemmerton, NJ 98558', 'Manager', 'Neque odit eos eligendi quia.', NULL, NULL, 'New', NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(100, '24f078c8-2ce7-3ab7-943f-c639851833bf', 'Mathias', 'Veum', 'candidate100@test.test', '78XXXXX100', 'cid_100', NULL, 2, '2590 Madie Lodge Suite 279\nNorth Anna, OR 17385', 'Clerk', 'Officiis labore illum est neque dolorem.', NULL, NULL, 'New', NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(101, '05b588f4-39dc-42ea-9904-e6478d5d5ff7', 'test001', '123456', 'jilkakaran02@gmail.com', '1234567980', 'test001', '', 1, 'dsfas', '', '', '{\"lat\": 22.2887936, \"lng\": 70.77560319999999}', NULL, 'In Progress', '2020-12-21 18:57:35', NULL, 0, 0, 1, 0, 0, 1, 1, NULL, '2020-12-20 08:25:37', '2020-12-21 13:27:35', NULL),
(102, 'fed52b6f-4a37-4fbd-8d3a-4aa31754559e', 'test 002', '002', 'jilkakaran@gmail.com', '1234567871', 'test002', NULL, 1, 'Volodymyrska St, 15, Kyiv, Ukraine, 02000', NULL, NULL, '{\"lat\": 22.2920704, \"lng\": 70.7821568}', NULL, 'In Progress', '2020-12-24 18:26:27', NULL, 0, 0, 0, 1, 0, 1, 1, NULL, '2020-12-21 13:28:17', '2020-12-24 12:56:27', NULL),
(103, 'ebf5f422-2f40-48a5-8076-0a25f379a28a', 'Swati', 'Singh', 'nits@clyfe.co', '9136250098', 'CID_231', 'JOB_101', 8, 'Bangalore', 'Test Manager', '', '{\"lat\": 19.1957948, \"lng\": 72.872501}', NULL, 'Suspicious', '2020-12-23 09:17:19', NULL, 0, 0, 0, 0, 0, 7, 1, NULL, '2020-12-23 02:01:29', '2020-12-23 03:47:19', NULL),
(104, 'bb075b01-5f82-4176-b5a6-6b8e9697ca1a', 'Aman', 'J', 'shweta.j371@gmail.com', '1234567890', 'cid_1210', 'job_1210', 10, 'Pune', 'Executive', 'Test Comment !@$234 23/ 2342', '{\"lat\": 18.4862468, \"lng\": 73.9526658}', '{\"lat\": 18.4858228, \"lng\": 73.9523667}', 'Suspicious', '2020-12-23 09:07:05', NULL, 0, 0, 0, 0, 0, 1, 1, NULL, '2020-12-23 03:11:51', '2020-12-23 03:37:05', NULL),
(105, '41ffac5a-4154-4f65-bc4b-9241b0bf2f5d', 'Swati', 'Jain', 'shweta@nextjobhunt.com', '1234567890', 'cid_1219', 'Job_1219', 10, 'Pune', 'Executive', 'test commnet 23423 234j @#@$ skdjfh', '{\"lat\": 18.4858228, \"lng\": 73.9523667}', NULL, 'Suspicious', '2020-12-23 09:11:14', NULL, 0, 0, 0, 0, 0, 1, 1, NULL, '2020-12-23 03:26:09', '2020-12-23 03:41:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_xid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aadhar_number` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_ifsc_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_account_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_aadhar_verified` tinyint(1) NOT NULL DEFAULT 0,
  `passport_changed` tinyint(1) NOT NULL DEFAULT 0,
  `pan_card_changed` tinyint(1) NOT NULL DEFAULT 0,
  `identity_proof_changed` tinyint(1) NOT NULL DEFAULT 0,
  `current_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'new',
  `current_status_changed_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `compare_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'pending',
  `customer_form_fill_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'pending',
  `customer_form_submitted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customer_answers`
--

CREATE TABLE `customer_answers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `organization_question_id` bigint(20) UNSIGNED NOT NULL,
  `answers` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customer_comments`
--

CREATE TABLE `customer_comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_comment_xid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'new',
  `current_status_changed_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `disk` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `directory` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extension` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aggregate_type` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) UNSIGNED NOT NULL,
  `variant_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `original_media_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `disk`, `directory`, `filename`, `extension`, `mime_type`, `aggregate_type`, `size`, `variant_name`, `original_media_id`, `created_at`, `updated_at`) VALUES
(1, 's3', '', 'a0e33e18-8b3e-48f3-bf02-a717dee31b61', 'jpg', 'image/jpeg', 'image', 22798, NULL, NULL, '2020-12-20 08:25:58', '2020-12-20 08:25:58'),
(2, 's3', '', 'dc5edc80-b745-4138-acd8-dd74910f619b', 'jpg', 'image/jpeg', 'image', 15341, NULL, NULL, '2020-12-20 08:32:40', '2020-12-20 08:32:40'),
(3, 's3', 'canimg', 'cec63e5f-1a84-4e9d-ac9f-baf674a70266', 'jpg', 'image/jpeg', 'image', 160528, NULL, NULL, '2020-12-21 13:27:35', '2020-12-21 13:27:35'),
(4, 's3', 'canimg', '4afbc9ce-26a8-4998-9f82-214f8378b09e', 'jpg', 'image/jpeg', 'image', 68411, NULL, NULL, '2020-12-21 13:28:28', '2020-12-21 13:28:28'),
(5, 's3', 'canimg', 'e3f68726-b81e-4ed9-ad7a-065493873ad6', 'jpg', 'image/jpeg', 'image', 15912, NULL, NULL, '2020-12-21 13:29:24', '2020-12-21 13:29:24'),
(6, 's3', 'canimg', '935caac9-1713-400e-9594-a64b46086c36', 'jpg', 'image/jpeg', 'image', 10035, NULL, NULL, '2020-12-23 02:07:14', '2020-12-23 02:07:14'),
(7, 's3', 'canimg', '681edbde-9bba-4594-907d-dbd60cbc2ac3', 'jpg', 'image/jpeg', 'image', 11729, NULL, NULL, '2020-12-23 02:07:30', '2020-12-23 02:07:30'),
(8, 's3', 'canimg', 'e7d24ec6-6d87-44ef-aeb7-7566c4ce1ad6', 'jpg', 'image/jpeg', 'image', 45975, NULL, NULL, '2020-12-23 02:10:39', '2020-12-23 02:10:39'),
(9, 's3', 'canimg', 'ce975098-6bde-404a-8987-37e34062dacc', 'jpg', 'image/jpeg', 'image', 24434, NULL, NULL, '2020-12-23 02:10:51', '2020-12-23 02:10:51'),
(10, 's3', 'canimg', 'c5b7b770-af0e-4453-b18c-581064636901', 'jpg', 'image/jpeg', 'image', 78981, NULL, NULL, '2020-12-23 02:11:43', '2020-12-23 02:11:43'),
(11, 's3', 'canimg', 'cd6538bc-5880-4810-b2c7-ba0a06dfc39d', 'jfif', 'image/jpeg', 'image', 8240, NULL, NULL, '2020-12-23 02:12:49', '2020-12-23 02:12:49'),
(12, 's3', 'canimg', '794033f9-0014-4b65-995f-e7e20df548d1', 'jfif', 'image/jpeg', 'image', 8399, NULL, NULL, '2020-12-23 02:15:12', '2020-12-23 02:15:12'),
(13, 's3', 'canimg', '5fa147f4-c3c7-4875-9952-b35ce3e94821', 'jpeg', 'image/jpeg', 'image', 27819, NULL, NULL, '2020-12-23 02:18:23', '2020-12-23 02:18:23'),
(14, 's3', 'canimg', '92b825dc-bb7b-496a-a12a-2ef65cdb03c3', 'jpg', 'image/jpeg', 'image', 19981, NULL, NULL, '2020-12-23 03:21:37', '2020-12-23 03:21:37'),
(15, 's3', 'canimg', 'ebd1ad89-a30e-4869-a2b5-251ad6b62710', 'jpg', 'image/jpeg', 'image', 19080, NULL, NULL, '2020-12-23 03:21:52', '2020-12-23 03:21:52'),
(16, 's3', 'canimg', '3393d73b-e01d-4c99-87d2-634b5c82f8e9', 'jpg', 'image/jpeg', 'image', 7936, NULL, NULL, '2020-12-23 03:22:51', '2020-12-23 03:22:51'),
(17, 's3', 'canimg', '1f05032d-379f-4c7a-841e-6da471d19dd2', 'jpg', 'image/jpeg', 'image', 8419, NULL, NULL, '2020-12-23 03:23:46', '2020-12-23 03:23:46'),
(18, 's3', 'canimg', 'e667b8d8-cbcf-4360-8f26-9d4769dce372', 'jpg', 'image/jpeg', 'image', 30542, NULL, NULL, '2020-12-23 03:24:12', '2020-12-23 03:24:12'),
(19, 's3', 'canimg', '6b27624b-4626-4d87-941d-6b141f39155c', 'jpg', 'image/jpeg', 'image', 18110, NULL, NULL, '2020-12-23 03:35:46', '2020-12-23 03:35:46'),
(20, 's3', 'canimg', 'd92a95c3-bef0-480c-8b45-6409142508e9', 'jpg', 'image/jpeg', 'image', 18362, NULL, NULL, '2020-12-23 03:36:04', '2020-12-23 03:36:04'),
(21, 's3', 'canimg', '2f9b3770-85a4-4b98-af16-4db4370eabfe', 'jpg', 'image/jpeg', 'image', 17648, NULL, NULL, '2020-12-23 03:40:16', '2020-12-23 03:40:16'),
(22, 's3', 'canimg', 'c6ab585a-58fc-4917-a73c-5a958a64d7de', 'jpg', 'image/jpeg', 'image', 18168, NULL, NULL, '2020-12-23 03:40:28', '2020-12-23 03:40:28'),
(23, 's3', 'canimg', '6f611664-9938-406c-b8d1-b3cf56b14fc5', 'jpg', 'image/jpeg', 'image', 41988, NULL, NULL, '2020-12-23 03:41:12', '2020-12-23 03:41:12'),
(24, 's3', 'canimg', 'cfa6513d-f21d-4bbd-9188-13b3f3ab7f79', 'jpg', 'image/jpeg', 'image', 7936, NULL, NULL, '2020-12-23 03:47:16', '2020-12-23 03:47:16'),
(25, 's3', 'canimg', '9a4259df-2124-439f-a09a-921a185f0672', 'jpg', 'image/jpeg', 'image', 66517, NULL, NULL, '2020-12-24 12:56:27', '2020-12-24 12:56:27');

-- --------------------------------------------------------

--
-- Table structure for table `mediables`
--

CREATE TABLE `mediables` (
  `media_id` int(10) UNSIGNED NOT NULL,
  `mediable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mediable_id` int(10) UNSIGNED NOT NULL,
  `tag` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mediables`
--

INSERT INTO `mediables` (`media_id`, `mediable_type`, `mediable_id`, `tag`, `order`) VALUES
(1, 'App\\Models\\Candidate', 101, 'step_1_image', 1),
(2, 'App\\Models\\Candidate', 101, 'master_image', 1),
(4, 'App\\Models\\Candidate', 102, 'step_1_image', 1),
(5, 'App\\Models\\Candidate', 102, 'master_image', 1),
(7, 'App\\Models\\Candidate', 103, 'master_image', 1),
(8, 'App\\Models\\Candidate', 103, 'step_1_image', 1),
(9, 'App\\Models\\Candidate', 103, 'step_2_image', 1),
(10, 'App\\Models\\Candidate', 103, 'step_3_image', 1),
(15, 'App\\Models\\Candidate', 104, 'master_image', 1),
(16, 'App\\Models\\Candidate', 104, 'step_1_image', 1),
(20, 'App\\Models\\Candidate', 104, 'final_validation_image', 1),
(22, 'App\\Models\\Candidate', 105, 'master_image', 1),
(23, 'App\\Models\\Candidate', 105, 'step_1_image', 1),
(25, 'App\\Models\\Candidate', 102, 'step_2_image', 1),
(3, 'App\\Models\\Candidate', 101, 'step_1_image', 2),
(11, 'App\\Models\\Candidate', 103, 'step_3_image', 2),
(17, 'App\\Models\\Candidate', 104, 'step_1_image', 2),
(12, 'App\\Models\\Candidate', 103, 'step_3_image', 3),
(18, 'App\\Models\\Candidate', 104, 'step_1_image', 3),
(13, 'App\\Models\\Candidate', 103, 'step_3_image', 4),
(24, 'App\\Models\\Candidate', 103, 'step_3_image', 5);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_09_24_171856_create_table_organization_type', 1),
(2, '2014_10_11_180033_create_timezones_table', 1),
(3, '2014_10_11_180034_create_table_organizations', 1),
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(7, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(8, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(9, '2016_06_01_000004_create_oauth_clients_table', 1),
(10, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(11, '2016_06_27_000000_create_mediable_tables', 1),
(12, '2019_08_19_000000_create_failed_jobs_table', 1),
(13, '2019_09_11_124525_create_verify_users_table', 1),
(14, '2019_10_01_181543_create_roles_table', 1),
(15, '2019_10_01_181545_create_organization_user_xrefs_table', 1),
(16, '2019_12_16_052435_create_permission_tables', 1),
(17, '2020_10_12_000000_add_variants_to_media', 1),
(18, '2020_10_22_090623_create_candidates_table', 1),
(19, '2020_10_25_102758_create_action_logs_table', 1),
(20, '2020_10_29_174021_create_web_links', 1),
(21, '2020_10_29_175050_create_otps', 1),
(22, '2020_10_31_111920_create_rekognition_result_logs_table', 1),
(23, '2020_11_12_183431_alter_table_candidate_add_column_status_date', 1),
(24, '2020_12_06_111549_alter_password_resets_table', 1),
(25, '2020_12_15_175917_create_customer_table', 1),
(26, '2020_12_15_184848_create_customer_comment_table', 1),
(27, '2020_12_15_185452_create_organization_questions_table', 1),
(28, '2020_12_15_185837_create_customer_answers_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('00b6f69059819ca6d0aa267957b6e9b4d8f40aa6ce8cd30f38996d9b39119062047eb6219fd3af1e', 1, 1, 'Personal Access Token', '[]', 0, '2020-12-20 23:10:04', '2020-12-20 23:10:04', '2020-12-21 12:40:04'),
('1093117ab4d69e74b40dc3f033e1412d5a67cf1d691b910c02dda5ae3a5e4fbe2fb133258294e59d', 1, 1, 'Personal Access Token', '[]', 0, '2020-12-20 08:22:57', '2020-12-20 08:22:57', '2020-12-20 21:52:57'),
('2a1c04ba1430d8da0e45f5d5f85fb085c0dffbca5669f63710294764266ab80244d5f994b0d3461c', 1, 1, 'Personal Access Token', '[]', 0, '2020-12-27 00:14:50', '2020-12-27 00:14:50', '2020-12-27 13:44:50'),
('31bb73268be85d271223ecb355a2a54a6873860d77c0e55ead0b6af133f7873398b77ecd1c4e04cf', 7, 1, 'Personal Access Token', '[]', 0, '2020-12-23 01:50:19', '2020-12-23 01:50:19', '2020-12-23 15:20:19'),
('3722d0a61f0ff387ca86a513c06222fb9aabd3467ebb54f903e5f99f5505f166dcc9b8824d0a5538', 1, 1, 'Personal Access Token', '[]', 0, '2020-12-23 11:20:24', '2020-12-23 11:20:24', '2020-12-24 00:50:24'),
('419adfcec5a1451f25fae1dbe5541815435b26c364663b7956226be8c80639229df5c709a1a0328a', 1, 1, 'Personal Access Token', '[]', 0, '2020-12-29 09:11:00', '2020-12-29 09:11:00', '2020-12-29 22:41:00'),
('446a2d3f2669d6835008af029fa4049f262291ebd467205dc1ed886f935a0e4c304fa3c13cc60cc4', 1, 1, 'Personal Access Token', '[]', 0, '2020-12-21 02:59:03', '2020-12-21 02:59:03', '2020-12-21 16:29:03'),
('59e8bfb9d62bd47dabd629f8ab5cdfcef1a7c29237a0ee5efff57b603efcdb8d7d5548351f1ce4b7', 1, 1, 'Personal Access Token', '[]', 0, '2020-12-20 10:08:11', '2020-12-20 10:08:11', '2020-12-20 23:38:11'),
('6801130cd5e386c38ca39dea2cd35d024c9402b9bbe4a727ef4459e25970a65e2b3d7b4f5cbe1d55', 7, 1, 'Personal Access Token', '[]', 0, '2020-12-23 02:30:03', '2020-12-23 02:30:03', '2020-12-23 16:00:03'),
('6a6cf6d3fc2bfd7a7a4b5b5660aeefb6dd36cf047c168b293dca22c0b99705acd12c350b4e7cd578', 1, 1, 'Personal Access Token', '[]', 0, '2020-12-25 02:35:06', '2020-12-25 02:35:06', '2020-12-25 16:05:06'),
('758b9599770d1ece7cbb98cd20865e7c4822f25890c15c032cac43a1b82f19bbe5cdaab747aff0f6', 1, 1, 'Personal Access Token', '[]', 0, '2020-12-23 02:29:08', '2020-12-23 02:29:08', '2020-12-23 15:59:07'),
('89c69783ee297bf8719ed4dafc44863e9b67853e591444069f119958aa8260fabd37f3341cac2c3d', 7, 1, 'Personal Access Token', '[]', 0, '2020-12-23 02:48:27', '2020-12-23 02:48:27', '2020-12-23 16:18:27'),
('8c99af97686adeed5eea53afc226961616883ae580eaf5837604030f3c8b7b4d4313041ed7abc8e8', 1, 1, 'Personal Access Token', '[]', 0, '2020-12-20 11:19:25', '2020-12-20 11:19:25', '2020-12-21 00:49:25'),
('94015da6a8dc497263a05e59a6b0c77bc999cad7b0c2c7269e1ca19a3ace27dcecf5b49379e3202a', 1, 1, 'Personal Access Token', '[]', 0, '2020-12-24 12:52:37', '2020-12-24 12:52:37', '2020-12-25 02:22:37'),
('9a5eba69f89dfe5bcbfb7d84ad034f35ddbc757be67e9175cd9a133d2753940a89c7379228f3246f', 1, 1, 'Personal Access Token', '[]', 0, '2020-12-23 02:28:18', '2020-12-23 02:28:18', '2020-12-23 15:58:18'),
('b755e526602995ada1df6a52b4f1feb817bdc8ea1f439e5f1b54d0911ad2d8b987dabdb72792152e', 1, 1, 'Personal Access Token', '[]', 0, '2020-12-23 02:33:38', '2020-12-23 02:33:38', '2020-12-23 16:03:38'),
('b939cb2648b334b6b237e5fd466d350dbcbdc2e3bddac463f8ab407c56315321e839ae9257788931', 1, 1, 'Personal Access Token', '[]', 0, '2020-12-21 13:27:12', '2020-12-21 13:27:12', '2020-12-22 02:57:12'),
('c6cfc7483d252488507968c07a29992de57d4a36da667eb7ac4e1e87f113e05859bdad24963496de', 1, 1, 'Personal Access Token', '[]', 0, '2020-12-22 23:18:47', '2020-12-22 23:18:47', '2020-12-23 12:48:47'),
('e1e094c9d099b8f60a3d020aa4e42746a592bfac20cf117a76581708c847b7ec353e59b4a93b05a9', 1, 1, 'Personal Access Token', '[]', 0, '2020-12-27 00:55:27', '2020-12-27 00:55:27', '2020-12-27 14:25:27'),
('f48b0761b066f014993e35226328e0b05600f31430969e6e6e8d5a05c456ce1537690228b11fca5e', 1, 1, 'Personal Access Token', '[]', 0, '2020-12-22 12:08:20', '2020-12-22 12:08:20', '2020-12-23 01:38:20');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'CelebratingLife Personal Access Client', 'WBWRFtKslA4ayoNHy98gCWckZZIjqUkBkSBSAE33', NULL, 'http://localhost', 1, 0, 0, '2020-12-20 08:22:53', '2020-12-20 08:22:53'),
(2, NULL, 'CelebratingLife Password Grant Client', 'JiVxHaq5U9xqFD5DWVd8JIxEDxY2Jbn6TblnZNmJ', 'users', 'http://localhost', 0, 1, 0, '2020-12-20 08:22:53', '2020-12-20 08:22:53');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-12-20 08:22:53', '2020-12-20 08:22:53');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE `organizations` (
  `id` int(10) UNSIGNED NOT NULL,
  `organization_xid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_organization_id` int(10) UNSIGNED DEFAULT NULL,
  `timezone_id` int(10) UNSIGNED DEFAULT NULL,
  `organization_type_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_person` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_expire_hours` int(10) UNSIGNED NOT NULL DEFAULT 24,
  `compare_score_threshold` int(10) UNSIGNED NOT NULL DEFAULT 96,
  `is_active` tinyint(1) NOT NULL DEFAULT 0,
  `allow_send_sms` tinyint(1) NOT NULL DEFAULT 1,
  `allow_send_email` tinyint(1) NOT NULL DEFAULT 1,
  `transaction_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trial_ends_at` datetime DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `organizations`
--

INSERT INTO `organizations` (`id`, `organization_xid`, `parent_organization_id`, `timezone_id`, `organization_type_id`, `name`, `contact_person`, `email`, `phone_number`, `link_expire_hours`, `compare_score_threshold`, `is_active`, `allow_send_sms`, `allow_send_email`, `transaction_id`, `card_brand`, `card_last_four`, `trial_ends_at`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'a7965f6d-9b6e-42ab-ae38-4eecda3a9047', NULL, 248, 1, 'Clyfe', NULL, NULL, NULL, 24, 96, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-20 07:42:40', '2020-12-20 07:42:40', NULL),
(2, '08810c23-61a9-46fc-84f5-72e76c9aad2f', 1, 248, 2, 'Company Organization 1', NULL, NULL, NULL, 24, 96, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(3, 'd485dd1b-eff6-49bd-9c3d-defb864b435b', 1, 248, 2, 'Company Organization 2', NULL, NULL, NULL, 24, 96, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(4, 'e1b53209-c608-4e61-9f6c-23b146931988', 2, 248, 3, 'Vendor Organization 1', NULL, NULL, NULL, 24, 96, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(5, '2e903368-96b9-4fd9-8cbe-6079c26e12cd', 3, 248, 3, 'Vendor Organization 2', NULL, NULL, NULL, 24, 96, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(6, '3268fec3-0730-412c-83ea-02f56f17a340', 1, 248, 2, 'Deloitte India', 'Ajit Samant', 'nits@clyfe.co', '9136250098', 24, 96, 1, 1, 1, NULL, NULL, NULL, NULL, '1', '1', NULL, '2020-12-23 01:46:25', '2020-12-23 02:30:42', NULL),
(7, '15a6fd4e-5749-4705-badb-83fcef3049ce', 6, 248, 3, 'Vendor Nokia', 'Devesh Singh', 'test@test.com', '1111111123', 24, 96, 1, 1, 1, NULL, NULL, NULL, NULL, '7', '7', NULL, '2020-12-23 01:51:44', '2020-12-23 01:51:44', NULL),
(8, 'b7069349-5b53-4991-b9d2-4cf9389b695c', 6, 248, 3, 'Vendor HeroHonda', 'Sonia Mathur', 'test@test.com', '2222222222', 24, 96, 1, 1, 1, NULL, NULL, NULL, NULL, '7', '7', NULL, '2020-12-23 01:52:26', '2020-12-23 01:52:26', NULL),
(9, '3af4d817-a52d-4d33-94bf-d3bbb71db9d3', 1, 248, 2, 'Maaks Services', 'Aman', 'maaks@gmail.com', '1234567890', 24, 96, 1, 1, 1, NULL, NULL, NULL, NULL, '1', '1', NULL, '2020-12-23 02:30:14', '2020-12-23 02:30:14', NULL),
(10, 'd11849e4-7246-49b8-b822-5bf17d47dc3e', 9, 248, 3, 'Maaks Vendor 1', 'John', 'john@gmail.com', '1234567890', 24, 96, 1, 1, 1, NULL, NULL, NULL, NULL, '1', '1', NULL, '2020-12-23 02:38:47', '2020-12-23 02:38:47', NULL),
(11, '935f9ab9-569b-451e-b88c-64d8863575c5', 9, 248, 3, 'Maaks Vendor 2', 'Elex', 'alex@gmail.com', '1234567890', 24, 96, 1, 1, 1, NULL, NULL, NULL, NULL, '1', '1', NULL, '2020-12-23 02:39:14', '2020-12-23 02:39:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `organization_questions`
--

CREATE TABLE `organization_questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL,
  `question` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `field_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'textarea',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `organization_types`
--

CREATE TABLE `organization_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `organization_type_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `organization_types`
--

INSERT INTO `organization_types` (`id`, `organization_type_name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'SuperAdmin', NULL, '2020-12-20 07:42:40', '2020-12-20 07:42:40'),
(2, 'Company', NULL, '2020-12-20 07:42:40', '2020-12-20 07:42:40'),
(3, 'CompanyVendor', NULL, '2020-12-20 07:42:40', '2020-12-20 07:42:40');

-- --------------------------------------------------------

--
-- Table structure for table `organization_user_xrefs`
--

CREATE TABLE `organization_user_xrefs` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `organization_user_xrefs`
--

INSERT INTO `organization_user_xrefs` (`id`, `user_id`, `organization_id`, `role_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 1, NULL, NULL, NULL, '2020-12-20 07:42:40', '2020-12-20 07:42:40', NULL),
(2, 2, 1, 1, NULL, NULL, NULL, '2020-12-20 07:42:40', '2020-12-20 07:42:40', NULL),
(3, 3, 2, 2, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(4, 4, 3, 2, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(5, 5, 4, 3, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(6, 6, 5, 3, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(7, 7, 6, 2, 1, 1, NULL, '2020-12-23 01:47:56', '2020-12-23 01:47:56', NULL),
(8, 8, 8, 3, 1, 1, NULL, '2020-12-23 02:42:31', '2020-12-23 02:42:31', NULL),
(9, 9, 9, 2, 1, 1, NULL, '2020-12-23 03:09:18', '2020-12-23 03:09:18', NULL),
(10, 10, 4, 3, 1, 1, NULL, '2020-12-23 11:56:32', '2020-12-23 11:56:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `otps`
--

CREATE TABLE `otps` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `candidate_id` bigint(20) UNSIGNED NOT NULL,
  `web_link_id` bigint(20) UNSIGNED NOT NULL,
  `access_token` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wrong_attempts` tinyint(4) NOT NULL DEFAULT 0,
  `to_phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `used_at` timestamp NULL DEFAULT NULL,
  `expire_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `otps`
--

INSERT INTO `otps` (`id`, `candidate_id`, `web_link_id`, `access_token`, `otp_number`, `wrong_attempts`, `to_phone_number`, `to_email`, `used_at`, `expire_at`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 101, 14, 'bc055292-cc8f-4220-a505-906eaf497bc7', '3558', 0, '1234567980', NULL, '2020-12-20 08:32:53', '2020-12-20 09:29:44', NULL, NULL, NULL, '2020-12-20 08:29:44', '2020-12-20 08:32:53', NULL),
(2, 102, 24, '0fd9d733-aac0-4f09-b645-ae9c37ec1697', '8376', 0, '1234567871', NULL, '2020-12-21 13:29:34', '2020-12-21 14:28:49', NULL, NULL, NULL, '2020-12-21 13:28:49', '2020-12-21 13:29:34', NULL),
(3, 103, 31, 'febc0cc6-98f4-4c59-ab1a-ad49061cbaa1', '8879', 0, NULL, 'nits@clyfe.co', NULL, '2020-12-23 03:02:26', NULL, NULL, NULL, '2020-12-23 02:02:26', '2020-12-23 02:07:39', '2020-12-23 02:07:39'),
(4, 103, 31, 'e5d23bea-6b54-45e2-ab4a-65e14b790859', '3583', 0, NULL, 'nits@clyfe.co', '2020-12-23 02:07:39', '2020-12-23 03:05:32', NULL, NULL, NULL, '2020-12-23 02:05:32', '2020-12-23 02:07:39', NULL),
(5, 104, 33, NULL, '7255', 0, NULL, 'shweta.j371@gmail.com', NULL, '2020-12-23 04:16:36', NULL, NULL, NULL, '2020-12-23 03:16:36', '2020-12-23 03:16:36', NULL),
(6, 104, 32, '589ea1ee-16b2-49d9-b4ee-b1fdc4def3ab', '6104', 0, NULL, 'shweta.j371@gmail.com', '2020-12-23 03:22:04', '2020-12-23 04:17:20', NULL, NULL, NULL, '2020-12-23 03:17:20', '2020-12-23 03:22:04', NULL),
(7, 104, 34, '5a709e2c-72ab-43ea-b831-6d88ef42b46c', '5542', 0, NULL, 'shweta.j371@gmail.com', '2020-12-23 03:36:12', '2020-12-23 04:33:26', NULL, NULL, NULL, '2020-12-23 03:33:26', '2020-12-23 03:36:12', NULL),
(8, 105, 35, '71640d05-5367-462e-b8f5-9ca968b59125', '9042', 0, NULL, 'shweta@nextjobhunt.com', '2020-12-23 03:40:34', '2020-12-23 04:38:10', NULL, NULL, NULL, '2020-12-23 03:38:10', '2020-12-23 03:40:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`id`, `email`, `token`, `created_at`, `updated_at`) VALUES
(1, 'nits@clyfe.co', 'CzsmFhvWy6Z2oOqaB8726G4MbKLNVdtkvfONcFWrpbeVYJOCdOcDQ7eiT37H', '2020-12-23 01:49:13', '2020-12-23 01:49:13');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rekognition_result_logs`
--

CREATE TABLE `rekognition_result_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `organization_id` int(10) UNSIGNED DEFAULT NULL,
  `candidate_id` int(10) UNSIGNED DEFAULT NULL,
  `source_media_id` int(10) UNSIGNED DEFAULT NULL,
  `target_media_id` int(10) UNSIGNED DEFAULT NULL,
  `rekognition_result` double(5,2) DEFAULT NULL,
  `status` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rekognition_result_logs`
--

INSERT INTO `rekognition_result_logs` (`id`, `organization_id`, `candidate_id`, `source_media_id`, `target_media_id`, `rekognition_result`, `status`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 101, 2, 1, 0.00, 'Suspicious', 1, '2020-12-20 08:33:27', '2020-12-20 08:33:27', NULL),
(2, 1, 102, 5, 4, 0.00, 'Suspicious', 1, '2020-12-21 13:29:50', '2020-12-21 13:29:50', NULL),
(3, 8, 103, 7, 8, 96.87, 'Verified', 7, '2020-12-23 02:10:55', '2020-12-23 02:10:55', NULL),
(4, 8, 103, 7, 9, 0.00, 'Suspicious', 7, '2020-12-23 02:10:55', '2020-12-23 02:10:55', NULL),
(5, 8, 103, 7, 10, 0.00, 'Suspicious', 7, '2020-12-23 02:11:52', '2020-12-23 02:11:52', NULL),
(6, 8, 103, 7, 11, 0.00, 'Suspicious', 7, '2020-12-23 02:12:56', '2020-12-23 02:12:56', NULL),
(7, 8, 103, 7, 12, 98.14, 'Verified', 7, '2020-12-23 02:15:20', '2020-12-23 02:15:20', NULL),
(8, 10, 104, 15, 16, 0.00, 'Suspicious', 1, '2020-12-23 03:22:56', '2020-12-23 03:22:56', NULL),
(9, 10, 104, 15, 17, 0.00, 'Suspicious', 1, '2020-12-23 03:23:49', '2020-12-23 03:23:49', NULL),
(10, 10, 104, 15, 18, 0.00, 'Suspicious', 1, '2020-12-23 03:24:17', '2020-12-23 03:24:17', NULL),
(11, 10, 104, 15, 20, 0.00, 'Suspicious', 1, '2020-12-23 03:37:05', '2020-12-23 03:37:05', NULL),
(12, 10, 105, 22, 23, 0.00, 'Suspicious', 1, '2020-12-23 03:41:14', '2020-12-23 03:41:14', NULL),
(13, 8, 103, 7, 24, 0.00, 'Suspicious', 1, '2020-12-23 03:47:19', '2020-12-23 03:47:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_xid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `organization_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role_xid`, `organization_type`, `name`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '51561f27-6f87-4ace-bb46-0d104d465c1c', 'super', 'admin', NULL, NULL, NULL, '2020-12-20 07:42:40', '2020-12-20 07:42:40', NULL),
(2, '685c29b5-acca-4ea1-9850-7231b2ac6d05', 'company', 'admin', NULL, NULL, NULL, '2020-12-20 07:42:40', '2020-12-20 07:42:40', NULL),
(3, '6cf06d2a-0770-4269-a7ee-ff6f7a1cf0af', 'vendor', 'admin', NULL, NULL, NULL, '2020-12-20 07:42:40', '2020-12-20 07:42:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `timezones`
--

CREATE TABLE `timezones` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gmt_offset` time NOT NULL DEFAULT '00:00:00',
  `is_active` tinyint(1) NOT NULL DEFAULT 0,
  `has_day_light_saving` tinyint(1) NOT NULL DEFAULT 0,
  `day_light_offset` time DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `timezones`
--

INSERT INTO `timezones` (`id`, `name`, `gmt_offset`, `is_active`, `has_day_light_saving`, `day_light_offset`, `created_at`, `updated_at`) VALUES
(1, 'Africa/Accra', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(2, 'Africa/Abidjan', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(3, 'Africa/Addis_Ababa', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(4, 'Africa/Algiers', '01:00:00', 1, 0, '00:00:00', NULL, NULL),
(5, 'Africa/Asmara', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(6, 'Africa/Asmera', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(7, 'Africa/Bamako', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(8, 'Africa/Bangui', '01:00:00', 1, 0, '00:00:00', NULL, NULL),
(9, 'Africa/Banjul', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(10, 'Africa/Bissau', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(11, 'Africa/Blantyre', '02:00:00', 1, 0, '00:00:00', NULL, NULL),
(12, 'Africa/Brazzaville', '01:00:00', 1, 0, '00:00:00', NULL, NULL),
(13, 'Africa/Bujumbura', '02:00:00', 1, 0, '00:00:00', NULL, NULL),
(14, 'Africa/Cairo', '02:00:00', 1, 0, '00:00:00', NULL, NULL),
(15, 'Africa/Casablanca', '00:00:00', 1, 1, '01:00:00', NULL, NULL),
(16, 'Africa/Ceuta', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(17, 'Africa/Conakry', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(18, 'Africa/Dakar', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(19, 'Africa/Dar_es_Salaam', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(20, 'Africa/Djibouti', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(21, 'Africa/Douala', '01:00:00', 1, 0, '00:00:00', NULL, NULL),
(22, 'Africa/El_Aaiun', '00:00:00', 1, 1, '01:00:00', NULL, NULL),
(23, 'Africa/Freetown', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(24, 'Africa/Gaborone', '02:00:00', 1, 0, '00:00:00', NULL, NULL),
(25, 'Africa/Harare', '02:00:00', 1, 0, '00:00:00', NULL, NULL),
(26, 'Africa/Johannesburg', '02:00:00', 1, 0, '00:00:00', NULL, NULL),
(27, 'Africa/Juba', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(28, 'Africa/Kampala', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(29, 'Africa/Khartoum', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(30, 'Africa/Kigali', '02:00:00', 1, 0, '00:00:00', NULL, NULL),
(31, 'Africa/Kinshasa', '01:00:00', 1, 0, '00:00:00', NULL, NULL),
(32, 'Africa/Lagos', '01:00:00', 1, 0, '00:00:00', NULL, NULL),
(33, 'Africa/Libreville', '01:00:00', 1, 0, '00:00:00', NULL, NULL),
(34, 'Africa/Lome', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(35, 'Africa/Luanda', '01:00:00', 1, 0, '00:00:00', NULL, NULL),
(36, 'Africa/Lubumbashi', '02:00:00', 1, 0, '00:00:00', NULL, NULL),
(37, 'Africa/Lusaka', '02:00:00', 1, 0, '00:00:00', NULL, NULL),
(38, 'Africa/Malabo', '01:00:00', 1, 0, '00:00:00', NULL, NULL),
(39, 'Africa/Maputo', '02:00:00', 1, 0, '00:00:00', NULL, NULL),
(40, 'Africa/Maseru', '02:00:00', 1, 0, '00:00:00', NULL, NULL),
(41, 'Africa/Mbabane', '02:00:00', 1, 0, '00:00:00', NULL, NULL),
(42, 'Africa/Mogadishu', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(43, 'Africa/Monrovia', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(44, 'Africa/Nairobi', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(45, 'Africa/Ndjamena', '01:00:00', 1, 0, '00:00:00', NULL, NULL),
(46, 'Africa/Niamey', '01:00:00', 1, 0, '00:00:00', NULL, NULL),
(47, 'Africa/Nouakchott', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(48, 'Africa/Ouagadougou', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(49, 'Africa/Porto-Novo', '01:00:00', 1, 0, '00:00:00', NULL, NULL),
(50, 'Africa/Sao_Tome', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(51, 'Africa/Timbuktu', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(52, 'Africa/Tripoli', '02:00:00', 1, 0, '00:00:00', NULL, NULL),
(53, 'Africa/Tunis', '01:00:00', 1, 0, '00:00:00', NULL, NULL),
(54, 'Africa/Windhoek', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(55, 'America/Adak', '-10:00:00', 1, 1, '-09:00:00', NULL, NULL),
(56, 'America/Anchorage', '-09:00:00', 1, 1, '-08:00:00', NULL, NULL),
(57, 'America/Anguilla', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(58, 'America/Antigua', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(59, 'America/Araguaina', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(60, 'America/Argentina/Buenos_Aires', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(61, 'America/Argentina/Catamarca', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(62, 'America/Argentina/ComodRivadavia', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(63, 'America/Argentina/Cordoba', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(64, 'America/Argentina/Jujuy', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(65, 'America/Argentina/La_Rioja', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(66, 'America/Argentina/Mendoza', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(67, 'America/Argentina/Rio_Gallegos', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(68, 'America/Argentina/Salta', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(69, 'America/Argentina/San_Juan', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(70, 'America/Argentina/San_Luis', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(71, 'America/Argentina/Tucuman', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(72, 'America/Argentina/Ushuaia', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(73, 'America/Aruba', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(74, 'America/Asuncion', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(75, 'America/Atikokan', '-05:00:00', 1, 0, '00:00:00', NULL, NULL),
(76, 'America/Atka', '-10:00:00', 1, 1, '-09:00:00', NULL, NULL),
(77, 'America/Bahia', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(78, 'America/Bahia_Banderas', '-06:00:00', 1, 1, '-05:00:00', NULL, NULL),
(79, 'America/Barbados', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(80, 'America/Belem', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(81, 'America/Belize', '-06:00:00', 1, 0, '00:00:00', NULL, NULL),
(82, 'America/Blanc-Sablon', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(83, 'America/Boa_Vista', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(84, 'America/Bogota', '-05:00:00', 1, 0, '00:00:00', NULL, NULL),
(85, 'America/Boise', '-07:00:00', 1, 1, '-06:00:00', NULL, NULL),
(86, 'America/Buenos_Aires', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(87, 'America/Cambridge_Bay', '-07:00:00', 1, 1, '-06:00:00', NULL, NULL),
(88, 'America/Campo_Grande', '-04:00:00', 1, 1, '-03:00:00', NULL, NULL),
(89, 'America/Cancun', '-05:00:00', 1, 0, '00:00:00', NULL, NULL),
(90, 'America/Caracas', '-04:30:00', 1, 0, '00:00:00', NULL, NULL),
(91, 'America/Catamarca', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(92, 'America/Cayenne', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(93, 'America/Cayman', '-05:00:00', 1, 0, '00:00:00', NULL, NULL),
(94, 'America/Chicago', '-06:00:00', 1, 1, '-05:00:00', NULL, NULL),
(95, 'America/Chihuahua', '-07:00:00', 1, 1, '-06:00:00', NULL, NULL),
(96, 'America/Coral_Harbour', '-05:00:00', 1, 0, '00:00:00', NULL, NULL),
(97, 'America/Cordoba', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(98, 'America/Costa_Rica', '-06:00:00', 1, 0, '00:00:00', NULL, NULL),
(99, 'America/Creston', '-07:00:00', 1, 0, '00:00:00', NULL, NULL),
(100, 'America/Cuiaba', '-04:00:00', 1, 1, '-03:00:00', NULL, NULL),
(101, 'America/Curacao', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(102, 'America/Danmarkshavn', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(103, 'America/Dawson', '-08:00:00', 1, 1, '-07:00:00', NULL, NULL),
(104, 'America/Dawson_Creek', '-07:00:00', 1, 0, '00:00:00', NULL, NULL),
(105, 'America/Denver', '-07:00:00', 1, 1, '-06:00:00', NULL, NULL),
(106, 'America/Detroit', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(107, 'America/Dominica', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(108, 'America/Edmonton', '-07:00:00', 1, 1, '-06:00:00', NULL, NULL),
(109, 'America/Eirunepe', '-05:00:00', 1, 0, '00:00:00', NULL, NULL),
(110, 'America/El_Salvador', '-06:00:00', 1, 0, '00:00:00', NULL, NULL),
(111, 'America/Ensenada', '-08:00:00', 1, 1, '-07:00:00', NULL, NULL),
(112, 'America/Fort_Nelson', '-07:00:00', 1, 0, '00:00:00', NULL, NULL),
(113, 'America/Fort_Wayne', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(114, 'America/Fortaleza', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(115, 'America/Glace_Bay', '-04:00:00', 1, 1, '-03:00:00', NULL, NULL),
(116, 'America/Godthab', '-03:00:00', 1, 1, '-02:00:00', NULL, NULL),
(117, 'America/Goose_Bay', '-04:00:00', 1, 1, '-03:00:00', NULL, NULL),
(118, 'America/Grand_Turk', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(119, 'America/Grenada', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(120, 'America/Guadeloupe', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(121, 'America/Guatemala', '-06:00:00', 1, 0, '00:00:00', NULL, NULL),
(122, 'America/Guayaquil', '-05:00:00', 1, 0, '00:00:00', NULL, NULL),
(123, 'America/Guyana', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(124, 'America/Halifax', '-04:00:00', 1, 1, '-03:00:00', NULL, NULL),
(125, 'America/Havana', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(126, 'America/Hermosillo', '-07:00:00', 1, 0, '00:00:00', NULL, NULL),
(127, 'America/Indiana/Indianapolis', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(128, 'America/Indiana/Knox', '-06:00:00', 1, 1, '-05:00:00', NULL, NULL),
(129, 'America/Indiana/Marengo', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(130, 'America/Indiana/Petersburg', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(131, 'America/Indiana/Tell_City', '-06:00:00', 1, 1, '-05:00:00', NULL, NULL),
(132, 'America/Indiana/Vevay', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(133, 'America/Indiana/Vincennes', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(134, 'America/Indiana/Winamac', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(135, 'America/Indianapolis', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(136, 'America/Inuvik', '-07:00:00', 1, 1, '-06:00:00', NULL, NULL),
(137, 'America/Iqaluit', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(138, 'America/Jamaica', '-05:00:00', 1, 0, '00:00:00', NULL, NULL),
(139, 'America/Jujuy', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(140, 'America/Juneau', '-09:00:00', 1, 1, '-08:00:00', NULL, NULL),
(141, 'America/Kentucky/Louisville', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(142, 'America/Kentucky/Monticello', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(143, 'America/Knox_IN', '-06:00:00', 1, 1, '-05:00:00', NULL, NULL),
(144, 'America/Kralendijk', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(145, 'America/La_Paz', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(146, 'America/Lima', '-05:00:00', 1, 0, '00:00:00', NULL, NULL),
(147, 'America/Los_Angeles', '-08:00:00', 1, 1, '-07:00:00', NULL, NULL),
(148, 'America/Louisville', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(149, 'America/Lower_Princes', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(150, 'America/Maceio', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(151, 'America/Managua', '-06:00:00', 1, 0, '00:00:00', NULL, NULL),
(152, 'America/Manaus', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(153, 'America/Marigot', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(154, 'America/Martinique', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(155, 'America/Matamoros', '-06:00:00', 1, 1, '-05:00:00', NULL, NULL),
(156, 'America/Mazatlan', '-07:00:00', 1, 1, '-06:00:00', NULL, NULL),
(157, 'America/Mendoza', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(158, 'America/Menominee', '-06:00:00', 1, 1, '-05:00:00', NULL, NULL),
(159, 'America/Merida', '-06:00:00', 1, 1, '-05:00:00', NULL, NULL),
(160, 'America/Metlakatla', '-08:00:00', 1, 0, '00:00:00', NULL, NULL),
(161, 'America/Mexico_City', '-06:00:00', 1, 1, '-05:00:00', NULL, NULL),
(162, 'America/Miquelon', '-03:00:00', 1, 1, '-02:00:00', NULL, NULL),
(163, 'America/Moncton', '-04:00:00', 1, 1, '-03:00:00', NULL, NULL),
(164, 'America/Monterrey', '-06:00:00', 1, 1, '-05:00:00', NULL, NULL),
(165, 'America/Montevideo', '-03:00:00', 1, 1, '-02:00:00', NULL, NULL),
(166, 'America/Montreal', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(167, 'America/Montserrat', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(168, 'America/Nassau', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(169, 'America/New_York', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(170, 'America/Nipigon', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(171, 'America/Nome', '-09:00:00', 1, 1, '-08:00:00', NULL, NULL),
(172, 'America/Noronha', '-02:00:00', 1, 0, '00:00:00', NULL, NULL),
(173, 'America/North_Dakota/Beulah', '-06:00:00', 1, 1, '-05:00:00', NULL, NULL),
(174, 'America/North_Dakota/Center', '-06:00:00', 1, 1, '-05:00:00', NULL, NULL),
(175, 'America/North_Dakota/New_Salem', '-06:00:00', 1, 1, '-05:00:00', NULL, NULL),
(176, 'America/Ojinaga', '-07:00:00', 1, 1, '-06:00:00', NULL, NULL),
(177, 'America/Panama', '-05:00:00', 1, 0, '00:00:00', NULL, NULL),
(178, 'America/Pangnirtung', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(179, 'America/Paramaribo', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(180, 'America/Phoenix', '-07:00:00', 1, 0, '00:00:00', NULL, NULL),
(181, 'America/Port-au-Prince', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(182, 'America/Port_of_Spain', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(183, 'America/Porto_Acre', '-05:00:00', 1, 0, '00:00:00', NULL, NULL),
(184, 'America/Porto_Velho', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(185, 'America/Puerto_Rico', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(186, 'America/Rainy_River', '-06:00:00', 1, 1, '-05:00:00', NULL, NULL),
(187, 'America/Rankin_Inlet', '-06:00:00', 1, 1, '-05:00:00', NULL, NULL),
(188, 'America/Recife', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(189, 'America/Regina', '-06:00:00', 1, 0, '00:00:00', NULL, NULL),
(190, 'America/Resolute', '-06:00:00', 1, 1, '-05:00:00', NULL, NULL),
(191, 'America/Rio_Branco', '-05:00:00', 1, 0, '00:00:00', NULL, NULL),
(192, 'America/Rosario', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(193, 'America/Santa_Isabel', '-08:00:00', 1, 1, '-07:00:00', NULL, NULL),
(194, 'America/Santarem', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(195, 'America/Santiago', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(196, 'America/Santo_Domingo', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(197, 'America/Sao_Paulo', '-03:00:00', 1, 1, '-02:00:00', NULL, NULL),
(198, 'America/Scoresbysund', '-01:00:00', 1, 1, '00:00:00', NULL, NULL),
(199, 'America/Shiprock', '-07:00:00', 1, 1, '-06:00:00', NULL, NULL),
(200, 'America/Sitka', '-09:00:00', 1, 1, '-08:00:00', NULL, NULL),
(201, 'America/St_Barthelemy', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(202, 'America/St_Johns', '-03:30:00', 1, 1, '-02:30:00', NULL, NULL),
(203, 'America/St_Kitts', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(204, 'America/St_Lucia', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(205, 'America/St_Thomas', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(206, 'America/St_Vincent', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(207, 'America/Swift_Current', '-06:00:00', 1, 0, '00:00:00', NULL, NULL),
(208, 'America/Tegucigalpa', '-06:00:00', 1, 0, '00:00:00', NULL, NULL),
(209, 'America/Thule', '-04:00:00', 1, 1, '-03:00:00', NULL, NULL),
(210, 'America/Thunder_Bay', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(211, 'America/Tijuana', '-08:00:00', 1, 1, '-07:00:00', NULL, NULL),
(212, 'America/Toronto', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(213, 'America/Tortola', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(214, 'America/Vancouver', '-08:00:00', 1, 1, '-07:00:00', NULL, NULL),
(215, 'America/Virgin', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(216, 'America/Whitehorse', '-08:00:00', 1, 1, '-07:00:00', NULL, NULL),
(217, 'America/Winnipeg', '-06:00:00', 1, 1, '-05:00:00', NULL, NULL),
(218, 'America/Yakutat', '-09:00:00', 1, 1, '-08:00:00', NULL, NULL),
(219, 'America/Yellowknife', '-07:00:00', 1, 1, '-06:00:00', NULL, NULL),
(220, 'Antarctica/Casey', '08:00:00', 1, 0, '00:00:00', NULL, NULL),
(221, 'Antarctica/Davis', '07:00:00', 1, 0, '00:00:00', NULL, NULL),
(222, 'Antarctica/DumontDUrville', '10:00:00', 1, 0, '00:00:00', NULL, NULL),
(223, 'Antarctica/Macquarie', '11:00:00', 1, 0, '00:00:00', NULL, NULL),
(224, 'Antarctica/Mawson', '05:00:00', 1, 0, '00:00:00', NULL, NULL),
(225, 'Antarctica/McMurdo', '12:00:00', 1, 1, '13:00:00', NULL, NULL),
(226, 'Antarctica/Palmer', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(227, 'Antarctica/Rothera', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(228, 'Antarctica/South_Pole', '12:00:00', 1, 1, '13:00:00', NULL, NULL),
(229, 'Antarctica/Syowa', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(230, 'Antarctica/Troll', '00:00:00', 1, 1, '02:00:00', NULL, NULL),
(231, 'Antarctica/Vostok', '06:00:00', 1, 0, '00:00:00', NULL, NULL),
(232, 'Arctic/Longyearbyen', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(233, 'Asia/Aden', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(234, 'Asia/Almaty', '06:00:00', 1, 0, '00:00:00', NULL, NULL),
(235, 'Asia/Amman', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(236, 'Asia/Anadyr', '12:00:00', 1, 0, '00:00:00', NULL, NULL),
(237, 'Asia/Aqtau', '05:00:00', 1, 0, '00:00:00', NULL, NULL),
(238, 'Asia/Aqtobe', '05:00:00', 1, 0, '00:00:00', NULL, NULL),
(239, 'Asia/Ashgabat', '05:00:00', 1, 0, '00:00:00', NULL, NULL),
(240, 'Asia/Ashkhabad', '05:00:00', 1, 0, '00:00:00', NULL, NULL),
(241, 'Asia/Baghdad', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(242, 'Asia/Bahrain', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(243, 'Asia/Baku', '04:00:00', 1, 1, '05:00:00', NULL, NULL),
(244, 'Asia/Bangkok', '07:00:00', 1, 0, '00:00:00', NULL, NULL),
(245, 'Asia/Beirut', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(246, 'Asia/Bishkek', '06:00:00', 1, 0, '00:00:00', NULL, NULL),
(247, 'Asia/Brunei', '08:00:00', 1, 0, '00:00:00', NULL, NULL),
(248, 'Asia/Calcutta', '05:30:00', 1, 0, '00:00:00', NULL, NULL),
(249, 'Asia/Chita', '08:00:00', 1, 0, '00:00:00', NULL, NULL),
(250, 'Asia/Choibalsan', '08:00:00', 1, 1, '09:00:00', NULL, NULL),
(251, 'Asia/Chongqing', '08:00:00', 1, 0, '00:00:00', NULL, NULL),
(252, 'Asia/Chungking', '08:00:00', 1, 0, '00:00:00', NULL, NULL),
(253, 'Asia/Colombo', '05:30:00', 1, 0, '00:00:00', NULL, NULL),
(254, 'Asia/Dacca', '06:00:00', 1, 0, '00:00:00', NULL, NULL),
(255, 'Asia/Damascus', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(256, 'Asia/Dhaka', '06:00:00', 1, 0, '00:00:00', NULL, NULL),
(257, 'Asia/Dili', '09:00:00', 1, 0, '00:00:00', NULL, NULL),
(258, 'Asia/Dubai', '04:00:00', 1, 0, '00:00:00', NULL, NULL),
(259, 'Asia/Dushanbe', '05:00:00', 1, 0, '00:00:00', NULL, NULL),
(260, 'Asia/Gaza', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(261, 'Asia/Harbin', '08:00:00', 1, 0, '00:00:00', NULL, NULL),
(262, 'Asia/Hebron', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(263, 'Asia/Ho_Chi_Minh', '07:00:00', 1, 0, '00:00:00', NULL, NULL),
(264, 'Asia/Hong_Kong', '08:00:00', 1, 0, '00:00:00', NULL, NULL),
(265, 'Asia/Hovd', '07:00:00', 1, 1, '08:00:00', NULL, NULL),
(266, 'Asia/Irkutsk', '08:00:00', 1, 0, '00:00:00', NULL, NULL),
(267, 'Asia/Istanbul', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(268, 'Asia/Jakarta', '07:00:00', 1, 0, '00:00:00', NULL, NULL),
(269, 'Asia/Jayapura', '09:00:00', 1, 0, '00:00:00', NULL, NULL),
(270, 'Asia/Jerusalem', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(271, 'Asia/Kabul', '04:30:00', 1, 0, '00:00:00', NULL, NULL),
(272, 'Asia/Kamchatka', '12:00:00', 1, 0, '00:00:00', NULL, NULL),
(273, 'Asia/Karachi', '05:00:00', 1, 0, '00:00:00', NULL, NULL),
(274, 'Asia/Kashgar', '06:00:00', 1, 0, '00:00:00', NULL, NULL),
(275, 'Asia/Kathmandu', '05:45:00', 1, 0, '00:00:00', NULL, NULL),
(276, 'Asia/Katmandu', '05:45:00', 1, 0, '00:00:00', NULL, NULL),
(277, 'Asia/Khandyga', '09:00:00', 1, 0, '00:00:00', NULL, NULL),
(278, 'Asia/Kolkata', '05:30:00', 1, 0, '00:00:00', NULL, NULL),
(279, 'Asia/Krasnoyarsk', '07:00:00', 1, 0, '00:00:00', NULL, NULL),
(280, 'Asia/Kuala_Lumpur', '08:00:00', 1, 0, '00:00:00', NULL, NULL),
(281, 'Asia/Kuching', '08:00:00', 1, 0, '00:00:00', NULL, NULL),
(282, 'Asia/Kuwait', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(283, 'Asia/Macao', '08:00:00', 1, 0, '00:00:00', NULL, NULL),
(284, 'Asia/Macau', '08:00:00', 1, 0, '00:00:00', NULL, NULL),
(285, 'Asia/Magadan', '10:00:00', 1, 0, '00:00:00', NULL, NULL),
(286, 'Asia/Makassar', '08:00:00', 1, 0, '00:00:00', NULL, NULL),
(287, 'Asia/Manila', '08:00:00', 1, 0, '00:00:00', NULL, NULL),
(288, 'Asia/Muscat', '04:00:00', 1, 0, '00:00:00', NULL, NULL),
(289, 'Asia/Nicosia', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(290, 'Asia/Novokuznetsk', '07:00:00', 1, 0, '00:00:00', NULL, NULL),
(291, 'Asia/Novosibirsk', '06:00:00', 1, 0, '00:00:00', NULL, NULL),
(292, 'Asia/Omsk', '06:00:00', 1, 0, '00:00:00', NULL, NULL),
(293, 'Asia/Oral', '05:00:00', 1, 0, '00:00:00', NULL, NULL),
(294, 'Asia/Phnom_Penh', '07:00:00', 1, 0, '00:00:00', NULL, NULL),
(295, 'Asia/Pontianak', '07:00:00', 1, 0, '00:00:00', NULL, NULL),
(296, 'Asia/Pyongyang', '08:30:00', 1, 0, '00:00:00', NULL, NULL),
(297, 'Asia/Qatar', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(298, 'Asia/Qyzylorda', '06:00:00', 1, 0, '00:00:00', NULL, NULL),
(299, 'Asia/Rangoon', '06:30:00', 1, 0, '00:00:00', NULL, NULL),
(300, 'Asia/Riyadh', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(301, 'Asia/Riyadh87', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(302, 'Asia/Riyadh88', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(303, 'Asia/Riyadh89', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(304, 'Asia/Saigon', '07:00:00', 1, 0, '00:00:00', NULL, NULL),
(305, 'Asia/Sakhalin', '10:00:00', 1, 0, '00:00:00', NULL, NULL),
(306, 'Asia/Samarkand', '05:00:00', 1, 0, '00:00:00', NULL, NULL),
(307, 'Asia/Seoul', '09:00:00', 1, 0, '00:00:00', NULL, NULL),
(308, 'Asia/Shanghai', '08:00:00', 1, 0, '00:00:00', NULL, NULL),
(309, 'Asia/Singapore', '08:00:00', 1, 0, '00:00:00', NULL, NULL),
(310, 'Asia/Srednekolymsk', '11:00:00', 1, 0, '00:00:00', NULL, NULL),
(311, 'Asia/Taipei', '08:00:00', 1, 0, '00:00:00', NULL, NULL),
(312, 'Asia/Tashkent', '05:00:00', 1, 0, '00:00:00', NULL, NULL),
(313, 'Asia/Tbilisi', '04:00:00', 1, 0, '00:00:00', NULL, NULL),
(314, 'Asia/Tehran', '03:30:00', 1, 1, '04:30:00', NULL, NULL),
(315, 'Asia/Tel_Aviv', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(316, 'Asia/Thimbu', '06:00:00', 1, 0, '00:00:00', NULL, NULL),
(317, 'Asia/Thimphu', '06:00:00', 1, 0, '00:00:00', NULL, NULL),
(318, 'Asia/Tokyo', '09:00:00', 1, 0, '00:00:00', NULL, NULL),
(319, 'Asia/Ujung_Pandang', '08:00:00', 1, 0, '00:00:00', NULL, NULL),
(320, 'Asia/Ulaanbaatar', '08:00:00', 1, 1, '09:00:00', NULL, NULL),
(321, 'Asia/Ulan_Bator', '08:00:00', 1, 1, '09:00:00', NULL, NULL),
(322, 'Asia/Urumqi', '06:00:00', 1, 0, '00:00:00', NULL, NULL),
(323, 'Asia/Vientiane', '07:00:00', 1, 0, '00:00:00', NULL, NULL),
(324, 'Asia/Vladivostok', '10:00:00', 1, 0, '00:00:00', NULL, NULL),
(325, 'Asia/Yakutsk', '09:00:00', 1, 0, '00:00:00', NULL, NULL),
(326, 'Asia/Yekaterinburg', '05:00:00', 1, 0, '00:00:00', NULL, NULL),
(327, 'Asia/Yerevan', '04:00:00', 1, 0, '00:00:00', NULL, NULL),
(328, 'Atlantic/Azores', '-01:00:00', 1, 1, '00:00:00', NULL, NULL),
(329, 'Atlantic/Bermuda', '-04:00:00', 1, 1, '-03:00:00', NULL, NULL),
(330, 'Atlantic/Canary', '00:00:00', 1, 1, '01:00:00', NULL, NULL),
(331, 'Atlantic/Cape_Verde', '-01:00:00', 1, 0, '00:00:00', NULL, NULL),
(332, 'Atlantic/Faeroe', '00:00:00', 1, 1, '01:00:00', NULL, NULL),
(333, 'Atlantic/Faroe', '00:00:00', 1, 1, '01:00:00', NULL, NULL),
(334, 'Atlantic/Jan_Mayen', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(335, 'Atlantic/Madeira', '00:00:00', 1, 1, '01:00:00', NULL, NULL),
(336, 'Atlantic/Reykjavik', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(337, 'Atlantic/South_Georgia', '-02:00:00', 1, 0, '00:00:00', NULL, NULL),
(338, 'Atlantic/St_Helena', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(339, 'Atlantic/Stanley', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(340, 'Australia/ACT', '10:00:00', 1, 1, '11:00:00', NULL, NULL),
(341, 'Australia/Adelaide', '09:30:00', 1, 1, '10:30:00', NULL, NULL),
(342, 'Australia/Brisbane', '10:00:00', 1, 0, '00:00:00', NULL, NULL),
(343, 'Australia/Broken_Hill', '09:30:00', 1, 1, '10:30:00', NULL, NULL),
(344, 'Australia/Canberra', '10:00:00', 1, 1, '11:00:00', NULL, NULL),
(345, 'Australia/Currie', '10:00:00', 1, 1, '11:00:00', NULL, NULL),
(346, 'Australia/Darwin', '09:30:00', 1, 0, '00:00:00', NULL, NULL),
(347, 'Australia/Eucla', '08:45:00', 1, 0, '00:00:00', NULL, NULL),
(348, 'Australia/Hobart', '10:00:00', 1, 1, '11:00:00', NULL, NULL),
(349, 'Australia/LHI', '10:30:00', 1, 1, '11:00:00', NULL, NULL),
(350, 'Australia/Lindeman', '10:00:00', 1, 0, '00:00:00', NULL, NULL),
(351, 'Australia/Lord_Howe', '10:30:00', 1, 1, '11:00:00', NULL, NULL),
(352, 'Australia/Melbourne', '10:00:00', 1, 1, '11:00:00', NULL, NULL),
(353, 'Australia/NSW', '10:00:00', 1, 1, '11:00:00', NULL, NULL),
(354, 'Australia/North', '09:30:00', 1, 0, '00:00:00', NULL, NULL),
(355, 'Australia/Perth', '08:00:00', 1, 0, '00:00:00', NULL, NULL),
(356, 'Australia/Queensland', '10:00:00', 1, 0, '00:00:00', NULL, NULL),
(357, 'Australia/South', '09:30:00', 1, 1, '10:30:00', NULL, NULL),
(358, 'Australia/Sydney', '10:00:00', 1, 1, '11:00:00', NULL, NULL),
(359, 'Australia/Tasmania', '10:00:00', 1, 1, '11:00:00', NULL, NULL),
(360, 'Australia/Victoria', '10:00:00', 1, 1, '11:00:00', NULL, NULL),
(361, 'Australia/West', '08:00:00', 1, 0, '00:00:00', NULL, NULL),
(362, 'Australia/Yancowinna', '09:30:00', 1, 1, '10:30:00', NULL, NULL),
(363, 'Brazil/Acre', '-05:00:00', 1, 0, '00:00:00', NULL, NULL),
(364, 'Brazil/DeNoronha', '-02:00:00', 1, 0, '00:00:00', NULL, NULL),
(365, 'Brazil/East', '-03:00:00', 1, 1, '-02:00:00', NULL, NULL),
(366, 'Brazil/West', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(367, 'CET', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(368, 'CST6CDT', '-06:00:00', 1, 1, '-05:00:00', NULL, NULL),
(369, 'Canada/Atlantic', '-04:00:00', 1, 1, '-03:00:00', NULL, NULL),
(370, 'Canada/Central', '-06:00:00', 1, 1, '-05:00:00', NULL, NULL),
(371, 'Canada/East-Saskatchewan', '-06:00:00', 1, 0, '00:00:00', NULL, NULL),
(372, 'Canada/Eastern', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(373, 'Canada/Mountain', '-07:00:00', 1, 1, '-06:00:00', NULL, NULL),
(374, 'Canada/Newfoundland', '-03:30:00', 1, 1, '-02:30:00', NULL, NULL),
(375, 'Canada/Pacific', '-08:00:00', 1, 1, '-07:00:00', NULL, NULL),
(376, 'Canada/Saskatchewan', '-06:00:00', 1, 0, '00:00:00', NULL, NULL),
(377, 'Canada/Yukon', '-08:00:00', 1, 1, '-07:00:00', NULL, NULL),
(378, 'Chile/Continental', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(379, 'Chile/EasterIsland', '-05:00:00', 1, 0, '00:00:00', NULL, NULL),
(380, 'Cuba', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(381, 'EET', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(382, 'EST', '-05:00:00', 1, 0, '00:00:00', NULL, NULL),
(383, 'EST5EDT', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(384, 'Egypt', '02:00:00', 1, 0, '00:00:00', NULL, NULL),
(385, 'Eire', '00:00:00', 1, 1, '01:00:00', NULL, NULL),
(386, 'Etc/GMT', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(387, 'Etc/GMT+0', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(388, 'Etc/GMT+1', '-01:00:00', 1, 0, '00:00:00', NULL, NULL),
(389, 'Etc/GMT+10', '-10:00:00', 1, 0, '00:00:00', NULL, NULL),
(390, 'Etc/GMT+11', '-11:00:00', 1, 0, '00:00:00', NULL, NULL),
(391, 'Etc/GMT+12', '-12:00:00', 1, 0, '00:00:00', NULL, NULL),
(392, 'Etc/GMT+2', '-02:00:00', 1, 0, '00:00:00', NULL, NULL),
(393, 'Etc/GMT+3', '-03:00:00', 1, 0, '00:00:00', NULL, NULL),
(394, 'Etc/GMT+4', '-04:00:00', 1, 0, '00:00:00', NULL, NULL),
(395, 'Etc/GMT+5', '-05:00:00', 1, 0, '00:00:00', NULL, NULL),
(396, 'Etc/GMT+6', '-06:00:00', 1, 0, '00:00:00', NULL, NULL),
(397, 'Etc/GMT+7', '-07:00:00', 1, 0, '00:00:00', NULL, NULL),
(398, 'Etc/GMT+8', '-08:00:00', 1, 0, '00:00:00', NULL, NULL),
(399, 'Etc/GMT+9', '-09:00:00', 1, 0, '00:00:00', NULL, NULL),
(400, 'Etc/GMT-0', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(401, 'Etc/GMT-1', '01:00:00', 1, 0, '00:00:00', NULL, NULL),
(402, 'Etc/GMT-10', '10:00:00', 1, 0, '00:00:00', NULL, NULL),
(403, 'Etc/GMT-11', '11:00:00', 1, 0, '00:00:00', NULL, NULL),
(404, 'Etc/GMT-12', '12:00:00', 1, 0, '00:00:00', NULL, NULL),
(405, 'Etc/GMT-13', '13:00:00', 1, 0, '00:00:00', NULL, NULL),
(406, 'Etc/GMT-14', '14:00:00', 1, 0, '00:00:00', NULL, NULL),
(407, 'Etc/GMT-2', '02:00:00', 1, 0, '00:00:00', NULL, NULL),
(408, 'Etc/GMT-3', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(409, 'Etc/GMT-4', '04:00:00', 1, 0, '00:00:00', NULL, NULL),
(410, 'Etc/GMT-5', '05:00:00', 1, 0, '00:00:00', NULL, NULL),
(411, 'Etc/GMT-6', '06:00:00', 1, 0, '00:00:00', NULL, NULL),
(412, 'Etc/GMT-7', '07:00:00', 1, 0, '00:00:00', NULL, NULL),
(413, 'Etc/GMT-8', '08:00:00', 1, 0, '00:00:00', NULL, NULL),
(414, 'Etc/GMT-9', '09:00:00', 1, 0, '00:00:00', NULL, NULL),
(415, 'Etc/GMT0', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(416, 'Etc/Greenwich', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(417, 'Etc/UCT', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(418, 'Etc/UTC', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(419, 'Etc/Universal', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(420, 'Etc/Zulu', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(421, 'Europe/Amsterdam', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(422, 'Europe/Andorra', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(423, 'Europe/Athens', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(424, 'Europe/Belfast', '00:00:00', 1, 1, '01:00:00', NULL, NULL),
(425, 'Europe/Belgrade', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(426, 'Europe/Berlin', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(427, 'Europe/Bratislava', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(428, 'Europe/Brussels', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(429, 'Europe/Bucharest', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(430, 'Europe/Budapest', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(431, 'Europe/Busingen', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(432, 'Europe/Chisinau', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(433, 'Europe/Copenhagen', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(434, 'Europe/Dublin', '00:00:00', 1, 1, '01:00:00', NULL, NULL),
(435, 'Europe/Gibraltar', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(436, 'Europe/Guernsey', '00:00:00', 1, 1, '01:00:00', NULL, NULL),
(437, 'Europe/Helsinki', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(438, 'Europe/Isle_of_Man', '00:00:00', 1, 1, '01:00:00', NULL, NULL),
(439, 'Europe/Istanbul', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(440, 'Europe/Jersey', '00:00:00', 1, 1, '01:00:00', NULL, NULL),
(441, 'Europe/Kaliningrad', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(442, 'Europe/Kiev', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(443, 'Europe/Lisbon', '00:00:00', 1, 1, '01:00:00', NULL, NULL),
(444, 'Europe/Ljubljana', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(445, 'Europe/London', '00:00:00', 1, 1, '01:00:00', NULL, NULL),
(446, 'Europe/Luxembourg', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(447, 'Europe/Madrid', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(448, 'Europe/Malta', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(449, 'Europe/Mariehamn', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(450, 'Europe/Minsk', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(451, 'Europe/Monaco', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(452, 'Europe/Moscow', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(453, 'Europe/Nicosia', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(454, 'Europe/Oslo', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(455, 'Europe/Paris', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(456, 'Europe/Podgorica', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(457, 'Europe/Prague', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(458, 'Europe/Riga', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(459, 'Europe/Rome', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(460, 'Europe/Samara', '04:00:00', 1, 0, '00:00:00', NULL, NULL),
(461, 'Europe/San_Marino', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(462, 'Europe/Sarajevo', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(463, 'Europe/Simferopol', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(464, 'Europe/Skopje', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(465, 'Europe/Sofia', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(466, 'Europe/Stockholm', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(467, 'Europe/Tallinn', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(468, 'Europe/Tirane', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(469, 'Europe/Tiraspol', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(470, 'Europe/Uzhgorod', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(471, 'Europe/Vaduz', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(472, 'Europe/Vatican', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(473, 'Europe/Vienna', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(474, 'Europe/Vilnius', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(475, 'Europe/Volgograd', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(476, 'Europe/Warsaw', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(477, 'Europe/Zagreb', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(478, 'Europe/Zaporozhye', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(479, 'Europe/Zurich', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(480, 'Factory', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(481, 'GB', '00:00:00', 1, 1, '01:00:00', NULL, NULL),
(482, 'GB-Eire', '00:00:00', 1, 1, '01:00:00', NULL, NULL),
(483, 'GMT', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(484, 'GMT+0', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(485, 'GMT-0', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(486, 'GMT0', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(487, 'Greenwich', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(488, 'HST', '-10:00:00', 1, 0, '00:00:00', NULL, NULL),
(489, 'Hongkong', '08:00:00', 1, 0, '00:00:00', NULL, NULL),
(490, 'Iceland', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(491, 'Indian/Antananarivo', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(492, 'Indian/Chagos', '06:00:00', 1, 0, '00:00:00', NULL, NULL),
(493, 'Indian/Christmas', '07:00:00', 1, 0, '00:00:00', NULL, NULL),
(494, 'Indian/Cocos', '06:30:00', 1, 0, '00:00:00', NULL, NULL),
(495, 'Indian/Comoro', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(496, 'Indian/Kerguelen', '05:00:00', 1, 0, '00:00:00', NULL, NULL),
(497, 'Indian/Mahe', '04:00:00', 1, 0, '00:00:00', NULL, NULL),
(498, 'Indian/Maldives', '05:00:00', 1, 0, '00:00:00', NULL, NULL),
(499, 'Indian/Mauritius', '04:00:00', 1, 0, '00:00:00', NULL, NULL),
(500, 'Indian/Mayotte', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(501, 'Indian/Reunion', '04:00:00', 1, 0, '00:00:00', NULL, NULL),
(502, 'Iran', '03:30:00', 1, 1, '04:30:00', NULL, NULL),
(503, 'Israel', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(504, 'Jamaica', '-05:00:00', 1, 0, '00:00:00', NULL, NULL),
(505, 'Japan', '09:00:00', 1, 0, '00:00:00', NULL, NULL),
(506, 'Kwajalein', '12:00:00', 1, 0, '00:00:00', NULL, NULL),
(507, 'Libya', '02:00:00', 1, 0, '00:00:00', NULL, NULL),
(508, 'MET', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(509, 'MST', '-07:00:00', 1, 0, '00:00:00', NULL, NULL),
(510, 'MST7MDT', '-07:00:00', 1, 1, '-06:00:00', NULL, NULL),
(511, 'Mexico/BajaNorte', '-08:00:00', 1, 1, '-07:00:00', NULL, NULL),
(512, 'Mexico/BajaSur', '-07:00:00', 1, 1, '-06:00:00', NULL, NULL),
(513, 'Mexico/General', '-06:00:00', 1, 1, '-05:00:00', NULL, NULL),
(514, 'Mideast/Riyadh87', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(515, 'Mideast/Riyadh88', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(516, 'Mideast/Riyadh89', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(517, 'NZ', '12:00:00', 1, 1, '13:00:00', NULL, NULL),
(518, 'NZ-CHAT', '12:45:00', 1, 1, '13:45:00', NULL, NULL),
(519, 'Navajo', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(520, 'PRC', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(521, 'PST8PDT', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(522, 'Pacific/Apia', '13:00:00', 1, 1, '14:00:00', NULL, NULL),
(523, 'Pacific/Auckland', '12:00:00', 1, 1, '13:00:00', NULL, NULL),
(524, 'Pacific/Bougainville', '11:00:00', 1, 0, '00:00:00', NULL, NULL),
(525, 'Pacific/Chatham', '12:45:00', 1, 1, '13:45:00', NULL, NULL),
(526, 'Pacific/Chuuk', '10:00:00', 1, 0, '00:00:00', NULL, NULL),
(527, 'Pacific/Easter', '-05:00:00', 1, 0, '00:00:00', NULL, NULL),
(528, 'Pacific/Efate', '11:00:00', 1, 0, '00:00:00', NULL, NULL),
(529, 'Pacific/Enderbury', '13:00:00', 1, 0, '00:00:00', NULL, NULL),
(530, 'Pacific/Fakaofo', '13:00:00', 1, 0, '00:00:00', NULL, NULL),
(531, 'Pacific/Fiji', '12:00:00', 1, 1, '13:00:00', NULL, NULL),
(532, 'Pacific/Funafuti', '12:00:00', 1, 0, '00:00:00', NULL, NULL),
(533, 'Pacific/Galapagos', '-06:00:00', 1, 0, '00:00:00', NULL, NULL),
(534, 'Pacific/Gambier', '-09:00:00', 1, 0, '00:00:00', NULL, NULL),
(535, 'Pacific/Guadalcanal', '11:00:00', 1, 0, '00:00:00', NULL, NULL),
(536, 'Pacific/Guam', '10:00:00', 1, 0, '00:00:00', NULL, NULL),
(537, 'Pacific/Honolulu', '-10:00:00', 1, 0, '00:00:00', NULL, NULL),
(538, 'Pacific/Johnston', '-10:00:00', 1, 0, '00:00:00', NULL, NULL),
(539, 'Pacific/Kiritimati', '14:00:00', 1, 0, '00:00:00', NULL, NULL),
(540, 'Pacific/Kosrae', '11:00:00', 1, 0, '00:00:00', NULL, NULL),
(541, 'Pacific/Kwajalein', '12:00:00', 1, 0, '00:00:00', NULL, NULL),
(542, 'Pacific/Majuro', '12:00:00', 1, 0, '00:00:00', NULL, NULL),
(543, 'Pacific/Marquesas', '-09:30:00', 1, 0, '00:00:00', NULL, NULL),
(544, 'Pacific/Midway', '-11:00:00', 1, 0, '00:00:00', NULL, NULL),
(545, 'Pacific/Nauru', '12:00:00', 1, 0, '00:00:00', NULL, NULL),
(546, 'Pacific/Niue', '-11:00:00', 1, 0, '00:00:00', NULL, NULL),
(547, 'Pacific/Norfolk', '11:00:00', 1, 0, '00:00:00', NULL, NULL),
(548, 'Pacific/Noumea', '11:00:00', 1, 0, '00:00:00', NULL, NULL),
(549, 'Pacific/Pago_Pago', '-11:00:00', 1, 0, '00:00:00', NULL, NULL),
(550, 'Pacific/Palau', '09:00:00', 1, 0, '00:00:00', NULL, NULL),
(551, 'Pacific/Pitcairn', '-08:00:00', 1, 0, '00:00:00', NULL, NULL),
(552, 'Pacific/Ponape', '11:00:00', 1, 0, '00:00:00', NULL, NULL),
(553, 'Pacific/Port_Moresby', '10:00:00', 1, 0, '00:00:00', NULL, NULL),
(554, 'Pacific/Rarotonga', '-10:00:00', 1, 0, '00:00:00', NULL, NULL),
(555, 'Pacific/Saipan', '10:00:00', 1, 0, '00:00:00', NULL, NULL),
(556, 'Pacific/Samoa', '-11:00:00', 1, 0, '00:00:00', NULL, NULL),
(557, 'Pacific/Tahiti', '-10:00:00', 1, 0, '00:00:00', NULL, NULL),
(558, 'Pacific/Tarawa', '12:00:00', 1, 0, '00:00:00', NULL, NULL),
(559, 'Pacific/Tongatapu', '13:00:00', 1, 0, '00:00:00', NULL, NULL),
(560, 'Pacific/Truk', '10:00:00', 1, 0, '00:00:00', NULL, NULL),
(561, 'Pacific/Wake', '12:00:00', 1, 0, '00:00:00', NULL, NULL),
(562, 'Pacific/Wallis', '12:00:00', 1, 0, '00:00:00', NULL, NULL),
(563, 'Pacific/Yap', '10:00:00', 1, 0, '00:00:00', NULL, NULL),
(564, 'Poland', '01:00:00', 1, 1, '02:00:00', NULL, NULL),
(565, 'Portugal', '00:00:00', 1, 1, '01:00:00', NULL, NULL),
(566, 'ROC', '08:00:00', 1, 0, '00:00:00', NULL, NULL),
(567, 'ROK', '09:00:00', 1, 0, '00:00:00', NULL, NULL),
(568, 'Singapore', '08:00:00', 1, 0, '00:00:00', NULL, NULL),
(569, 'Turkey', '02:00:00', 1, 1, '03:00:00', NULL, NULL),
(570, 'UCT', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(571, 'US/Alaska', '-09:00:00', 1, 1, '-08:00:00', NULL, NULL),
(572, 'US/Aleutian', '-10:00:00', 1, 1, '-09:00:00', NULL, NULL),
(573, 'US/Arizona', '-07:00:00', 1, 0, '00:00:00', NULL, NULL),
(574, 'US/Central', '-06:00:00', 1, 1, '-05:00:00', NULL, NULL),
(575, 'US/East-Indiana', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(576, 'US/Eastern', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(577, 'US/Hawaii', '-10:00:00', 1, 0, '00:00:00', NULL, NULL),
(578, 'US/Indiana-Starke', '-06:00:00', 1, 1, '-05:00:00', NULL, NULL),
(579, 'US/Michigan', '-05:00:00', 1, 1, '-04:00:00', NULL, NULL),
(580, 'US/Mountain', '-07:00:00', 1, 1, '-06:00:00', NULL, NULL),
(581, 'US/Pacific', '-08:00:00', 1, 1, '-07:00:00', NULL, NULL),
(582, 'US/Pacific-New', '-08:00:00', 1, 1, '-07:00:00', NULL, NULL),
(583, 'US/Samoa', '-11:00:00', 1, 0, '00:00:00', NULL, NULL),
(584, 'UTC', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(585, 'Universal', '00:00:00', 1, 0, '00:00:00', NULL, NULL),
(586, 'W-SU', '03:00:00', 1, 0, '00:00:00', NULL, NULL),
(587, 'WET', '00:00:00', 1, 1, '01:00:00', NULL, NULL),
(588, 'Zulu', '00:00:00', 1, 0, '00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_xid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timezone_id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 0,
  `is_superadmin` tinyint(1) NOT NULL DEFAULT 0,
  `first_login` tinyint(1) NOT NULL DEFAULT 1,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_xid`, `first_name`, `last_name`, `email`, `password`, `phone_number`, `timezone_id`, `is_active`, `is_superadmin`, `first_login`, `email_verified_at`, `created_by`, `updated_by`, `deleted_by`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '04bd67f4-75bf-43bd-896b-b01361b6cc15', 'Admin', 'Test', 'test@gmail.com', '$2y$10$UBawyK.i44/4pk2MbVBQo..kCrxxG1wjB3BXYrGfCLkWC70fc6nB6', '1234567890', 248, 1, 1, 0, NULL, NULL, '1', NULL, NULL, NULL, '2020-12-20 07:42:40', '2020-12-20 08:23:13'),
(2, '96d9f0cb-24a5-42bb-ab82-0d72d5539dac', 'Frederik', 'Dach', 'preynolds@gmail.com', '$2y$10$yBlbVanSYXE2zAh46qG4DOfifKRncki8QJxAA.HdIDzByrysQxIda', '286.525.5515 x4087', 248, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-20 07:42:40', '2020-12-20 07:42:40'),
(3, '02528250-cd2c-42f5-ae52-b0a5078e9f26', 'Kristina', 'Thiel', 'madisen55@yahoo.com', '$2y$10$FzPbrnLtReA9.jrYrCcK0.e6BuNfwH/xxjOwLp2de/DDwolFhD.qq', '(360) 601-6735 x360', 248, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41'),
(4, '17496e69-1cf3-4103-8161-7f34d5019755', 'Maegan', 'Blick', 'green.larson@douglas.com', '$2y$10$YGjucQiduwKbz2ptT2j7Mus4mXs5ZyAJBjU37exKdMJZcnTTKGF4q', '475-428-1529', 248, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41'),
(5, '6441c196-00ad-468e-bc6b-a4ee01003861', 'Kayley', 'Cartwright', 'tdaniel@hotmail.com', '$2y$10$BpJXnWS0bQ69qDIjNTxlxOpwb48/cw6VeYCZ8awwgd4N.a4Cbv..a', '+1-962-241-0749', 248, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41'),
(6, '1e5b1cb4-4b1d-421e-be55-7186b25b380a', 'Murl', 'Reynolds', 'adell.conroy@parisian.net', '$2y$10$O3h5CaelRBtcSgri.Q2P2OegUKB3TVI3KY7Qtnglh0aLcamnXkxe6', '584.634.6030', 248, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41'),
(7, '414c58ea-c4fe-4879-b697-2eb6fd5572a5', 'Ajit', 'Samant', 'nits@clyfe.co', '$2y$10$MWFdsHjFi51I9EbLFLCfwusOIuuJ1f0z5OeZTyv3wohE8wUsQ2mkS', '9136250098', 248, 1, 0, 0, '2020-12-23 01:47:56', '1', '7', NULL, NULL, NULL, '2020-12-23 01:47:56', '2020-12-23 02:32:03'),
(8, '47edc5fa-d2e5-4357-b424-0e46bc4f6da3', 'Sonia', 'Mathur', 'test1@test.com', '$2y$10$y/xl5N6DBl5RZLLlkTdPpO71AcGfpLdoYrjLSvPnudVzC3DeVSCLC', '1111111100', 248, 1, 0, 1, '2020-12-23 02:42:31', '1', '1', NULL, NULL, NULL, '2020-12-23 02:42:31', '2020-12-23 02:42:31'),
(9, '2a8e9ce8-326e-4133-b6da-5c84f7181ecd', 'Albert', 'Pinto', 'albert@test.com', '$2y$10$harbl32qaYBTT.Za8gy7jus/Wgw0sq5jrKk6U9RXeZc/NZDTq388S', '1111111111', 248, 1, 0, 1, '2020-12-23 03:09:18', '1', '1', NULL, NULL, NULL, '2020-12-23 03:09:18', '2020-12-23 03:09:18'),
(10, 'c895eaf2-c7c3-4c06-8caa-3c8d4864a498', 'test002', 'sdfasfd', 'test002@test.com', '$2y$10$Eh89u9DTa1ZsW4kADnpDOOpekbqu.cfTblHekQIegN9YowzfKat22', '1234567891', 248, 1, 0, 1, '2020-12-23 11:56:32', '1', '1', NULL, NULL, NULL, '2020-12-23 11:56:32', '2020-12-23 11:56:32');

-- --------------------------------------------------------

--
-- Table structure for table `verify_users`
--

CREATE TABLE `verify_users` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `web_links`
--

CREATE TABLE `web_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL,
  `candidate_id` bigint(20) UNSIGNED DEFAULT NULL,
  `applicant_id` bigint(20) UNSIGNED DEFAULT NULL,
  `link_token` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to_phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_copy_link_generated` tinyint(1) NOT NULL DEFAULT 0,
  `used_at` timestamp NULL DEFAULT NULL,
  `expire_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `web_links`
--

INSERT INTO `web_links` (`id`, `organization_id`, `candidate_id`, `applicant_id`, `link_token`, `link_type`, `to_phone_number`, `to_email`, `is_copy_link_generated`, `used_at`, `expire_at`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 4, 1, NULL, '39baf3b8-4dd2-4560-a18d-53fd18a6f863', 'candidate_selfie_1', NULL, 'candidate001@test.test', 0, NULL, '2020-12-21 07:42:41', NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(2, 3, 2, NULL, '1951d755-a4ae-40e0-84b2-e0bc5fd5ce67', 'candidate_selfie_1', NULL, 'candidate002@test.test', 0, NULL, '2020-12-21 07:42:41', NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(3, 5, 3, NULL, '27890505-841c-4c78-b554-5e6a469c1454', 'candidate_selfie_1', NULL, 'candidate003@test.test', 0, NULL, '2020-12-21 07:42:41', NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(4, 4, 4, NULL, '7baed611-0c66-47c5-96bd-60605df52e91', 'candidate_selfie_1', NULL, 'candidate004@test.test', 0, NULL, '2020-12-21 07:42:41', NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(5, 1, 5, NULL, '734b1b04-ea80-4c99-bbe5-2bc1e6953ed2', 'candidate_selfie_1', NULL, 'candidate005@test.test', 0, NULL, '2020-12-21 07:42:41', NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(6, 5, 6, NULL, '9f56dde7-dd78-4e5f-acdb-236023a7d04c', 'candidate_selfie_1', NULL, 'candidate006@test.test', 0, NULL, '2020-12-21 07:42:41', NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(7, 5, 7, NULL, '656cbb34-1dc0-4353-80bf-21780f9731e8', 'candidate_selfie_1', NULL, 'candidate007@test.test', 0, NULL, '2020-12-21 07:42:41', NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(8, 1, 8, NULL, '93f33d93-3906-4409-9d16-2dec22744322', 'candidate_selfie_1', NULL, 'candidate008@test.test', 0, NULL, '2020-12-21 07:42:41', NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(9, 3, 9, NULL, '84ef21d3-ad37-4969-86ce-0da48114f940', 'candidate_selfie_1', NULL, 'candidate009@test.test', 0, NULL, '2020-12-21 07:42:41', NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(10, 5, 10, NULL, '453db5d2-ece8-4073-80a3-58e942e45e66', 'candidate_selfie_1', NULL, 'candidate010@test.test', 0, NULL, '2020-12-21 07:42:41', NULL, NULL, NULL, '2020-12-20 07:42:41', '2020-12-20 07:42:41', NULL),
(11, 1, 101, NULL, 'f954299b-c838-45cd-a42d-1e76191482ee', 'candidate_selfie_1', NULL, 'jilkakaran@gmail.com', 0, NULL, '2020-12-21 08:26:12', '1', '1', NULL, '2020-12-20 08:26:12', '2020-12-20 08:32:53', '2020-12-20 08:32:53'),
(12, 1, 101, NULL, '77b0ab05-2df9-4d55-b8c1-377cb3adf112', 'candidate_selfie_1', NULL, 'jilkakaran@gmail.com', 0, NULL, '2020-12-21 08:27:13', '1', '1', NULL, '2020-12-20 08:27:13', '2020-12-20 08:32:53', '2020-12-20 08:32:53'),
(13, 1, 101, NULL, '8de122c2-de28-4115-a896-a89f31fc58b3', 'candidate_selfie_1', NULL, 'jilkakaran@gmail.com', 0, NULL, '2020-12-21 08:28:15', '1', '1', NULL, '2020-12-20 08:28:15', '2020-12-20 08:32:53', '2020-12-20 08:32:53'),
(14, 1, 101, NULL, '35de9eaf-8bcb-4edc-9739-53f2d882db46', 'candidate_selfie_1', NULL, NULL, 1, '2020-12-20 08:32:53', '2020-12-21 08:29:28', '1', NULL, NULL, '2020-12-20 08:29:28', '2020-12-20 08:32:53', NULL),
(15, 1, 101, NULL, '64c0a397-f363-4b70-9450-2988809b8b04', 'candidate_selfie_2', NULL, 'jilkakaran@gmail.com', 0, NULL, '2020-12-21 08:39:33', '1', '1', NULL, '2020-12-20 08:39:33', '2020-12-20 08:39:33', NULL),
(16, 1, 101, NULL, '2b79e5b2-a00a-4b15-b456-db179fddcb29', 'candidate_selfie_2', NULL, 'jilkakaran@gmail.com', 0, NULL, '2020-12-21 08:46:13', '1', '1', NULL, '2020-12-20 08:46:13', '2020-12-20 08:46:13', NULL),
(17, 1, 101, NULL, '0a914a22-9538-4df2-8b64-3cb9fb49b814', 'candidate_selfie_2', NULL, 'jilkakaran@gmail.com', 0, NULL, '2020-12-21 08:47:37', '1', '1', NULL, '2020-12-20 08:47:37', '2020-12-20 08:47:37', NULL),
(18, 1, 101, NULL, 'af7d2eff-f2e5-44fd-a301-1ae77a615207', 'candidate_selfie_2', NULL, 'jilkakaran@gmail.com', 0, NULL, '2020-12-21 08:49:01', '1', '1', NULL, '2020-12-20 08:49:01', '2020-12-20 08:49:01', NULL),
(19, 1, 101, NULL, '3323ac65-a8d2-4f09-8faa-b225c9479ac4', 'candidate_selfie_2', NULL, 'jilkakaran@gmail.com', 0, NULL, '2020-12-22 02:59:43', '1', '1', NULL, '2020-12-21 02:59:43', '2020-12-21 02:59:43', NULL),
(20, 1, 101, NULL, '7f19462f-88d8-4f62-a40e-5128ee3359b4', 'candidate_selfie_2', NULL, 'jilkakaran@gmail.com', 0, NULL, '2020-12-22 03:01:28', '1', '1', NULL, '2020-12-21 03:01:28', '2020-12-21 03:01:28', NULL),
(21, 1, 101, NULL, 'd4d303f5-34a9-420e-8e4e-24c83952a9c3', 'candidate_selfie_2', NULL, 'jilkakaran@gmail.com', 0, NULL, '2020-12-22 03:02:13', '1', '1', NULL, '2020-12-21 03:02:13', '2020-12-21 03:02:13', NULL),
(22, 1, 101, NULL, '471e9b15-7b0b-4115-a5ea-3a38c214755f', 'candidate_selfie_2', NULL, 'jilkakaran@gmail.com', 0, NULL, '2020-12-22 03:03:11', '1', '1', NULL, '2020-12-21 03:03:11', '2020-12-21 03:03:11', NULL),
(23, 1, 101, NULL, 'da397e24-cf5b-4737-9afb-5a233d616586', 'candidate_selfie_2', NULL, 'jilkakaran@gmail.com', 0, NULL, '2020-12-22 03:14:33', '1', '1', NULL, '2020-12-21 03:14:33', '2020-12-21 03:14:33', NULL),
(24, 1, 102, NULL, 'da23636d-bb43-42a1-b75d-a24bee38bd01', 'candidate_selfie_1', NULL, NULL, 1, '2020-12-21 13:29:34', '2020-12-22 13:28:33', '1', NULL, NULL, '2020-12-21 13:28:33', '2020-12-21 13:29:34', NULL),
(25, 1, 102, NULL, '1e1ab01a-be13-41d2-bd44-7075dab25b68', 'candidate_selfie_2', NULL, 'test002@test.com', 0, NULL, '2020-12-22 13:36:36', '1', '1', NULL, '2020-12-21 13:36:36', '2020-12-21 13:36:36', NULL),
(26, 1, 102, NULL, 'df8cdbd8-9c62-4cb5-b197-7ff676fcb048', 'candidate_selfie_2', NULL, 'test002@test.com', 0, NULL, '2020-12-22 13:39:00', '1', '1', NULL, '2020-12-21 13:39:00', '2020-12-21 13:39:00', NULL),
(27, 1, 102, NULL, '787dc36f-9202-435d-a023-70d671f2fb73', 'candidate_selfie_2', NULL, 'test002@test.com', 0, NULL, '2020-12-23 12:08:32', '1', '1', NULL, '2020-12-22 12:08:32', '2020-12-22 12:08:32', NULL),
(28, 1, 102, NULL, 'e509126b-b560-4182-8dbd-7fcccdf1bd92', 'candidate_selfie_2', NULL, 'dallinhamptonKnu@teleosaurs.xyz', 0, NULL, '2020-12-23 12:09:58', '1', '1', NULL, '2020-12-22 12:09:58', '2020-12-22 12:09:58', NULL),
(29, 1, 102, NULL, 'c970a4af-d312-446c-b07f-e32f31d514fb', 'candidate_selfie_2', NULL, 'jilkakaran@gmail.com', 0, NULL, '2020-12-23 12:12:43', '1', '1', NULL, '2020-12-22 12:12:43', '2020-12-22 12:12:43', NULL),
(30, 1, 102, NULL, '34b64fd4-8026-4961-9a5a-c182c607b7a1', 'candidate_selfie_2', NULL, 'jilkakaran@gmail.com', 0, NULL, '2020-12-23 12:16:33', '1', '1', NULL, '2020-12-22 12:16:33', '2020-12-22 12:16:33', NULL),
(31, 8, 103, NULL, 'd4c290e2-35b2-475e-bfee-16652599f5a7', 'candidate_selfie_1', NULL, NULL, 1, '2020-12-23 02:07:39', '2020-12-24 02:02:09', '7', NULL, NULL, '2020-12-23 02:02:09', '2020-12-23 02:07:39', NULL),
(32, 10, 104, NULL, 'edc7ac4d-1b1e-4ee5-94ac-8ad974202632', 'candidate_selfie_1', NULL, NULL, 1, '2020-12-23 03:22:04', '2020-12-24 03:14:12', '1', NULL, NULL, '2020-12-23 03:14:12', '2020-12-23 03:22:04', NULL),
(33, 10, 104, NULL, '7d8fabcb-0287-4716-a5f5-c1048361f343', 'candidate_selfie_1', NULL, 'shweta.j371@gmail.com', 0, NULL, '2020-12-24 03:14:25', '1', '1', NULL, '2020-12-23 03:14:25', '2020-12-23 03:22:04', '2020-12-23 03:22:04'),
(34, 10, 104, NULL, '1037cb02-0ca6-4df8-9284-2a5476504263', 'candidate_selfie_2', NULL, 'shweta.j371@gmail.com', 0, '2020-12-23 03:36:12', '2020-12-24 03:24:30', '1', NULL, NULL, '2020-12-23 03:24:30', '2020-12-23 03:36:12', NULL),
(35, 10, 105, NULL, 'c7399230-bba7-4536-93ad-ace17f1879e4', 'candidate_selfie_1', NULL, 'shweta@nextjobhunt.com', 0, '2020-12-23 03:40:34', '2020-12-24 03:26:19', '1', NULL, NULL, '2020-12-23 03:26:19', '2020-12-23 03:40:34', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_customer_xid_unique` (`customer_xid`),
  ADD KEY `customers_organization_id_index` (`organization_id`);

--
-- Indexes for table `customer_answers`
--
ALTER TABLE `customer_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_answers_organization_question_id_foreign` (`organization_question_id`);

--
-- Indexes for table `customer_comments`
--
ALTER TABLE `customer_comments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customer_comments_customer_comment_xid_unique` (`customer_comment_xid`),
  ADD KEY `customer_comments_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `media_disk_directory_filename_extension_unique` (`disk`,`directory`,`filename`,`extension`),
  ADD KEY `media_aggregate_type_index` (`aggregate_type`),
  ADD KEY `original_media_id` (`original_media_id`);

--
-- Indexes for table `mediables`
--
ALTER TABLE `mediables`
  ADD PRIMARY KEY (`media_id`,`mediable_type`,`mediable_id`,`tag`),
  ADD KEY `mediables_mediable_id_mediable_type_index` (`mediable_id`,`mediable_type`),
  ADD KEY `mediables_tag_index` (`tag`),
  ADD KEY `mediables_order_index` (`order`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `organizations_timezone_id_foreign` (`timezone_id`),
  ADD KEY `organizations_organization_type_id_foreign` (`organization_type_id`),
  ADD KEY `organizations_organization_xid_index` (`organization_xid`);

--
-- Indexes for table `organization_questions`
--
ALTER TABLE `organization_questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `organization_questions_organization_id_foreign` (`organization_id`);

--
-- Indexes for table `organization_types`
--
ALTER TABLE `organization_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organization_user_xrefs`
--
ALTER TABLE `organization_user_xrefs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `organization_user_xrefs_id_index` (`id`),
  ADD KEY `organization_user_xrefs_user_id_index` (`user_id`),
  ADD KEY `organization_user_xrefs_organization_id_index` (`organization_id`),
  ADD KEY `organization_user_xrefs_role_id_index` (`role_id`),
  ADD KEY `organization_user_xrefs_created_by_index` (`created_by`),
  ADD KEY `organization_user_xrefs_updated_by_index` (`updated_by`),
  ADD KEY `organization_user_xrefs_deleted_by_index` (`deleted_by`);

--
-- Indexes for table `otps`
--
ALTER TABLE `otps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `otps_web_link_id_foreign` (`web_link_id`),
  ADD KEY `otps_candidate_id_foreign` (`candidate_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `rekognition_result_logs`
--
ALTER TABLE `rekognition_result_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_role_xid_unique` (`role_xid`),
  ADD KEY `roles_organization_type_index` (`organization_type`),
  ADD KEY `roles_created_by_index` (`created_by`),
  ADD KEY `roles_updated_by_index` (`updated_by`),
  ADD KEY `roles_deleted_by_index` (`deleted_by`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `timezones`
--
ALTER TABLE `timezones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_user_xid_unique` (`user_xid`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_phone_number_unique` (`phone_number`),
  ADD KEY `users_timezone_id_foreign` (`timezone_id`);

--
-- Indexes for table `web_links`
--
ALTER TABLE `web_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `web_links_organization_id_foreign` (`organization_id`),
  ADD KEY `web_links_candidate_id_foreign` (`candidate_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `action_logs`
--
ALTER TABLE `action_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `candidates`
--
ALTER TABLE `candidates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer_answers`
--
ALTER TABLE `customer_answers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer_comments`
--
ALTER TABLE `customer_comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `organization_questions`
--
ALTER TABLE `organization_questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `organization_types`
--
ALTER TABLE `organization_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `organization_user_xrefs`
--
ALTER TABLE `organization_user_xrefs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `otps`
--
ALTER TABLE `otps`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rekognition_result_logs`
--
ALTER TABLE `rekognition_result_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `timezones`
--
ALTER TABLE `timezones`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=589;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `web_links`
--
ALTER TABLE `web_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customers_organization_id_foreign` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`);

--
-- Constraints for table `customer_answers`
--
ALTER TABLE `customer_answers`
  ADD CONSTRAINT `customer_answers_organization_question_id_foreign` FOREIGN KEY (`organization_question_id`) REFERENCES `organization_questions` (`id`);

--
-- Constraints for table `customer_comments`
--
ALTER TABLE `customer_comments`
  ADD CONSTRAINT `customer_comments_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`);

--
-- Constraints for table `media`
--
ALTER TABLE `media`
  ADD CONSTRAINT `original_media_id` FOREIGN KEY (`original_media_id`) REFERENCES `media` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `mediables`
--
ALTER TABLE `mediables`
  ADD CONSTRAINT `mediables_media_id_foreign` FOREIGN KEY (`media_id`) REFERENCES `media` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `organizations`
--
ALTER TABLE `organizations`
  ADD CONSTRAINT `organizations_organization_type_id_foreign` FOREIGN KEY (`organization_type_id`) REFERENCES `organization_types` (`id`),
  ADD CONSTRAINT `organizations_timezone_id_foreign` FOREIGN KEY (`timezone_id`) REFERENCES `timezones` (`id`);

--
-- Constraints for table `organization_questions`
--
ALTER TABLE `organization_questions`
  ADD CONSTRAINT `organization_questions_organization_id_foreign` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `organization_user_xrefs`
--
ALTER TABLE `organization_user_xrefs`
  ADD CONSTRAINT `organization_user_xrefs_organization_id_foreign` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `organization_user_xrefs_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `organization_user_xrefs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `otps`
--
ALTER TABLE `otps`
  ADD CONSTRAINT `otps_candidate_id_foreign` FOREIGN KEY (`candidate_id`) REFERENCES `candidates` (`id`),
  ADD CONSTRAINT `otps_web_link_id_foreign` FOREIGN KEY (`web_link_id`) REFERENCES `web_links` (`id`);

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_timezone_id_foreign` FOREIGN KEY (`timezone_id`) REFERENCES `timezones` (`id`);

--
-- Constraints for table `web_links`
--
ALTER TABLE `web_links`
  ADD CONSTRAINT `web_links_candidate_id_foreign` FOREIGN KEY (`candidate_id`) REFERENCES `candidates` (`id`),
  ADD CONSTRAINT `web_links_organization_id_foreign` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
